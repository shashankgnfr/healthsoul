namespace :utility do
  include Rails.application.routes.url_helpers

  desc "Set states for doctors"
  task :set_dr_state, [:xlsx_file_name] => :environment do |t, args|
    file = args[:xlsx_file_name]
    file_name = File.basename(file, File.extname(file))
    workbook = Roo::Excelx.new("#{Rails.root}/docs/#{file}")
    sheet = workbook.sheet(0)
    errors = []

    header = sheet.row(1) 
    (2..sheet.last_row).each do |i|
      row = sheet.row(i)
      country = row[3].try(:strip)
      state = row[4].try(:strip)
      if country && state
        country_slug = country.parameterize.gsub("'", "''")
        state_slug = state.parameterize.gsub("'", "''")
        name = row[1].gsub("'","''")
        ActiveRecord::Base.connection.execute("
          update locations
          set 
            country_slug = '#{country_slug}', country = '#{country}', 
            state = '#{state}', state_slug = '#{state_slug}'
          where name = '#{name}' and id = #{row[0]} and (state is null or state = '')
        ")
        else
        errors << row
      end
    end
    errors = errors.compact

    unless errors.empty?
      error_file = "#{Rails.root}/docs/Error_#{file_name}.csv"
      puts "Errors while processing #{file_name}"
      CSV.open(error_file, "w") do |csv|
        csv << ['ID', 'Name', 'City', 'Country', 'State']
        errors.each do |i|
          csv << i
        end
      end
      puts "#{error_file} created."
    end

    puts "Processed #{file_name} successfully."
  end

  desc "Set states for locations"
  task :set_location_state, [:xlsx_file_name] => :environment do |t, args|
    file = "#{Rails.root}/docs/#{args[:xlsx_file_name]}"
    file_name = File.basename(file, File.extname(file))
    workbook = Roo::Excelx.new(file)
    sheet = workbook.sheet(0)
    errors = []

    header = sheet.row(1) 
    (2..sheet.last_row).each do |i|
      row = sheet.row(i)
      state = row[2].try(:strip)
      if state.present?
        state = state.gsub("'", "''")
        city = row[0].strip.gsub("'", "''")
        state_slug = state.parameterize.gsub("'", "''")
        ActiveRecord::Base.connection.execute("
          update locations
          set state_slug = '#{state_slug}', state = '#{state}'
          where city = '#{city}' and country = '#{row[1].strip}' and (state is null or state = '')
        ")
      else
        errors << row
      end
    end
    
    errors = errors.compact

    unless errors.empty?
      error_file = "#{Rails.root}/docs/Error_#{file_name}.csv"
      puts "Errors while processing #{file_name}"
      CSV.open(error_file, "w") do |csv|
        csv << ['City', 'Country', 'State']
        errors.each do |i|
          csv << i
        end
      end
      puts "#{error_file} created."
    end

    puts "Processed #{file_name} successfully."
  end

  desc 'List doctors and hospitals which has either missing country, state or city'
  task :missing_require_fields => :environment do |t|
    repo = "#{Rails.root}/tmp"
    Dir.mkdir repo unless File.exists?(repo)
    book = Spreadsheet::Workbook.new
    header_format = Spreadsheet::Format.new weight: :bold, size: 8

    buggy_doctors = Doctor.where("(name is null) or (city is null) or (state is null) or (country is null)")
    unless buggy_doctors.empty?
      doctor_sheet = book.create_worksheet name: 'Doctor'
      doctor_sheet.row(0).default_format = header_format
      header = ['Doctor ID', 'Doctor URL', 'Doctor Name', 'City', 'State', 'Country']
      doctor_sheet.row(0).concat header
      buggy_doctors.each_with_index do |d, index|
        href = url_for(host: HOST, controller: 'admin/doctors', action: :edit, id: d.slug || d.id)

        row_data = [d.id, Spreadsheet::Link.new(href.to_s, href), d.name, d.city, d.state, d.country]
        doctor_sheet.row(index + 1).concat row_data
      end
    end

    location_type = LocationType.where(name: 'Hospital').first
    buggy_hospitals = location_type.locations.where("(name is null) or (city is null) or (state is null) or (country is null)")
    unless buggy_hospitals.empty?
      hospital_sheet = book.create_worksheet name: 'Hospital'
      hospital_sheet.row(0).default_format = header_format
      header = ['Location ID', 'Location URL', 'Location Name', 'City', 'State', 'Country']
      hospital_sheet.row(0).concat header 
      buggy_hospitals.each_with_index do |h, index|
        href = url_for(host: HOST, controller: 'admin/locations', action: :edit, id: h.slug || h.id)
        row_data = [h.id, Spreadsheet::Link.new(href.to_s, href), h.name, h.city, h.state, h.country]
        hospital_sheet.row(index + 1).concat row_data
      end
    end

    buggy_data_file = "#{repo}/buggy_doctors_hospitals.xls"
    book.write buggy_data_file
    puts "#{buggy_data_file} created."
  end

  desc 'List distinct city, state, country for doctors and locations'
  task :dump_location, [:country_slug] => :environment do |t, args|
    repo = "#{Rails.root}/tmp"
    Dir.mkdir repo unless File.exists?(repo)
    book = Spreadsheet::Workbook.new
    header_format = Spreadsheet::Format.new weight: :bold, size: 8

    doctors = Doctor.where(country_slug: args[:country_slug]).order("country asc, state asc, city asc").select("distinct country, state, city")
    doctor_sheet = book.create_worksheet name: 'Doctors'
    doctor_sheet.row(0).default_format = header_format
    header = ['City', 'State', 'Country']
    doctor_sheet.row(0).concat header
    doctors.each_with_index do |d, index|
      row_data = [d.city, d.state, d.country]
      doctor_sheet.row(index + 1).concat row_data
    end
    
    locations = Location.where(country_slug: args[:country_slug]).order("country asc, state asc, city asc").select("distinct country, state, city")
    location_sheet = book.create_worksheet name: 'Locations'
    location_sheet.row(0).default_format = header_format
    header = ['City', 'State', 'Country']
    location_sheet.row(0).concat header
    locations.each_with_index do |l, index|
      row_data = [l.city, l.state, l.country]
      location_sheet.row(index + 1).concat row_data
    end

    data_file = "#{repo}/location_for_doctors_hospitals_#{args[:country_slug]}.xls"
    book.write data_file
    puts "#{data_file} created."
  end

  desc 'List doctor with specialities'
  task :doctor_specialities => :environment do |t|
    repo = "#{Rails.root}/tmp"
    Dir.mkdir repo unless File.exists?(repo)
    book = Spreadsheet::Workbook.new
    header_format = Spreadsheet::Format.new weight: :bold, size: 8

    doctors = Doctor.connection.execute("
      SELECT doctors.id, concat('#{HOST}/doctors/', doctors.slug) as url, doctors.name as dr_name, 
        practice_specialities.name as speciality,   
        count(practice_specialities.id)
      FROM doctors
      LEFT join doctor_specialities on doctors.id  = doctor_specialities.doctor_id
      LEFT join practice_specialities on doctor_specialities.practice_speciality_id  = practice_specialities.id
      GROUP BY
        doctors.id, url, dr_name, speciality
      HAVING count(practice_specialities.id) > 1;
    ")

    doctor_sheet = book.create_worksheet name: 'Doctors'
    doctor_sheet.row(0).default_format = header_format
    header = ['URL', 'DR. Name', 'Speciality Name', 'Count']
    doctor_sheet.row(0).concat header
    doctors.each_with_index do |d, index|
      row_data = [Spreadsheet::Link.new(d['url'], d['url']) , d['dr_name'], d['speciality'], d['count']]
      doctor_sheet.row(index + 1).concat row_data
      dr = Doctor.includes(:practice_specialities).where(id: d['id']).first
      uniq_speciality_ids = dr.practice_speciality_ids.uniq.compact
      dr.practice_speciality_ids = []
      dr.practice_speciality_ids = uniq_speciality_ids
    end

    data_file = "#{repo}/doctor_specialities.xls"
    book.write data_file
    puts "#{data_file} created."

  end

  # TODO: every 3 hours below task will run.
  desc 'update best doctors and hospitals'
  task :update_best_doctors_and_hospitals, [:rails_env] => :environment do |t, args|

    dr_repo = "#{Rails.root}/lib/best_doctors"
    Dir.mkdir dr_repo unless File.exists?(dr_repo)
    hospital_repo = "#{Rails.root}/lib/best_hospitals"
    Dir.mkdir hospital_repo unless File.exists?(hospital_repo)
    statistic_repo = "#{Rails.root}/lib/"
    Dir.mkdir statistic_repo unless File.exists?(statistic_repo)

    Rails.logger.info 'Ran update_best_doctors_and_hospitals : ' + args[:rails_env].to_s
    HOST = if args[:rails_env] == 'staging'
      'http://stg.healthsoul.com'
    elsif args[:rails_env] == 'development'
      'http://localhost:3000'
    else
     'https://www.healthsoul.com'
    end
    
    fJson = File.open("#{statistic_repo}/statistic.json","w")
    json_statistic = {}
    json_statistic[:doc_count] = Doctor.count
    json_statistic[:hospital_count] = Location.connection.execute(
    "
      select count(locations.id) as hospital_count 
      from locations
      left join location_types on locations.location_type_id = location_types.id
      where location_types.name = 'Hospital'
    "
    ).first['hospital_count']

    json_statistic[:hi_count] = Location.connection.execute(
    "
      select count(locations.id) as hi_count 
      from locations
      left join location_types on locations.location_type_id = location_types.id
      where location_types.name = 'Health Insurance'
    ").first['hi_count'] + 
      Location.connection.execute(
    "
      select count(locations.id) as hi_count 
      from locations
      left join location_types on locations.location_type_id = location_types.id
      where location_types.name = 'Travel Health Insurance'
    "
    ).first['hi_count']
    
    
    json_statistic[:total_reviews] = DoctorReview.count + Rating.count
    
    fJson.write JSON.pretty_generate(json_statistic)
    fJson.close

    search = Search::TravelHealthInsurance.new
    ["North America", "Asia", "Europe", "South America", "Oceania"].each do |continent|
      countries = search.continents_to_h[continent].reject{|i| i.first.blank?}

      countries.each do |hash_country|
        country = hash_country.last
        best_doctors, best_hospitals, best_hospitals_based_on_cities = get_best_drs_and_hospitals(country)

        # best doctors
        if best_doctors.present?
          json_drs = {}
          best_doctors.each do |bd|
            states_or_cities = bd.json_agg
            cities_link = []
            states_or_cities[0..4].each do |hash_states_or_cities|
              key = hash_states_or_cities.keys.first
              country, state, city = key.scan(/\(([^\)]+)\)/).flatten
              state_or_city = key.split('(')[0]
              href = url_for(host: HOST, controller: :doctor_search, action: :best_doctors, speciality: bd.practice_specialities_title, set_locale: country, state: state, city: city)
              cities_link << {state_or_city => {
                href: href,
                href_title: "#{bd.practice_specialities_name} - #{state_or_city}"
                }
              }
            end
            json_drs[bd.practice_specialities_name] = cities_link
          end

          fJson = File.open("#{dr_repo}/#{country}.json","w")
          fJson.write JSON.pretty_generate(json_drs)
          fJson.close
        end

        # best hospitals
        if best_hospitals_based_on_cities.present? || best_hospitals.present?
          json_hospitals = {}
          best_hospitals_based_on_cities.each do |bh|
            city = bh.city || 'N/A'
            href = url_for(host: HOST, controller: :searches, action: :best_hospitals, set_locale: bh.country_slug, state:  bh.state_slug, city: bh.city_slug)
            json_hospitals[city] = {
              href: href,
              href_title:  "#{city} - Hospitals"
              }
          end

          best_hospitals.each do |bh|
            state = bh.state || 'N/A'
            href = url_for(host: HOST, controller: :searches, action: :best_hospitals, set_locale: bh.country_slug, state:  bh.state_slug)
            json_hospitals[state] = {
              href: href,
              href_title:  "#{state} - Hospitals"
            }
          end

          fJson = File.open("#{hospital_repo}/#{country}.json","w")
          fJson.write JSON.pretty_generate(json_hospitals)
          fJson.close
        end
      end
    end
  end

  desc 'Set Latitude, Longitude'
  task :set_lat_lng => :environment do |t|
    ['Doctor', 'Location'].each do |cls|
      eval(cls).where(country: '').update_all(country: nil)
      eval(cls).where(state: '').update_all(state: nil)
      eval(cls).where(city: '').update_all(city: nil)
      eval(cls).where(latitude: nil, longitude: nil).where("country is not null or state is not null or city is not null").each do |d|
        d.save!
      end
      eval(cls).where(latitude: nil, longitude: nil).where.not(country: nil).each do |l|
        if !l.state.nil? && !l.city.nil?
          geo = Geocoder.search([l.city, l.state, l.country].join(', ')).first
          if geo
            lat_lng_hash = geo.data['geometry']['location']
            l.update_column :latitude, lat_lng_hash['lat']
            l.update_column :longitude, lat_lng_hash['lng']
          end
        elsif l.state.nil? && !l.city.nil?
          geo = Geocoder.search([l.city, l.country].join(', ')).first
          if geo
            lat_lng_hash = geo.data['geometry']['location']
            l.update_column :latitude, lat_lng_hash['lat']
            l.update_column :longitude, lat_lng_hash['lng']
          end
        elsif !l.state.nil? && l.city.nil?
          geo = Geocoder.search([l.state, l.country].join(', ')).first
          if geo
            lat_lng_hash = geo.data['geometry']['location']
            l.update_column :latitude, lat_lng_hash['lat']
            l.update_column :longitude, lat_lng_hash['lng']
          end
        else
          geo = Geocoder.search(l.country).first
          if geo
            lat_lng_hash = geo.data['geometry']['location']
            l.update_column :latitude, lat_lng_hash['lat']
            l.update_column :longitude, lat_lng_hash['lng']
          end
        end
      end
    end
  end
  
  desc 'Map countries'  
  task :map_countries => :environment do |t|
    map_countries = {'US' => ['USA', 'usa']}
    map_countries.each_pair do |k, v|
      Doctor.connection.execute("
        Update doctors
        set country_slug = '#{v.last}',
            country = '#{v.first}'
        where
          country = '#{k}'
        ;
      ")
    end
  end

  desc 'Set continent'
  task :set_continent, [:xlsx_file] => :environment do |t, args|
    file = args[:xlsx_file]
    file_name = File.basename(file, File.extname(file))
    workbook = Roo::Excelx.new("#{Rails.root}/docs/#{file}")
    sheet = workbook.sheet(0)
    errors = []
    header = sheet.row(1)

    (2..sheet.last_row).each do |i|
      row = sheet.row(i)
      country_id = row[0]
      country_name = row[1].try(:strip)
      country_continent = row[2].try(:strip)
      if country_continent
        Country.find_by_id_and_name(country_id, country_name ).update_column :continent, country_continent
      else
        errors << row
      end
    end

    errors = errors.compact

    unless errors.empty?
      error_file = "#{Rails.root}/docs/Error_#{file_name}.csv"
      puts "Errors while processing #{file_name}"
      CSV.open(error_file, "w") do |csv|
        csv << header
        errors.each do |i|
          csv << i
        end
      end
      puts "#{error_file} created."
      puts "Processed #{file_name} partially."
    else
      puts "Processed #{file_name} successfully."
    end

    Country.where.not(continent: nil).pluck(:name, :continent).each do |c|
      Location.where(country: c.first).update_all(continent: c.last)
      Doctor.where(country: c.first).update_all(continent: c.last)
    end
  end

  desc 'Fix country code'
  task :fix_country_code => :environment do |t|
    Location.where(country: 'Iceland').update_all(country_code: 'ISL')
    # Countries

    countries = {'United Arab Emirates' => 'UAE',
    'Saint Barth_lemy' => 'Saint-Barthélemy (France)', 
    'Saint Barth_lemy (France)' => 'Saint-Barthélemy (France)',
    'Commonwealth of Dominica' => 'Dominica',
    'Ecuador' => 'Eucador',
    'St. Lucia' => 'Saint Lucia',
    'Saint Martin' => 'Saint Martin (France)',
    'Martinique' => 'Martinique (France)',
    'Saint Pierre and Miquelon' => 'Saint Pierre and Miquelon (France)',
    'Turks & Caicos Islands' => 'Turks and Caicos Islands',
    'Saint Vincent and the Grenadines' => 'St. Vincent and The Grenadines',
    'US Virgin Islands' => 'U.S. Virgin Islands'}

    practice_speciality_countries = []
    patient_story_countries = []
    countries.each_pair do |k,v|
      c = Country.where(name: k).first
      unless c.nil?
        if c.practice_specialities.count == 0 && c.patient_stories.count == 0
          c.destroy
        elsif c.practice_specialities.count > 0
          practice_speciality_countries << c
        else
          patient_story_countries << c
        end
      end
    end

    if practice_speciality_countries.present?
      puts "#{practice_speciality_countries.join(', ')} has Practice Specialities."
    end

    if patient_story_countries.present?
      puts "#{patient_story_countries.join(', ')} has Patient stories."
    end

    # doctor

    Doctor.where(country: 'USA', country_code: nil).update_all(country_code: 'USA')
    Doctor.where(country: 'India', country_code: nil).update_all(country_code: 'IND')

    # location

    {'Commonwealth of Dominica' => 'Dominica',
    'Turks & Caicos Islands' => 'Turks and Caicos Islands'}.each_pair do |k, v|
      Location.where(country: k).update_all(country: v, country_slug: v.try(:parameterize))
    end

    { 'Greenland' => 'GRL',
      'Grenada' => 'GRD',
      'Iceland' => 'ISL'}.each_pair do |k,v|
      Location.where(country: k).update_all(country_code: v)
    end
   
  end

  desc 'Fix City'
  task :fix_city,  [:xlsx_file] => :environment do |t, args|
    workbook = Roo::Excelx.new(args[:xlsx_file])
    sheet = workbook.sheet(0)
    header = sheet.row(1).compact

    if header == ["City", "State", "Country", "Correction"]
      repo = "#{Rails.root}/docs"
      Dir.mkdir repo unless File.exists?(repo)
      city_file = "#{repo}/Doctors_and_Locations_city_update.csv"
      CSV.open(city_file, "a") do |csv|
        csv << ['Object Type', 'Object Id', 'City', 'State', 'Country']
        (2..sheet.last_row).each do |i|
          row = sheet.row(i)
          old_city = row[0].try(:strip)
          state = row[1].try(:strip)
          country = row[2].try(:strip)
          new_city = row[3].try(:strip)
          if new_city && new_city != old_city
            drs = Doctor.where(city: old_city, state: state, country: country)
            if drs.present?
              # We need to use model.save to recompute Geocoder - lat, lng.
              Rails.logger.info "#{drs.count} doctors with #{old_city}  replaced with #{new_city}." if drs.present?
              drs.update_all(city: new_city, city_slug: new_city.parameterize)
              drs.each do |d|
                csv << ['Doctor', d.id, new_city, d.state, d.country]
              end
            end
            locs = Location.where(city: old_city, state: state, country: country)
            if locs.present?
              Rails.logger.info "#{locs.count} doctors with #{old_city}  replaced with #{new_city}." if locs.present?
              locs.update_all(city: new_city, city_slug: new_city.parameterize)
              locs.each do |l|
                csv << ['Location', l.id, new_city, l.state, l.country]
              end
            end
          end
        end
        puts "#{city_file} amended."
      end
    else
      file_name = File.basename(args[:xlsx_file])
      Rails.logger.error "Mismatch header - #{file_name}"
    end
  end
   
  private

  def get_best_drs_and_hospitals(country)
    if ['usa', 'india'].include? country
      best_doctors = BestDoctorView.connection.execute("
        SET healthsoul.country_slug = '#{country}';
      ") && BestDoctorView.all
    else
      best_doctors = BestDoctorWithoutPrioritySpecialityView.connection.execute("
        SET healthsoul.country_slug = '#{country}';
      ") && BestDoctorWithoutPrioritySpecialityView.all
    end
      
    if country == 'india'
      best_hospitals = BestHospitalView.connection.execute("
        SET healthsoul.country_slug = '#{country}';
      ") && BestHospitalView.order("array_position(
        ARRAY['Punjab', 'Haryana', 'Delhi', 'Chandigarh', 'Goa', 'Sikkim', 'Chhattisgarh', 'Odisha', 'Uttarakhand', 'Assam', 'Andhra Pradesh', 'Arunachal Pradesh', 'Himachal Pradesh', 'Madhya Pradesh', 'Uttar Pradesh']::varchar[], state), count_all desc" ).limit(24)
    elsif country == 'usa'
      best_hospitals = BestHospitalView.connection.execute("
        SET healthsoul.country_slug = '#{country}';
      ") && BestHospitalView.order("array_position(
        ARRAY['NY']::varchar[], state), count_all desc" ).limit(24)
    else
      best_hospitals = BestHospitalView.connection.execute("
        SET healthsoul.country_slug = '#{country}';
      ") && BestHospitalView.all.limit(18)
    end

    best_hospitals_based_on_cities = BestHospitalBasedOnCitiesView.where(priority_city_level: 1, country_slug:  country)

    return best_doctors, best_hospitals, best_hospitals_based_on_cities
  end
  

end