desc "refresh sitemap after deploy"
task :sitemap_refresh_later => :environment do
  SitemapRefreshJob.perform_async
end
