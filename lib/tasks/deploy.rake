APP_NAME = "healthworld"
IMPORT_DB = "healthworld_development"
HEROKU_REMOTE = "production"
HEROKU_STAGING_REMOTE = "staging"
GIT_REMOTE = "origin"
DUMP_NAME = "tmp/production.dump"

namespace :deploy do

  task :production => ["production:standard"]
  namespace :production do

    desc "Deploy to Heroku production"
    task :standard do
      git_push_origin
      deploy
    end

    desc "Deploy to Heroku production with pgbackup"
    task :with_pgbackup do
      git_push_origin
      deploy
      pgbackup
    end

    desc "Deploy to Heroku production with pgbackup, migration and restart"
    task :with_migration do
      git_push_origin
      deploy
      migrate
      restart_app
    end
  end

  namespace :staging do

    desc "Deploy to Heroku staging"
    task :standard do
      sh "git push #{GIT_REMOTE} staging:staging"
      sh "git push #{HEROKU_STAGING_REMOTE} staging:master"
    end
  end
end

desc "Backup production db"
task :backup_db do
  pgbackup
end

desc "Pull a copy of the production db and install locally"
task :pull_production_database do
  pgbackup
  Bundler.with_clean_env { sh "curl $(heroku pg:backups public-url --quiet -a #{APP_NAME}) > #{DUMP_NAME}" }
  system("pg_restore --verbose --clean --no-acl --no-owner -h localhost -d #{IMPORT_DB} #{DUMP_NAME}")
  sh "rm -f #{DUMP_NAME}"
end

def git_push_origin
  sh "git push #{GIT_REMOTE} master"
end

def deploy
  sh "git push #{HEROKU_REMOTE} master"
end

def pgbackup
  Bundler.with_clean_env { sh "heroku pg:backups capture -a #{APP_NAME}" }
end

def migrate
  Bundler.with_clean_env {
    sh "heroku run rake db:migrate -a #{APP_NAME}"
  }
end

def restart_app
  Bundler.with_clean_env { sh "heroku restart -a #{APP_NAME}" }
  `curl http://#{APP_NAME}.herokuapp.com > /dev/null 2> /dev/null`
end
