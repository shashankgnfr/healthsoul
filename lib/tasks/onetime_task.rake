namespace :onetime_task do

  desc 'T24-21564 set country for doctor/location'
  task :set_country => :environment do |t|
    # Doctors
    Doctor.where(id: 1159126).update_all(country: 'India', country_slug: 'india', country_code: 'IND', continent: 'Asia')

    Doctor.where(id: [1615155,1614924,1614265,1614260,1613610,1613607,1613602,1613059,1615351,1615151]).update_all(country: 'USA', country_slug: 'usa', country_code: 'USA', continent: 'North America')

    # Locations

    Location.where(id: [37221,37210,37205,37211,37212,37226,37216,37217,37218,
      37197,37198,37199,37200,37201,37202,37203,37204,37227,37229,37230,37206,
      37207,37208,37209,37228,37223,37225,37220,37219,37214,37215,37196,37224,
      37222,37213]).update_all(country: 'Mexico', country_slug: 'mexico', country_code: 'MEX', continent: 'North America')

    Location.where(id: [36]).update_all(country: 'USA', country_slug: 'usa', country_code: 'USA', continent: 'North America')

    Location.where(id: [51579, 51578]).update_all(country: 'Brazil', country_slug: 'brazil', country_code: 'BRA', continent: 'South America')

    # delete location where name not defined.
    # locations_about_to_delete = Location.where(id: [316, 257, 291, 292, 293, 294, 259, 260, 261, 262, 313, 314, 315, 258, 263, 264, 265, 317, 318, 319, 320, 321, 322, 323, 324, 325, 326, 327, 328, 329, 330, 331, 332, 333, 334, 335, 336, 337, 338])
    # locations_about_to_delete.each do |l|
    #   l.destroy
    # end
  end

  desc 'Move Yext Locations'
  task :move_yext_locations => :environment do |t|
    hospital_location_id = LocationType.find_by_name('Hospital').id
    Location.where(location_type_id: Location::YEXT_HOSPITAL_IDS).update_all(location_type_id: hospital_location_id)

    practice_location_id = LocationType.find_by_name('Practice').id
    Location.where(location_type_id: Location::YEXT_PRACTICE_IDS).update_all(location_type_id: practice_location_id)
  end

  desc 'Delete Unused Yext Locations'
  task :delete_yext_locations => :environment do |t|
    LocationType.where(id: Location::YEXT_HOSPITAL_IDS).destroy_all
    LocationType.where(id: Location::YEXT_PRACTICE_IDS).destroy_all
  end

  desc 'Export Doctor - Organization information'
  task :export_doctors_organization_information => :environment do |t|
    file = "#{Rails.root}/tmp/doctors_organization.csv"
    CSV.open(file, "w") do |csv|
      csv << ['id', 'name', 'email', 'website', 
        'organization_name', 'organization_email', 'organization_website', 
        'organization', 'organization_phone_body', 'organization_phone_country_code', 'organization_phone']
      Doctor.order(name: :asc).each do |dr|
        csv << [dr.id, dr.name, dr.email, dr.website, 
          dr.organization_name, dr.organization_email, dr.organization_website,
          dr.organization_phone_body, dr.organization_phone_country_code,
          dr.organization_phone]
      end
    end
    puts "Created #{file} successfully."
  end

  desc 'Map Organization Email, Website, Phone'
  task :map_organization_detail_with_doctor => :environment do |t|
    file = "#{Rails.root}/tmp/doctors_map_org_data_to_dr.csv"
    CSV.open(file, "w") do |csv|
      csv << ['id', 'email', 'website', 'phone']
      Doctor.where("(email is null or website is null or phone is null) and (organization_email  is not null or organization_website is not null or organization_phone is not null)").each do |d|
        updates = []
        csv_data = []
        if !d.email.present? && d.organization_email.present?
          csv_data[0] = d.organization_email
          updates << "email = '#{d.organization_email}'" 
        end
        if !d.website.present? && d.organization_website.present?
          csv_data[1] = d.organization_website
          updates << "website = '#{d.organization_website}'" 
        end
        if !d.phone.present? && (d.organization_phone.present? || d.organization_phone_body.present? || d.organization_phone_country_code.present?)
          if d.organization_phone.present? 
            phone = d.organization_phone
          else
            phone = [d.organization_phone_body, d.organization_phone_country_code].compact.join('')
          end
          csv_data[2] = phone
          updates << "phone = '#{phone}'" 
        end
        if !updates.empty?
          Doctor.connection.execute("Update doctors set #{updates.join(', ')} where id = #{d.id}")
          csv << [d.id, csv_data].flatten
        end
      end
    end
    puts "Created #{file} successfully."
  end

  desc 'Set default language as English'
  task :doctor_default_language => :environment do |t|
    english = Language.where(title: 'English').first.id
    Doctor.pluck(:id).each do |d|
      Doctor.connection.execute("insert into doctors_languages (doctor_id, language_id) values (#{d}, #{english})")
    end 
  end
end
