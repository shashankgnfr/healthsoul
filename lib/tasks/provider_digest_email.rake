namespace :provider_digest_email do
	desc "Send monthly digest email to providers"
	task :send => :environment do |t|
		#if Time.zone.now.wday == 2 ##run every tuesday
			providers = User.select(:id, :email, :marked_as_provider).where(marked_as_provider: true, email_opt_in: true)
			providers.find_in_batches(batch_size: 100) do |providers_in_batches|
				providers_in_batches.each do |provider|
		      begin
		          UserMailer.provider_digest(provider).deliver_now if provider.email.present?
		      rescue Exception => e
		      end
				end
			end
		#end
	end
end
