namespace :yext do
  desc "Add Yext Locations"
  task :add_locations => :environment do
    csv = CSV.read('spec/fixtures/yext_categories.csv', {:headers => true, :col_sep => ',', :encoding => 'ISO-8859-1'})
    csv.each do |row|
      LocationType.find_or_create_by(name: row["Name"], yext_id: row ["Id"])
    end
    LocationType.find_by_name('Hospital').update_columns(yext_id: 1276)
  end
  
  desc "Add Yext Categories"
  task :add_categories => :environment do
    csv = CSV.read('spec/fixtures/categories.csv', {:headers => true, :col_sep => ',', :encoding => 'ISO-8859-1'})
    csv.each do |row|
      specialty = PracticeSpeciality.find_by_id(row["id"])
      if specialty.present?
        specialty.update_attributes(yext_id: row["yext_id"]) 
        p specialty.yext_id
      end
    end
  end

end
