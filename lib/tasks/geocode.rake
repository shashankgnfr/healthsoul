namespace :geocode do
  desc "Run geocode job on all non geocoded hospitals (rake geocode:locations[50000])"
  task :locations,[:id] => :environment do |t, args|
    id = args[:id]
    lt = LocationType.where(name: "Hospital").first
    locations = lt
      .locations
      .where("longitude is null")
      .where("id > ?", id)
      .order(id: :asc)
    p "#{ locations.size } Locations...."
    locations.each do |location|
      #if location.geocode.present?
      #else
      #  location.latitude = 0
      #end
      location.simple_geocode = true
      if location.geocode
        location.save
        p "#{location.id}"
      else
        p "NO GEOCODE ------------- |#{location.id}|#{location.hwaid}"
      end
      sleep 0.25
    end
  end

end
