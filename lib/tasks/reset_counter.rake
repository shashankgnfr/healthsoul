desc "Reset the ratings counter on locations"
task :reset_counter => :environment do
  ids = Rating.pluck(:location_id).uniq
  ids.each do |id|
    l = Location.where(id: id).first
    Location.reset_counters(l.slug, :location_ratings)
  end
end
