namespace :awards do
	desc "Distribute Awards to Doctors"
	task :distribute_to_doctors => :environment do
            doctors = Doctor.where("overall >= 3")
            current_year = Date.today.year
            doctors.find_in_batches(batch_size: 500) do |doc_in_batches|
			doc_in_batches.each do |doc|
				begin
                            award = Award.where(location_id: doc.id, location_type: 'Doctor', year: current_year).first
                            award = Award.new if award.blank?
                            award.attributes = {location_id: doc.id, location_type: 'Doctor', year: current_year, award_type: doc.get_award_type}
                            award.save
			      rescue Exception => e
				end
			end
		end
	end

      desc "Distribute Awards to Hospitals, HealthInsurance, TravelHealthInsurance every year on 1st April"
	task :distribute_to_others => :environment do
		#location type id= 1 (hospitals), id = 2(HealthInsurance), id=3(TravelHealthInsurance)
	      locations = Location.includes(:location_type).where(location_type_id: [1, 2, 3]).where("overall >= 3")
            current_year = Date.today.year
            locations.find_in_batches(batch_size: 500) do |loc_in_batches|
			loc_in_batches.each do |loc|
				begin
				    location_type = loc.location_type.name
                            award = Award.where(location_id: loc.id, location_type: location_type, year: current_year).first
                            award = Award.new if award.blank?
                            award.attributes = {location_id: loc.id, location_type: location_type, year: current_year, award_type: loc.get_award_type}
                            award.save
			      rescue Exception => e
				end
			end
		end

	end

end