desc "Move DoctorOrganization data to Doctor Table"
task :migrate_organization_data => :environment do
  time_at_start = Time.now
  DoctorOrganization.find_each do |doctor_organization|
    doctor_organization.doctors.update_all(address: doctor_organization.address,
                                           organization_name: doctor_organization.name,
                                           city: doctor_organization.city,
                                           state: doctor_organization.state,
                                           zip: doctor_organization.zip,
                                           latitude: doctor_organization.latitude,
                                           longitude: doctor_organization.longitude,
                                           simple_geocode: doctor_organization.simple_geocode,
                                           phone: doctor_organization.phone,
                                           organization_website: doctor_organization.website,
                                           country: doctor_organization.country,
                                           country_code: doctor_organization.country_code,
                                           continent: doctor_organization.continent)
    p "Moved doctor_organization with id of #{doctor_organization.id}"
  end
  time_at_end = Time.now
  p "Migration took #{time_at_end - time_at_start} seconds"
end
