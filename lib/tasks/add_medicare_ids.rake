desc "Add Medicare Ids"
task :add_medicare_ids => :environment do
  # Reads a csv with columns [medicare id, hwaid], and sets medicare id for each hospital found by hwaid.
  # Run
  # rake add_medicare_ids
  # to run. Running multiple times should have no adverse effects.
  csv = CSV.read('lib/assets/hospitals.csv', {:headers => false, :col_sep => ',', :encoding => 'ISO-8859-1'})
  counter = 0
  csv.each do |row|
    counter += 1
    puts "#{counter} Done" if (counter % 100) == 0
    if row[0].present?
      begin
        location = Location.find_by(hwaid: row[1])
        if location.present?
          location.update_attribute(:medicare_id, row[0])
          puts "Added Medicare id #{row[0]} to hospital with hwaid #{row[1]}"
        end
      rescue
        puts("Hospital not found with hwaid: #{row[1]}")
      end
    end
  end
end