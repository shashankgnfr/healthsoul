desc "Add Physician Assistant specialties"
task :add_pa_specialty => :environment do
  csv = CSV.read('lib/assets/pa.csv', {:headers => true, :col_sep => ',', :encoding => 'ISO-8859-1'})
  counter = 0
  puts "setting PAs"
  csv.each do |row|
    counter += 1
    puts "#{counter} Done" if (counter % 100) == 0
    doctor = Doctor.find_by_npi(row["Npi"])
    if doctor.present?
      ds = DoctorSpeciality.new(doctor_id: doctor.id, practice_speciality_id: 385)
      if ds.valid?
        ds.save
        print "."
      else
        print "x"
      end
    else
      print "m"
    end
  end
end

desc "Add Nurse Practitioner specialties"
task :add_np_specialty => :environment do
  csv = CSV.read('lib/assets/np.csv', {:headers => true, :col_sep => ',', :encoding => 'ISO-8859-1'})
  counter = 0
  puts "setting nurses"
  csv.each do |row|
    counter += 1
    puts "#{counter} Done" if (counter % 100) == 0
    doctor = Doctor.find_by_npi(row["Npi"])
    if doctor.present?
      ds = DoctorSpeciality.new(doctor_id: doctor.id, practice_speciality_id: 116)
      if ds.valid?
        ds.save
        print "."
      else
        print "x"
      end
    else
      print "m"
    end
  end
end
