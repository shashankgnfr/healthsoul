desc "Import Users/subscribers"
task :import_users, [:xlsx_file_name] => :environment do |t, args|
  file = "#{Rails.root}/docs/#{args[:xlsx_file_name]}"
  file_name = File.basename(file, File.extname(file))
  workbook = Roo::Excelx.new(file)
  sheet = workbook.sheet(0)
  errors = []

  header = sheet.row(1) 
  (2..sheet.last_row).each do |i|
    row = sheet.row(i)
    password = Devise.friendly_token.first(10)
    user = User.new(
      { 
        first_name: row[0],
        last_name: row[1],
        address: row[2],
        city: row[3],
        state: row[4],
        zip: row[5],
        co: row[6],
        email: row[7],
        age_group: which_age_group(row[8].to_i),
        subscription_only: true,
        password: password, 
        password_confirmation: password,
        email_opt_in: true
      }
    )
    user.skip_confirmation! 
    unless user.save
      errors << [user.email, user.errors.full_messages.to_sentence]
    end
  end
  errors = errors.compact

  unless errors.empty?
    error_file = "#{Rails.root}/docs/Error_#{file_name}.csv"
    puts "Errors while processing #{file_name}"
    CSV.open(error_file, "w") do |csv|
      csv << ['Email', 'Error']
      errors.each do |i|
        csv << i
      end
    end
    puts "#{error_file} created."
  end

  puts "Processed #{file_name} successfully."
end

private

def which_age_group(age)
  User.age_group_ranges.each_pair do |key, val|
    return val if key.include? age
  end
  return nil
end
