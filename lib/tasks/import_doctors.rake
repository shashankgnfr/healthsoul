desc "Import Doctors on DoctorUpload list"
task :import_doctors => :environment do |t, args|
  Importer::Doctors.new.import
end

task remove_duplicate_doctors: :environment do
  dups = Doctor.select("npi, count(id)").having("count(id) > 1").group("npi").where.not(npi: nil).to_a
  dups.each do |dup|
    dr =
      Doctor
      .joins("LEFT OUTER JOIN doctor_reviews
              ON doctor_reviews.doctor_id = doctors.id")
      .select(:id, :npi, "count(doctor_reviews.id) as review_count")
      .group(:id, :npi, :updated_at)
      .where(npi: dup.npi)
      .having("count(doctor_reviews.id) < 1")
      .order(:updated_at).first
    p dr.npi
    dr.destroy
  end
end

task remove_dentists: :environment do
  dentistry = PracticeSpeciality.where(name: "Dentistry").first
  dentists = dentistry.doctors.where.not(state: "IL")
  dentists.find_each do |dentist|
    p dentist.name
    dentist.destroy
  end
end

task :add_specialities => :environment do
  offset = Time.current.min * 200
  DoctorUpload.where(spec: false).where(:Primary_Specialty => 'Dentist').each do |row|
    doctor = Doctor.find_or_initialize_by(
      name: sani(row["Name"]),
      credential: sani(row["Credential"]),
      graduation: sani(row["Graduation_year"]),
    )
    if doctor.persisted?
      sp = []
      specialities = doctor.practice_speciality_ids
      spec1 = row["Primary_Specialty"]
      spec2 = row["Secondary_Specialty_1"]
      if spec1.present?
        sp1 = PracticeSpeciality.find_or_create_by(
          name: sani(row["Primary_Specialty"])
        )
        unless specialities.include?(sp1.id)
          sp << sp1
        end
      end
      if spec2.present?
        sp2 = PracticeSpeciality.find_or_create_by(
          name: sani(row["Secondary_Specialty_1"])
        )
        unless specialities.include?(sp2.id)
          sp << sp2
        end
      end
      if sp.size > 0
        doctor.practice_specialities << sp
        p "Added #{spec1} and #{spec2} to #{doctor.id} - #{doctor.name}"
        doctor.save
      else
        p "Recaching #{doctor.id} - #{doctor.name}"
        doctor.save
      end
      row.update(spec: true)
    end
  end
end

task duplicate_records_with_dependencies: :environment do
  dups = Doctor.select("slug, COUNT(*) count").having("count(id) > 1").group("slug").to_a
  repo = "#{Rails.root}/docs"
  Dir.mkdir repo unless File.exists?(repo)
  book = Spreadsheet::Workbook.new
  header_format = Spreadsheet::Format.new weight: :bold, size: 8
  doctor_sheet = book.create_worksheet name: 'Doctor'
  doctor_sheet.row(0).default_format = header_format
  header = ['ID', 'Name', 'SLUG', 'NPI', 'yext_id', 'created_at', 'updated_at', 'practice_specialities', 'reviews', 'questions', 'answers', 'claim_requests', 'hospitals', 'blogs', 'appointments', 'favorites', 'business_hours', 'additional_locations', 'awards']
  doctor_sheet.row(0).concat header
  
  index = 0
  dups.each do |dup|
    Doctor.where(slug: dup.slug).each do |dr|
      index += 1
      row_data = [
        dr.id,
        dr.name,
        dr.slug,
        dr.npi,
        dr.yext_id,
        dr.created_at,
        dr.updated_at,
        dr.practice_specialities.pluck(:name).join(','),
        dr.reviews.count,
        dr.questions.count,
        dr.answers.count,
        dr.claim_requests.count,
        dr.hospital_ids.join(','),
        dr.blog_ids.join(','),
        dr.appointments.count,
        dr.favorites.count,
        dr.business_hours.count,
        dr.additional_locations.count,
        dr.awards.count
      ]
      doctor_sheet.row(index).concat row_data
    end
  end

  duplicate_doctors_file = "#{repo}/duplicate_doctors.xls"
  book.write duplicate_doctors_file
  puts "#{duplicate_doctors_file} created."
end

# TODO: in future, we need to amend this task to consider reviews count
task remove_duplicate_doctors_with_no_reviews: :environment do

  dups = Doctor.select("slug, COUNT(*) count").having("count(id) > 1").group("slug").where.not(slug: [nil, 
    'dr-jaspreet-chotala',
    'dr-javed-saifi',
    'dr-sandeep-nagar',
    'dr-sankalp-singh-2',
    'dr-shalu-uberoi',
    'dr-shiraz-faridi-dental-clinic',
    'dr-shokat-ali-2',
    'dr-sonam-gupta',
    'dr-vinay-kumar-sharma-2',
    'dr-vinit',
    'ratra-s-cosmetic-dental-centre']).to_a
  repo = "#{Rails.root}/docs"
  Dir.mkdir repo unless File.exists?(repo)
  book = Spreadsheet::Workbook.new
  header_format = Spreadsheet::Format.new weight: :bold, size: 8
  doctor_sheet = book.create_worksheet name: 'Doctor'
  doctor_sheet.row(0).default_format = header_format
  header = ['Preserved ID', 'Name', 'SLUG', 'Deleted IDs']
  doctor_sheet.row(0).concat header
  index = 0

  dups.each do |dup|
    dup_drs = Doctor.where(slug: dup.slug).order(updated_at: :desc)
    preserved_dr = dup_drs[0]
    row_data = [
      preserved_dr.id,
      preserved_dr.name,
      preserved_dr.slug,
      dup_drs[1..-1].pluck(:id).join(', ')
    ]
    dup_drs[1..-1].each do |d|
      d.destroy
    end
    index += 1
    doctor_sheet.row(index).concat row_data
    
    # Doctor.connection.execute("
    #   DELETE
    #   FROM
    #     doctors a
    #   USING doctors b
    #   WHERE
    #     a.id < b.id
    #     AND a.slug = b.slug;
    #   ")
  end
  duplicate_doctors_file = "#{repo}/deleted_duplicate_doctors_with_no_reviews.xls"
  book.write duplicate_doctors_file
  puts "#{duplicate_doctors_file} created."
end

# TODO: in future, we need to amend this task to consider reviews count
task remove_duplicate_doctors_with_reviews: :environment do
  dup_drs_with_reviews = ['dr-jaspreet-chotala',
    'dr-javed-saifi',
    'dr-sandeep-nagar',
    'dr-sankalp-singh-2',
    'dr-shalu-uberoi',
    'dr-shiraz-faridi-dental-clinic',
    'dr-shokat-ali-2',
    'dr-sonam-gupta',
    'dr-vinay-kumar-sharma-2',
    'dr-vinit',
    'ratra-s-cosmetic-dental-centre']

  repo = "#{Rails.root}/docs"
  Dir.mkdir repo unless File.exists?(repo)
  book = Spreadsheet::Workbook.new
  header_format = Spreadsheet::Format.new weight: :bold, size: 8
  doctor_sheet = book.create_worksheet name: 'Doctor'
  doctor_sheet.row(0).default_format = header_format
  header = ['Name', 'SLUG', 'Deleted IDs', 'Preserved IDs']
  doctor_sheet.row(0).concat header
  index = 0

  dup_drs_with_reviews.each do |slug|
    dup_drs = Doctor.where(slug: slug)
    row_data = [dup_drs.first.name, dup_drs.first.slug]
    deleted_ids = []
    preserved_ids = []
    dup_drs.each do |dr|
      # only delete duplicate record if there is no review
      if dr.reviews.count == 0
        deleted_ids << dr.id
        # this makes task slow - but we need to make sure all dependent records deleted.
        dr.destroy
      else
        preserved_ids << dr.id
      end
    end
    row_data[2] = deleted_ids.join(',')
    row_data[3] = preserved_ids.join(', ')
    index += 1
    doctor_sheet.row(index).concat row_data
  end

  duplicate_doctors_file = "#{repo}/deleted_duplicate_doctors_with_reviews.xls"
  book.write duplicate_doctors_file
  puts "#{duplicate_doctors_file} created."
end


def sani(string)
  return nil if string.blank?
  string = string.gsub(/\s+/, ' ').strip
  string.force_encoding('iso8859-1').encode('utf-8')
end
