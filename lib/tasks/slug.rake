namespace :slug do

  desc "Fix slug for location"
  task :location => :environment do |t|

    missing_location_name

    missing_location_type

    locations_without_slug = Location.where(slug: nil).where.not(name: nil).where.not(location_type_id: nil).order(id: :asc)
    locations_without_slug.each do |l|
      l.save
    end
  end

  private

  def missing_location_name
    repo = "#{Rails.root}/docs"
    Dir.mkdir repo unless File.exists?(repo)

    location_without_name = Location.joins(:location_type).where(name: nil)
    unless location_without_name.empty?
      error_file = "#{repo}/Error_Location_Name.csv"
      CSV.open(error_file, "w") do |csv|
        csv << ['Location ID', 'Location Type', 'Location Sub Type']
        location_without_name.each do |l|
          csv << [l.id, l.location_type.name, l.sub_type]
        end
      end
      puts "#{error_file} created."
    end
  end

  def missing_location_type
    location_without_location_type = Location.joins(:location_type).where(location_type_id: nil)
    unless location_without_location_type.empty?
      error_file = "#{Rails.root}/docs/Error_Location_type.csv"
      CSV.open(error_file, "w") do |csv|
        csv << ['Location ID', 'Location Name', 'Location Sub Type']
        location_without_location_type.each do |l|
          csv << [l.id, l.name, l.sub_type]
        end
      end
      puts "#{error_file} created."
    end
  end

end