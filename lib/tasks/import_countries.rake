desc "Import countires list"
task :import_countries => :environment do
  csv = CSV.read('spec/fixtures/countries.csv', {:headers => true, :col_sep => ',', :encoding => 'ISO-8859-1'})
  csv.each do |row|
    Country.create(
      alpha2: row["alpha2"],
      alpha3: row["alpha3"],
      numeric: row["numeric"],
      fips: row["fips"],
      name: row["country"],
      capital: row["capital"],
      areakm: row["areakm"],
      pop: row["pop"],
      continent_code: row["continent_code"],
      continent: row["continent"],
    )
  end
end

desc "Import countires codes to locaions"
task :import_country_codes => :environment do
  countries = Location.pluck(:country).uniq
  countries.each do |co|
    code = Country.where(name: co).first
    if code.present?
      Location.where(country: co).update_all(country_code: code.alpha3)
    else
      p "#{co} not found"
    end
  end

  {"USA" => "USA", "England" => "GBR", "Northern Ireland" => "GBR"}.each do |k, v|
      Location.where(country: k).update_all(country_code: v)
  end

end
