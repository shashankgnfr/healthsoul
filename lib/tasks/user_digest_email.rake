namespace :user_digest_email do
	desc "Send weekly digest email to users"
	task :send => :environment do
		#get top 4 blogs
		if Time.zone.now.wday == 2 ##run every tuesday
			blogs = Blog.order("published_at desc").limit(4)
			users = User.select(:id, :email, :total_points, :marked_as_provider).where(marked_as_provider: false, email_opt_in: true)
			users.find_in_batches(batch_size: 100) do |users_in_batches|
				users_in_batches.each do |user|
		      begin
		      	if user.email.present?
		          UserMailer.weekly_digest(user, blogs).deliver_now
		        end
		      rescue Exception => e
		      end
				end
			end
		end
	end
end
