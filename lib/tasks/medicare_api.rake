desc "Import Doctors From Medicare Api Not In Our Database"

task get_api_specialties: :environment do
  pr_spe_array = []
  client = SODA::Client.new({:domain => "data.medicare.gov", :app_token => "HQLwGOPe1OiXFvbUMSor6yrax"})
  (0..53).to_a.each do |offset_num|
    p "pulling offset #{offset_num}"
    batch_results = client.get("c8qv-268j", :$limit => 50000, :$offset => offset_num)
    results_prs = batch_results.collect{|dr| dr['pri_spec']}.uniq
    pr_spe_array.push(results_prs).flatten!.uniq!
  end
  p pr_spe_array
end

desc "Import Data From Medicare Api"

task pull_medicare_drs: :environment do
  include MedicarePuller
  include ActionView::Helpers::NumberHelper
  client = SODA::Client.new({:domain => "data.medicare.gov", :app_token => "HQLwGOPe1OiXFvbUMSor6yrax"})
  doctors_pulled = 0
  start_range = Time.current.day.to_i
  end_range = start_range + 24
  (0..42).to_a.each do |offset_num|
    print "#{offset_num}"
    batch_results = client.get("c8qv-268j", :$order => :npi, :$limit => 50000, :$offset => 50000 * offset_num)
    batch_results.body.each do |their_dr|
      our_dr = []
      our_dr = Doctor.find_by_npi(their_dr['npi']) if their_dr['npi'].present?
      print "."
      unless our_dr.present? || their_dr["pri_spec"] == 'CLINICAL SOCIAL WORKER'
        begin
          new_dr = Doctor.new(name: pull_name(their_dr),
                                 gender: pull_gender(their_dr['gndr']),
                                 npi: their_dr['npi'],
                                 address: pull_address(their_dr),
                                 city: their_dr['cty']&.capitalize,
                                 state: their_dr['st'],
                                 zip: their_dr['zip'],
                                 country_code: 'USA',
                                 country: 'USA',
                                 phone: number_to_phone(their_dr['phn_numbr'], area_code: true),
                                 graduation: their_dr['grd_yr']&.to_i)
          if new_dr.save
            set_dr_specialty(new_dr, their_dr["pri_spec"])
            print "pulled #{new_dr.name}"
            doctors_pulled += 1
          end
        rescue => e
          p "Could not save Doctor due to: #{e}"
        end
      end
    end
  end
  puts "Pulled #{doctors_pulled} Doctors From Medicare API"
end

desc "Import Practice Data From Medicare Api"

task pull_medicare_practices: :environment do
  include MedicarePuller
  include ActionView::Helpers::NumberHelper
  practice_id = LocationType.find_by_name('Practice').id
  client = SODA::Client.new({:domain => "data.medicare.gov", :app_token => "HQLwGOPe1OiXFvbUMSor6yrax"})
  pratices_pulled = 0
  start_range = Time.current.day.to_i
  end_range = start_range + 24
  (0..42).to_a.each do |offset_num|
    print "#{offset_num}"
    batch_results = client.get("c8qv-268j", :$order => :npi, :$limit => 50000, :$offset => 50000 * offset_num)
    batch_results.body.each do |their_dr|
      our_practice = []
      our_practice = Location.find_by_practice_id(their_dr['org_pac_id']) if their_dr['org_pac_id'].present?
      print "."
      our_dr = Doctor.find_by_npi(their_dr['npi']) if their_dr['npi'].present?
      if our_practice.present?
        if our_dr.present? && !our_practice.doctors.include?(our_dr)
          our_practice.doctors << our_dr
        end
      elsif their_dr['org_pac_id'].present? && their_dr['org_lgl_nm'].present?
        begin
          new_practice = Location.new(
            name: pull_org_name(their_dr['org_lgl_nm']),
            practice_id: their_dr['org_pac_id'],
            address: pull_address(their_dr),
            city: their_dr['cty']&.split(' ')&.map(&:capitalize)&.join(' '),
            state: their_dr['st'],
            zip: pull_zip(their_dr['zip']),
            country_code: 'USA',
            country: 'USA',
            phone: number_to_phone(their_dr['phn_numbr'], area_code: true),
            location_type_id: practice_id
          )
          if new_practice.save
            set_practice_specialty(new_practice, their_dr["pri_spec"])
            print "pulled #{new_practice.name}"
            if our_dr.present? && !new_practice.doctors.include?(our_dr)
              new_practice.doctors << our_dr
            end
            pratices_pulled += 1
          end
        rescue => e
          p "Could not save Practice due to: #{e}"
        end
      end
    end
  end
  puts "Pulled #{pratices_pulled} Practices From Medicare API"
end
