namespace :import_hospitals do 
  
  # load Hospital General Information
  desc "Import Hospitals - Hospital General Information"
  task :medicare_quality, [:xlsx_file] => :environment do |t, args|

    location_type_id = LocationType.where(name: 'Hospital').first.try(:id)

    workbook = Roo::Spreadsheet.open args[:xlsx_file]
    worksheet = workbook.sheets[0]
    worksheet_data = workbook.sheet(worksheet).parse(headers: true)
    
    file = args[:xlsx_file]
    file_name = File.basename(file, File.extname(file))
    processed_workbook = Spreadsheet::Workbook.new
    processed_sheet = processed_workbook.create_worksheet name: worksheet
    processed_sheet.row(0).concat ['Id', 'HWAID', 'Medicare ID', 'Facility Name', 'Processed', 'Error']

    worksheet_data[1..-1].each_with_index do |row, index|
      id = row['id']
      hwaid = row['hwaid']
      medicare_id = row['Facility ID']
      facility_name = strip_field(row['Facility Name'])

      hospital = Location.where(id: id, hwaid: hwaid, location_type_id: location_type_id).first
      processed_row = [id, hwaid, medicare_id, facility_name]

      if hospital
        data = {
          medicare_id: medicare_id,
          ownership: strip_field(row['Hospital Ownership']),
          emergency_services: process_boolean(row['Emergency Services']),
          hospital_overall_rating: row['Hospital overall rating'],
          mortality_national: strip_field(row['Mortality national comparison']),
          safety_of_care_national: strip_field(row['Safety of care national comparison']),
          readmission_national: strip_field(row['Readmission national comparison']),
          patient_experience_national: strip_field(row['Patient experience national comparison']),
          effectiveness_of_care_national: strip_field(row['Effectiveness of care national comparison']),
          timeliness_of_care_national: strip_field(row['Timeliness of care national comparison']),
          efficient_use_of_medical_imaging_national: strip_field(row['Efficient use of medical imaging national comparison']),
          hospital_overall_rating_graph: row['Hospital overall rating graph'],
          mortality_national_graph: row['Mortality national comparison graph'],
          safety_of_care_national_graph: row['Safety of care national comparison graph'],
          readmission_national_graph: row['Readmission national comparison graph'],
          patient_experience_national_graph: row['Patient experience national comparison graph'],
          effectiveness_of_care_national_graph: row['Effectiveness of care national comparison graph'],
          timeliness_of_care_national_graph: row['Timeliness of care national comparison graph'],
          efficient_use_of_medical_imaging_national_graph: row['Efficient use of medical imaging national comparison graph']

        }
        Location.where(id: hospital.id).update_all(data)
        processed_row << 'Yes'
      else
        processed_row << 'No'
        processed_row << 'No Match'
      end
      processed_sheet.row(index+1).concat processed_row
    end

    data_file = "#{Rails.root}/tmp/#{file_name}.xls"
    processed_workbook.write data_file
    puts "#{data_file} created."
  end

  # load hvbp_hcahps file
  desc "Import Hospitals - hvbp_hcahps"
  task :patient_experience, [:xlsx_file] => :environment do |t, args|

    location_type_id = LocationType.where(name: 'Hospital').first.try(:id)

    workbook = Roo::Spreadsheet.open args[:xlsx_file]
    worksheet = workbook.sheets[0]
    worksheet_data = workbook.sheet(worksheet).parse(headers: true)
    
    file = args[:xlsx_file]
    file_name = File.basename(file, File.extname(file))
    processed_workbook = Spreadsheet::Workbook.new
    processed_sheet = processed_workbook.create_worksheet name: worksheet
    processed_sheet.row(0).concat ['Provider Number', 'Processed', 'Error']

    worksheet_data[1..-1].each_with_index do |row, index|
      medicare_id = row['Provider Number']
      hospital = Location.where(medicare_id: medicare_id, location_type_id: location_type_id).first
      processed_row = [medicare_id]

      if hospital
        data = {
          communication_with_nurses_rate: row['Communication with Nurses Performance Rate'],
          communication_with_drs_rate: row['Communication with Doctors Performance Rate'],
          responsiveness_of_hospital_staff_rate: row['Responsiveness of Hospital Staff Performance Rate'],
          care_transition_rate: row['Care Transition Performance Rate'],
          communication_about_medicine_rate: row['Communication about Medicines Performance Rate'],
          cleanliness_quietness_rate: row['Cleanliness and Quietness of Hospital Environment Performance Rate'],
          discharge_rate: row['Discharge Information Performance Rate'],
          overall_performance_rate: row['Overall Rating of Hospital Performance Rate']
        }
        Location.where(id: hospital.id).update_all(data)
        processed_row << 'Yes'
      else
        processed_row << 'No'
        processed_row << 'No Match'
      end
      processed_sheet.row(index+1).concat processed_row
    end

    data_file = "#{Rails.root}/tmp/#{file_name}.xls"
    processed_workbook.write data_file
    puts "#{data_file} created."
  end
end

private

def process_boolean(field)
  field.strip.try(:downcase) == 'yes' ? true : false 
end

def strip_field(field)
  field.try(:strip)
end