require 'rails_helper'

RSpec.describe "hdetails/new", type: :view do
  before(:each) do
    assign(:hdetail, Hdetail.new(
      :platform => "MyString",
      :website_url => "MyString",
      :name => 1,
      :address => 1,
      :phone => 1,
      :photo => 1,
      :website => 1,
      :speciallity => 1,
      :hour => 1,
      :fee => 1,
      :fee => 1,
      :about_me => 1,
      :education => 1,
      :rating => 1,
      :number_of_reviews => 1,
      :total_rating => 1,
      :doctor => nil
    ))
  end

  it "renders new hdetail form" do
    render

    assert_select "form[action=?][method=?]", hdetails_path, "post" do

      assert_select "input#hdetail_platform[name=?]", "hdetail[platform]"

      assert_select "input#hdetail_website_url[name=?]", "hdetail[website_url]"

      assert_select "input#hdetail_name[name=?]", "hdetail[name]"

      assert_select "input#hdetail_address[name=?]", "hdetail[address]"

      assert_select "input#hdetail_phone[name=?]", "hdetail[phone]"

      assert_select "input#hdetail_photo[name=?]", "hdetail[photo]"

      assert_select "input#hdetail_website[name=?]", "hdetail[website]"

      assert_select "input#hdetail_speciallity[name=?]", "hdetail[speciallity]"

      assert_select "input#hdetail_hour[name=?]", "hdetail[hour]"

      assert_select "input#hdetail_fee[name=?]", "hdetail[fee]"

      assert_select "input#hdetail_fee[name=?]", "hdetail[fee]"

      assert_select "input#hdetail_about_me[name=?]", "hdetail[about_me]"

      assert_select "input#hdetail_education[name=?]", "hdetail[education]"

      assert_select "input#hdetail_rating[name=?]", "hdetail[rating]"

      assert_select "input#hdetail_number_of_reviews[name=?]", "hdetail[number_of_reviews]"

      assert_select "input#hdetail_total_rating[name=?]", "hdetail[total_rating]"

      assert_select "input#hdetail_doctor_id[name=?]", "hdetail[doctor_id]"
    end
  end
end
