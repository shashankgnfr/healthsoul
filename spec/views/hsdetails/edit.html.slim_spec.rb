require 'rails_helper'

RSpec.describe "hsdetails/edit", type: :view do
  before(:each) do
    @hsdetail = assign(:hsdetail, Hsdetail.create!(
      :platform => "MyString",
      :website_url => "MyString",
      :name => 1,
      :address => 1,
      :phone => 1,
      :photo => 1,
      :website => 1,
      :speciallity => 1,
      :hour => 1,
      :fee => 1,
      :fee => 1,
      :about_me => 1,
      :education => 1,
      :rating => 1,
      :number_of_reviews => 1,
      :total_rating => 1,
      :doctor => nil
    ))
  end

  it "renders the edit hsdetail form" do
    render

    assert_select "form[action=?][method=?]", hsdetail_path(@hsdetail), "post" do

      assert_select "input#hsdetail_platform[name=?]", "hsdetail[platform]"

      assert_select "input#hsdetail_website_url[name=?]", "hsdetail[website_url]"

      assert_select "input#hsdetail_name[name=?]", "hsdetail[name]"

      assert_select "input#hsdetail_address[name=?]", "hsdetail[address]"

      assert_select "input#hsdetail_phone[name=?]", "hsdetail[phone]"

      assert_select "input#hsdetail_photo[name=?]", "hsdetail[photo]"

      assert_select "input#hsdetail_website[name=?]", "hsdetail[website]"

      assert_select "input#hsdetail_speciallity[name=?]", "hsdetail[speciallity]"

      assert_select "input#hsdetail_hour[name=?]", "hsdetail[hour]"

      assert_select "input#hsdetail_fee[name=?]", "hsdetail[fee]"

      assert_select "input#hsdetail_fee[name=?]", "hsdetail[fee]"

      assert_select "input#hsdetail_about_me[name=?]", "hsdetail[about_me]"

      assert_select "input#hsdetail_education[name=?]", "hsdetail[education]"

      assert_select "input#hsdetail_rating[name=?]", "hsdetail[rating]"

      assert_select "input#hsdetail_number_of_reviews[name=?]", "hsdetail[number_of_reviews]"

      assert_select "input#hsdetail_total_rating[name=?]", "hsdetail[total_rating]"

      assert_select "input#hsdetail_doctor_id[name=?]", "hsdetail[doctor_id]"
    end
  end
end
