require 'rails_helper'

RSpec.describe "hsdetails/show", type: :view do
  before(:each) do
    @hsdetail = assign(:hsdetail, Hsdetail.create!(
      :platform => "Platform",
      :website_url => "Website Url",
      :name => 2,
      :address => 3,
      :phone => 4,
      :photo => 5,
      :website => 6,
      :speciallity => 7,
      :hour => 8,
      :fee => 9,
      :fee => 10,
      :about_me => 11,
      :education => 12,
      :rating => 13,
      :number_of_reviews => 14,
      :total_rating => 15,
      :doctor => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Platform/)
    expect(rendered).to match(/Website Url/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/4/)
    expect(rendered).to match(/5/)
    expect(rendered).to match(/6/)
    expect(rendered).to match(/7/)
    expect(rendered).to match(/8/)
    expect(rendered).to match(/9/)
    expect(rendered).to match(/10/)
    expect(rendered).to match(/11/)
    expect(rendered).to match(/12/)
    expect(rendered).to match(/13/)
    expect(rendered).to match(/14/)
    expect(rendered).to match(/15/)
    expect(rendered).to match(//)
  end
end
