require 'rails_helper'

RSpec.describe "hsdetails/index", type: :view do
  before(:each) do
    assign(:hsdetails, [
      Hsdetail.create!(
        :platform => "Platform",
        :website_url => "Website Url",
        :name => 2,
        :address => 3,
        :phone => 4,
        :photo => 5,
        :website => 6,
        :speciallity => 7,
        :hour => 8,
        :fee => 9,
        :fee => 10,
        :about_me => 11,
        :education => 12,
        :rating => 13,
        :number_of_reviews => 14,
        :total_rating => 15,
        :doctor => nil
      ),
      Hsdetail.create!(
        :platform => "Platform",
        :website_url => "Website Url",
        :name => 2,
        :address => 3,
        :phone => 4,
        :photo => 5,
        :website => 6,
        :speciallity => 7,
        :hour => 8,
        :fee => 9,
        :fee => 10,
        :about_me => 11,
        :education => 12,
        :rating => 13,
        :number_of_reviews => 14,
        :total_rating => 15,
        :doctor => nil
      )
    ])
  end

  it "renders a list of hsdetails" do
    render
    assert_select "tr>td", :text => "Platform".to_s, :count => 2
    assert_select "tr>td", :text => "Website Url".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => 5.to_s, :count => 2
    assert_select "tr>td", :text => 6.to_s, :count => 2
    assert_select "tr>td", :text => 7.to_s, :count => 2
    assert_select "tr>td", :text => 8.to_s, :count => 2
    assert_select "tr>td", :text => 9.to_s, :count => 2
    assert_select "tr>td", :text => 10.to_s, :count => 2
    assert_select "tr>td", :text => 11.to_s, :count => 2
    assert_select "tr>td", :text => 12.to_s, :count => 2
    assert_select "tr>td", :text => 13.to_s, :count => 2
    assert_select "tr>td", :text => 14.to_s, :count => 2
    assert_select "tr>td", :text => 15.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
