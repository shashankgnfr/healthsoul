require 'rails_helper'

describe "referral links" do
  create_user

  it 'adds the new user to the referring users list' do
    user.update(referral_activated: true)
    visit new_user_registration_path(ref: user.id)
    fill_in "Email", :with => "ref@ref.com"
    fill_in "user[password]", :with => "xyz1234987"
    fill_in "user[password_confirmation]", :with => "xyz1234987"
    click_on "Sign up"
    expect(user.referred_users.map(&:email)).to include("ref@ref.com")
  end

  context 'user has been referred' do
    let(:ref_user){ create(:user, email: 'ref@ref.com') }
    sign_in_user

    before do
      user.referred_users << ref_user
    end

    it 'user can see their referral link in profile' do
      user.update(referral_activated: true)
      visit user_path(user)
      expect(page).to have_link("#{root_url}?ref=#{user.id}")
      u = User.last
      expect(page).to have_content u.username
    end

    context 'referral program not activiated for user' do
      it 'user will not see link' do
        visit user_path(user)
        expect(page).to_not have_content "REFERRAL"
      end
    end
  end


end
