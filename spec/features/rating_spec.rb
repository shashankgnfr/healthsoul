require 'rails_helper'
require 'sucker_punch/testing/inline'

describe "Creating A Rating" do
  create_user
  add_locations
  sign_in_admin

  it 'is visible by default' do
    visit location_path location
    expect(page).to have_content "Reviews: 0"
    find("[data-write-review]").click
    fill_in "rating[use_date]", with: "02-2017"
    choose "Eye Care"
    choose "Yes"
    fill_in "rating[comment]", with: "some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments "
    click_on "Submit"
    expect(page).to have_content "Reviews: 1"
    expect(page).to have_content(location.name)
    expect(page).to have_content("Eye Care")
  end

end

describe "review for doctor" do
  create_user
  add_doctor

  context "when user is already signed in" do
    sign_in_admin

    it 'can crate a review', job: true do
      manager = create(:user, email: 'manager@mm.com')
      RoleAssigner.new(manager, "moderator", doctor).assign
      visit doctor_path doctor
      expect(page).to have_content "Reviews: 0"
      all("a", text: "WRITE A REVIEW").first.click
      fill_in "doctor_review[use_date]", with: "02-2017"
      choose "Inpatient/Hospital"
      choose "Yes"
      fill_in "doctor_review[comment]", with: "some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments "
      click_on "Submit"
      expect(email_count).to eq 2
      expect(page).to have_css("h5.total-reviews", text: "Reviews: 1")
      expect(page).to have_content(doctor.name)
      expect(page).to have_content("Inpatient")

      visit user_path(user)
      expect(page).to have_content(doctor.name)
      expect(page).to have_content("Inpatient")
      expect(page).to have_css(".total-points", text: 5)
      click_link doctor.name
      expect(page).to have_css(:h3, text: doctor.name)
    end
  end

  context "when use is not signed in" do
    it 'can crate a review after signing in' do
      visit doctor_path doctor
      expect(page).to have_content "(0 reviews)"
      within ".review-summary" do
        click_link "Write Review"
      end
      fill_in "doctor_review[use_date]", with: "02-2017"
      choose "Inpatient/Hospital"
      choose "Yes"
      fill_in "doctor_review[comment]", with: "some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments "
      click_on "Submit"
      fill_in "Email", with: user.email
      fill_in "Password", with: user.password
      click_on "Log in"
      expect(page).to have_content "(1 reviews)"
      expect(page).to have_content(doctor.name)
      expect(page).to have_content("Inpatient")
    end
    
  end

end
