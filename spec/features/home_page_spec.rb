require 'rails_helper'

describe "Landing on the homepage" do
  create_user
  add_doctor
  add_doctor_review
  sign_in_user

  it 'displayes recently reviewd doctors' do
    visit root_path
    expect(page).to have_css(:h2, text: "RECENTLY REVIEWED DOCTORS")
    expect(page).to have_content doctor.name
  end
end
