require 'rails_helper'

describe "Doctor Search" do
  add_doctors

  it 'can search' do
    visit search_doctors_path
    click_on "Clear Search"
    fill_in "Name", :with => "Jess"
    find("form.new_search_doctor button.button").click()
    expect(page).to have_content "Jess Brown"
    click_link "Jess Brown"
    expect(page).to have_css(:h1, text: "Jess Brown")
  end

  context 'searching by speciality' do
    add_doctor_speciality
    before { patty_org.update(country_code: "USA")}

    it 'returns the doctor' do
      visit search_doctors_path

      click_on "Clear Search"
      select "All", from: "search_doctor_speciality"
      find("form.new_search_doctor button.button").click()
      expect(page).to have_content "Jess Brown"
      expect(page).to have_content "Patty"

      select spec1.name, from: "search_doctor_speciality"
      find("form.new_search_doctor button.button").click()
      expect(page).to have_content "Jess Brown"
      expect(page).to_not have_content "Patty"
    end
  end
end
