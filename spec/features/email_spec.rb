require 'rails_helper'

describe "sending email" do
  create_user
  add_locations
  sign_in_admin

  context 'when a rating is created' do
    it 'sends an email to the user' do
      visit location_path location
      find_all("a", text: "Write Review").first.click
      fill_in "rating[use_date]", with: "02-2017"
      choose "Eye Care"
      choose "Yes"
      fill_in "rating[comment]", with: "some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments "
      click_on "Submit"
      expect(page).to have_content(location.name)
      expect(page).to have_content("Eye Care")
      expect(last_email.subject).to eq "Thank you"
    end

    it 'sends the page manager an email' do
      manager = create(:user, email: 'manager@mm.com')
      RoleAssigner.new(manager, "moderator", location).assign
      visit location_path location
      find_all("a", text: "Write Review").first.click
      fill_in "rating[use_date]", with: "02-2017"
      choose "Eye Care"
      choose "Yes"
      fill_in "rating[comment]", with: "some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments some comments "
      click_on "Submit"
      expect(last_email.subject).to eq "A new review has been submitted for Brown Hospital"
    end
  end

  context 'when a question is asked' do
    it 'sends the manager an email' do
      reset_email!
      manager = create(:user, email: 'manager@mm.com')
      RoleAssigner.new(manager, "moderator", location).assign
      visit location_path location
      find_all("a", text: "Ask A Question").first.click
      fill_in "question[body]", with: "What is 2 plus 2?"
      click_on "Create Question"
      expect(last_email.subject).to eq "A new Question has been submitted for Brown Hospital"
    end
  end
end
