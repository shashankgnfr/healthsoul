require 'rails_helper'


describe "Doctor claiming profile" do
  create_user
  add_doctor

  context "when user is already signed in" do
    sign_in_user

    it 'can crate a claim request' do
      visit doctor_path doctor
      click_link "Request access"
      fill_in "Email", :with => user.email
      fill_in "Position", :with => "Ownwer"
      fill_in "Phone", :with => "87987897"
      fill_in "Description", :with => "i want this"
      click_on "Create Claim request"
      claim_request = ClaimRequest.last
      expect(claim_request.user).to eq user
      expect(claim_request.claimable).to eq doctor
      expect(last_email.subject).to eq "New Claim Request"
    end
  end

  context 'when the user is not signed' do
    sign_in_user
    it 'cannot access the manage profile' do
      visit manage_doctor_path(doctor)
      expect(page).to have_content "You are not authorized to view this page"
    end

    it 'cannot access the edit profile' do
      visit edit_manage_doctor_path(doctor)
      expect(page).to have_content "You are not authorized to view this page"
    end
  end

  context 'when the request is approved' do
    sign_in_user
    before do
      create(:claim_request, claimable: doctor, user: user)
      user.add_role(:moderator, doctor)
    end

    describe "the doctor and edit the profile" do
      let!(:organization2) { create(:doctor_organization, name: "Brown Org") }
      let!(:med_school){ create(:med_school, name: "Brown School") }
      let!(:spec1) { create(:practice_speciality, name: "Spec 1")}
      let!(:spec2) { create(:practice_speciality, name: "Spec 2")}

      it 'shows the stats' do
        expect(user.has_role?(:moderator, doctor)).to be true
        visit root_path
        within ".navbar-nav" do
          click_link "Profile"
        end
        click_link doctor.name
        expect(page).to have_content "Total Page Views"
        click_link "Edit Profile"
        fill_in "Name", :with => "new name"
        fill_in "Credential", :with => "O.G."
        choose "female"
        select "Brown Org", from: "Organization"
        select "Brown School", from: "Med school"
        fill_in "Graduation", :with => "2001"
        select "Researcher", from: "Academic affiliation"
        fill_in "Affiliated hospitals", :with => "Clearview monroe"
        check "Spec 1"
        click_on "Update Doctor"
        expect(page).to have_content "Profile Updated"
        expect(page).to have_content "O.G."
        expect(page).to have_css(:h1, text: "new name")
        click_on "View Public Page"
        expect(page).to have_content "Female"
        expect(page).to have_content "Brown Org"
        expect(page).to have_content "Brown School"
        expect(page).to have_content "2001"
        expect(page).to have_content "Researcher"
        expect(page).to have_content "Clearview monroe"
        expect(page).to have_content "Spec 1"
      end
    end
  end

  describe "the admin can edit the claim request" do
    make_user_admin
    sign_in_user

    it 'allows access' do
      u = create(:user)
      l = create(:location, name: "new location")
      create(:claim_request, claimable: doctor, user: u)
      visit admin_claim_requests_path
      click_link "Edit"
      check "Granted"
      click_on "Update Claim request"
      expect(u.has_role?(:moderator, doctor)).to be true
      expect(page).to have_content "true"
      visit edit_admin_user_path(u)
      expect(page).to have_content "moderator, Dr Jess"
      fill_in "Role name", :with => "moderator"
      fill_in "Role Resource Id", :with => l.id
      select "Location", from: "Role resource type"
      click_on "Update User"
      visit edit_admin_user_path(u)
      expect(page).to have_content "moderator, #{l.name}"
    end
  end
end
