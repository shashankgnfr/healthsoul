require 'rails_helper'

describe "admin search" do
  create_user
  make_user_admin
  sign_in_admin
  add_locations

  it 'searches locations' do
    visit admin_root_path
    click_link "Locations"
    select "Hospital", from: "Location type"
    select "USA", from: "search_admin_search_co"
    click_on "Search"
    expect(page).to have_content location.name
  end
end
