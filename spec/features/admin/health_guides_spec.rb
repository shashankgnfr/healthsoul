require 'rails_helper'

describe "health guides" do
  it 'returns the guides' do
    visit health_guide_path
    expect(page.status_code).to eq 200
    links = find_all(".health-guides a")
    links.each do |link|
      visit link[:href]
      expect(page.status_code).to eq 200
    end
  end

end
