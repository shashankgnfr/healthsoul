require 'rails_helper'


describe "Requesting Doctor Appt" do
  create_user
  add_doctor

  it 'can create the appt' do
    visit doctor_path doctor
    expect(page).to_not have_link "APPOINTMENTS"
    doctor.update(request_appt: true)
    visit current_path
    click_link "APPOINTMENTS"
    fill_in "Date", :with => Date.today.to_s
    select "1:00 pm", from: "Time"
    fill_in "Name", :with => "Jess Brown"
    fill_in "Email", :with => "jess@appt.com"
    fill_in "Phone", :with => "770-266-5594"
    fill_in "Reason", :with => "Physical"
    click_on "Request"
    appt = Appointment.first
    expect(appt.name).to eq "Jess Brown"
    expect(appt.date.to_s).to eq Date.today.to_s
    expect(I18n.l(appt.time, format: :appt)).to eq " 1:00 pm"
    expect(appt.provider).to eq doctor
    expect(page).to have_content "#{Date.today.day} - 1:00 pm"
    expect(page).to have_content "Jess Brown"
    expect(page).to have_content "Physical"
    click_link "Back To #{doctor.name}"
    expect(last_email.subject).to eq "Appointment Confirmation"
    expect(last_email).to have_content "Jess Brown"
    expect(last_email).to have_content "#{Date.today.day} - 1:00 pm"
  end
end
