require 'rails_helper'

describe "Managing a location" do
  let(:user){ create(:user) }
  let(:location_type){ create(:location_type) }
  let(:location){ create(:location, location_type: location_type) }

  before do
    user.add_role(:moderator, location)
  end

  context 'manager is signed in' do
    sign_in_user
    it 'allows the user the edit the location' do
      visit user_path(user)
      click_link location.name
      click_link "Edit Location"
      fill_in "Name", :with => "New Name"
      fill_in "Website", :with => "www.google.com"
      fill_in "Email", :with => "bb@bb.com"
      click_on "Update Location"
      within ".fortis_hospital_frame-right-frame" do
        expect(page).to have_content "New Name"
        expect(page).to have_content "bb@bb.com"
        website = find("a.website-link")
        expect(website[:href]).to eq "//www.google.com"
      end
    end
  end

  it 'does not allow non user to edit' do
    visit edit_manage_location_path(location)
    expect(page).to have_content "You are not authorized to view this page"
  end
end
