require 'rails_helper'

describe "Creating A qustion" do
  create_user
  add_locations
  sign_in_admin

  it 'adds the qustion' do
    visit location_path location
    within ".cta-buttons" do
      click_link "Ask A Question"
    end
    fill_in "question_body", :with => "A new question I'm asking here..."
    click_on "Create Question"
    expect(page).to have_content("A new question I'm asking here...")
    expect(page).to have_link("1 Questions")

    # answer question
    click_link "Answer this question"
    fill_in "answer_body", :with => "This is the answer"
    click_on "Create Answer"
    expect(page).to have_content("This is the answer")
    expect(page).to have_link("View 1 Answers")
  end

  context "for a doctor" do
    add_doctor

    it 'can create a question for dr' do
      visit doctor_path(doctor)
      within ".cta-buttons" do
        click_link "Ask A Question"
      end
      fill_in "question_body", :with => "A new question I'm asking here..."
      click_on "Create Question"
      expect(page).to have_content("A new question I'm asking here...")
      expect(page).to have_link("1 Questions")

      # answer question
      click_link "Answer this question"
      fill_in "answer_body", :with => "This is the answer"
      click_on "Create Answer"
      expect(page).to have_content("This is the answer")
      expect(page).to have_link("View 1 Answers")
    end
  end
end
