require 'rails_helper'

RSpec.describe UserReferral, type: :model do
  let(:user){ create(:user) }
  let(:referred_user){ create(:user, email: 'ref@ref.com') }

  describe "relationships" do
    it 'user has many referred users' do
      user.referred_users << referred_user
      expect(user.referred_users).to include(referred_user)
      expect(referred_user.referring_user).to eq user
    end

    it 'a user cannot be referred more than once' do
      user.referred_users << referred_user
      user2 = create(:user, email: 'user2@uu.com')
      ref = UserReferral.new(user: user2, referred_user: referred_user)
      expect(ref).to be_invalid
      expect(ref.errors.messages.to_s).to include("has already been taken")
    end
  end
end
