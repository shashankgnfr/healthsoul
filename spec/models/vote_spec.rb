require 'rails_helper'
require 'sucker_punch/testing/inline'

RSpec.describe Vote, type: :model do
  let(:user){ create(:user) }
  let(:question){ create(:question, user: user) }
  let(:answer){ create(:answer, user: user) }
  let!(:location){ create(:location, location_type: create(:location_type)) }
  let!(:rating){ create(:rating, user: user, location: location) }

  describe "relationships" do
    it 'vote belongs to question' do
      vote = create(:vote, voteable: question, user: user)
      expect(vote.voteable).to eq question
    end

    it 'vote belongs to user' do
      vote = create(:vote, user: user)
      expect(vote.user).to eq user
    end

    it 'user has many votes' do
      vote = create(:vote, voteable: question, user: user)
      expect(user.votes).to include vote
    end

    it 'question has many votes' do
      vote = create(:vote, voteable: question, user: user)
      expect(question.votes).to include vote
    end

    it 'answer has many votes' do
      vote = create(:vote, voteable: answer, user: user)
      expect(answer.votes).to include vote
    end

    it 'rating has many votes' do
      vote = create(:vote, voteable: rating, user: user)
      expect(rating.votes).to include vote
    end
  end

  describe "validatsion" do
    it 'only allows a user to create one vote per votable' do
      create(:vote, voteable: rating, user: user)
      vote = build(:vote, voteable: rating, user: user)
      expect(vote).to_not be_valid
    end
  end

  describe ".helpful_rank" do
    let!(:vote){  create(:vote, user: create(:user), voteable: question, status: 'helpful') }
    let!(:vote1){ create(:vote, user: create(:user), voteable: question, status: 'helpful') }
    let!(:vote2){ create(:vote, user: create(:user), voteable: question, status: 'unhelpful') }

    it 'returns 1' do
      expect(question.votes.helpful_rank).to eq 1
    end
  end
end
