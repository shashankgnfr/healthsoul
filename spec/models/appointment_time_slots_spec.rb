require 'rails_helper'

describe AppointmentTimeSlots do
  let(:doctor){ create(:doctor) }
  let(:appointment){ create(:appointment, provider: doctor) }

  subject { appointment.time_slots }

  it 'creates a range of avail time slots' do
    expect(subject.first.hour).to eq 9
    expect(subject.first.min).to eq 0
    expect(subject[1].min).to eq 15
  end
end
