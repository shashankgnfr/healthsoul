require 'rails_helper'
require 'sucker_punch/testing/inline'

RSpec.describe ReviewPointsCalculator, type: :model do
  let(:user){ create(:user) }
  let!(:location){ create(:location, location_type: create(:location_type)) }
  let!(:rating){ create(:rating, user: user, location: location) }
  let!(:question){ create(:question, user: user) }
  let!(:answer){ create(:answer, user: user) }

  #5 points for each review
  #2 points for each question or answer
  #1 point for each liked review or question.

  subject { ReviewPointsCalculator.new(user) }

  describe "#review_points" do
    it 'returns 5 points' do
      # user submit 1 review
      expect(subject.review_points).to eq 5
    end
  end

  describe "#quesitons_points" do
    it 'returns 4 points' do
      # user submits ques and ans
      expect(subject.questions_points).to eq 4
    end
  end

  describe "#likes_points" do
    it 'returns 3 points' do
      add_likes
      # user likes rating, question, ans
      expect(subject.likes_points).to eq 3
    end
  end

  describe "#total_points" do
    it 'totals all points' do
      add_likes
      expect(subject.total_points).to eq 12
    end
  end

  describe "#save" do
    it 'updates the user record' do
      add_likes
      subject.save
      expect(user.reload.total_points).to eq 12
    end
  end

  describe "calling the job after each save" do
    it 'calls after a rating save' do
      expect(rating).to receive(:run_calculator)
      rating.save
    end

    it 'calls after a question save' do
      expect(question).to receive(:run_calculator)
      question.save
    end

    it 'calls after a answer save' do
      expect(answer).to receive(:run_calculator)
      answer.save
    end

    it 'calls after a vote save' do
      add_likes
      vote = Vote.first
      expect(vote).to receive(:run_calculator)
      vote.save
    end
  end

  def add_likes
    rating.votes.create(user: user)
    question.votes.create(user: user)
    answer.votes.create(user: user)
  end

end
