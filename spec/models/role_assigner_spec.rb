require 'rails_helper'

RSpec.describe RoleAssigner, type: :model do
  let(:location) { create(:location) }
  let!(:user){ create(:user) }

  describe "without a claimable" do
    subject { RoleAssigner.new(user, "admin" )}
    it 'sets up the class' do
      expect(subject.name).to eq "admin"
    end

    it 'can assign a global role' do
      subject.assign
      expect(user.roles.count).to eq 1
      expect(user.has_role?(:admin)).to be_truthy
    end
  end

  describe "assigning a claimable" do
    subject { RoleAssigner.new(user, "admin", location )}
    
    it 'can add a location role' do
      subject.assign
      expect(user.reload.has_role?(:admin, location)).to be true
    end
  end

end
