require 'rails_helper'

RSpec.describe Question, type: :model do
  describe "relationships" do
    let(:user){ create(:user) }
    let(:location){ create(:location) }
    let(:question){ create(:question, user: user, questionable: location) }

    it 'question has a user and a location' do
      expect(question.user).to eq user
      expect(question.questionable).to eq location
    end

    it 'user has many questions' do
      expect(user.questions).to include question
    end

    it 'location has many questions' do
      expect(location.questions).to include question
    end
  end
end
