require 'rails_helper'

RSpec.describe Doctor, type: :model do
  describe "relationships" do
    let(:doctor){ create(:doctor, ) }

    it 'has many hosptials' do
      hospital = create(:location)
      doctor.hospitals << hospital
      expect(hospital.doctors).to include doctor
      expect(doctor.reload.hospitals).to include hospital
    end
  end

  describe "slugs" do
    it 'doesnt allow blank slugs' do
      doctor = create(:doctor, name: "some slug", slug: nil)
      expect(doctor.slug).to_not be_blank
    end

    it 'doesnt dup' do
      doctor = create(:doctor, name: "some slug", slug: nil)
      doctor2 = create(:doctor, name: "some slug", slug: nil)
      expect(doctor.slug).to_not be_blank
      expect(doctor2.slug).to_not be_blank
      expect(doctor.slug).to_not eq doctor2.slug
      expect(doctor2.slug).to eq "some-slug-2"
      doctor3 = create(:doctor, name: "some slug", slug: nil)
      expect(doctor3.slug).to eq "some-slug-3"
    end
  end

end
