require 'rails_helper'

describe "Doctor Search" do
  add_doctors

  subject { Search::Doctor.new }

  it 'can search by name' do
    subject.name = "jess"
    expect(subject.results).to include doctor1
  end

  it 'can search by country' do
    subject.co = "USA"
    expect(subject.results).to include doctor1
    expect(subject.count).to eq 1
  end

  it 'can search by state' do
    subject.state = "GA"
    expect(subject.results).to include doctor1
    expect(subject.count).to eq 1
  end

  context 'searching by speciality' do
    add_doctor_speciality
    it 'can search by spciality' do
      subject.speciality = spec1.id
      expect(subject.results).to include doctor1
      expect(subject.results).to_not include doctor2
      subject = Search::Doctor.new
      subject.speciality = spec2.id
      expect(subject.results).to include doctor2
      expect(subject.results).to_not include doctor1
    end
  end
end
