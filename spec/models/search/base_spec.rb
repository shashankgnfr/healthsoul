require 'rails_helper'
require 'sucker_punch/testing/inline'

RSpec.describe Search::Base, type: :model do

  describe "search by department reviews" do
    setup_search_by_department

    subject { Search::Hospital.new(co: "USA", departments: [Rating::Reasons["Surgery"].to_s]) }

    context "when search 1 dept" do
      it 'returns both with ratings in that dept' do
        expect(subject.count).to eq 2
        expect(subject.results).to include(location1, location2)
      end
    end
  end

  describe "country filter" do
    let!(:location_type){ create(:location_type, name: "Hospital") }
    let!(:location){ create(:location, city: "Athens", state: "GA", location_type: location_type, overall: 3.4, country_code: "US", country: "US") }
    
    subject { Search::Hospital.new(state: "GA", stars: [3, 4], co: "US") }

    it 'returns the country for US' do
      expect(subject.results).to include location
    end

    it 'should not return the result for Canada' do
      subject.co = "Canada"
      expect(subject.results).to_not include location
    end
  end
  describe "searching ratings" do
    let!(:location_type){ create(:location_type, name: "Hospital") }
    let!(:location){ create(:location, city: "Athens", state: "GA", location_type: location_type, overall: 3.4, country: "US") }

    subject { Search::Hospital.new(state: "GA", stars: [3, 4]) }

    it 'returns the location' do
      expect(subject.results).to include location
    end

    context "we expect higher stars" do
      it 'does not return the location' do
        subject.stars = [5]
        expect(subject.results).to_not include location
      end
    end
  end

  describe "#stars_min" do

    subject { Search::Hospital.new(state: "GA", stars: ["", 3, 4]) }

    it 'returns 3 for 3 & 4' do
      expect(subject.stars_min).to eq 3
    end
  end

  describe "#only_country?" do
    context 'when only country' do
      subject { Search::Hospital.new(co: "US").only_country? }
      it 'returns true' do
        expect(subject).to be_truthy
      end
    end

    context 'when only country' do
      subject { Search::Hospital.new(co: "US", name: "jess").only_country? }
      it 'returns false' do
        expect(subject).to eq false
      end
    end
    
  end
end
