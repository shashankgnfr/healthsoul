require 'rails_helper'
require 'sucker_punch/testing/inline'

RSpec.describe DepartmentRating, type: :model do

  setup_department_ratings

  it 'calculates the average overall rating for each department / reason the person visited the hospital' do
    expect(location.department_ratings.for(Rating::Reasons["Surgery"])).to eq 4.0
    expect(location.department_ratings.for(Rating::Reasons["Eye Care"])).to eq 3.0
  end

  context 'when we make a rating hidden' do
    it 'updates the average rating for that department' do
      expect(location.department_ratings.for(Rating::Reasons["Surgery"])).to eq 4.0
      rating2.update(status: 'hidden')
      expect(location.department_ratings.for(Rating::Reasons["Surgery"])).to eq 3.0
    end

    it 'removes if there are 0 depart ratings visible' do
      expect(location.department_ratings.for(Rating::Reasons["Surgery"])).to eq 4.0
      rating2.update(status: 'hidden')
      expect(location.department_ratings.for(Rating::Reasons["Surgery"])).to eq 3.0
      rating1.update(status: 'hidden')
      expect(location.department_ratings.for(Rating::Reasons["Surgery"])).to eq 0.0
    end
  end
end
