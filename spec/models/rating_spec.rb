require 'rails_helper'

RSpec.describe Rating, type: :model do
  let(:user){ create(:user) }
  let!(:location_type){ create(:location_type, name: "Hospitals") }
  let(:location){ create(:location, location_type: location_type) }
  let(:comment) {
    "one two three four five
    one two three four five
    one two three four five
    one two three four five"
  }
  let(:rating){ create(:rating, user: user, reason: 1,
                       location: location, recommend: "Maybe",
                      comment: comment) }

  describe "visible scopes" do
    it 'does not retun the hidden but does for admin all' do
      hidden = rating.dup
      hidden.save
      expect(Rating.all.count).to eq 2
      expect(hidden.location.ratings_count).to eq 2
      hidden.update(status: 'hidden')
      expect(Rating.all.count).to eq 1
      expect(hidden.location.ratings_count).to eq 1
      expect(Rating.admin_all.count).to eq 2
      hidden.update(status: 'visible')
      expect(Rating.all.count).to eq 2
      expect(hidden.location.ratings_count).to eq 2
    end
  end

  describe "relationships" do
    it 'a rating belongs to user and location' do
      expect(rating.user).to eq user
      expect(rating.location).to eq location
    end

    it 'a user has many ratings' do
      expect(user.ratings).to include rating
    end

    it 'a location has many ratings' do
      expect(location.ratings).to include rating
    end
  end

  describe ".by_type()" do
    let!(:location_type){ create(:location_type, name: "Hospitals") }

    it 'returns ratings by location type' do
      rating
      location.location_type = location_type
      location.save
      expect(Rating.by_type("Hospitals")).to include(rating)
    end
  end

  describe "validations" do
    let!(:rating2){ build(:rating, user: user, location: location, :overall_experience => 3, comment: comment) }
    let!(:rating3){ build(:rating, user: user, location: location, :overall_experience => 3, comment: comment) }

    it 'should only allow user to rate location once' do
      allow(location).to receive(:hospital?).and_return(true)
      rating
      expect(rating2).to be_valid
      rating2.save
      expect(rating3).to_not be_valid

      rating.created_at = DateTime.now - (2.months + 1.day)
      rating.save

      expect(rating3).to be_valid
    end

    it 'should require comment length to be 10 words' do
      rating3.comment = "some words"
      expect(rating3).to_not be_valid
      expect(rating3.errors.messages[:comment]).to eq ["Please enter at least 10 words"]
    end

  end

  describe "updating the location overall" do
    let!(:rating){ build(:rating, user: user, location: location, overall_experience: 3) }

    it 'sets the average rating' do
      expect(RankingJob).to receive(:perform_async).with(location)
      rating.save
    end
  end

  describe ".rating_for" do
    let(:user2){ create(:user, email: "bb@bbcom") }
    let!(:rating2){ create(:rating, user: user2, location: location, :overall_experience => 3) }

    it 'gets the rating for the category' do
      rating.overall_experience = 4
      rating.save
      expect(location.ratings.rating_for(:overall_experience)).to eq 3.5
    end
  end

  describe "#recommend_percentage" do
    let!(:rating2){ create(:rating, user: user, created_at: DateTime.now - 3.months, location: location, recommend: "Yes") }
    let!(:rating3){ create(:rating, user: user, location: location, recommend: "No") }

    it 'should return 1.5 / 3' do
      rating
      expect(location.ratings.recommend_percentage).to eq 50
    end
  end
end
