require 'rails_helper'

RSpec.describe ClaimRequest, type: :model do
  describe "validations" do
    let(:claim_request){ build(:claim_request) }

    it 'requires claimable type and id' do
      expect(claim_request.valid?).to be false
      expect(claim_request.errors.messages.keys).to include(:claimable_id, :claimable_type)
      expect(claim_request.errors.messages.values).to include([ "can't be blank" ])
    end
  end
end
