require 'rails_helper'
require 'sucker_punch/testing/inline'

RSpec.describe Answer, type: :model do
  let(:user){ create(:user) }
  let(:location){ create(:location) }
  let(:question){ create(:question, user: user, questionable: location) }
  let(:answer){ create(:answer, user: user, question: question) }

  describe "relationships" do

    it 'has a user and a question' do
      expect(answer.user).to eq user
      expect(answer.question).to eq question
    end

    it 'a location has many answers through questions' do
      expect(location.answers).to include answer
    end

    it 'question has many answers' do
      expect(question.answers).to include answer
    end

    it 'user has many answers' do
      expect(user.answers).to include answer
    end
  end

  describe "validations" do
    let(:answer2){ build(:answer, user: user, question: question) }
    it 'only allows user to submit 1 answer per question' do
      answer
      expect(answer2).to_not be_valid
    end
  end
end
