require 'rails_helper'

RSpec.describe PatientStory, type: :model do
  describe "validations" do
    let(:patient_story){ create(:patient_story, name: 'Rahul', email: 'rrpathak007@gmail.com') }

    it 'patient_story has no city, country' do
      expect(patient_story.city.nil?).to eq true
      expect(patient_story.country.nil?).to eq true
    end

    it 'patient_story has city, country' do
      patient_story.city = 'Ahmedabad'
      patient_story.country = Country.create(name: 'India')
      expect(patient_story.city.nil?).to eq false
      expect(patient_story.country.nil?).to eq false
    end

  end
end
