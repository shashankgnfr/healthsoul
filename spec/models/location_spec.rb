require 'rails_helper'
require 'sucker_punch/testing/inline'

RSpec.describe Location, type: :model do

  let(:location_type){ create(:location_type, name: "Hospital") }
  let(:location){ create(:location, location_type: location_type, state: "IL", name: "Test Hospital",
                         city: "Chicago", overall: 3.5, country: "USA", country_code: "USA") }

  before do
    allow_any_instance_of(Location).to receive(:hospital?).and_return(true)
  end

  describe "removing a location" do
    setup_location_with_all_dependancies

    it 'removes all the records' do
      rating.save
      expect(Location.count).to eq 1
      expect(Rating.count).to eq 1
      expect(Question.count).to eq 1
      expect(Answer.count).to eq 1
      expect(Vote.count).to eq 2
      expect(ClaimRequest.count).to eq 1
      expect(DepartmentRating.count).to eq 1

      location.destroy
      expect(Location.count).to eq 0
      expect(Rating.count).to eq 0
      expect(Question.count).to eq 0
      expect(Answer.count).to eq 0
      expect(Vote.count).to eq 0
      expect(ClaimRequest.count).to eq 0
      expect(DepartmentRating.count).to eq 0
    end
  end

  describe "slugs" do
    it 'doesnt allow blank slugs' do
      location2 = create(:location, name: "some slug", slug: nil)
      expect(location2.slug).to_not be_blank
    end

    it 'doesnt dup' do
      location2 = create(:location, name: "some slug", slug: nil)
      location3 = create(:location, name: "some slug", slug: nil)
      expect(location2.slug).to_not be_blank
      expect(location3.slug).to_not be_blank
      expect(location2.slug).to_not eq location3.slug
      expect(location3.slug).to eq "some-slug-2"
      location4 = create(:location, name: "some slug", slug: nil)
      expect(location4.slug).to eq "some-slug-3"
    end
  end

  describe "#in_state_ranking" do
    let!(:location2){ create(:location, state: "IL", overall: 3.2, country: "USA") }
    let!(:location3){ create(:location, state: "GA", city: "Chicago", overall: 3.8, country: "USA") }

    #it 'correct rankings for locations' do
    #  location.update_rankings
    #  expect(location.in_state_ranking).to eq 1
    #  expect(location2.in_state_ranking).to eq 2
    #end

    describe "#in_city_ranking" do
      let!(:location4){ create(:location, state: "IL", city: "Chicago", overall: 4.1) }

      it 'returns correct rankings for city' do
        location.update_rankings
        expect(location.reload.in_city_ranking).to eq 2
        expect(location4.reload.in_city_ranking).to eq 1
      end
    end

    describe "#in_country_ranking" do
      # only for non hospitals
      let!(:location4){ create(:location, state: "IL", city: "Chicago", overall: 4.1, country: "CAN") }

      before do
        allow_any_instance_of(Location).to receive(:hospital?).and_return(false)
      end

      it 'returns correct rankings for city' do
        location.update_rankings
        location4.update_rankings
        expect(location.reload.in_country_ranking).to eq 2
        expect(location4.reload.in_country_ranking).to eq 1
      end
    end
  end

  describe "#update_overall" do
    let(:user){ create(:user) }
    let!(:rating){ create(:rating, user: user, location: location, overall_experience: 3) }
    let(:user2){ create(:user, email: 'user2@u.com') }
    let!(:rating2){ create(:rating, user: user2, location: location, overall_experience: 4) }


    it 'sets the overall to 3.5' do
      location.update_overall
      expect(location.overall).to eq 3.5
    end

    #it 'calls update rankings' do
    #  expect(location).to receive(:update_rankings)
    #  location.update_overall
    #end
  end

  describe "#recently_reviewed" do
    let!(:user1){ create(:user, email: 'user1@u.com') }
    let!(:user2){ create(:user, email: 'user2@u.com') }
    let!(:user3){ create(:user, email: 'user3@u.com') }

    let!(:location2){ create(:location, name: "Second Location",
                             location_type: location_type, state: "IL",
                             city: "Chicago", overall: 3.5, country: "USA") }
    let!(:rating1){ create(:rating, user: user1, location: location, recommend: "Maybe") }
    let!(:rating2){ create(:rating, user: user2, location: location, recommend: "Maybe") }
    let!(:rating3){ create(:rating, user: user3, location: location2, recommend: "Maybe") }

    it 'only returns 2 locations' do
      expect(Location.by_type("Hospital").recently_reviewed.to_a.size).to eq 2
    end

    it 'sorts them in DESC order' do
      rating3.created_at = DateTime.now - 5.days
      rating3.save
      expect(Location.by_type("Hospital").recently_reviewed.first.name).to eq location.name
    end

    context 'getting the right number on the hompage' do
      let(:location3){ create(:location, name: "Third Location",
                               location_type: location_type, state: "IL",
                               city: "Monroe", overall: 3.5, country_code: "IND") }
      let!(:rating4){ create(:rating, user: user1, location: location3, recommend: "Maybe") }

      let(:location4){ create(:location, name: "Forth Location",
                               location_type: location_type, state: "IL",
                               city: "Monroe", overall: 3.5, country_code: "USA") }
      let!(:rating5){ create(:rating, user: user1, location: location4, recommend: "Maybe") }

      it 'returns 2 from us' do
        result = Location.by_type("Hospital").recently_reviewed_for_country("USA", 2)
        expect(result.to_a.size).to eq 2
      end

      it 'returns 1 from India' do
        result = Location.by_type("Hospital").recently_reviewed_for_country("IND", 2)
        expect(result.to_a.size).to eq 1
      end

      context 'other than us and india' do
        let(:location5){ create(:location, name: "Fifth Location",
                                 location_type: location_type, state: "IL",
                                 city: "Monroe", overall: 3.5, country: "CAN") }
        let!(:rating6){ create(:rating, user: user1, location: location5, recommend: "Maybe") }

        it 'returns non india and us' do
          result = Location.by_type("Hospital").recently_reviewed_not_for_country(["USA", "IND" ], 10)
          expect(result.to_a.size).to eq 1
          expect(result.first).to eq location5
        end
      end

    end

  end
end
