require 'rails_helper'

RSpec.describe User, type: :model do
  describe "validations" do
    let!(:user){ create(:user, username: "aaa") }
    it 'cannot allow usernames with the same name' do
      expect(build(:user, username: 'aaa').valid?).to_not be true
    end
  end

  describe "#set_username" do
    let!(:user){ create(:user, username: "aaa") }
    it 'creates the record' do
      expect {
        create(:user, email: 'aaa@bbb.com')
      }.to change(User, :count).by 1
    end

    it 'sets the username to aaa1' do
      user2 = create(:user, email: 'aaa@bbb.com', username: nil)
      expect(user2.reload.username).to match(/aaa\d+/)
    end

    it 'only gets the first 3 letters' do
      user2 = create(:user, email: 'aaabbb123@bbb.com', username: nil)
      expect(user2.reload.username).to match(/aaa\d+/)
    end
  end
end
