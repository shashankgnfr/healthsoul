require 'rails_helper'
require 'sucker_punch/testing/inline'

RSpec.describe DoctorReview, type: :model do
  let(:doctor) { create(:doctor) }
  let(:user) { create(:user) }
  let(:comment) {
    "one two three four five
    one two three four five
    one two three four five
    one two three four five"
  }
  let!(:review) { create(:doctor_review, use_date: "02-2017", doctor: doctor, user: user, overall_experience: 2.3, comment: comment, status: 'visible') }

  describe 'relationships' do
    it 'doctor has many reviews' do
      expect(doctor.reviews).to include(review)
    end

    it 'a review has a doctor' do
      expect(review.doctor).to eq doctor
    end
  end

  describe "updating overall" do
    it 'updates the overall for doctor' do
      expect(doctor.reload.overall).to eq 2.3
    end

    it 'updated the user' do
      expect(user.total_reviews).to eq 1
    end
  end

  describe "use date" do
    it 'correctly converts the date' do
      expect(review.use_date).to eq Date.new(2017,02,01)
    end

    it 'can resave' do
      expect(review.use_date).to eq Date.new(2017,02,01)
      r = DoctorReview.first
      r.save
      expect(review).to be_valid
    end

    it 'can update other fields' do
      expect(review.use_date).to eq Date.new(2017,02,01)
      review.update(overall_experience: 2.5)
      expect(review.reload.use_date).to eq Date.new(2017,02,01)
    end

    it 'can resave with new date' do
      expect(review.use_date).to eq Date.new(2017,02,01)
      review.update(use_date: "03-2017")
      expect(review.reload.use_date).to eq Date.new(2017,03,01)
    end
  end

  describe '.where' do
    it 'returns the scope' do
      expect(doctor.ratings.send(:inpatient)).to include review
    end
  end
end
