require "rails_helper"

RSpec.describe HdetailsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/hdetails").to route_to("hdetails#index")
    end

    it "routes to #new" do
      expect(:get => "/hdetails/new").to route_to("hdetails#new")
    end

    it "routes to #show" do
      expect(:get => "/hdetails/1").to route_to("hdetails#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/hdetails/1/edit").to route_to("hdetails#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/hdetails").to route_to("hdetails#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/hdetails/1").to route_to("hdetails#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/hdetails/1").to route_to("hdetails#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/hdetails/1").to route_to("hdetails#destroy", :id => "1")
    end
  end
end
