require "rails_helper"

RSpec.describe HsdetailsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/hsdetails").to route_to("hsdetails#index")
    end

    it "routes to #new" do
      expect(:get => "/hsdetails/new").to route_to("hsdetails#new")
    end

    it "routes to #show" do
      expect(:get => "/hsdetails/1").to route_to("hsdetails#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/hsdetails/1/edit").to route_to("hsdetails#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/hsdetails").to route_to("hsdetails#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/hsdetails/1").to route_to("hsdetails#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/hsdetails/1").to route_to("hsdetails#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/hsdetails/1").to route_to("hsdetails#destroy", :id => "1")
    end
  end
end
