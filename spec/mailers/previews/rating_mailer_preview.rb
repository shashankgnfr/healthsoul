# Preview all emails at http://localhost:3000/rails/mailers/rating_mailer
class RatingMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/rating_mailer/confirmation
  def confirmation
    user = User.new
    location = Location.last
    rating = Rating.new(location: location, user: user)
    RatingMailer.confirmation(rating)
  end

  def notify_manager
    user = User.new
    location = Location.last
    rating = Rating.new(location: location, user: user)
    RatingMailer.notify_manager(rating)
  end

  def notify_doctor
    user = User.new
    doctor = Doctor.find('dr-stephanie-tarracciano-2')
    rating = DoctorReview.new(doctor: doctor, user: user)
    manager = User.find_by(email: 'jess@brownwebdesign.com')
    RatingMailer.notify_doctor(rating)
  end

end
