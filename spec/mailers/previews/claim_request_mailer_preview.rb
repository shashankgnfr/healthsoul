# Preview all emails at http://localhost:3000/rails/mailers/claim_request_mailer
class ClaimRequestMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/claim_request_mailer/notify
  def notify
    @claim_request = ClaimRequest.last
    ClaimRequestMailer.notify(@claim_request)
  end

end
