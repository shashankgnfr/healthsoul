# Preview all emails at http://localhost:3000/rails/mailers/appt_mailer
class ApptMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/appt_mailer/notify
  def notify
    ApptMailerMailer.notify
  end

end
