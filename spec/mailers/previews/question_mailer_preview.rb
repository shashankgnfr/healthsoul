# Preview all emails at http://localhost:3000/rails/mailers/question_mailer
class QuestionMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/question_mailer/notify_manager
  def notify_manager
    question = Question.last
    QuestionMailer.notify_manager(question)
  end

end
