FactoryGirl.define do
  factory :doctor_organization do
    name "MyString"
    address "MyString"
    city "MyString"
    state "MyString"
    zip "MyString"
    phone "MyString"
    country "USA"
    country_code "USA"
  end
end
