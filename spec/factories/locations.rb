FactoryGirl.define do
  factory :location do
    name "Test Location"
    country_code "USA"
  end
end
