FactoryGirl.define do
  factory :faq_answer do
    body "MyText"
    question nil
    user nil
    approved false
  end
end
