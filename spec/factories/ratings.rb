FactoryGirl.define do
  factory :rating do
    use_date Date.today + 2.days
    overall_experience 3
    comment "one two three four five one two three four five one two three four five one two three four five"
    reason 1
  end
end
