FactoryGirl.define do
  factory :faq do
    body "MyText"
    user nil
    country "MyString"
    approved false
  end
end
