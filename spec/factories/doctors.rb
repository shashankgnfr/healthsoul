FactoryGirl.define do
  factory :doctor do
    name "Dr Jess"
    gender 1
    graduation 1998
    npi "10233"
    medicare false
  end
end
