FactoryGirl.define do
  factory :country do
    alpha2 "MyString"
    alpha3 "MyString"
    numeric "MyString"
    fips "MyString"
    name "MyString"
    capital "MyString"
    areakm "MyString"
    pop "MyString"
    continent_code "MyString"
    continent "MyString"
  end
end
