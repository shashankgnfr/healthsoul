FactoryGirl.define do
  factory :blog do
    title "MyString"
    content "MyText"
    user nil
    featured_image "MyString"
    published_at "2017-11-12"
  end
end
