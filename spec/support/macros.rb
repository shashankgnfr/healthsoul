module Macros
  def create_user
    let(:user) { create(:user) }
    let!(:country) { create(:country, alpha3: "USA")}
  end

  def make_user_admin
    before { user.add_role(:admin) }
  end

  def sign_in_admin
    before do
      visit new_user_session_path
      fill_in "Email", with: user.email
      fill_in "Password", with: user.password
      click_on "Log in"
    end
  end

  def sign_in_user
    sign_in_admin
  end

  def add_locations
    let!(:location_type) { create(:location_type, name: "Hospital") }
    let!(:location) { create(:location, location_type: location_type, country: "USA", name: "Brown Hospital", hwaid: "HWA123") }
  end

  def setup_department_ratings

    let!(:user1){ create(:user, email: 'user1@u.com') }
    let!(:user2){ create(:user, email: 'user2@u.com') }
    let!(:user3){ create(:user, email: 'user3@u.com') }
    let!(:location_type) { create(:location_type, name: "Hospital") }

    let!(:location){ create(:location, name: "First Location", location_type: location_type, state: "IL", city: "Chicago", overall: 3.5, country: "USA") }

    let!(:rating1){ create(:rating, user: user1, overall_experience: 3, reason: Rating::Reasons["Surgery"], location: location, recommend: "Maybe") }
    let!(:rating2){ create(:rating, user: user2, overall_experience: 5, reason: Rating::Reasons["Surgery"], location: location, recommend: "Maybe") }
    let!(:rating3){ create(:rating, user: user3, overall_experience: 2, reason: Rating::Reasons["Eye Care"], location: location, recommend: "Maybe") }
    let!(:rating4){ create(:rating, user: user3, overall_experience: 4, reason: Rating::Reasons["Eye Care"], location: location, recommend: "Maybe") }

    # factory girl does not directly call save
    before { Rating.all.map(&:save) }
  end

  def setup_search_by_department
    let!(:user1){ create(:user, email: 'user1@u.com') }
    let!(:user2){ create(:user, email: 'user2@u.com') }
    let!(:location_type) { create(:location_type, name: "Hospital") }

    let!(:location1){ create(:location, name: "First Location", location_type: location_type, state: "IL", city: "Chicago", overall: 3.5, country: "USA") }
    let!(:location2){ create(:location, name: "Secnd Location", location_type: location_type, state: "IL", city: "Chicago", overall: 3.5, country: "USA") }
    let!(:location3){ create(:location, name: "3rd Location", location_type: location_type, state: "IL", city: "Chicago", overall: 3.5, country: "USA") }

    let!(:rating1){ create(:rating, user: user1, overall_experience: 3, reason: Rating::Reasons["Surgery"].to_s, location: location1, recommend: "Maybe") }
    let!(:rating2){ create(:rating, user: user2, overall_experience: 5, reason: Rating::Reasons["Eye Care"].to_s, location: location1, recommend: "Maybe") }
    let!(:rating3){ create(:rating, user: user2, overall_experience: 2, reason: Rating::Reasons["Surgery"].to_s, location: location2, recommend: "Maybe") }
    let!(:rating4){ create(:rating, user: user2, overall_experience: 2, reason: Rating::Reasons["Eye Care"].to_s, location: location3, recommend: "Maybe") }

    # factory girl does not directly call save
    before { Rating.all.map(&:save) }

  end

  def setup_location_with_all_dependancies
    add_locations
    let!(:user){ create(:user, email: 'user1@u.com') }
    let!(:rating){ create(:rating, user: user, location: location, overall_experience: 3, reason: Rating::Reasons["Surgery"], recommend: "Maybe") }
    let!(:question) { create(:question, questionable: location, user: user) }
    let!(:answer) { create(:answer, question: question, user: user) }
    let!(:vote1) { create(:vote, voteable: question, user: user) }
    let!(:claim_request) { create(:claim_request, email: 'claim@bb.com', user: user, phone: "987897", claimable: location) }
    let!(:vote2) { create(:vote, voteable: answer, user: user) }
  end

  def add_doctor
    let(:doctor) { create(:doctor) }
  end

  def add_doctor_review
    let!(:doctor_review) { create(:doctor_review, doctor: doctor) }
  end

  def add_doctors
    let!(:country) { create(:country, alpha3: 'USA') }
    let!(:doctor1) { create(:doctor, name: "Jess Brown", 
                             country_code: "USA",
                             state: "GA") }
    let!(:doctor2) { create(:doctor, name: "Patty", 
                              country_code: "CAN",
                              state: "Alberta") }
  end

  def add_doctor_speciality
    let(:spec1) { create(:practice_speciality, name: "Spec 1")}
    let(:spec2) { create(:practice_speciality, name: "Spec 2")}
    before do
      doctor1.practice_specialities << spec1
      doctor2.practice_specialities << spec2
    end
  end

end

RSpec.configure do |config|
  config.extend(Macros)
end

