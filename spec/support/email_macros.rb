module EmailMacros
  def reset_email!
    ActionMailer::Base.deliveries = []
  end

  def last_email
    ActionMailer::Base.deliveries.last
  end

  def email_count
    ActionMailer::Base.deliveries.count
  end

  def emails
    ActionMailer::Base.deliveries
  end
end

RSpec.configure do |config|
  config.include EmailMacros
end
