xml.instruct! :xml, :version => "1.0"
xml.rss :version => "2.0", 'xmlns:dc'=>"http://purl.org/dc/elements/1.1/", 'xmlns:atom'=>"http://www.w3.org/2005/Atom" do
 	xml.channel do
		xml.title "Healthsoul Blog"
		xml.description "Health Education & Tips"
		xml.link blogs_url
		xml.tag!("atom:link", href: "#{blogs_url.to_s}.rss", rel: "self")
		@blogs.each do  |blog|
      		xml.item do
		        xml.title  blog.title
		        xml.tag!("dc:creator", blog.author_name)
		        xml.description blog.meta_description
		        xml.pubDate blog.created_at.to_s(:rfc822)
				xml.link blog_url(blog)
				xml.guid blog_url(blog)
		    end
		end
	end
end