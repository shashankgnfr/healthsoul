json.array! @doctors do |doctor|
  json.id doctor.id
  json.text ([  doctor.name.to_s, doctor.city.to_s, doctor.state.to_s  ]).join(" - ")
end
