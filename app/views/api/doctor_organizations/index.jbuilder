json.array! @organizations do |organization|
  json.id organization.id
  json.text ([  organization.name.to_s, organization.city.to_s, organization.state.to_s  ]).join(" - ")
end

