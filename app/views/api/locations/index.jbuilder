json.array! @locations do |location|
  json.id location.id
  json.text ([  location.name.to_s, location.location_type&.name&.to_s, location.hwaid.to_s, location.city.to_s, location.state.to_s  ]).join(" - ")
 end