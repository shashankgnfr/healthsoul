xml.instruct!

xml.sitemapindex('xmlns:xsi'=>"http://www.w3.org/2001/XMLSchema-instance", 'xsi:schemaLocation'=>"http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd", 'xmlns'=>"http://www.sitemaps.org/schemas/sitemap/0.9") do
  xml.sitemap do
    xml.loc "#{main_sitemap_url}.xml"
    xml.lastmod DateTime.now
  end

  xml.sitemap do
    xml.loc "#{categories_sitemap_url}.xml"
    xml.lastmod DateTime.now
  end

  (@indian_docs_with_reviews/40000.to_f).ceil.times do |n|
    xml.sitemap do
      xml.loc "#{doc_sitemap_with_data_url('India', n+1)}.xml"
      xml.lastmod DateTime.now
    end
  end

  (@indian_docs_without_reviews/40000.to_f).ceil.times do |n|
    xml.sitemap do
      xml.loc "#{doc_sitemap_without_data_url('India', n+1)}.xml"
      xml.lastmod DateTime.now
    end
  end

  (@us_docs_with_reviews/40000.to_f).ceil.times do |n|
    xml.sitemap do
      xml.loc "#{doc_sitemap_with_data_url('USA', n+1)}.xml"
      xml.lastmod DateTime.now
    end
  end
  (@us_docs_without_reviews/40000.to_f).ceil.times do |n|
    xml.sitemap do
      xml.loc "#{doc_sitemap_without_data_url('USA', n+1)}.xml"
      xml.lastmod DateTime.now
    end
  end


  @countries_of_locations_with_reviews.each do |lt, countries|
    countries.each do |c|
      xml.sitemap do
        xml.loc "#{loc_sitemap_with_data_url(lt.parameterize.pluralize, c.parameterize)}.xml"
        xml.lastmod DateTime.now
      end
    end
  end

  @countries_of_locations_without_reviews.each do |lt, countries|
    countries.each do |c|
      xml.sitemap do
        xml.loc "#{loc_sitemap_without_data_url(lt.parameterize.pluralize, c.parameterize)}.xml"
        xml.lastmod DateTime.now
      end
    end
  end
end