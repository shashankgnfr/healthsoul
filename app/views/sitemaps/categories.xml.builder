xml.instruct!
xml.urlset('xmlns'=>"http://www.sitemaps.org/schemas/sitemap/0.9", 'xmlns:xsi'=>"http://www.w3.org/2001/XMLSchema-instance", 'xsi:schemaLocation' => "http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd") do
  @url_props.each do |props|
    xml.url do
      xml.loc props[:path] if props[:path].present?
      xml.changefreq props[:changefreq] if props[:changefreq].present?
      xml.priority props[:priority] if props[:priority].present?
      xml.lastmod Date.today
    end
  end
end