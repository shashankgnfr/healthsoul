xml.instruct!
xml.urlset('xmlns'=>"http://www.sitemaps.org/schemas/sitemap/0.9", 'xmlns:xsi'=>"http://www.w3.org/2001/XMLSchema-instance", 'xsi:schemaLocation' => "http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd") do
  @locations_data.each do |data|
    xml.url do
      xml.loc data[:path] if data[:path].present?
      xml.changefreq data[:changefreq] if data[:changefreq].present?
      xml.priority data[:priority] if data[:priority].present?
      xml.lastmod Date.today
    end
  end
end