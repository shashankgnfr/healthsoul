class ContactMailer < ApplicationMailer

  def submit(contact)
    @contact = contact

    mail from: @contact.email
  end

  def request_notification(story)
    @story = story
    # Note uploaded_file is of class ActionDispatch::Http::UploadedFile
    attachments[story.word_document.file.filename] = File.read(story.word_document.file.path) unless story.word_document.blank?
    attachments[story.image.file.filename] = File.read(story.image.file.path) unless story.image.blank?
    attachments[story.video.file.filename] = File.read(story.video.file.path) unless story.video.blank?
    mail(from: @story.email,
         subject: I18n.t('contact_mailer.success'),
         sent_on: Time.now)
  end

  def request_acknowledgement(story)
    @story = story
    mail(to: @story.email,
         subject: I18n.t('contact_mailer.acknowledgement', type: "Patient story"))
  end
end
