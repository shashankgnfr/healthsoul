class RatingMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.rating_mailer.confirmation.subject
  #
  def confirmation(rating)
    @rating = rating

    if @rating.user.present?
      mail to: @rating.user.email
    end
  end

  def notify_manager(rating)
    @location = rating.location
    @rating = rating
    @location.managers.each do |manager|
      mail to: manager.email, subject: t("rating_mailer.notify_manager.subject", name: @location.name)
    end
  end

  def notify_doctor(rating)
    @doctor = rating.doctor
    @rating = rating
    @doctor.managers.each do |manager|
      mail to: manager.email, subject: t("rating_mailer.notify_manager.subject", name: @doctor.name)
    end
  end

  def verify_rating(rating)
    @rating = rating
    @source = rating.class.to_s == 'DoctorReview' ? rating.doctor : rating.location
    mail to: ::REVIEWER_MAIL, subject: t("rating_mailer.notify_manager.subject", name: @source.name)
  end
end
