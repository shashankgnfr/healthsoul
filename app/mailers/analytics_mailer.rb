class AnalyticsMailer < ApplicationMailer
  helper :application

  def analytics_report(obj, params)
    @obj = obj
    @analytics_report = params[:analytic_chart_href]

    attachments['chart.png'] = File.read(@analytics_report)

    mail(to: params[:email],
        cc: @obj.email,
        subject: "Analytics for #{obj.name}")
  end
end