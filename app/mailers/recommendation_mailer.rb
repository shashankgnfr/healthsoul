class RecommendationMailer < ApplicationMailer

  def notify(request)
    @request = request
    mail subject: "New #{request.source_type} has been recommended to add."
  end

end