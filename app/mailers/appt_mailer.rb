class ApptMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.appt_mailer.notify.subject
  #
	def notify(appt, just_healthsoul=false)
		@appointment = appt
		@user = @appointment.user
		if just_healthsoul
			mail to: "info@healthsoul.com"
		else
			mail to: appt.provider.email, bcc: "info@healthsoul.com"
		end
	end

	def notify_of_cancellation(appt, just_healthsoul=false)
		@appointment = appt
		@user = @appointment.user
		if just_healthsoul
			mail to: "info@healthsoul.com"
		else
			mail to: appt.provider.email, bcc: "info@healthsoul.com"
		end
	end

	def notify_of_modification(appt, just_healthsoul=false)
		@appointment = appt
		@user = @appointment.user
		if just_healthsoul
			mail to: "info@healthsoul.com"
		else
			mail to: appt.provider.email, bcc: "info@healthsoul.com"
		end
	end
end
