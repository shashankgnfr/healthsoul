class ClaimRequestMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.claim_request_mailer.notify.subject
  #
  def notify(claim_request)
    @claim_request = claim_request

    mail to: "info@healthsoul.com"
  end
end
