class EventMailer < ApplicationMailer

  def request_notification(event_member)
    @event_member = event_member
    mail(from: @event_member.email,
         subject: I18n.t('event_mailer.notification'),
         sent_on: Time.now)
  end

  def request_acknowledgement(event_member)
    @event_member = event_member
    mail(
      to: @event_member.email,
      subject: I18n.t('event_mailer.acknowledgement')
    )
  end
end
