class QuestionMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.question_mailer.notify_manager.subject
  #
  def notify_manager(question)
    @question = question
    @location = question.location
    @location.managers.each do |manager|
      mail to: manager.email, subject: t("question_mailer.notify_manager.subject", name: @location.name)
    end
  end
end
