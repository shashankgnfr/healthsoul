class ApplicationMailer < ActionMailer::Base
  helper :location
  helper :doctor

  default from: ::MAILER_FROM
  default to: ::MAILER_TO

  layout 'mailer'
end
