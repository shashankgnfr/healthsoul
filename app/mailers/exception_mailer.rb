class ExceptionMailer < ApplicationMailer

  def exception_notification(exception, source_url)
    @exception = exception
    @source_url = source_url
    mail(to: ::EXCEPTION_MAILER,
         subject: I18n.t('exception_mailer.subject'))
  end

  def runtime_error_mail(exception, env, parameters)
    @exception = exception
    @env = env
    @params = parameters
    mail(to: ::EXCEPTION_MAILER,
         subject: 'Unhandled exception on HealthSoul')
  end 
end
