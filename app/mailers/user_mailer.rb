class UserMailer < ApplicationMailer
	layout false
	def notify_info_of_new_subscriber email
		@email = email
		mail(to: "info@healthsoul.com", subject: "#{@email} is interested in a subscription")
	end

	def send_calculator_result email, content, email_subject
		@content = content
		mail(to: email, subject: email_subject)
	end

	def send_welcome_mailer_to_new_user user
		['healthsoul-e-mailer-logo.png', 'healthsoul-build-img.jpg', 'step-1.png',
			'step-2.png', 'step-3.png', 'step-4.png', 'healthsoul-referal-img.jpg', 'healthsoul-e-mailer-logo.png', 'healthsoul-build-img.jpg', 'row-img06.png'].each do |img|
				attachments.inline[img] = File.read("#{Rails.root}/app/assets/images/mailer/#{img}")
		end
		@user = user
		mail(to: user.email, subject: 'Welcome to Healthsoul')
	end

	def notify_info_of_interested_provider(provider_contact_params)
		@provider_contact_params = provider_contact_params
		mail(to: "info@healthsoul.com", subject: "#{@provider_contact_params[:email]} would like to be signed up as a provider")
	end

	def confirm_interested_provider email
		attachments.inline['healthsoul-e-mailer-logo.png'] = File.read("#{Rails.root}/app/assets/images/mailer/healthsoul-e-mailer-logo.png")
		mail(to: email, subject: "Your request is being processed.")
	end

	def notify_user_of_comment comment
		@comment_text = comment.body
		mail(to: comment.commentable.user.email, subject: "#{comment.user.display_name} has responded to your review on Healthsoul.com")
      end

	def weekly_digest(user, blogs)
		['healthsoul-e-mailer-logo.png', '05.jpg'].each do |img|
		  attachments.inline[img] = File.read("#{Rails.root}/app/assets/images/mailer/#{img}")
		end
		@user = user
  	@blogs = blogs
  	mail(to: user.email, subject: "Review your doctor and get Amazon gift card")
	end

	def send_welcome_email_to_new_provider email
		['step-1.png', 'step-2.png', 'step-3.png','step-4.png','05.png', 'healthsoul-build-img.jpg', 'healthsoul-e-mailer-logo.png', '07.png'].each do |img|
		  attachments.inline[img] = File.read("#{Rails.root}/app/assets/images/mailer/#{img}")
		end
		@email = email
		mail(to: email, subject: 'Welcome to Healthsoul')
	end

	def provider_digest(provider)
		['healthsoul-e-mailer-logo.png', 'img-top1.jpg', 'step-1.png', 'step-2.png',
			'step-3.png'].each do |img|
				attachments.inline[img] = File.read("#{Rails.root}/app/assets/images/mailer/#{img}")
		end
		@provider = provider
		mail(to: provider.email, subject: "Healthsoul")
	end

end