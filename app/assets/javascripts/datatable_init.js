document.addEventListener("turbolinks:load", function() {
  var table = $("[data-datatable]").DataTable();
  $("[data-nosort]").DataTable({
     retrieve: true,
    "order": [],
    "paging": false,
    "info": false
  });

  $("[data-nosearch]").DataTable({
     retrieve: true,
    'searching': false,
    "order": [],
    "paging": false,
    "info": false
  });
  // var loopThroughTimeSpans = function(){
  //   $('.moment-time').each(function() {
  //     this.textContent = moment(this.textContent).format("YYYY-MM-DD hh:mm A");
  //   });
  // }
  // table.on( 'draw', function () {
  //   loopThroughTimeSpans();
  // } );
  // loopThroughTimeSpans();
})