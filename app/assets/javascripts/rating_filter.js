document.addEventListener("turbolinks:load", function() {
  $reviews = $("[data-review-type]");
  if($reviews.length > 0) {
    $("select[data-review-filter]").change(function(){
      filterReviews($(this).val());
      //toggleDepartments($(this));
    })
  }
})

function toggleDepartments(el) {

  if ($filterOn.length > 0) {
    $(".department-ratings [data-reason]").css("opacity", .2);
    console.log($filterOn)
    $.each($filterOn, function() {
      id = this
      $('.department-ratings [data-reason="'+id+'"]').css("opacity", 1);
    })
  } else {
    $(".department-ratings [data-reason]").css("opacity", 1);
  }
}

function filterReviews(value) {
  if (value != ''){
    selector = "[data-review-type='" + value +"']"
    $filteredReviews = $(selector);
    $reviews.hide();
    $filteredReviews.show();
  }else{
    $reviews.show();
  } 
}

function toggleButton(button) {
  id = button.data("reason");
  if(button.hasClass("hollow")) {
    button.removeClass("hollow")
    $filterOn.push(id);
    filterReviews();
  } else {
    button.addClass("hollow")
    index = $filterOn.indexOf(id)
    $filterOn.splice(index, 1)
    filterReviews();
  }
}
