document.addEventListener("turbolinks:load", function() {

  $(".enter-rating").each(function() {
    t = $(this)
    id = t.attr("id")
    stars = $("<div class='stars " + id + "' />")
    stars.insertAfter(this)
    if(t.hasClass("overall-experience")) {
      na = "";
    } else {
      na = $("<span class='label na " + id + "' />")
      na.html("N/A")
    }
    setupEnterRating(id, t.val());
      stars.append(na)
  })

  $(".stars span.na").click(function() {
    t = $(this)
    parent = t.parents(".stars")
    if(t.hasClass("na-active")){
      t.removeClass("na-active")
      parent.removeClass("stars-inactive")
    } else {
      input = parent.siblings(".enter-rating")
      parent.raty('cancel', true);
      t.addClass("na-active")
      parent.addClass("stars-inactive")
    }
  })

  initDisplayStars();
})

function initDisplayStars() {
  $starsDisplayed = $("i.star-on-png").length > 0
  if(!$starsDisplayed) {
    $("[data-stars]").each(function() {
      stars = $(this).data("stars")
      if(stars > 0) {
        displayStars($(this), stars)
      } else {
        $(this).html("N/A")
      }
    })
  }
}

function thisMonth() {
  var now = new Date();
  return now
}

function twoYearsAgo() {
  var now = new Date();
  var twoAgo = new Date();
  twoAgo.setYear(now.getFullYear() - 2);
  return twoAgo;
}

function displayStars(element, score) {
  element.raty({
  readOnly : true,
  precision: true,
  half: true,
  score    : score,
  starType: 'i'
});

}

function setupEnterRating(id, score) {
  $(".stars." + id).raty({
    half       : true,
    target     : "#" + id,
    targetKeep : true,
    hints       : ["Very poor", "Poor", "Fair", "Good", "Excellent"],
    targetType : 'number',
    score: score,
    starType: 'i'
  })
}
