$(document).ready(function() {
  initSelect2();
})

function initSelect2() {
  $(".chosen-select-org").select2({
    width: '90%',
    ajax: {
      url: "/api/doctor_organizations",
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return {
          q: params.term, // search term
        };
      },
      processResults: function (data, params) {
        return {
          results: data
        };
      },
      cache: true
    },
  });
}

document.addEventListener("turbolinks:load", function() {
  if (!$(".select2").hasClass("select2-hidden-accessible")){
    $( ".select2" ).select2({width: "100%"});
  }


  // Initilize the hospital search select2.
  // Note: this has to be done after the bulk initliziation above.
  $(".select2-ajax").select2({
    minimumInputLength: 5,
    allowClear: true,
    ajax: {
      url: "/api/locations",
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return {
          q: params.term, // search term
        };
      },
      processResults: function (data, params) {
        return {
          results: data
        };
      },
      cache: true
    },
  });
  //
  
  function search_blogs() {
    var tag = $('#tag').val(); 
    if(tag != ''){
      tag = "/tag/" + encodeURIComponent(tag);
    }else{
      tag = '/tag/NA'
    }
    var searchValue = $('#search-input').val();
    if(searchValue.length > 0){
      searchValue = "?q=" + searchValue
    }
    window.location = "/blog" + tag + searchValue;
  }

  $("#search-blogs").click(search_blogs);
  $("#tag").change(search_blogs);
});
