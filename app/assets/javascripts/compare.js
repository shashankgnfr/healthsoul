$(document).ready(function() {
  compareForm = {
    button: function() {
      return $("a.compare-button")
    },
    boxes: function() {
      return $("input.compare-checkbox")
    },
    boxesChecked: function() {
      return $("input.compare-checkbox:checked")
    },
    boxesCheckedCount: function() {
      return this.boxesChecked().length
    },
    disableButton: function() {
      this.button().removeClass('show')
    },
    showButton: function() {
      this.updateCount();
      this.button().addClass('show')
    },
    checkedIds: function() {
      id = []
      this.boxesChecked().each(function() {
        id.push("locations[]=" + $(this).val())
      })
      return id.join('&');
    },
    updateCount: function() {
      this.button().find('span').text(this.boxesCheckedCount())
      console.log(this.checkedIds())
      this.button().attr('href', "/locations/compare?" + this.checkedIds())
    },
    validate: function() {
      if (this.boxesCheckedCount() == 0){
        this.disableButton();
        return true;
      }
      else if (this.boxesCheckedCount() > 4){
        alert("Please only choose up to 4 locations.")
        return false;
      } else {
        // show button
        this.showButton()
        return true;
      }
    }
  }

  $("body").on("click", compareForm.boxes(), function(){
    if(compareForm.validate()) {
      return true;
    } else {
      return false;
    }
  })

})

