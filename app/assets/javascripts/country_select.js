document.addEventListener("turbolinks:load", function() {
  $(".home-checkboxes input").click(function(){
    $('.home-checkboxes input').prop("checked", false)
    box = $(this)
    box.prop("checked", true)
    $("form.search").attr("action", box.data("path"))
    if(box.data("path")=="/hospitals"){
    } else {
      $("select.country").hide().attr("name", '')
      $("input.near").hide()
      $(".country." + box.attr("class")).show().attr("name", "search[co]")
    }
  })
})

function initCountrySelect() {
  $("[data-country-name], [data-cancel]").click(function() {
    countries = $(this).parents(".hwacountry-select").find(".countries")
    if(countries.is(":visible")) {
      countries.hide();
    } else {
      countries.show();
    }
    return false;
  })

  $("form .countries [data-country]").click(function(e) {
    e.preventDefault;
    value = $(this).text();
    country = $(this).data("country")
    code = $(this).data("country-code")
    parent = $(this).parents(".hwacountry-select")
    countries = parent.find(".countries")
    $("[data-country-field]").val(country)
    parent.find("[data-country-name] span").text($(this).text())
    parent.find("[data-country-name] .flag").attr("class", "flag " + code)
    countries.hide();
    return false;
  })

  $(".menu-country-select .countries [data-country]").click(function(e) {
    e.preventDefault;
    value = $(this).text();
    country = $(this).data("country")
    code = $(this).data("country-code")
    parent = $(this).parents(".hwacountry-select")
    countries = parent.find(".countries")
    //parent.find("[data-country-name] span").text($(this).text())
    parent.find(".flag").attr("class", "flag " + code)
    countries.hide();   
    if (jQuery.isEmptyObject(window.location.pathname.split("/").filter(Boolean))){
      window.location = "/home/" + country;
    } 
    else{
      window.location.href.replace(window.location.search, '');
      ctrl = window.location.pathname.split("/").filter(Boolean)[0];
      if (['doctors', 'best-doctor-ratings', 'best_doctors'].includes(ctrl)){
        ctrl = 'best-doctor-ratings/specialist';
        url = window.location.origin + "/" + ctrl + "/" + country ; 
        window.location.href = url;   
        window.location = url;  
      }else if (['hospitals', 'best-hospital-ratings', 'best_hospitals'].includes(ctrl)){
        ctrl = 'best-hospital-ratings';
        url = window.location.origin + "/" + ctrl + "/" + country ; 
        window.location.href = url;   
        window.location = url;  
      }else if (['health-insurance', 'travel-health-insurance'].includes(ctrl)){
        window.location = "/" + ctrl + "/" + country;
      }else{
        window.location = "/home/" + country;
      } 
    }
    return false;
  })

  $country = $("#country")
  if($country.length > 0) {
    countries = $country.data("countries")
    selectedCountry = $country.data("selected")
    $("#country").countrySelect({
      preferredCountries: ["us", "in"],
      onlyCountries: countries,
    });
    if(selectedCountry != "") {
      $("#country").countrySelect("selectCountry", selectedCountry)
    }
  }
}
