document.addEventListener("turbolinks:load", function() {
  if ($('[data-details-page]').length > 0) { 
    $(".user-care_main_frame-readmore").click(function(){
      $(".user-care_main_frame-hide").show();
      $(".user-care_main_frame-readmore").hide();
    });
    $(".user-care_main_frame-showless-2").click(function(){
      $(".user-care_main_frame-hide").hide();
      $(".user-care_main_frame-readmore").show();
    });
    $(".overview").click(function() {
      $('html, body').animate({
        scrollTop: $("#overview").offset().top
      }, 1000);
    });
    $(".servicereviews").click(function() {
      $('html, body').animate({
        scrollTop: $("#servicereviews").offset().top
      }, 1000);
    });
    $(".departmentreviews").click(function() {
      $('html, body').animate({
        scrollTop: $("#departmentreviews").offset().top
      }, 1000);
    });
    $(".userreviews").click(function() {
      $('html, body').animate({
        scrollTop: $("#userreviews").offset().top
      }, 1000);
    });
    $(".claimhospital").click(function() {
      $('html, body').animate({
        scrollTop: $("#claimhospital").offset().top
      }, 1000);
    });
    $(".qa").click(function() {
      $('html, body').animate({
        scrollTop: $("#qa").offset().top
      }, 1000);
    });
  };
});


function set_label(id){
  if ($('#'+ id).html() == 'More ..'){
    $('#'+ id).html('Less ..');
  }else{
    $('#'+ id).html('More ..');
  }
  
}