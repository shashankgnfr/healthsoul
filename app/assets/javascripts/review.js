document.addEventListener("turbolinks:load", function() {
  $doctor_review_input = $('.doctor_review_where input')
  show_hide_review($doctor_review_input, '.doctor_review_where')

  $practice_review_input = $('.practice_review_where input')
  show_hide_review($practice_review_input, '.practice_review_where')
  
  

  function show_hide_review(id, cls_name){
    if(id.length > 0) {
      id.click(function() {
        $(".where").hide();
        val = "." + $(this).val()
        $(val).show()
      })

      var val = $(cls_name + ' input:checked').val()
      $("." + val).show()
    }
  }
})
