$("[data-stars]").each(function() {
  stars = $(this).data("stars")
  if(stars > 0) {
    displayStars($(this), stars)
  } else {
    $(this).html("N/A")
  }
})

function displayStars(element, score) {
  element.raty({
  readOnly : true,
  precision: true,
  half: true,
  score    : score,
  starType: 'i'
});
}

function share_blog(blog_id, social_platform) {
  $.ajax({
    url: "/blog/" + blog_id + "/share_blog/" + social_platform + ".json",
    method: "PATCH"
  }).success(function(data){
    $('.a2a_button_' + social_platform + " .a2a_count span").html(data["shared_count"]);
  })
}


// Take a calculated bmi and generate styled text interpereting it.
function parseBMI(bmi) {
  if(bmi < 18.5) {
    result = "<div style='color: blue;'> Your BMI is <b>"+ bmi + "</b>, which is underweight. </div>"
  }
  else if (bmi < 25) {
    result = "<div style='color: green;'> Your BMI is <b>"+ bmi + "</b>, which is normal weight. </div>"
  }
  else if (bmi < 30) {
    result = "<div style='color: orange;'> Your BMI is <b>"+ bmi + "</b>, which is overweight. </div>"
  }
  else {
    result = "<div style='color: red;'> Your BMI is <b>"+ bmi + "</b>, which is obese. </div>"
  }
  return result
}

$( document ).on('turbolinks:load', function() {

  //toggle phone login / signup when login with click
  $('#phone-login').click(function(){
    $('.login-with-phone-sec').toggle();
  });

  $('#dpMonths').fdatepicker({
    closeButton: true,
    startDate: twoYearsAgo(),
    endDate: thisMonth()
  });

  $('.date-picker').fdatepicker({
    closeIcon: 'X',
    closeButton: true,
    format: 'yyyy-mm-dd',
    disableDblClickSelection: true,
    language: 'vi'
  });

  if ($(".comment-select").length) {
    $('.comment-select').select2({
      tags: true,
      placeholder: ""
    });
  }
  $("#bmi-calculator").click(function() {
    error = ""
    if ($("#pills-imperial").is(".active")) {

      if ($("#weight-pounds").val() && $("#height-inches").val() && $("#height-feet").val()){
        weight = parseInt($("#weight-pounds").val())
        height = 12*parseInt($("#height-feet").val()) + parseInt($("#height-inches").val())
        result_bmi = (weight/(height*height)) * 703;
      }
      else {
        error = "Please enter a weight and height."
      }

    }
    else if ($("#pills-metric").is(".active")) {

      if ($("#weight-kilograms").val() && $("#height-centimeters").val()){
        weight = $("#weight-kilograms").val()
        height = $("#height-centimeters").val()
        if (weight>0 && height>0) {
          result_bmi = (weight/(height*height)) * 10000;
        }
      }
      else {
        error = "Please enter a weight and height."
      }
    }
    if (error.length > 0){
      $("#bmi-result-holder").html("<span> "+ error+ " </span>")
    }
    else if (result_bmi) {
      $("#bmi_table").show();
      $("#bmi-result-holder").html(parseBMI(result_bmi.toFixed(1)))
      $("#email_contents").val(parseBMI(result_bmi.toFixed(1)))
      $("#email_results_form").show()
    }
  });

  $("#waist-hip-calculator").click(function() {
    error = ""

    // Since this is a ratio where units cancel out, we don't really need to seperate into metric and imperial branches, but
    // it makes it easier to avoid someone entering waist circ in inches, and hip circ in centimeters, and then getting the wrong
    // result.
    if ($("#pills-imperial").is(".active")) {

      if ($("#waist-inches").val() && $("#hip-inches").val()){
        waist = parseFloat($("#waist-inches").val())
        hip = parseFloat($("#hip-inches").val())
        if (waist > 0 && hip > 0) {
          result_ratio = waist/hip;
        }
      }
      else {
        error = "Please enter a waist and hip circumference."
      }

    }
    else if ($("#pills-metric").is(".active")) {
      if ($("#waist-centimeters").val() && $("#hip-centimeters").val()){
        waist = parseFloat($("#waist-centimeters").val())
        hip = parseFloat($("#hip-centimeters").val())
        if (waist > 0 && hip > 0) {
          result_ratio = waist/hip;
        }
      }
      else {
        error = "Please enter a waist and hip circumference."
      }
    }

    // If an error was generated at any point, show that instead of a calculated value.
    if (error.length > 0){
      $("#waist-hip-result-holder").html("<div> "+ error+ " </div>")
    }
    else if (result_ratio) {
      $("#waist-hip-result-holder").html("<div> Your waist to hip ratio is "+ result_ratio.toFixed(1) + " </div>")
      $("#email_contents").val("<div> Your waist to hip ratio is "+ result_ratio.toFixed(1) + " </div>")
      $("#email_results_form").show()
    }
  });

  $("#bmr-calculator").click(function() {
    error = ""
    gender = $("input[name='gender']:checked").val();
    activity = $("input[name='activity']:checked").val();
    age = parseInt($('#age-years').val())
    unadjusted_bmr = null
    if ($("#pills-imperial").is(".active")) {
      if ($("#weight-pounds").val() && $("#height-inches").val() && $("#height-feet").val() && gender && age){
        weight = parseInt($("#weight-pounds").val())
        height = 12*parseInt($("#height-feet").val()) + parseInt($("#height-inches").val())
        unadjusted_bmr = (4.536 * weight) + (15.88 * height) - (5 * age)
      }
      else {
        error = "Please enter a value for all fields."
      }

    }
    else if ($("#pills-metric").is(".active")) {

      if ($("#weight-kilograms").val() && $("#height-centimeters").val()  && gender && age){
        weight = $("#weight-kilograms").val()
        height = $("#height-centimeters").val()
        unadjusted_bmr = (10 * weight) + (6.25 * height) - (5 * age)
      }
      else {
        error = "Please enter a value for all fields."
      }
    }
    else {
      error = "Please enter a value for all fields."
    }

    // Adjust the bmr depending on reported gender.
    if (unadjusted_bmr) {
      if (gender == "male" && unadjusted_bmr) {
        result_bmr = unadjusted_bmr + 5
      }
      else if (gender == "female" && unadjusted_bmr) {
        result_bmr = unadjusted_bmr - 161
      }
      else {
        error = "Please enter a value for all fields."
      }


      // Multiply the bmr by a scaler based on reported activity level to get daily caloric intake.
      if (result_bmr) {
        if (activity=="none") {
          dci = result_bmr * 1.2
        }
        else if (activity=="light") {
          dci = result_bmr * 1.375
        }
        else if (activity=="moderate") {
          dci = result_bmr * 1.55
        }
        else if (activity=="heavy") {
          dci = result_bmr * 1.725
        }
        else if (activity=="very_heavy") {
          dci = result_bmr * 1.9
        }
      }
      else {
        error = "Please enter a value for all fields."
      }
    }
    else {
      error = "Please enter a value for all fields."
    }

    // If an error was generated at any point, show that instead of a calculated value.
    if (error.length > 0){
      $("#bmr-result-holder").html("<span> "+ error+ " </span>")
    }
    else if (result_bmr && dci) {
      if ($(this).data("report-calories-first")) {
        $("#bmr-result-holder").html("<span> Your daily recommended caloric intake is <b>" + dci.toFixed(0) + "</b> calories. <br> Your BMR is <b>"+ result_bmr.toFixed(1) + "</b>. </span> ")
        $("#email_contents").val("<span> Your daily recommended caloric intake is <b>" + dci.toFixed(0) + "</b> calories. <br> Your BMR is <b>"+ result_bmr.toFixed(1) + "</b>. </span> ")
        $("#email_results_form").show()

      }
      else {
        $("#bmr-result-holder").html("<span> Your BMR is <b>"+ result_bmr.toFixed(1) + "</b>. <br> Based on your BMR and reported activity level, your daily recommended caloric intake is <b>" + dci.toFixed(0) + "</b> calories. </span> ")
        $("#email_contents").val("<span> Your BMR is <b>"+ result_bmr.toFixed(1) + "</b>. <br> Based on your BMR and reported activity level, your daily recommended caloric intake is <b>" + dci.toFixed(0) + "</b> calories. </span> ")
        $("#email_results_form").show()
      }
    }
  });

  $(".country-code-select").change(function(){
    new_value = $(".country-code-select").val() + $(".ten-digit-number").val()
    $(".hidden_phone_number").val(new_value)
  })
  
  $(".ten-digit-number").change(function(){
    new_value = $(".country-code-select").val() + $(".ten-digit-number").val()
    $(".hidden_phone_number").val(new_value)
  })

  $(window).load(function() {
    function hide_and_disable_email_field(){
      $("#email-fields").hide();
      $("#user_email").val("");
      $("#user_email").prop("disabled", true);
    }

    function hide_and_disable_phone_number_fields(){
      // hides phone fields containner
      $(".phone-number-fields").hide();

      // disable fields within phone fields containner
      $("#user_phone_country_code").prop("disabled", true);
      $("#user_phone_body").prop("disabled", true);

      // resets values for phone fields
      $("#user_phone_country_code").val($("#user_phone_country_code:first").val());
      $("#user_phone_body").val("");
    }

    function show_and_enable_email_address_field() {
      // enable email fields with email fields containner
      $("#user_email").prop("disabled", false);

      // show email address containner
      $("#email-fields").show();
    }

    function show_and_enable_phone_number_fields() {
      // enable fields within phone fields containner
      $("#user_phone_country_code").prop("disabled", false);
      $("#user_phone_body").prop("disabled", false);

      // show phone fields containner
      $(".phone-number-fields").show();
    }

    $("#phone-number-login").bind("click", function (e) {
      e.preventDefault();

      // hides and disables email address fields
      hide_and_disable_email_field();

      // hide phone number button
      $("#phone-number-login").hide();

      // shows and enabled email address field
      show_and_enable_phone_number_fields();

      // show show button
      $("#email-address-login").show();
    });

    $("#email-address-login").bind("click", function (e) {
      e.preventDefault();

      // hides and disables phone number fields
      hide_and_disable_phone_number_fields();

      // hide email address button
      $("#email-address-login").hide();

      // shows and enabled email address field
      show_and_enable_email_address_field();

      // show phone number button
      $("#phone-number-login").show();
    });
  });



  //resend otp timer
  var spn = document.getElementById("btnCounter");
  var btn = document.getElementById("resendOtpBtn");
  if (typeof timerVal !== 'undefined'){
    var countRef = 30 - timerVal;     // Set count
  }
  var timerRef = null;  // For referencing the timer
  if(spn != null) {
    (function countDown(){
      // Display counter and start counting down
      spn.textContent = countRef;

      // Run the function again every second if the count is not zero
      if(countRef > 0){
        timerRef = setTimeout(countDown, 1000);
        countRef--; // decrease the timer
      } else {
        // Enable the button and hide timeleft span
        $('#timeLeftSpan').hide();
        btn.removeAttribute("disabled");
      }
    }());
  }

});

$("body").on("click", ".show-hide-practice-name", function() {
  $(".practice-name-filter").toggleClass("hide")
  return false;
})
