$(document).ready(function() {

  $("body").on("submit", "form[data-search-form]", function(){
    near = $("#near").val()
    if(near == "") {
      $("input[data-geo]").val('')
    }
    if($('.simple_form').data("search-form") == 'ajax'){
      reload = true;
    }
    else{
      reload = false;
    }
    submitSearchForm(reload);
    return false;
  })

  $("body").on("change", "form[data-search-form] .filter select, .sort select", function(){
    form = $("form[data-search-form]")
    if($(this).attr("id")=="search_hospital_state") {
      form.find(".cities_checkboxes input").prop("checked", false)
    } else {
    }
    submitSearchForm();
    return false;
  })

  // $("body").on("focusout", "form[data-search-form] .filter input", function(){
  //   submitSearchForm();
  // })

  $("body").on("click", "form[data-search-form] .filter input[type='checkbox']", function(){
    submitSearchForm();
  })

  $(".advanced-search").click(function() {
    toggleDiv(".advanced-search-form")
    return false;
  })

  if($("form[data-search-form]").length > 0) {
    window.onpopstate = function(event) {
      if(event.state && event.state.data !== undefined && $(".search").length){
        link = window.location.href
        renderSearchResults(link)
      }
      else {
        link = window.location.href
        window.location.href = link
      }
    }
  }

  openAdvancedSearch();

})

function submitSearchForm(reload = false) {
  btn = document.activeElement.getAttribute('value');
  $(".result").css('opacity', .5);
  form = $("form[data-search-form]")
  data = form.serialize();
  url = form.attr("action")
  url = url + "?button=" + btn + '&' + data
  window.history.pushState({data: data, button: btn},"Search ", url);
  if(reload){
    window.location.reload()
  }
  renderSearchResults(url)
}

function renderSearchResults(url) {
  $.ajax({
    url: url,
    method: 'GET',
    data: {ajax: true},
    dataType:'html'
  }).done(
    function(data) {
      //html = $(data).find(".search").html()
      $(".search").html(data);
      $(".result").css('opacity', 1);
      initGeoComplete();
      initDisplayStars();
      document.addEventListener("turbolinks:load", function() {
        initCountrySelect();
        initSelect2();
      })
    }
  ).then(function() {
      $(document).attr("title", $('#page_title').text());
    }
  )
}

function openAdvancedSearch() {
  advancedForm = $(".advanced-search-form")
  advanced = advancedForm.data("advanced")
  if(advanced) {
    advancedForm.slideDown();
  }
}

function toggleDiv(selector) {
  sel = $(selector)
  if(sel.is(":visible")){
    sel.slideUp();
  } else {
    sel.slideDown();
  }
}
