// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
// Libs
//= require jquery
//= require jquery_ujs
//= require jquery.raty
//= require jquery.geocomplete.min
//= require nested_form_fields
//= require bootstrap.min.js
// App code
//= require highcharts
//= require google_init
//= require design
//= require details_page
//= require flatpickr.min.js
//= require dataTables/jquery.dataTables
//= require dataTables/jquery.dataTables.foundation
//= require moment
//= require foundation
//= require turbolinks
//= require chosen-jquery
//= require jquery.geocomplete.min
//= require foundation-datepicker.min
//= require progressbar.min
//= require country_select
//= require compare
//= require rating_filter
//= require countrySelect.min
//= require answer
//= require chosen_init
//= require circle
//= require datatable_init
//= require rating
//= require review
//= require scroll
//= require search
//= require vote
//= require select2
//= require select2_init
//= require froala_editor.min.js
//= require froala_editor_init
//= require plugins/paragraph_format.min.js
//= require plugins/align.min.js
//= require plugins/code_view.min.js
//= require plugins/image.min.js
//= require plugins/link.min.js
//= require plugins/font_size.min.js
//= require plugins/lists.min.js
//= require plugins/paragraph_style.min.js
//= require plugins/word_paste.min.js
//= require cocoon

document.addEventListener("turbolinks:load", function() {
  $(document).foundation();
  initCountrySelect();
  $('html, body, document').animate({scrollTop: 0})
})
$(document).ready(function() {
  $(".country-code-select").change(function(){
    new_value = $(".country-code-select").val() + $(".ten-digit-number").val();
    $(".hidden_phone_number").val(new_value);
  });
  $(".ten-digit-number").change(function(){
    new_value = $(".country-code-select").val() + $(".ten-digit-number").val();
    $(".hidden_phone_number").val(new_value);
  });
});
function shuffle(array) {
  return array.sort(() => Math.random() - 0.5)[0];
}
