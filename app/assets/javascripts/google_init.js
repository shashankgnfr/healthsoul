function initMap(lat, lng, name, id) {
    id = id || 'map';
    if (document.getElementById(id)){
      var myLatLng = {lat: lat, lng: lng};

      var map = new google.maps.Map(document.getElementById(id), {
        zoom: 12,
        center: myLatLng
      });

      var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: name
      });
    }
 }

function initGeoComplete() {
  $("#search_hospital_near, #search_doctor_near, form #near").geocomplete({
    details: "form",
    detailsAttribute: "data-geo"
  })
}

function initPlacePhoto(lat, lng, name) {
  var locationPlace = new google.maps.LatLng(lat,lng);
  var search = {
    location: locationPlace,
    keyword: name,
    //rankBy: google.maps.places.RankBy.DISTANCE
    rankBy: google.maps.places.RankBy.PROMINENCE,
    radius: '1000',
    //type: 'hospital'
  };

  service = new google.maps.places.PlacesService(document.getElementById('photo'));
  service.nearbySearch(search, displayPlacePhoto);
}

function initShowPlacePhotos(lat, lng, name, placeid) {
  service = new google.maps.places.PlacesService(document.getElementById('place-photos'));

  if(placeid) {
    service.getDetails({placeId: placeid}, function(place, status) {
      showPlacePhotos(place.photos, placeid);
    });
  }
    var locationPlace = new google.maps.LatLng(lat,lng);
    var search = {
      location: locationPlace,
      keyword: name,
      //rankBy: google.maps.places.RankBy.DISTANCE
      rankBy: google.maps.places.RankBy.PROMINENCE,
      radius: '3000',
      //type: 'hospital'
    };

    service.nearbySearch(search, displayAllPlacePhotos);
}

function displayPlacePhoto(places, status) {
  if(places.length > 0) {
    placesWithPhotos = places.filter(function(p) {
      if(p.photos != undefined && p.photos.length > 0) {
        return p;
      }
    })
    if(placesWithPhotos.length > 0) {
      photos = placesWithPhotos[0].photos
      if(photos && photos.length > 0) {
        place_id = placesWithPhotos[0].place_id
        photo = photos[0].getUrl({maxHeight: 500});
        img = $(".h-image")
        id = img.data("id")
        data = {placeid: place_id, place_photo: photo}
        $.ajax({
          url: "/locations/" + id + ".json",
          data: data,
          method: "PATCH"
        }).success(function(data){
        })
        img.addClass('hospital-image');
        img.css("background-image", "url(" + photo + ")");
      }
    }
  }
}

function displayAllPlacePhotos(places, status) {
  if(places.length > 0) {
    placesWithPhotos = places.filter(function(p) {
      if(p.photos != undefined && p.photos.length > 0) {
        return p;
      }
    })
    for (place in placesWithPhotos) {
      photos = placesWithPhotos[place].photos
      placeid = placesWithPhotos[place].place_id
      service.getDetails({placeId: placeid}, function(place, status) {
        showPlacePhotos(place.photos, placeid);
      });
    }
  }
}

function showPlacePhotos(photos, placeid) {
  if(photos && photos.length > 0) {
    for(photo in photos) {
      img = photos[photo].getUrl({maxHeight: 500});
      $("#place-photos").append("<img data-placeid='"+placeid+"' src='" + img + "' />");
    }
  }
}

document.addEventListener("turbolinks:load", function() {
  $(".nav_click").click(function() {
    $(".nav_munu_main_section").slideToggle(1000);
  });
  $(".close-nav").click(function() {
    $(".nav_munu_main_section").slideToggle('fast');
  });
  
  $("body").on("click", "#place-photos img", function() {
    id = $("#place-photos").data("id")
    data = {location: {placeid: $(this).data("placeid"), place_photo: $(this).attr('src') }}
    $.ajax({
      url: "/admin/locations/" + id + ".json",
      data: data,
      method: "PATCH"
    }).success(function(data){
    })
  })
})
