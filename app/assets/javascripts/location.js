// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
// Libs
//= require jquery
//= require jquery_ujs
//= require jquery.raty
//= require jquery.geocomplete.min
//= require bootstrap.min.js
// App code
//= require design
//= require details_page
//= require moment
//= require foundation
//= require turbolinks
//= require chosen-jquery
//= require foundation-datepicker.min
//= require country_select
//= require rating_filter
//= require countrySelect.min
//= require answer
//= require chosen_init
//= require circle
//= require google_init
//= require home
//= require rating
//= require review
//= require scroll
//= require search
//= require vote
//= require select2
//= require select2_init


document.addEventListener("turbolinks:load", function() {
  $(document).foundation();
})
document.addEventListener("turbolinks:load", function() {
  initCountrySelect();
})
$(document).ready(function() {
  $(".country-code-select").change(function(){
    new_value = $(".country-code-select").val() + $(".ten-digit-number").val();
    $(".hidden_phone_number").val(new_value);
  });
  $(".ten-digit-number").change(function(){
    new_value = $(".country-code-select").val() + $(".ten-digit-number").val();
    $(".hidden_phone_number").val(new_value);
  });
});
