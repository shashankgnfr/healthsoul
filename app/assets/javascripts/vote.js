$(".vote-buttons a").click(function() {
  link = $(this)
  container = link.parents(".vote-buttons")
  resource_type = container.data("resource-type")
  resource_id = container.data("resource-id")
  status = link.data("vote")
  url = "/api/votes/"+resource_type+"/"+resource_id+"/"+status
  $.get( url, function( data ) {
    if(data.status == 1) {
      link.find(".count").text(data.count)
    }
  });
  return false;
})
