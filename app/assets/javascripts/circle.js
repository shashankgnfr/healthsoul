document.addEventListener("turbolinks:load", function() {
  $reviewCircles = $(".review-circle")
  $circlesDrawn = $(".progressbar-text").length > 0
  if($reviewCircles.length > 0 && !$circlesDrawn) {
    $reviewCircles.each(function() {
      setupCircle($(this));
    })
  }
})

var duration = 0;
function setupCircle(circle) {
  duration = duration + 1200;
  var bar = new ProgressBar.Circle("#" + circle.attr("id"), {
    color: '#fff',
    // This has to be the same size as the maximum width to
    // prevent clipping
    strokeWidth: 4,
    trailWidth: 1,
    easing: 'easeInOut',
    duration: duration,
    text: {
      autoStyleContainer: false
    },
    from: { color: '#fff', width: 1 },
    to: { color: 'rgb(231, 213, 14)', width: 4 },
    // Set default step function for all animate calls
    step: function(state, circle) {
      circle.path.setAttribute('stroke', state.color);
      circle.path.setAttribute('stroke-width', state.width);

      var value = (circle.value() * 5).toFixed(2);
      if (value === 0) {
        circle.setText('');
      } else {
        circle.setText(value);
      }

    }
  });
  bar.text.style.fontSize = '2rem';

  bar.animate(circle.data("rating") / 5.0);  // Number from 0.0 to 1.0
}
