//= require jquery.flexslider
$(window).load(function() {
 // || $(".flexslider2").length > 0
    if($('.flexslider').length > 0) {
      $('.flexslider').flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 360,
        itemMargin: 30,
        minItems: 1,
        move: 1,
        slideshowSpeed: 111111111111000,
        animationSpeed: 1000,
        maxItems: 3
      });

      // $('.flexslider2').flexslider({
      //   animation: "slide",
      //   animationLoop: false,
      //   itemWidth: 270,
      //   itemMargin: 20,
      //   minItems: 1,
      //   move: 1,
      //   slideshowSpeed: 111111111111000,
      //   animationSpeed: 1000,
      //   maxItems: 4
      // });

    }
})