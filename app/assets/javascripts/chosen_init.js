document.addEventListener("turbolinks:load", function() {
  $(".chosen-select").chosen();
  if ($(".chosen-select-org-search").length) {
    $(".chosen-select-org-search").select2({
      ajax: {
        url: "/api/doctor_organizations",
        dataType: 'json',
        delay: 250,
        data: function (params) {
          return {
            q: params.term, // search term
          };
        },
        processResults: function (data, params) {
          return {
            results: data
          };
        },
        cache: true
      },
    }).on("change", function() {
      id = $(this).val()
      window.location.href = "/admin/doctor_organizations/" + id + "/edit"
    });
  }
  $(".chosen-select-org").select2({
    ajax: {
      url: "/api/doctor_organizations",
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return {
          q: params.term, // search term
        };
      },
      processResults: function (data, params) {
        return {
          results: data
        };
      },
      cache: true
    },
  });
  $(".chosen-select-doctor").select2({
    allowClear: true,
    placeholder:"None Selected",
    ajax: {
      url: "/api/doctors",
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return {
          q: params.term, // search term
        };
      },
      processResults: function (data, params) {
        return {
          results: data
        };
      },
      cache: true
    },
  });

  // On load, initialize the country_code, and listen for changes to choose how to sort hospital optgroups.
  var country_code = $("#doctor_country_code").val()
  $("#doctor_country_code").on("change", function() {
    country_code = $(this).val()
  });

  $(".select2-doctor-ajax").select2({
    ajax: {
      url: "/api/doctors",
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return {
          q: params.term, // search term
        };
      },
      processResults: function (data, params) {
        // parse the results into the format expected by Select2
        // since we are using custom formatting functions we do not need to
        // alter the remote JSON data, except to indicate that infinite
        // scrolling can be used

        return {
          results: data
        };
      },
      cache: true
    },
    //escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
    //minimumInputLength: 1,
    //templateResult: formatRepo, // omitted for brevity, see the source of this page
    //templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
  });
  $('.tag-list-select').select2();
})
