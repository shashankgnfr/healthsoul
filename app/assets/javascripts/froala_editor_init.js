document.addEventListener("turbolinks:load", function() {
  var temp_token = $( 'meta[name="csrf-token"]' ).attr( 'content' );

  $('.blog-form').froalaEditor({
    key: 'teknB-13ezg1H-9i1zB3D-17vw==',
    imageUploadURL: '/blog/image-upload',
    imageUploadMethod: 'POST',
    imageUploadParams: {'authenticity_token': temp_token},
    imageDefaultAlign: 'left',
    imageDefaultWidth: 500,
    heightMin: 400,
    zIndex: 999
    // Only copy plain text to avoid style issue but then uncopied hyperlink causes issue.
    // pastePlain: true
  });

  $('#promotion-title-form').froalaEditor({
    key: 'teknB-13ezg1H-9i1zB3D-17vw==',
    pastePlain: true,
    zIndex: 999
  });

  $('#promotion-blurb-form').froalaEditor({
    key: 'teknB-13ezg1H-9i1zB3D-17vw==',
    pastePlain: true,
    zIndex: 999
  });

  $('#promotion-terms-conditions-form').froalaEditor({
    key: 'teknB-13ezg1H-9i1zB3D-17vw==',
    pastePlain: true,
    zIndex: 999
  });

  $('.froala_editor').froalaEditor({
    key: 'teknB-13ezg1H-9i1zB3D-17vw==',
    zIndex: 999
  });


});
