$(document).ready(function() {
  $(".toggle-answers").click(function(event){
    event.preventDefault();
    toggleAnswers(this)
  });
})

function toggleAnswers(el) {
  $question = $(el).parents(".question")
  $questionBody = $question.find(".body")
  $el = $question.find(".answers")
  $arrow = $el.find("i.fa")
  $answers = $el.find(".answer")
  $answerLable = $el.find("a span.answer-label")
  visible = $answers.first().is(":visible")
  if(visible) {
    $answers.slideUp()
    $arrow.removeClass("fa-chevron-up")
    $arrow.addClass("fa-chevron-down")
    $answerLable.text("View")
    $questionBody.removeClass("primary")
  } else {
    $answers.slideDown()
    $arrow.removeClass("fa-chevron-down")
    $answerLable.text("Hide")
    $arrow.addClass("fa-chevron-up")
    $questionBody.addClass("primary")
  }
}
