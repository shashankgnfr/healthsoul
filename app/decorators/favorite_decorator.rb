class FavoriteDecorator < DocLocDecorator
  delegate_all

  def pretty_type
    object.favoriteable_type == 'Location' ? object.favoriteable.location_type.name : object.favoriteable_type
  end

  def link_path
    return "/#{object.favoriteable.slug}" if object.favoriteable_type == 'Location'
    return "/doctors/#{object.favoriteable.slug}" if object.favoriteable_type == 'Doctor'
  end

  def circle_image
    return loc_circle_image if object.favoriteable_type == 'Location'
    return doc_circle_image if object.favoriteable_type == 'Doctor'
  end

  def loc_circle_image
    alt = object.favoriteable.photo_alt
    if object.favoriteable.image.present?
      if object.favoriteable.photo?
        h.content_tag(:div, nil, class: :circle_image, style: "background-image: url(#{object.favoriteable.photo.thumb.url})", title: alt)
      else
        h.content_tag(:div, nil, class: :circle_image, style: "background-image: url(#{icon_url})", title: alt)
      end
    else
      h.content_tag(:div, object.favoriteable.initials, class: :circle_image, title: alt)
    end
  end

  def doc_circle_image
    alt = object.favoriteable.photo_alt
    if object.favoriteable.photo?
      h.content_tag(:div, nil, class: :circle_image, style: "background-image: url(#{object.favoriteable.photo.thumb.url})", title: alt)
    else
      h.content_tag(:div, object.favoriteable.initials, class: :circle_image, title: alt)
    end
  end

  def icon_url
    if object.favoriteable.image.present?
      object.favoriteable.image
    else
      ActionController::Base.helpers.asset_path("img/re-www.png")
    end
  end

end
