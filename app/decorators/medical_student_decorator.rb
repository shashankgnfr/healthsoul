class MedicalStudentDecorator < ApplicationDecorator

  def details_header_photo
    alt = object.name
    if object.photo?
      h.image_tag object.photo.thumb.url, width: 200, itemprop: "image", title: alt, alt: alt
    else
      h.image_tag "default-doctor.png", width: 200, class: 'circle no-pic', itemprop: "image", title: alt, alt: alt
    end
  end

end