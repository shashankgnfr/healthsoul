class DoctorDecorator < DocLocDecorator
  delegate_all
  # delegate :phone, :lat_lng_present?, :website, to: :organization

  def address_is_present?
    object.address_is_present?
  end

  def primary_specialty
    object.primary_speciality
  end

  def speciality_title
    object.practice_specialities&.first&.title
  end
  
  def org_name
    object.organization_name
  end

  def full_name
    [ object.name, object.credential ].reject(&:blank?).join(", ")
  end

  def med_school_info
    return "" if object.med_school.blank?
   object.med_school.name
  end

  def dr_website
    object.website
  end

  def photo
    alt = object.photo_alt
    if object.photo?
      h.image_tag object.photo.thumb.url, class: 'circle', itemprop: "image", title: alt, alt: alt, width: 175
    else
      h.image_tag "default-doctor.png", class: 'circle no-pic', itemprop: "image", title: alt, alt: alt, width: 175
    end
  end

  def share_image_url
    ActionController::Base.helpers.asset_path('footerlogo.png')
  end

  def hospital_links
    links = []
    object.only_hospitals.each do |hospital|
      hospital_link = h.link_to(hospital.name, hospital)
      hospital_link += ('&nbsp;' + h.content_tag(:span, nil,  data: {stars: "#{hospital.overall}"})).html_safe if hospital.overall != 0.0
      links << hospital_link
    end
    links << object.affiliated_hospitals
    links.join("<br/>").html_safe
  end
  
  def practice_links
    links = []
    object.only_practices.each do |practice|
      practice_link = h.link_to(practice.name, practice)
      practice_link += ('&nbsp;' + h.content_tag(:span, nil,  data: {stars: "#{practice.overall}"})).html_safe if practice.overall != 0.0
      links << practice_link
    end
    links.join("<br/>").html_safe
  end

  def address
    object.address
  end

  def address2
    ""
  end

  def address3
    city_state_zip
  end

  def city_state_zip
    object.city_state_zip
  end

  def details_header_photo
    photo
  end

  def email
    object.email
  end

  def overall_rating
    object.overall.round(1)
  end

  def city_country
    "#{city}, #{state}"
  end

  def circle_image
    alt = object.photo_alt
    if object.photo?
      h.content_tag(:div, nil, class: :circle_image, style: "background-image: url(#{object.photo.thumb.url})", title: alt)
    else
      h.content_tag(:div, object.initials, class: :circle_image, title: alt)
    end
  end

  def city_slug
    object.city_slug
  end

  def state_slug
    object.state_slug
  end

  def country_slug
    object.country_slug
  end

  def top_speciality
    object.top_speciality
  end

  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end

end
