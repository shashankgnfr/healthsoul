class DocLocDecorator < ApplicationDecorator

  def stars
    h.content_tag(:span, nil,  data: {stars: "#{object.overall}"})
  end

  def self.collection_decorator_class
    PaginatingDecorator
  end

  def overview
    if object.description.present?
      h.simple_format h.auto_link(object.description)
    else
      default_description
    end
  end

  def city_state_country
    "#{object.city}, #{object.state}, #{object.country}"
  end

  def hopital_variation
    object.country_code === 'USA' ? "a #{object.sub_type}" : 'multispecilaity hospital'
  end

  def default_description
    if object.doctor?
      doctor_default
    elsif object.hospital?
      "#{object.name} is #{hopital_variation} in #{city_state_country}.  Find patient reviews, specialty ranking and compare with nearby hospitals."
    elsif object.insurance_company?
      "#{object.name} is a health insurance company in #{object.country}. #{object.name} has several different health plans to suit your insurance needs. You can find contact information for #{object.name} on this page including their website and phone number."
    elsif object.travel_insurance?
      "#{object.name} is a travel health insurance company in #{object.country}. #{object.name} has  different travel health insurance plans for international travellers and expats to suit your health needs during vacation, overseas studies or business."
    elsif object.practice?
      "#{object.name} is a #{object.practice_speciality_name} practice in #{city_state_country}. There are #{object.doctors.count} providers affiliated with this practice. You can find location, contact information and reviews about this practice on this page."
    end
  end

  def doctor_default
    object.practice_specialities.present? ? doctorized_specialty_description(object.practice_specialities.first) : ''
  end

  def doctorized_specialty_description(practice_speciality)
    return '' unless practice_speciality.default_description.present?
    practice_speciality.default_description.
      gsub('%%name%%', object.name || '').
      gsub('%%location%%', city_state_country || '').
      gsub('%%specialities%%', object.specialities || '').
      gsub('%%hospitals%%', object.hospitals.present? ? object.hospitals.map(&:name).join(', ') : 'N/A')
  end

  def overall
    "%g" % object.overall
  end


end
