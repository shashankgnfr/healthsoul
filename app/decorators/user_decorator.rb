class UserDecorator < ApplicationDecorator
  def photo
    if object.photo?
    	h.image_tag object.photo.thumb.url, class: 'circle'
    else
    	h.content_tag(:h1, "Add Picture", style: "color: white; text-align: center; margin-top: 25%;")
    end
  end
end