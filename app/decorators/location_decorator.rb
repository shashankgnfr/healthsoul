class LocationDecorator < DocLocDecorator
  delegate_all

  def city_country
    "#{city}, #{state}" if object.hospital?
  end

  def primary_specialty
    ''
  end

  def address3
    "#{city}, #{state} #{zip} #{country_code}"
  end

  def icon
    h.image_tag icon_url
  end

  def icon_url
    if object.image.present?
      object.image
    else
      ActionController::Base.helpers.asset_path("img/re-www.png")
    end
  end

  def overall_rating
    overall.round(1)
  end

  def photo
    circle_image
  end

  def details_header_photo
    alt = object.photo_alt
    if object.photo?
      h.image_tag object.photo.thumb.url, width: 175, itemprop: "image", title: alt, alt: alt
    elsif object.image.present?
      h.image_tag object.image, width: 175, itemprop: "image", title: alt, alt: alt
    else
      if object.hospital?
        img = 'h.png'
      else
        # TODO: Need to replace as per location_type
        img = 'logo.png'
      end
      h.image_tag img, width: 175, itemprop: "image", title: alt, alt: alt
    end
  end

  def large_photo
    if object.image.present?
      if object.photo?
        object.photo.url
      else
        place_photo
      end
    end
  end

  def share_image_url
    if object.hospital?
      large_photo.present? ? large_photo : ActionController::Base.helpers.asset_path('footerlogo.png')
    elsif object.insurance_company?
      ActionController::Base.helpers.asset_path('search-ins.jpg')
    elsif object.travel_insurance?
      ActionController::Base.helpers.asset_path('search-travel.jpg')
    else
      ActionController::Base.helpers.asset_path('footerlogo.png')
    end
  end

  def circle_image
    alt = object.photo_alt
    if object.image.present?
      if object.photo?
        h.content_tag(:div, nil, class: :circle_image, style: "background-image: url(#{object.photo.thumb.url})", title: alt)
      else
        h.content_tag(:div, nil, class: :circle_image, style: "background-image: url(#{icon_url})", title: alt)
      end
    else
      h.content_tag(:div, object.initials, class: :circle_image, title: alt)
    end
  end

  def claim_text
    if object.travel_insurance?
      "You can respond to your consumer reviews for free"
    else
      "You can respond to patient reviews for free"
    end
  end

  def city_slug
    object.city_slug
  end

  def state_slug
    object.state_slug
  end

  def country_slug
    object.country_slug
  end
end
