class CountryDecorator < ApplicationDecorator
  
  def self.collection 
    Country.order('name').pluck(:name, :id)
  end

end