class BlogDecorator < ApplicationDecorator
  delegate_all

  def self.patient_story?(tag)
   tag == 'Patient Stories'
  end

  def featured_or_avtar
    if featured_image.present?
      h.image_tag featured_image.small_thumb.url, class: 'circle', title: blog.title
    else
      author_avatar
    end
  end

  def author_avatar
    if doctor.present? && doctor.photo? && !author.present?
      alt = doctor.photo_alt
      h.image_tag doctor.photo.thumb.url, class: 'circle', title: alt, alt: alt
    elsif author.present? && custom_author_photo.present?
      h.image_tag custom_author_photo.thumb.url, class: 'circle'
    else
      if doctor.present?
        img = 'default-doctor.png'
        alt = doctor.photo_alt
      else
        img = 'logo.png'
        alt = 'HealthSoul'
      end
      h.image_tag img, class: 'circle', title: alt, alt: alt
    end
  end

  def author_name
    return author if author.present?
    doctor.present? ? "#{doctor.name}" : Blog::BLANK_AUTHOR
  end

  def author_name_with_comma
    return "#{author}" if author.present?
    doctor.present? ? "#{doctor.name}, " : Blog::BLANK_AUTHOR
  end

  def author_specialty
    if doctor.present? && doctor.practice_specialities.present? && !author.present?
      "#{doctor.practice_specialities.first.name}, "
    else
      Blog::BLANK_SPECIALTY
    end
  end

  def author_location
    if doctor.present? && !author.present?
      "#{doctor.city}, #{doctor.state}, #{doctor.country}"
    else
      Blog::BLANK_LOCATION
    end
  end

  def tag_links

    Array(attached_tags).collect do |tag|
      h.link_to tag, h.blogs_tag_path(tag: tag.parameterize), title: tag
    end.join(", ").html_safe
  end

  def attached_tags
    tag_list.present? ? tag_list : Blog::BLANK_TAGS
  end

end
