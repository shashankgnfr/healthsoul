class PromotionDecorator < ApplicationDecorator
  def promotion_image
    if object.promotion_image?
      h.image_tag object.promotion_image.thumb.url, size: 90
    else
      h.image_tag "logo.png"
    end
  end

  # A larger version of the promotion image for use on the landing page.
  def users_info_landing_page_promotion_image
    if object.promotion_image?
      h.image_tag object.promotion_image.thumb.url, size: 220, style: "display: block; margin-left: auto;margin-right: auto;"
    else
      h.image_tag "footerlogo.png", size: 220, style: "display: block; margin-left: auto;margin-right: auto;"
    end
  end
end