class PromotionImageUploader < BaseUploader

  process resize_to_fit: [800, 800]

  version :thumb do
    process resize_to_fill: [200,200, 'North']
  end
  
end
