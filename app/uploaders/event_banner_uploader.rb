class EventBannerUploader < BaseUploader

  process resize_to_fit: [1130, 400]

  version :thumb do
    process resize_to_fill: [1130, 400]
  end

end