class AdvertisementImageUploader < BaseUploader

  version :carousel, if: :is_carousel? do
    process resize_to_fill: [330, 75, 'North']
  end

  version :vertical, if: :is_vertical? do
    process resize_to_fill: [120, 600, 'North']
  end

  version :horizontal, if: :is_horizontal? do
    process resize_to_fill: [728, 90, 'North']
  end

  protected

  def is_carousel?(advert)
    model.dimension == '330 X 75'
  end

  def is_vertical?(advert)
    model.dimension == '120 X 600'
  end

  def is_horizontal?(advert)
    model.dimension == '728 X 90'
  end
end