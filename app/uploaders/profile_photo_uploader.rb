class ProfilePhotoUploader < BaseUploader

  process resize_to_fit: [800, 800]

  version :thumb do
    process resize_to_fill: [200,200, 'North']
  end

  version :small_thumb, from_version: :thumb do
    process resize_to_fill: [100, 100]
  end

end
