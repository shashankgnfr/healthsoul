class BlogUploader < BaseUploader

  # process resize_to_fit: [1200, 1200]

  version :thumb do
    process resize_to_fill: [500,500]
  end

  version :small_thumb, from_version: :thumb do
    process resize_to_fill: [270, 100]
  end
end
