# encoding: utf-8
class DocUploader < CarrierWave::Uploader::Base

  storage :file

  def store_dir
    "#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def extension_whitelist
    %w(doc docx dot)
  end

  def size_range
    0..10.megabytes.to_i
  end

end
