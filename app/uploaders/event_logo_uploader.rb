class EventLogoUploader < BaseUploader

  version :thumb do
    process resize_to_fill: [200,200, 'North']
  end

  version :small_thumb, from_version: :thumb do
    process resize_to_fill: [160, 80]
  end

end
