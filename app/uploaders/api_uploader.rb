class ApiUploader < CarrierWave::Uploader::Base

  def extension_whitelist
    %w(jpg jpeg gif png)
  end

  def cache_dir
    "/tmp/uploads"
  end

  def filename
   "#{SecureRandom.uuid()}.#{file.extension}"
  end

end
