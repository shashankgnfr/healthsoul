class MakeSlugJob
  include SuckerPunch::Job

  def perform
    ActiveRecord::Base.connection_pool.with_connection do
      locations = Location.where("slug is null or slug like ''")
      locations.each do |location|
        location.importing = true
        location.save
      end
    end
  end
end
