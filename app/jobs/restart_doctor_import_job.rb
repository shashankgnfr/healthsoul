class RestartDoctorImportJob
  include SuckerPunch::Job

  def perform(params)
    ActiveRecord::Base.connection_pool.with_connection do
      Importer::Doctors.new.import
    end
  end
end
