class ReviewerPointsJob
  include SuckerPunch::Job

  def perform(user)
    ActiveRecord::Base.connection_pool.with_connection do
      ReviewPointsCalculator.new(user).save
    end
  end
end
