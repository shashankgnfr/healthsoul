class DepartmentRankingJob
  include SuckerPunch::Job

  def perform(rating)
    ActiveRecord::Base.connection_pool.with_connection do
      DepartmentRankingService.new(rating).rank
    end
  end
end
