class RankingJob
  include SuckerPunch::Job

  def perform(location)
    ActiveRecord::Base.connection_pool.with_connection do
      location.update_overall
    end
  end
end
