class ImportJob
  include SuckerPunch::Job

  def perform(params)
    ActiveRecord::Base.connection_pool.with_connection do
      Importer::Locations.new(params).save
    end
  end
end
