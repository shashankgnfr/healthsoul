class DoctorImportJob 
  include SuckerPunch::Job
  workers 5
  
  def perform(params)
    ActiveRecord::Base.connection_pool.with_connection do
      Importer::DoctorUploads.new(params).import
      #Importer::DoctorUpdater.new(params).import
      #DoctorOrganization.where("latitude is null").map(&:save)
      #Doctor.where("specialities is null or specialities like ''").all.map(&:save)
    end
  end
end
