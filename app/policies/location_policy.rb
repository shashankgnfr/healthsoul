class LocationPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def update?
    user.has_role?(:moderator, record) || user.has_role?(:admin)
  end

  def manage?
    user.has_role?(:moderator, record) || user.has_role?(:admin)
  end

  def permitted_attributes
    [
      :name, :description, :address, :address2, :city, :state, :zip,
      :website, :phone, :email, :photo, :photo_cache, :remove_photo
    ]
  end
end
