class BlogPolicy < ApplicationPolicy
  def manage?
    user.has_role?(:admin)
  end

  class Scope < Scope
    def resolve
      scope
    end
  end
end
