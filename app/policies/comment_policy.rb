class CommentPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def new?
    user.has_role?(:moderator, record.commentable.location) || user.has_role?(:admin)
  end

  def create?
    new?
  end
end
