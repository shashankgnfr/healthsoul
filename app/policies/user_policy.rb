class UserPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def update?
    user == record || user.has_role?(:admin)
  end

  def show?
    user == record || user.has_role?(:admin)
  end
end
