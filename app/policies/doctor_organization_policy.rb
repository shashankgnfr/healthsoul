class DoctorOrganizationPolicy < ApplicationPolicy

  def edit?
    user.has_role?(:admin)
  end


  class Scope < Scope
    def resolve
      scope
    end
  end
end
