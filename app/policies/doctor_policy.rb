class DoctorPolicy < ApplicationPolicy
  def manage?
    user.has_role?(:moderator, record) || user.has_role?(:admin)
  end

  def update?
    user.has_role?(:moderator, record) || user.has_role?(:admin)
  end

  def permitted_attributes
    if user.has_role?(:admin)
      [
        :photo_cache, :photo_alt, :med_school_other,
        :mobile_number, :mobile_country_code,
        :should_be_notified_by_email_for_appointments, :should_be_notified_by_sms_for_appointments,
        :name, :website, :email, :description, :organization_name,
        :address, :city, :state, :zip, :phone, :website, :country, :country_code,
        :credential, :gender, :photo, :remove_photo, :doctor_organization_id, :npi,
        :med_school_id, :academic_affiliation, :residency, :fellowship, :fellowship_2, :graduation, :areas_of_expertise,
        :latitude, :longitude, :affiliated_hospitals, :display_hours_and_fees, :new_patient_fees, :current_patient_fees, :practice_speciality_ids => [],
        :hospital_ids => [], business_hours_attributes:[:id, :day, :start_time, :end_time, :_destroy], additional_locations_attributes: [:id, :name, :address, :city, :state, :zip, :phone, :_destroy, business_hours_attributes:[:id, :day, :start_time, :end_time, :_destroy, :closed]], 
        :language_ids => [], :board_certification_ids => []
      ]
    elsif user.has_role?(:moderator, record)
      [
        :photo_cache, :photo_alt, :med_school_other,
        :mobile_number, :mobile_country_code,
        :should_be_notified_by_email_for_appointments, :should_be_notified_by_sms_for_appointments,
        :name, :website, :description, :organization_name, :address,
        :city, :state, :zip, :phone, :website, :country, :country_code, :credential, :gender,  :areas_of_expertise,
        :photo, :remove_photo, :doctor_organization_id, :med_school_id, :academic_affiliation,
        :residency, :fellowship, :fellowship_2, :graduation, :affiliated_hospitals, :npi, :latitude, :longitude,
        :display_hours_and_fees, :new_patient_fees, :current_patient_fees, :practice_speciality_ids => [], :hospital_ids => [], business_hours_attributes:[:id, :day, :start_time, :end_time, :_destroy],
        additional_locations_attributes: [:id, :name, :address, :city, :state, :zip, :phone, :_destroy, business_hours_attributes:[:id, :day, :start_time, :end_time, :_destroy, :closed]],
        :language_ids => [], :board_certification_ids => []
      ]
    end
  end

  class Scope < Scope
    def resolve
      scope
    end
  end
end
