class AppointmentPolicy < ApplicationPolicy
  def permitted_attributes
    [:name, :email, :phone, :time, :date, :reason, :new_patient, user: [:first_name, :last_name, :date_of_birth, :email, :phone_country_code, :phone_body, :phone_number]]
  end

  class Scope < Scope
    def resolve
      scope
    end
  end
end
