class FaqPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def admin?
    user.has_role? :admin
  end
end
