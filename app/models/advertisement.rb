class Advertisement < ApplicationRecord
  paginates_per 20
  is_impressionable

  validates :type, :subtype, presence: true
  validates :name, :reference_url, presence: true, uniqueness: true
  mount_uploader :image, AdvertisementImageUploader
  has_many :advertisements_resources, dependent: :destroy
  accepts_nested_attributes_for :advertisements_resources, allow_destroy: true
  belongs_to :country
  

  TYPE = ['Affiliate', 'InternalAdvert']
  AFFILIATE_SUBTYPE = ['Carousel']
  INTERNALADVERT_SUBTYPE = ['Vertical', 'Horizontal']
  # TODO: As we have only 1 dimension per subtype. So, in query i'm not using dimension but infuture we will use it.
  VERTICAL_DIMENSION = ['120 X 600'] 
  HORIZONTAL_DIMENSION = ['728 X 90']
  CAROUSEL_DIMENSION = ['330 X 75']
end
