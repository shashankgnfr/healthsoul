class Answer < ApplicationRecord
  include ReviewCalc
  include Analytics

  belongs_to :user
  belongs_to :question
  has_many :votes, as: :voteable, dependent: :destroy

  validates :user_id, :uniqueness => { :scope => [:question_id] }
end
