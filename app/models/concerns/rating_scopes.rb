module RatingScopes
  extend ActiveSupport::Concern


  included do

    def self.rating_for(category, scope = nil)
      if scope
        _ratings = where("#{category} > 0").send(scope)
      else
        _ratings = where("#{category} > 0")
      end
      if _ratings.present? && _ratings.average(category).present?
        _ratings.average(category).round(1)
      else
        0
      end
    end

    def self.recommend_percentage
      total = self.count
      return 0 if total == 0
      yes = where(recommend: "Yes").count
      maybe = where(recommend: "Maybe").count / 2.0
      ( (yes + maybe) / total ) * 100
    end

  end
end
