module WaveCell
  def self.wave_cell_send_sms(number, text)
    body = {source: "HealthSoul", destination: number, text: text }
    @max_retries = 1
    begin
      response = RestClient.post('https://api.wavecell.com/sms/v1/HealthSoul_8dDEA_hq/single', body.to_json, {"Authorization" => 'Bearer myn60CZOrGxsPm7Ppzaakv6PpNckWVEwrXb2A2DKkM', "Content-Type" => 'application/json'})
      Rails.logger.info "Message send response from wavecell => #{response}"
    rescue Exception => e
      @retries ||= 0
      if @retries < @max_retries
        @retries += 1
        Rails.logger.info "Message send response from wavecell retries => #{body} => #{@retries}"
        retry
      else
        
      end
    end
  end

end

