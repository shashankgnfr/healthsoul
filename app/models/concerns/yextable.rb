module Yextable

  def yext_json
    {
      parterId: id,
      status: yext_status&.upcase || 'AVAILABLE',
      name: name,
      address: address,
      gender: look_for_gender&.capitalize || '',
      url: website,
      npi: npi,
      type: "HealthcareProfessional",
      specialOffer: special_offer_json,
      categories: categories_array,
      locations: locations_array,
      ratings: reviews.rating_for(:overall_experience).round(2),
      total_reviews: reviews.count
    }
  end

  def rating_json
    {
      total: reviews.count,
      rating: reviews.rating_for(:overall_experience).round(2).to_f,
      maxRating: 5,
      reviews: review_list
    }
  end

  def review_list
    return [] unless reviews.present?
    reviews.map do |review|
      {
        reviewId: review.id,
        timestamp: review.created_at,
        authorName: review.user&.get_full_name || '',
        content: review.comment,
        url: "https://healthsoul.com#{entity_url}",
        rating: review.overall_experience.to_f
      }
    end
  end

  def special_offer_json
    {
      message: special_offer_message.present? ? special_offer_message : '',
      url: special_offer_url.present? ? special_offer_url : ''
    }
  end

  def categories_array
    practice_specialities.map{ |ps| {id: "#{ps.id}", name: "#{ps.name}"} }
  end

  def locations_array
    array_to_return = [{
      "officeName": organization_name,
      "postalCode": zip,
      "countryCode": country_code,
      "sublocality": nil,
      "visible": true,
      "address": address,
      "state": state,
      "city": city,
      "phone": phone,
      "latitude": latitude,
      "longitude": longitude
    }]
    if additional_locations.present?
      additional_locations.each do |al|
        array_to_return.push({
          "officeName": al.name,
          "postalCode": al.zip,
          "countryCode": al.country_code,
          "sublocality": nil,
          "visible": true,
          "address": al.address,
          "state": al.state,
          "city": al.city,
          "phone": al.phone,
          "latitude": al.latitude,
          "longitude": al.longitude
        })
      end
      array_to_return
    else
      array_to_return
    end
  end

  def nested_locations_array
    array_to_return = [{
      "officeName": organization_name,
      "address": {
        "postalCode": zip,
        "countryCode": country_code,
        "sublocality": nil,
        "visible": true,
        "address": address,
        "state": state,
        "city": city,
      },
      "phone": [
        {
          "number": {
            "number": phone
          },
          "type": "MAIN",
          "description": "Main"
        },
      ],
      "geoData": {
        "displayLatitude": latitude,
        "displayLongitude": longitude,
        "routableLongitude": latitude,
        "routableLatitude": longitude
      },
    }]
    if additional_locations.present?
      additional_locations.each do |al|
        array_to_return.push({
          "officeName": al.name,
          "address": {
            "postalCode": al.zip,
            "countryCode": al.country_code,
            "sublocality": nil,
            "visible": true,
            "address": al.address,
            "state": al.state,
            "city": al.city,
          },
          "phone": [
            {
              "number": {
                "number": al.phone
              },
              "type": "MAIN",
              "description": "Main"
            },
          ],
          "geoData": {
            "displayLatitude": al.latitude,
            "displayLongitude": al.longitude,
            "routableLongitude": al.latitude,
            "routableLatitude": al.longitude
          },
        })
      end
      array_to_return
    else
      array_to_return
    end
  end

  def parse_location_obj(location_obj, location_or_self)
    if location_obj[:address].present?
      address_obj = location_obj[:address]
      location_or_self.address = address_obj[:address]
      location_or_self.city = address_obj[:city]
      location_or_self.zip = address_obj[:postalCode]
      if address_obj[:countryCode] == 'US' || !address_obj[:countryCode].present?
        country_to_set = 'USA'
      else
        country_to_set = address_obj[:countryCode]
      end
      location_or_self.country = country_to_set
      location_or_self.country_code = country_to_set
      location_or_self.state = address_obj[:state]
    end
    if location_or_self.class == Doctor
      location_or_self.organization_name = location_obj[:officeName]
      location_or_self.description = location_obj[:description]
    else
      location_or_self.name = location_obj[:officeName]
    end
    if location_obj[:phones].present?
      location_obj[:phones].each{ |phone_attr| location_or_self.phone = phone_attr[:number][:number] if phone_attr[:type] == "MAIN" }
    end
    if location_obj[:geoData].present?
      location_or_self.latitude = location_obj[:geoData][:displayLatitude]
      location_or_self.longitude = location_obj[:geoData][:displayLongitude]
    end
    if location_obj[:urls].present?
      location_obj[:urls].each{ |their_url| location_or_self.website = their_url[:url] if their_url["type"] == 'WEBSITE' }
    end
    if location_or_self.class == Doctor && location_obj[:specialOffer].present?
      self.special_offer_message = location_obj[:specialOffer][:message]
      self.special_offer_url = location_obj[:specialOffer][:url]
    end
    self.save
    if location_obj[:hours].present?
      self.display_hours_and_fees = true
      location_obj[:hours].each do |day_hour|
        current_business_hours = location_or_self.business_hours.where(day: day_hour[:day].downcase)
        if day_hour[:intervals].present?
          current_business_hours.destroy_all if current_business_hours.present?
          day_hour[:intervals].each do |their_interval|
            location_or_self.business_hours.create(day: day_hour[:day].downcase, start_time: their_interval[:start], end_time: their_interval[:end])
          end
        else
          if current_business_hours.present?
            current_business_hours.update_all(closed: true)
          else
            location_or_self.business_hours.create(day: day_hour[:day].downcase, closed: true)
          end
        end
      end
    end
  end

  def parse_yext_order_body(params)
    self.name = params[:name] if params[:name].present?
    self.description = params[:description] if params[:description].present?
    self.yext_id = params[:yextId] if params[:yextId].present?
    if params[:gender].present?
      if ['male', 'female'].include?(params[:gender].downcase)
        self.gender = params[:gender].downcase
      else
        self.other_gender = params[:gender].downcase
      end

    end
    self.credential = params[:degrees][0]["abbreviation"] if params[:degrees].present?
    self.email = params[:emails][0]["address"] if params[:emails].present?
    if params[:locations].present?
      additional_locations.destroy_all
      params[:locations].each_with_index do |location_obj, index|
        if index == 0
          parse_location_obj(location_obj, self)
        else
          another_location = additional_locations.new
          parse_location_obj(location_obj, another_location)
        end
      end
    end
    self.remote_photo_url = params["headshot"]["url"] if params["headshot"].present?
    if params["educationAndTrainings"].present?
      params["educationAndTrainings"].each do |education_training|
        if education_training["type"] ==  "Medical School"
          school_from_params = MedSchool.find_by_name(education_training["institution"])
          if school_from_params.present?
            self.med_school = school_from_params
            self.graduation = education_training["yearFinished"]
          end
        end
      end
    end
    self.save
    if params["admittingHospitals"].present?
      params["admittingHospitals"].each do |admitting_hospital|
        hospital = Location.find_by_name(admitting_hospital)
        if hospital.present? && !hospitals.include?(hospital)
          self.hospitals << hospital
        end
      end
    end
    if params[:categories].present?
      self.practice_specialities.destroy_all
      params[:categories].each do |category|
        ps = PracticeSpeciality.find_by_yext_id(category["id"])
        self.practice_specialities << ps if ps.present?
      end
    end
  end

end
