module ReviewCalc
  extend ActiveSupport::Concern

  included do
    after_commit :run_calculator
  end

  def run_calculator
    ReviewerPointsJob.perform_async(user)
  end
end
