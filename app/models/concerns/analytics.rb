module Analytics
  extend ActiveSupport::Concern

  included do
    def self.last_month
      where("#{self.table_name}.created_at >= ?", 1.month.ago).count
    end

    def self.last_year
      where("#{self.table_name}.created_at >= ?", 1.year.ago).count
    end
  end
end
