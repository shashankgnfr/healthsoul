module Geocoding
  extend ActiveSupport::Concern

  included do
    attr_accessor :importing
    geocoded_by :geocode_address
    after_validation :geocode, :if => :should_geocode?
  end

  def full_street_address
    full_address = [address, city, state].reject(&:blank?)
    full_address = full_address.join(", ")
    full_address += " #{zip}" if zip.present?
    full_address += " #{country}" if country.present?
    full_address
  end

  def geocode_address
    simple_geocode? ? city_state_zip : full_street_address
  end

  def address_is_present?
    address.present? || city.present? || state.present? || country.present?
  end

  def should_geocode?
    return false if importing
    if self.new_record?
      latitude.blank? && longitude.blank? && address_is_present?
    else
      address_is_present?
    end
  end

  def city_state_zip
    [city, state, zip, country].reject(&:blank?).join(", ")
  end

  def lat_lng_present?
    latitude.present? && longitude.present?
  end

  def lat_lng
    [latitude, longitude].compact.join(', ')
  end
end
