module Friendly
  extend ActiveSupport::Concern

  included do
    validates :slug, uniqueness: true
    extend FriendlyId

    before_validation :check_slug
    friendly_id :slug_candidates, use: [:slugged, :finders]

    def self.find(id)
      if id.to_s.scan(/\D/).empty?
        super
      else
        find_by_slug(id)
      end
    end
  end

  def slug_candidates
    if self.class == Blog
      [:title, :title_and_sequence]
    else
      if [Doctor, Location, LocationImport].include? self.class
        self.country_slug = country.try(:parameterize)
        self.state_slug = state.try(:parameterize)
        self.city_slug = city.try(:parameterize)
      end
      if self.class == PracticeSpeciality
        slug_title = title.try(:parameterize)
        unless self.class.where(title_slug: slug_title).empty?
          self.title_slug = sequence_it('title_slug', title.try(:parameterize))
        else
          self.title_slug = slug_title
        end
      end
      [:name, :name_and_sequence]
    end
  end

  private
  def title_and_sequence
    sequence_it('slug', title.parameterize)
  end

  def name_and_sequence
    sequence_it('slug', name.try(:parameterize))
  end

  def sequence_it(key, slug)
    sequence = self.class.where("#{key} like ?", "#{slug}-%").count + 2
    "#{slug}-#{sequence}"
  end

  def check_slug

    if [Doctor, Location, LocationImport].include?(self.class)
      self.country_slug = country.try(:parameterize) if country_changed?
      self.state_slug = state.try(:parameterize) if state_changed?
      self.city_slug = city.try(:parameterize) if city_changed?
    end
    if self.class == PracticeSpeciality
      if title_changed?
        slug_title = title.try(:parameterize)
        unless self.class.where(title_slug: slug_title).empty?
          self.title_slug = sequence_it('title_slug', title.try(:parameterize))
        else
          self.title_slug = slug_title
        end
      end
    end
    if slug_changed?
      if slug.present?
        self.slug = slug.parameterize
      elsif self.class == Blog && title.present?
        self.slug = title.parameterize
      elsif self.attributes.keys.include?('name') && name.present?
        self.slug = name.parameterize
      end
    end
  end
end
