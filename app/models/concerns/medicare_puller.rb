module MedicarePuller

  def pull_name(their_dr)
    dr_string = dr_prefix(their_dr["pri_spec"])
    dr_string += "#{their_dr['frst_nm']&.capitalize} #{their_dr['lst_nm']&.capitalize}"
    dr_string
  end

  def pull_gender(api_gndr)
    return 'female' if api_gndr == 'F'
    return 'male' if api_gndr == 'M'
    ''
  end

  def pull_org_name(org_name = '')
    org_name&.split&.map(&:capitalize)&.join(' ')
  end

  def pull_zip(long_name)
    return '' unless long_name.present?
    long_name[0..4]
  end

  def pull_address(their_dr)
    address_for_dr = their_dr['adr_ln_1']&.split(' ')&.map(&:capitalize)&.join(' ')
    address_for_dr = their_dr['adr_ln_2'].present? ? address_for_dr + " #{their_dr['adr_ln_2']}" : address_for_dr
  end

  def dr_prefix(speciality_string)
    speciality_response = speciality_key(speciality_string)
    speciality_response.present? ? speciality_response[1] : ''
  end

  def speciality_key(speciality_string)
    speciality_hash = {
      "ADDICTION MEDICINE" => [48, 'Dr. '],
      "ALLERGY/IMMUNOLOGY" => [49, 'Dr. '],
      "ANESTHESIOLOGY" => [33, 'Dr. '],
      "AUDIOLOGIST" => [28, 'Dr. '],
      "CARDIAC ELECTROPHYSIOLOGY" => [5, 'Dr. '],
      "CARDIAC SURGERY" => [51, 'Dr. '],
      "CARDIOVASCULAR DISEASE (CARDIOLOGY)" => [3, 'Dr. '],
      "CERTIFIED NURSE MIDWIFE" => [380, ''],
      "CERTIFIED REGISTERED NURSE ANESTHETIST" => [381, ''],
      "CHIROPRACTIC" => [64, 'Dr. '],
      "CLINICAL NURSE SPECIALIST" => [382, ''],
      "CLINICAL PSYCHOLOGIST" => [383, ''],
      "CLINICAL SOCIAL WORKER" => [384, ''],
      "COLORECTAL SURGERY (PROCTOLOGY)" => [54, 'Dr. '],
      "CRITICAL CARE (INTENSIVISTS)" => [32, 'Dr. '],
      "DERMATOLOGY" => [23, 'Dr. '],
      "DIAGNOSTIC RADIOLOGY" => [39, 'Dr. '],
      "EMERGENCY MEDICINE" => [11, 'Dr. '],
      "ENDOCRINOLOGY" => [35, 'Dr. '],
      "GASTROENTEROLOGY" => [1, 'Dr. '],
      "GENERAL SURGERY" => [12, 'Dr. '],
      "GERIATRIC MEDICINE" => [29, 'Dr. '],
      "GERIATRIC PSYCHIATRY" => [40, 'Dr. '],
      "GYNECOLOGICAL ONCOLOGY" => [55, 'Dr. '],
      "HAND SURGERY" => [41, 'Dr. '],
      "HEMATOLOGY" => [31, 'Dr. '],
      "HEMATOLOGY/ONCOLOGY" => [31, 'Dr. '],
      "HOSPICE/PALLIATIVE CARE" => [56, 'Dr. '],
      "INFECTIOUS DISEASE" => [30, 'Dr. '],
      "INTERNAL MEDICINE" => [2, 'Dr. '],
      "INTERVENTIONAL PAIN MANAGEMENT" => [44, 'Dr. '],
      "INTERVENTIONAL RADIOLOGY" => [38, 'Dr. '],
      "MAXILLOFACIAL SURGERY" => [182, 'Dr. '],
      "MEDICAL ONCOLOGY" => [31, 'Dr. '],
      "NEPHROLOGY" => [10, 'Dr. '],
      "NEUROLOGY" => [15, 'Dr. '],
      "NEUROPSYCHIATRY" => [13, 'Dr. '],
      "NEUROSURGERY" => [46, 'Dr. '],
      "NUCLEAR MEDICINE" => [233, 'Dr. '],
      "NURSE PRACTITIONER" => [116, ''],
      "OBSTETRICS/GYNECOLOGY" => [14, 'Dr. '],
      "OCCUPATIONAL THERAPY" => [16, ''],
      "OPHTHALMOLOGY" => [24, 'Dr. '],
      "OPTOMETRY" => [17, 'Dr. '],
      "ORAL SURGERY" => [128, 'Dr. '],
      "ORTHOPEDIC SURGERY" => [21, 'Dr. '],
      "OSTEOPATHIC MANIPULATIVE MEDICINE" => [20, 'Dr. '],
      "OTOLARYNGOLOGY" => [22, 'Dr. '],
      "PAIN MANAGEMENT" => [34, 'Dr. '],
      "PATHOLOGY" => [63, 'Dr. '],
      "PEDIATRIC MEDICINE" => [8, 'Dr. '],
      "PERIPHERAL VASCULAR DISEASE" => [234, 'Dr. '],
      "PHYSICAL MEDICINE AND REHABILITATION" => [45, 'Dr. '],
      "PHYSICAL THERAPY" => [241, 'Dr. '],
      "PHYSICIAN ASSISTANT" => [385, 'Dr. '],
      "PLASTIC AND RECONSTRUCTIVE SURGERY" => [235, 'Dr. '],
      "PODIATRY" => [18, 'Dr. '],
      "PREVENTATIVE MEDICINE" => [353, 'Dr. '],
      "PSYCHIATRY" => [9, 'Dr. '],
      "PULMONARY DISEASE" => [36, 'Dr. '],
      "RADIATION ONCOLOGY" => [27, 'Dr. '],
      "REGISTERED DIETITIAN OR NUTRITION PROFESSIONAL" => [110, 'Dr. '],
      "RHEUMATOLOGY" => [43, 'Dr. '],
      "SPEECH LANGUAGE PATHOLOGIST" => [379, ''],
      "SURGICAL ONCOLOGY" => [50, 'Dr. '],
      "THORACIC SURGERY" => [52, 'Dr. '],
      "UNDEFINED PHYSICIAN TYPE (SPECIFY)" => [238, 'Dr. '],
      "UROLOGY" => [19, 'Dr. '],
      "VASCULAR SURGERY" => [42, 'Dr. ']
    }
    speciality_hash[speciality_string]
  end

  def set_dr_specialty(our_dr, speciality_string)
    speciality_response = speciality_key(speciality_string)
    if speciality_response.present? && PracticeSpeciality.where(id: speciality_response[0]).present?
      ds = DoctorSpeciality.new(doctor_id: our_dr.id, practice_speciality_id: speciality_response[0])
      ds.save if ds.valid?
    end
  end

  def set_practice_specialty(our_practice, speciality_string)
    speciality_response = speciality_key(speciality_string)
    if speciality_response.present? && PracticeSpeciality.where(id: speciality_response[0]).present?
      practice = LocationSpeciality.new(location_id: our_practice.id, practice_speciality_id: speciality_response[0])
      practice.save if practice.valid?
    end
  end


end
