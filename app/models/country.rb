class Country < ApplicationRecord
  has_many :patient_stories
  has_and_belongs_to_many :practice_specialities
  has_many :advertisements
end
