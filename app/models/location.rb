class Location < ApplicationRecord
  resourcify
  is_impressionable
  include Friendly
  belongs_to :location_type
  # has_many :location_specialities

  # def practice_specialities
  #   location_specialities.pluck(:practice_speciality).order(:name)
  # end
  has_many :location_specialities, dependent: :destroy
  has_many :practice_specialities, -> { order(:name) }, through: :location_specialities

  has_many :location_ratings, dependent: :destroy, class_name: "Rating", foreign_key: :location_id
  has_many :questions, as: :questionable, dependent: :destroy
  has_many :answers, through: :questions
  has_many :claim_requests, as: :claimable, dependent: :destroy
  has_many :business_hours, as: :hourable, dependent: :destroy, inverse_of: :hourable
  has_many :department_ratings, dependent: :destroy
  has_many :hospital_affiliations, dependent: :destroy
  has_many :doctors, through: :hospital_affiliations
  has_many :favorites, as: :favoriteable, dependent: :destroy
  has_many :awards, class_name: "Award", foreign_key: :location_id
  has_many :reviews, class_name: "PracticeReview", dependent: :delete_all, foreign_key: :location_id
  include PgSearch
  pg_search_scope :search, :against =>  :name, using: { tsearch: { prefix: true } }, order_within_rank: "name asc"
  include Geocoding
  validates :yext_id, uniqueness: true, :allow_blank => true
  validates :name, :location_type_id, presence: true
  validates :hwaid, uniqueness: {case_sensitive: false}, allow_blank: true, allow_nil: true
  validates :city, :state, :country, presence: true, if: Proc.new { |a| a.hospital? }
  validates :website, url: true, if: Proc.new { |a| a.website? }
  accepts_nested_attributes_for :business_hours, reject_if: :all_blank, allow_destroy: true

  normalize_attributes :hwaid, :photo_alt, :name, :city, :state, :country

  mount_uploader :photo, ProfilePhotoUploader
  enum yext_status: { available: 0, live: 1, claimed: 2, suppressed: 3, blocked: 4 }
  before_save :set_photo_alt, :set_continent_and_country_code

  scope :only_hospitals, -> { joins(:location_type).where(location_types: {name: 'Hospital'}) }
  scope :only_practices, -> { joins(:location_type).where(location_types: {name: 'Practice'}) }

  YEXT_HOSPITAL_IDS = [1135204, 1256, 1274, 1275, 1276, 1277, 128, 1325, 1325555, 1370]
  YEXT_PRACTICE_IDS = [1318, 1323, 1328, 1329, 1349, 1351, 1354, 1042, 1044, 1059495, 1061245, 1061246, 1062,1120591, 1120592, 1120593, 1120594, 1120596, 1120597, 1135517, 1135520, 1135521, 1135522, 1135524, 1135525, 1135527, 1135528, 1135529, 1135530, 1135531, 1135532, 1135533, 1135534, 1135535, 1135536, 1135537, 1135538, 1135539, 1135540, 1135541, 1135542, 1135543, 1135544, 1135545, 1135546, 1135547, 1135548, 1135549, 1135550, 1135551, 1135552, 1135553, 1135554, 1135556, 1135558, 1135559, 1135560, 1135561, 1135565, 1135566, 1135567, 1135568, 1135569, 1135570, 1135571, 1135572, 1135573, 1135574, 1135575, 1135576, 1234639, 1235, 1236, 1239, 1240, 1241, 1242, 1243, 1244, 1245, 1246, 1247, 1249, 1250, 1251, 1252, 1253, 1254, 1255, 1257, 1258, 1266, 1267, 1292, 1312, 1324, 1324880, 1326, 1327, 1330, 1337, 1338, 1339, 1341694, 1344572, 1350, 1352, 1353, 1355, 1356, 1357, 1358, 1359716, 1359746, 1359750, 1359761, 1359927, 1359935, 1359939, 1359940, 1359944, 1359997, 1360, 1361, 1362, 1363, 1364, 1366, 1366244, 1367, 1368, 1369, 1371, 163, 164, 166, 176, 177, 179, 181, 182,2116, 2144, 2145, 2146, 2147, 2148, 2150, 2151, 2152, 2153, 2154, 2155, 2156, 2157, 2158, 2159, 2161, 2162, 2163, 2164, 2166, 2167, 2173, 2174, 2175, 2176, 2177, 2178, 2179, 2180, 2181, 2182, 2183, 2184, 2185, 2253, 2254, 318, 320]

  HVBP_CHART_COLOR = ['red', 'red', 'yellow', 'green', 'green']
  HVBP_CHART_LABEL = ['Bad', 'Bad', 'Good', 'Better', 'Better']

  MEDICARE_CHART_COLOR = ['danger', 'warning', 'success']
  attr_accessor :analytic_chart

  def reload
    Location.find_by(id: self.id)
  end

  def ratings
    self.practice? ? reviews : location_ratings
  end

  def managers
    User.with_role(:moderator, self)
  end

  def doctors_by_ratings
    doctors.order("doctors.ratings_count DESC")
  end

  def has_reviews?
    overall > 0
  end

  def initials
    if name.present?
      name.split(" ").map{|w| w.first }.join.upcase
    end
  end

  def self.by_type(name)
    joins(:location_type).merge(LocationType.where(name: name))
  end

  def self.recently_reviewed
    joins(:location_ratings)
      .select("locations.*, max(ratings.created_at) as recent")
      .group("id")
      .order("recent DESC")
      .limit(6)
  end

  def self.recently_reviewed_not_for_country(co, number)
    recently_reviewed.where.not(country: co).limit(number)
  end

  def self.recently_reviewed_for_country(co, number)
    recently_reviewed.where(country_slug: co).limit(number)
  end

  def hospital?
    location_type.hospital? rescue nil
  end

  def insurance_company?
    location_type.insurance_company? rescue nil
  end

  def travel_insurance?
    location_type.travel_insurance? rescue nil
  end

  def practice?
    location_type.practice? rescue nil
  end

  def top_speciality
    practice_specialities.map(&:title_slug).first.try(:downcase) || 'specialist'
  end

  def practice_speciality_name
    practice_specialities.first&.name || 'specialist'
  end

  def healthcare_facilities?
    location_type.healthcare_facilities? rescue nil
  end

  def doctor?
    false
  end

  def map_type_slug
    if ['health-insurance', 'travel-health-insurance'].include? location_type.name.parameterize
      location_type.name.parameterize
    elsif self.practice?
      'doctor'
    else
      'hospital'
    end
  end

  def type_slug
    location_type.name.parameterize
  end

  def type_name
    @type_name ||= location_type.name
  end

  def in_state_rankings
    Location.select("locations.*, rank() OVER (ORDER BY overall DESC)").where(state: state)
  end

  def in_city_rankings
    Location.select("locations.*, rank() OVER (ORDER BY overall DESC)").where(city: city).where(state: state)
  end

  def in_country_rankings
    Location.select("locations.*, rank() OVER (ORDER BY overall DESC)").where(country: country)
  end

  def update_overall
    overall = ratings.rating_for(:overall_experience).round(2)
    update(overall: overall)

    # commenting this out for now. We're not using ranking by state and city since
    # there's not much data
    #update_rankings
  end

  def update_country_rankings
    in_country_rankings.each do |l|
      l.update(in_country_ranking: l.rank)
    end
  end

  def update_state_rankings
    in_state_rankings.each do |l|
      l.update(in_state_ranking: l.rank)
    end
  end

  def update_city_rankings
    in_city_rankings.each do |l|
      l.update(in_city_ranking: l.rank)
    end
  end

  def update_rankings
    if hospital?
      update_state_rankings
      update_city_rankings
    else
      update_country_rankings
    end
  end

  def image
    if photo?
      photo.thumb.url
    elsif place_photo.present?
      place_photo
    end
  end

  def entity_url
    if self.hospital?
      "/best-hospital-ratings/#{self.country_slug}/#{self.state_slug}/#{self.city_slug}/#{self.slug}/reviews"
    elsif self.practice?
      "/practices/#{self.top_speciality}/#{self.country_slug}/#{self.state_slug}/#{self.city_slug}/#{self.slug}/reviews"
    else
      "/#{self.slug}"
    end
  end

  def yext_json
    {
      parterId: id,
      status: yext_status&.upcase || 'AVAILABLE',
      name: name,
      sublocality: nil,
      address: address,
      state: state,
      zip: zip,
      countryCode: country_code,
      latitude: latitude,
      longitude: longitude,
      phone: phone,
      url: "https://www.healthsoul.com/#{slug}",
      websiteUrl: website,
      npi: nil,
      type: "HealthcareFacility",
      specialOffer: special_offer_json,
      ratings: ratings.rating_for(:overall_experience).round(2),
      total_reviews: ratings.count
    }
  end

  def rating_json
    {
      total: ratings.count,
      rating: ratings.rating_for(:overall_experience).round(2).to_f,
      maxRating: 5,
      reviews: review_list
    }
  end

  def review_list
    return [] unless ratings.present?
    ratings.map do |rating|
      {
        reviewId: rating.id,
        timestamp: rating.created_at,
        authorName: rating.user&.get_full_name || '',
        content: rating.comment,
        url: "https://healthsoul.com#{entity_url}",
        rating: rating.overall_experience.to_f
      }
    end
  end

  def special_offer_json
    {
      message: special_offer_message.present? ? special_offer_message : '',
      url: special_offer_url.present? ? special_offer_url : ''
    }
  end

  def parse_yext_order_body(params)
    if params[:address].present?
      address_attributes = params[:address]
      self.address = address_attributes[:address] if address_attributes[:address].present?
      self.zip = address_attributes[:postalCode] if address_attributes[:postalCode].present?
      if address_attributes[:countryCode].present?
        country_to_set = address_attributes[:countryCode] == 'US' ? 'USA' : address_attributes[:countryCode]
        self.country_code = country_to_set
        self.country = country_to_set
      end
      self.state = address_attributes[:state] if address_attributes[:state].present?
      self.city = address_attributes[:city] if address_attributes[:city].present?
    end
    self.name = params[:name] if params[:name].present?
    self.description = params[:description] if params[:description].present?
    self.yext_id = params[:yextId] if params[:yextId].present?
    if params[:phones].present?
      params[:phones].each{ |phone_attr| self.phone = phone_attr[:number][:number] if phone_attr[:type] == "MAIN" }
    end
    if params[:emails].present?
      params[:emails].each{ |email_attr| self.email = email_attr[:address] if email_attr[:description] == "Info" }
    end
    if params[:geoData].present?
      self.latitude = params[:geoData][:displayLatitude]
      self.longitude = params[:geoData][:displayLongitude]
    end
    if params[:urls].present?
      params[:urls].each{ |their_url| self.website = their_url[:url] if their_url["type"] == 'WEBSITE' }
    end
    if params[:specialOffer].present?
      self.special_offer_message = params[:specialOffer][:message]
      self.special_offer_url = params[:specialOffer][:url]
    end
    if params[:categories].present?
      params[:categories].each do |category|
        this_location_type = LocationType.find_by_yext_id(params[:categories].first["id"])
        this_location_type = LocationType.find_by_name(params[:categories].first["name"]) unless this_location_type.present?
        unless this_location_type.present?
          if YEXT_HOSPITAL_IDS.include?(params[:categories].first["id"].to_i)
            this_location_type = LocationType.find_by_name('Hospital')
          elsif YEXT_PRACTICE_IDS.include?(params[:categories].first["id"].to_i)
            this_location_type = LocationType.find_by_name('Practice')
          else
            this_location_type = LocationType.create(name: params[:categories].first["name"], yext_id: params[:categories].first["id"]) unless this_location_type.present?
          end
        end
        self.location_type = this_location_type if this_location_type.present?
      end
    end

    self.save
    if params[:hours].present?
      params[:hours].each do |day_hour|
        current_business_hours = business_hours.where(day: day_hour[:day].downcase)
        if day_hour[:intervals].present?
          current_business_hours.destroy_all if current_business_hours.present?
          day_hour[:intervals].each do |their_interval|
            self.business_hours.create(day: day_hour[:day].downcase, start_time: their_interval[:start], end_time: their_interval[:end])
          end
        else
          if current_business_hours.present?
            current_business_hours.update_all(closed: true)
          else
            self.business_hours.create(day: day_hour[:day].downcase, closed: true)
          end
        end
      end
    end
  end

  def get_award_type
     if overall >= 3 && overall < 4
         award = "silver"
     elsif overall >= 4 && overall < 5
         award = "gold"
     elsif overall == 5
         award = "diamond"
     end
     return award
  end

  # we already have sub_type as field - also practice_specialities - thus which need to preserve.
  # def sub_type
  #   practice_specialities.pluck(:name).uniq.join(', ')
  # end

  private

  def set_photo_alt
    if !self.new_record? && self.name_changed?
      self.photo_alt = "#{name} | HealthSoul"
    # this condition never execute until they remove validate presence on name. But for safety purpose it added.
    elsif !self.new_record? && self.location_type_id_changed? && !name.present?
      self.photo_alt = "#{self.location_type.name} | HealthSoul"
    elsif !photo_alt.present?
      self.photo_alt = name.present? ? "#{name} | HealthSoul" : "#{self.location_type.name} | HealthSoul"
    end
  end

  def set_continent_and_country_code
    c = Country.where(name: self.country).first
    self.continent = c.try(:continent)
    self.country_code = c.try(:alpha3)
  end
end
