class Question < ApplicationRecord
  paginates_per 20
  include ReviewCalc
  include Analytics
  belongs_to :user
  belongs_to :questionable, polymorphic: true
  has_many :answers, dependent: :destroy
  has_many :votes, as: :voteable, dependent: :destroy

  validates :body, presence: true

  def location
    questionable
  end
end
