class Vote < ApplicationRecord
  belongs_to :voteable, polymorphic: true
  belongs_to :user
  include ReviewCalc

  enum status: [ :unhelpful, :helpful ]

  def self.helpful_rank
    helpful.count - unhelpful.count
  end

  validates :user_id, :uniqueness => { :scope => [:voteable_type, :voteable_id] }
end
