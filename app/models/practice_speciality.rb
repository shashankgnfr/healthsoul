class PracticeSpeciality < ApplicationRecord
  paginates_per 20
  include Friendly
  has_many :doctor_specialities, dependent: :destroy
  has_many :doctors, through: :doctor_specialities
  has_and_belongs_to_many :conditions
  has_and_belongs_to_many :procedures
  has_and_belongs_to_many :countries
  has_many :location_specialities, dependent: :destroy
  has_many :locations, through: :location_specialities

  has_many :practice_specialities_faqs, -> {order(:faq_title)}

  validates :title, :title_slug, presence: true, uniqueness: true
  validates :name, :slug, presence: true, uniqueness: true

  def destroy
    if doctors.present?
      name = self.name
      sql = "specialities = replace(specialities, '#{name}', '')"
      doctors.update_all(sql)
    end
    super
  end

  def self.update_common_speciality_tag
    common_names = ["Pediatrics","Family Medicine","Internal Medicine","Chiropractor","Dentist","Allergy/Immunology","Cardiology (Heart)","Dermatology (Skin)","Diet and Nutrition",
    "Endocrinology","ENT","Gastroenterology","Hematology/Oncology (Cancer)", "Infectious Disease","Nephrology (Kidney)","Neurology","Obstetrics/Gynecology","Ophthalmology","Orthopedics",
    "Plastic & Reconstructive Surgery","Podiatry","Psychiatry","Pulmonary Disease","Rheumatology","General Surgery"]

    # Iterate through every PracticeSpeciality and, if the record's name is on the list of common specilities, and updates
    # its flag accordingly
    PracticeSpeciality.all.each do |ps|
      if common_names.include? ps.name
        ps.update_attribute(:common_speciality, true)
      else
        ps.update_attribute(:common_speciality, false)
      end
    end
  end
end
