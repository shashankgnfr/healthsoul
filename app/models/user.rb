include WaveCell
class User < ApplicationRecord
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and
  devise :database_authenticatable, :registerable, :omniauthable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable, :reset_password_keys => [:email, :phone_number]

  validates :username, uniqueness: true, allow_nil: true
  validates_format_of :first_name, :last_name, :with => /\A([^0-9]*)\z/

  validates :phone_number, uniqueness: true, allow_blank: true
  validates :email, uniqueness: true, allow_blank: true

  # Validate that the non country code part of the phone number is exactly 10 digits long.
  # this also ensures each character is a number
  validates_format_of :phone_body, :with => /\d{10}/, allow_blank: true

  # Validate that the country code of the phone number is either the US or India
  validates :phone_country_code, :inclusion=> { :in => ["01", "91"] }, allow_blank: true

  # Validate that the hidden field which will be checked for global uniqueness is truely the sum of the country code
  # and the 10 digit body. Since the phone number will only be verified if an email is not present, and since
  # a phone number is required to register if email is not present checking that email is not present fills the same role
  # as chcking that the phone number is present, but avoids false validation failure on users with old, unformatted phone
  # numbers.
  validate :phone_number_is_sum_of_country_code_and_body, :unless => :email?

  validates_presence_of :email, :unless => :phone_number?
  validates_presence_of :phone_number, :unless => :email?

  before_commit :set_username
  #after_commit :send_welcome_email_if_email_entered_later
  after_validation :geocode, :if => :address_is_present?

  geocoded_by :geocode_address

  attr_accessor :role_name, :role_resource_id, :role_resource_type
  attr_accessor :current_identity

  has_many :identities, dependent: :destroy
  has_many :shared_blogs, -> { order(updated_at: :desc) }, class_name: "SharedBlog", foreign_key: :user_id,  dependent: :destroy

  enum sex: [ :male, :female ]
  enum age_group: ["19-25",'26-35','36-45','46-55','56-65','>65']
  enum age_group_range: [19..25, 26..35, 36..45, 46..55, 56..65, 65..150]

  AVAILABLE_TITLES = ["Owner", "Manager", "Customer Service", "Director"]

  has_many :ratings, dependent: :destroy
  has_many :questions, dependent: :destroy
  has_many :answers, dependent: :destroy
  has_many :faqs, dependent: :destroy 
  has_many :votes, dependent: :destroy
  has_many :claim_requests, dependent: :destroy
  has_many :doctor_reviews, dependent: :destroy
  has_many :practice_reviews, dependent: :destroy
  has_many :user_referrals, dependent: :destroy
  has_many :referred_users, through: :user_referrals
  has_many :favorites, dependent: :destroy
  has_many :appointments, dependent: :destroy
  has_one :referral, class_name: 'UserReferral', foreign_key: :referred_user_id
  has_one :referring_user, through: :referral, source: :user
  has_one :event_member
  normalize_attributes :first_name, :last_name, :city, :state, :co, :zip
  mount_uploader :photo, ProfilePhotoUploader

  # Since we are calculating a phone number on the client side from the user entered country code and phone body, we will
  # verify that the phone number is equal to the fields the user actually entered.
  def phone_number_is_sum_of_country_code_and_body
     if [phone_country_code, phone_body].join('') != phone_number
       errors.add(:base, 'Phone Number is Not Valid')
     end
  end

  def get_full_name
    return "#{self.first_name} #{self.last_name}"
  end

  def name_for_welcome_mailer
    fullname = [self.first_name, self.last_name].reject{|n| n.blank?}.compact
    fullname.present? ? fullname.join(', ') : self.email 
  end

  # To send an sms via twillio, we need to use the phone number in e164 format. This means dropping the 0 at the begining of
  # the US country code, and adding a "+" to the begining.
  def phone_number_in_e164_format
    if phone_number.first=="0"
      # Drop the first character if it is a 0, the prepend a plus sign.
      e164_phone_number = "+#{phone_number[1..-1]}"
    else
      # Otherwise just return the number with the prepended plus sign.
      e164_phone_number = "+#{phone_number}"
    end

    return e164_phone_number
  end

  # Builds a new cannonical phone number formatted for display out of the user entered phone_body, and country code.
  def display_phone_number
    if phone_body and phone_country_code
      return "+#{phone_country_code}-#{phone_body[0..2]}-#{phone_body[3..5]}-#{phone_body[6..9]}"
    else
      return phone_number
    end
  end

  def can_make_appointment
    return (first_name.present? and last_name.present? and date_of_birth.present? and (email.present? or phone_number.present?))
  end

  def is_provider
    # At least one of two criteria must be met to consider an account a provider account
    # 1) The user must have a moderator role (gained through a claim request) on at least one healthcare entity (doctor, hospital, health insurance).
    # 2) The user must have signed up through the provider sign in form (they will appear in the provider index, but they still need to submit a claim request
    # to manage their healthcare entity).
    # Priyanka: Buggy line - roles.include?(:moderator).
    return (marked_as_provider or roles.include?(:moderator))
  end

  def geocode_address
    "#{city}, #{state}"
  end

  def total_reviews
    ratings.size + doctor_reviews.size + practice_reviews.size
  end

  def total_shared_blogs
    shared_blogs.sum(:shared_count)
  end

  def total_shared_points
    [shared_blogs.sum(:shared_count), 5].min * 5
  end

  def display_name
    username
  end

  def all_ratings
    ratings + doctor_reviews + practice_reviews
  end

  # Get a user's rank based on their total_points
  def badge_rank
    if self.total_points>=500
      return "Expert"
    elsif self.total_points>=250
      return "Guide"
    elsif self.total_points >=100
      return "Advisor"
    elsif self.total_points >= 25
      return "Reviewer"
    else
      return nil
    end
  end

  def self.send_reset_password_instructions(attributes={})
    # Overiding http://www.rubydoc.info/github/plataformatec/devise/Devise/Models/Recoverable/ClassMethods
    # This is how password resets are sent via both email and phone number.
    if attributes[:email].present?
      # If the user entered an email, take the normal route through the function
      recoverable = find_or_initialize_with_errors([:email], attributes, :not_found)
      # If the user was found by email, then recoverable will be persisted, if it was not, recoverable will
      # not be persisted and an email will not be sent.
      recoverable.send_reset_password_instructions if recoverable.persisted?
    elsif attributes[:phone_number].present?
      # If the user entered a phone number, do the same thing but find by phone number instead, and pass a
      # flag to the instance method send_reset_password_instruction to tell it to send a text.
      recoverable = find_or_initialize_with_errors([:phone_number], attributes, :not_found)
      recoverable.send_reset_password_instructions(reset_by_email=false) if recoverable.persisted?
    end
    recoverable
  end

  def send_reset_password_instructions reset_by_email=true
    # Overriding http://www.rubydoc.info/github/plataformatec/devise/Devise%2FModels%2FRecoverable%3Asend_reset_password_instructions
    # Set a token. If reset by email is true (as it is by default) follow the normal path through the method and return the token.
    token = set_reset_password_token
    if reset_by_email
      send_reset_password_instructions_notification(token)
    else
      # If reset by email is false (which will only occur if recoverable was found via phone nummber), then send a text message instead of an emaiil.
      if Rails.env.development?
        # Since we are overriding a model method, we don't have access to route helpers by default. For dev get the correct route on the host localhost:3000
        send_wavecell_password_reset_message_to_phone_number(phone_number_in_e164_format, Rails.application.routes.url_helpers.edit_user_password_url(:host=>DEVELOPMENT_HOST, :reset_password_token=>token))
      else
        # Otherwise get the route on the host www.healthsoul.com
        send_wavecell_password_reset_message_to_phone_number(phone_number_in_e164_format, Rails.application.routes.url_helpers.edit_user_password_url(:host=>PRODUCTION_HOST, :reset_password_token=>token))
      end
    end
    token
  end

  def self.find_for_oauth(auth, signed_in_resource = nil)

    # Get the identity and user if they exist
    identity = Identity.find_for_oauth(auth)

    # If a signed_in_resource is provided it always overrides the existing user
    # to prevent the identity being locked with accidentally created accounts.
    # Note that this may leave zombie accounts (with no associated identity) which
    # can be cleaned up at a later date.
    user = signed_in_resource ? signed_in_resource : identity.user

    # Create the user if needed
    if user.nil?

      # Get the existing user by email if the provider gives us a verified email.
      # If no verified email was provided we assign a temporary email and ask the
      # user to verify it on the next step via UsersController.finish_signup
      email = auth.info.email
      user = User.where(:email => email).first if email

      # Create the user if it's a new registration
      if user.nil?
        user = User.new(
          email: email,
          password: Devise.friendly_token[0,20]
        )
      end
    end

    # Associate the identity with the user if needed
    if identity.user != user
      identity.user = user
      user.current_identity = identity
      identity.save!
    end
    user
  end

  def is_admin?
    self.roles.pluck(:name).include? 'admin'
  end

  private

  # Used to make a wavecell api call to send a message to a phone number containing a link to a password reset form.
  def send_wavecell_password_reset_message_to_phone_number(user_phone_number, message_url)
    begin
          WaveCell.wave_cell_send_sms(user_phone_number, "Reset your password at #{message_url}")
    rescue
      return false
    end
  end

  def email_required?
    true unless phone_number?
  end
  def set_username
    if username.blank?
      uname = ""
      if email?
        u = email.split("@")
        uname = u.first[0..2].to_s
        uname = uname + rand(100..999).to_s
        while User.exists?(username: uname) do
          uname = uname + rand(100..999).to_s
        end
      else
        # If the user registered via phone number instead of email, use three random letters instead of the first three letters of an email.
        uname = (0...2).map { (65 + rand(26)).chr }.join
        uname = uname + rand(100..999).to_s
        while User.exists?(username: uname) do
          uname = uname + rand(100..999).to_s
        end
      end
      self.username = uname
      save
    end
  end

  def address_is_present?
    city.present? && state.present?
  end

  def send_welcome_email_if_email_entered_later
      if self.email.present? && self.previous_changes["email"].present? && self.previous_changes["email"][0].blank?
            if self.marked_as_provider
                 UserMailer.send_welcome_email_to_new_provider(self.email).deliver
            else
                 UserMailer.send_welcome_mailer_to_new_user(self).deliver
            end
      end
  end
end
