class Faq < ApplicationRecord
  belongs_to :user
  has_many :answers, class_name: "FaqAnswer", dependent: :destroy
  acts_as_taggable
end
