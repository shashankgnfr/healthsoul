class ClaimRequest < ApplicationRecord
  belongs_to :user
  # Author is the person who added this claim request - inshort it's login user who claim profile for self or behalf or other.
  belongs_to :author, class_name: "User", foreign_key: "created_by"
  accepts_nested_attributes_for :user,:update_only => true
  belongs_to :claimable, polymorphic: true, optional: true
  belongs_to :doctor, foreign_type: 'Doctor', foreign_key: 'claimable_id'
  belongs_to :location, foreign_type: 'Location', foreign_key: 'claimable_id'
  validates :user_id, uniqueness: { scope: [:claimable_id, :claimable_type], message: "You have already created a claim request" }, allow_nil: true
  validates :user_id, :description, :claimable_type, :claimable_id, :created_by, presence: true
  belongs_to :medical_student, foreign_type: 'MedicalStudent', foreign_key: 'claimable_id'

  scope :email_search, -> (query) { joins(:user).where("(REPLACE (lower(users.email), ' ', '' ) like ?)", "%#{query}%") }   

  def doctor
    return unless claimable_type == "Doctor"
    super
  end

   def location
     return unless claimable_type == "Location"
     super
   end

  def doctor?
    claimable_type == "Doctor"
  end

  def location?
    claimable_type == "Location"
  end

  def medical_student?
    claimable_type == "MedicalStudent"
  end

  def medical_student
    return unless claimable_type == "MedicalStudent"
    super
  end
end
