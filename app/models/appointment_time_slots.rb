class AppointmentTimeSlots
  attr_reader :appointment, :provider

  def initialize(appointment)
    @appointment = appointment
    @provider = appointment.provider
  end

  def time_slots
    (start_time.to_i..end_time.to_i).step(interval.to_i).map{|t| Time.zone.at(t) }
  end

  def start_time
    Time.current.change(hour: 9)
  end

  def end_time
    Time.current.change(hour: 18)
  end

  def interval
    1.hour
  end

end
