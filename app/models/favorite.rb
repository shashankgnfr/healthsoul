class Favorite < ApplicationRecord
  belongs_to :user
  belongs_to :favoriteable, polymorphic: true
  validates_presence_of :user, :favoriteable
end
