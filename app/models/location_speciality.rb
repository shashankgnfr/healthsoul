class LocationSpeciality < ApplicationRecord
  belongs_to :location
  belongs_to :practice_speciality
  validates_uniqueness_of :practice_speciality_id, :scope => :location_id
end
