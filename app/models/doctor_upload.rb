class DoctorUpload < ApplicationRecord
  def self.imported_count
    where(imported: true).count
  end
end
