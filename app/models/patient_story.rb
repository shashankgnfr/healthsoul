class PatientStory < ApplicationRecord
  belongs_to :country
  mount_uploader :image, ImageUploader
  mount_uploader :video, VideoUploader
  mount_uploader :word_document, DocUploader
  
  validates_integrity_of :image, :word_document, :video
  validates :name, :email, :city, :country_id, :description, :terms_of_service, :title, presence: true
  validates :title, uniqueness: { case_sensitive: false }
  validates :terms_of_service, acceptance: true

  normalize_attributes :comments
  
end
