class Event < ApplicationRecord
  paginates_per 20
  has_many :event_members
  validates :title, :start_date, :logo, :venue, presence: true
  validates :title, uniqueness: true
  validates_associated :event_members
  mount_uploader :logo, EventLogoUploader
  mount_uploader :banner_image, EventBannerUploader
  validate :active_event 
  normalize_attributes :title, :organizer, :venue, :description, :exhibit, :promotion, :event_members, :referral_code

  def date
    if start_date.present?
      if end_date.nil?
        start_date.strftime('%B %d, %Y')
      elsif start_date.year == end_date.year
        "#{start_date.strftime('%B %d')} - #{end_date.strftime('%d, %Y')}"
      else
        "#{start_date.strftime('%B %d, %Y')} - #{end_date.strftime('%B %d, %Y')}"
      end
    end
  end

  def active_event
    if active == true
      currently_active_event = Event.where.not(id: id).where(active: true).count
      errors.add(:active, "At a time only one event will be active. Thus, deactivate current event before activating this.") if currently_active_event != 0
    end
  end
end
