class DoctorOrganization < ApplicationRecord
  belongs_to :doctor
  has_many :business_hours, as: :hourable, dependent: :destroy, inverse_of: :hourable
  accepts_nested_attributes_for :business_hours, reject_if: :all_blank, allow_destroy: true
  validates :website, url: true, if: Proc.new { |a| a.website? }
end
