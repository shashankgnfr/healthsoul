class MedicalStudent < ApplicationRecord
  include Friendly
  has_many :claim_requests, as: :claimable, dependent: :delete_all
  has_one :event_member, as: :resource, dependent: :nullify

  validates :name, :email, :professional_school, :degree_type, :graduation_year, presence: true

  validates :email, uniqueness: { scope: [:name, :professional_school] }


  normalize_attributes :name, :email, :professional_school, :degree_type, :graduation_year, :publications, :care_philosophy

  mount_uploader :photo, ProfilePhotoUploader
end
