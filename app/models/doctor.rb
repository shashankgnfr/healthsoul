require 'resolv-replace'
class Doctor < ApplicationRecord
  resourcify
  is_impressionable
  searchkick locations: [:location]
  include Friendly
  belongs_to :med_school
  has_many :doctor_specialities, dependent: :delete_all
  has_many :practice_specialities, -> { order(:name) }, through: :doctor_specialities
  has_many :reviews, class_name: "DoctorReview", dependent: :delete_all
  has_many :questions, as: :questionable, dependent: :delete_all
  has_many :answers, through: :questions
  has_many :claim_requests, as: :claimable, dependent: :delete_all
  has_many :hospital_affiliations, dependent: :delete_all
  has_many :hospitals, through: :hospital_affiliations, source: :location
  has_many :blogs
  has_many :appointments, as: :provider, dependent: :destroy
  has_many :favorites, as: :favoriteable, dependent: :destroy
  has_many :business_hours, as: :hourable, dependent: :destroy, inverse_of: :hourable
  has_many :additional_locations, class_name: "DoctorOrganization", dependent: :delete_all
  has_many :health_scores,  dependent: :delete_all
  has_many :health_score_details,  dependent: :delete_all
  has_many :awards, class_name: "Award", foreign_key: :location_id
  has_one :event_member, as: :resource, dependent: :nullify
  has_and_belongs_to_many :languages, -> {order(:title)}
  has_and_belongs_to_many :board_certifications, -> {order(:certification)}
  enum gender: [:male, :female]
  enum academic_affiliation: [:no_affiliation ,:clinical_instructor, :researcher ,:assistant_professor ,:associate_professor, :professor]
  before_save :cache_specialities, :handle_med_school, :set_npi, :set_photo_alt, :set_continent_and_country_code

  include Geocoding
  include Yextable
  accepts_nested_attributes_for :business_hours, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :additional_locations, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :health_scores, allow_destroy: true
  accepts_nested_attributes_for :health_score_details, reject_if: :all_blank, allow_destroy: true
  validates :npi, uniqueness: true, :allow_blank => true, allow_nil: true
  validates :yext_id, uniqueness: true, :allow_blank => true
  validates :name, presence: true
  validates :slug, uniqueness: true
  validates :name, :city, :state, :country, presence: true
  validates :website, url: true, if: Proc.new { |a| a.website? }

  attr_accessor :med_school_other, :analytic_chart

  mount_uploader :photo, ProfilePhotoUploader
  enum yext_status: { available: 0, live: 1, claimed: 2, suppressed: 3, blocked: 4 }
  normalize_attributes :photo_alt, :name, :city, :state, :country
  scope :search_import, -> { includes(:practice_specialities) }
    
  def search_data
    {
      name: name,
      location: {lat: latitude, lon: longitude},
      practice_specialities: practice_specialities.ids,
      stars: overall.to_i,
      claimed: claimed?,
      reviews: reviews.count,
      city_slug: city_slug,
      country_slug: country_slug,
      state_slug: state_slug,
      npi: npi,
      is_claimed: is_claimed 
    }
  end

  # To send an sms via twillio, we need to use the phone number in e164 format. This means dropping the 0 at the begining of
  # the US country code, and adding a "+" to the begining.
  def phone_number_in_e164_format
    if phone.first=="0"
      # Drop the first character if it is a 0, the prepend a plus sign.
      e164_phone_number = "+#{phone[1..-1]}"
    else
      # Otherwise just return the number with the prepended plus sign.
      e164_phone_number = "+#{phone}"
    end

    return e164_phone_number
  end

  def self.recently_reviewed_for_country(co, number)
    recently_reviewed.select("doctors.country").group("country").where("doctors.country_slug = ?",  co).limit(number)
  end

  def self.recently_reviewed
    joins(:reviews)
      .select("doctors.*, max(doctor_reviews.created_at) as recent")
      .group("id")
      .order("recent DESC")
      .limit(6)
  end

  def only_hospitals
    hospitals.only_hospitals
  end

  def only_practices
    hospitals.only_practices
  end

  def managers
    User.with_role(:moderator, self)
  end

  def doctor?
    true
  end

  def practice?
    false
  end

  def type_name
    "Doctor"
  end

  def update_overall
    overall = reviews.rating_for(:overall_experience).round(2)
    count = reviews.size
    update(overall: overall, ratings_count: count)
  end

  def country
    super || "USA"
  end

  def image
    nil
  end

  def initials
    name.split(" ").last rescue nil
  end

  def sub_type
    practice_specialities.pluck(:name).uniq.join(', ')
  end

  def top_speciality
    practice_specialities.map(&:title_slug).first.try(:downcase) || 'specialist'
  end

  def primary_speciality
    practice_specialities.map(&:name).first
  end

  def collect_specialities
    practice_specialities.map(&:name)
  end

  def look_for_gender
    gender.present? ? gender : other_gender
  end

  def type_slug
    'doctor'
  end
  alias_method :map_type_slug, :type_slug

  def has_reviews?
    ratings_count > 0
  end

  def ratings
    reviews
  end

  def hospital?
    false
  end

  def blogs_by_specialities
    Blog.tagged_with(practice_specialities.map(&:name), :any => true)
  end

  def entity_url
    if top_speciality.present? && country_slug.present? && state_slug.present?
      "/best-doctor-ratings/#{top_speciality}/#{country_slug}/#{state_slug}/#{city_slug}/#{slug}"
    else
      "doctors/#{slug}"
    end
  end

  def get_award_type
     if overall >= 3 && overall < 4
         award = "silver"
     elsif overall >= 4 && overall < 5
         award = "gold"
     elsif overall == 5
         award = "diamond"
     end
     return award
  end

  def json_for_search
    {name: name, slug: entity_url, primary_speciality: primary_speciality, city: city}
  end

  private

  def handle_med_school
    if med_school_other.present?
      self.med_school = MedSchool.find_or_create_by(name: med_school_other)
    end
  end

  def cache_specialities
    self.specialities = collect_specialities.join(", ") if doctor_specialities.present?
  end

  def set_npi
    if self.npi.blank?
      self.npi = nil
    end
  end

  def set_photo_alt
     if (!self.new_record? && self.name_changed?) || (self.new_record? && name.present?) || (photo_alt == 'Doctor | HealthSoul' && name.present?)
      self.photo_alt = "#{name} | HealthSoul"
    end
  end

  def set_continent_and_country_code
    c = Country.where(name: self.country).first
    self.continent = c.try(:continent)
    self.country_code = c.try(:alpha3)
  end
end
