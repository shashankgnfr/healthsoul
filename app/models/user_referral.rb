class UserReferral < ApplicationRecord
  include ReviewCalc
  belongs_to :user
  belongs_to :referred_user, class_name: 'User'
  validates :referred_user_id, uniqueness: true
end
