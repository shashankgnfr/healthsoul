class DoctorSpeciality < ApplicationRecord
  belongs_to :doctor
  belongs_to :practice_speciality
  validates_uniqueness_of :practice_speciality_id, :scope => :doctor_id
  after_commit :reindex_doctor
  
  private
  
  def reindex_doctor
    doctor.reindex if doctor.present?
  end
  
end
