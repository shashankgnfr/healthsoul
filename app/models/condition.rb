class Condition < ApplicationRecord
  has_and_belongs_to_many :practice_specialities
  paginates_per 20

  
  # For each Speciality we genearte a new practice speciality if one does not exist, and then associate that speciality with
  # the condition at the head of each row.
  	CONDITIONS = [["Asthma", "Allergy/Immunology", nil, nil, nil],
				["Asthma in Children", "Allergy/Immunology", "Pediatrics", nil, nil],
				["Food Allergy", "Allergy/Immunology", nil, nil, nil],
				["Milk Allergy", "Allergy/Immunology", nil, nil, nil],
				["Lactose Intolerance", "Allergy/Immunology", nil, nil, nil],
				["Peanut Allergy", "Allergy/Immunology", nil, nil, nil],
				["Egg Allergy", "Allergy/Immunology", nil, nil, nil],
				["Latex Allergy", "Allergy/Immunology", nil, nil, nil],
				["Pollen Allergy", "Allergy/Immunology", nil, nil, nil],
				["Poison Ivy", "Allergy/Immunology", nil, nil, nil],
				["Dermatitis", "Allergy/Immunology", nil, nil, nil],
				["Eczema", "Allergy/Immunology", nil, nil, nil],
				["Rash", "Allergy/Immunology", nil, nil, nil],
				["Hives", "Allergy/Immunology", nil, nil, nil],
				["Urticaria", "Allergy/Immunology", nil, nil, nil],
				["Assistive Listening Devices", "Audiology", nil, nil, nil],
				["Hearing Aids", "Audiology", nil, nil, nil],
				["Hearing Loss", "Audiology", nil, nil, nil],
				["Tinnitus", "Audiology", "Internal Medicine", "Family Practice", nil],
				["Acute Myocardial Infarction", "Cardiology", nil, nil, nil],
				["Angina", "Cardiology", nil, nil, nil],
				["Aortic Aneurysm", "Cardiology", nil, nil, nil],
				["Aortic stenosis", "Cardiology", nil, nil, nil],
				["Arrhythmias", "Cardiology", "Electrophysiology", nil, nil],
				["Atrial fibrillation", "Cardiology", "Internal Medicine", "Electrophysiology", nil],
				["Cardiac risk assesment", "Cardiology", nil, nil, nil],
				["Chemotherapy and Cardiac Toxicity", "Cardiology", nil, nil, nil],
				["Chest pain", "Cardiology", nil, nil, nil],
				["Coronary artery disease", "Cardiology", nil, nil, nil],
				["Heart Attack", "Cardiology", nil, nil, nil],
				["Heart Failure (Cardiomyopathy)", "Cardiology", nil, nil, nil],
				["Hyperlipidemia", "Cardiology", nil, nil, nil],
				["Hypertension", "Cardiology", nil, nil, nil],
				["Leaky heart valve", "Cardiology", nil, nil, nil],
				["Mitral regurgitation", "Cardiology", nil, nil, nil],
				["Mitral stenosis", "Cardiology", nil, nil, nil],
				["Mitral valve prolapse", "Cardiology", nil, nil, nil],
				["Palpitations", "Cardiology", nil, nil, nil],
				["Peripheral vascular disease", "Cardiology", "Internal Medicine", "Family Practice", nil],
				["Shortness of breath", "Cardiology", "ENT", nil, nil],
				["Skipped beats", "Cardiology", nil, nil, nil],
				["Supraventricular Tachyardia", "Cardiology", "Internal Medicine", "Family Practice", nil],
				["Syncope", "Cardiology", "Internal Medicine", "Family Practice", nil],
				["Valve heart disease", "Cardiology", nil, nil, nil],
				["Women and Cardiovascular Disorders", "Cardiology", nil, nil, nil],
				["Rheumatic heart disease", "Cardiology", nil, nil, nil],
				["Heart murmur", "Cardiology", nil, nil, nil],
				["Stroke", "Cardiology", nil, nil, nil],
				["Aortic regurgitation", "Cardiology", nil, nil, nil],
				["Tricuspid regurgitation", "Cardiology", nil, nil, nil],
				["Hole in the Heart", "Cardiology", nil, nil, nil],
				["PFO (Patent Foramer Ovale)", "Cardiology", nil, nil, nil],
				["Ventricular Septal Defect (VSD)", "Cardiology", nil, nil, nil],
				["Atrial Septal Defect (ASD)", "Cardiology", nil, nil, nil],
				["Anorectal Disease", "Colon and Rectal Surgery", nil, nil, nil],
				["Colon Cancer", "Colon and Rectal Surgery", nil, nil, nil],
				["Colonoscopy", "Colon and Rectal Surgery", nil, nil, nil],
				["Diverticulitis", "Colon and Rectal Surgery", nil, nil, nil],
				["Hemmorhoids", "Colon and Rectal Surgery", nil, nil, nil],
				["Laparoscopic & Robotic Colon & Rectal Surgery", "Colon and Rectal Surgery", nil, nil, nil],
				["Rectal Cancer", "Colon and Rectal Surgery", nil, nil, nil],
				["Rectal Fsitula", "Colon and Rectal Surgery", nil, nil, nil],
				["Robotic and Laparoscopic Colon & Rectal Surgery", "Colon and Rectal Surgery", nil, nil, nil],
				["Surgical Treatment of Crohn's Disease", "Colon and Rectal Surgery", nil, nil, nil],
				["Surgical Treatment of IBD", "Colon and Rectal Surgery", nil, nil, nil],
				["Dental care", "Dentistry", nil, nil, nil],
				["Oral Health", "Dentistry", nil, nil, nil],
				["Bad breath", "Dentistry", nil, nil, nil],
				["Brushing", "Dentistry", nil, nil, nil],
				["Fluoride in water", "Dentistry", nil, nil, nil],
				["Impacted Teeth", "Dentistry", nil, nil, nil],
				["Dental Caries", "Dentistry", nil, nil, nil],
				["Cavities", "Dentistry", nil, nil, nil],
				["Temporomandibular joint disease", "Dentistry", nil, nil, nil],
				["Oral cancer", "Dentistry", nil, nil, nil],
				["Gum Bleeding", "Dentistry", nil, nil, nil],
				["toothache", "Dentistry", nil, nil, nil],
				["Gingivitis", "Dentistry", nil, nil, nil],
				["Periodontal disease", "Dentistry", nil, nil, nil],
				["Acne", "Dermatology", nil, nil, nil],
				["Aging Skin", "Dermatology", nil, nil, nil],
				["Alopecia", "Dermatology", nil, nil, nil],
				["Atopic Dermatitis (Eczema)", "Dermatology", nil, nil, nil],
				["Contact Dermatitis", "Dermatology", nil, nil, nil],
				["Cosmetic Removal of Unwanted Skin Lesions", "Dermatology", nil, nil, nil],
				["Seborrheic Dermatitis", "Dermatology", nil, nil, nil],
				["Tinea funal infections (Ringworm)", "Dermatology", "Infectious disease", nil, nil],
				["Calluses", "Dermatology", "Podiatry", nil, nil],
				["Corns", "Dermatology", "Podiatry", nil, nil],
				["Melanoma", "Dermatology", nil, nil, nil],
				["Moles", "Dermatology", nil, nil, nil],
				["Peels", "Dermatology", nil, nil, nil],
				["Psoriasis", "Dermatology", nil, nil, nil],
				["Rosacea", "Dermatology", nil, nil, nil],
				["Skin Cancer", "Dermatology", nil, nil, nil],
				["Skin cancer", "Dermatology", nil, nil, nil],
				["Skin Growths", "Dermatology", nil, nil, nil],
				["Sunburn", "Dermatology", nil, nil, nil],
				["Scabies", "Dermatology", nil, nil, nil],
				["Shingles", "Dermatology", nil, nil, nil],
				["Molluscm Contagiosum", "Dermatology", nil, nil, nil],
				["Warts", "Dermatology", nil, nil, nil],
				["Acromegaly", "Endocrinology", nil, nil, nil],
				["Addison's disease", "Endocrinology", nil, nil, nil],
				["Adrenal hyperplasia", "Endocrinology", nil, nil, nil],
				["Cretinism", "Endocrinology", nil, nil, nil],
				["Cushing's syndrome", "Endocrinology", nil, nil, nil],
				["Androgen insensitivity syndrome", "Endocrinology", nil, nil, nil],
				["Gigantisim", "Endocrinology", nil, nil, nil],
				["Hirsutism", "Endocrinology", nil, nil, nil],
				["Hypothyroidism", "Endocrinology", nil, nil, nil],
				["Hypogonadism", "Endocrinology", nil, nil, nil],
				["Hyperthyroidism", "Endocrinology", nil, nil, nil],
				["hyperaldosteronism", "Endocrinology", nil, nil, nil],
				["Hyperpituitarism", "Endocrinology", nil, nil, nil],
				["Hyperprolactinemia", "Endocrinology", nil, nil, nil],
				["Hyperparathyroidism", "Endocrinology", nil, nil, nil],
				["Hypopituitarism", "Endocrinology", nil, nil, nil],
				["Hypoparathyroidism", "Endocrinology", nil, nil, nil],
				["Empty sella syndrome", "Endocrinology", nil, nil, nil],
				["Nelson syndrome", "Endocrinology", nil, nil, nil],
				["Skin pigmentation", "Endocrinology", nil, nil, nil],
				["Pheochromocytoma", "Endocrinology", nil, nil, nil],
				["Polycystic ovarian syndrome", "Endocrinology", nil, nil, nil],
				["Delayed Puberty", "Endocrinology", nil, nil, nil],
				["Prolactinoma", "Endocrinology", nil, nil, nil],
				["Turner syndrome", "Endocrinology", nil, nil, nil],
				["Grave's disease", "Endocrinology", nil, nil, nil],
				["Precocious puberty", "Endocrinology", nil, nil, nil],
				["Hermaphroditism", "Endocrinology", nil, nil, nil],
				["Assistive Listening Devices", "ENT", nil, nil, nil],
				["Dizziness", "ENT", "Internal medicine", "Family Practice", nil],
				["Ear Infections", "ENT", "Internal medicine", "Family Practice", nil],
				["Sinusitis", "ENT", "Internal medicine", "Family Practice", nil],
				["TMJ syndrome", "ENT", "Internal medicine", "Family Practice", nil],
				["Acid Reflux", "Family Practice", "Internal Medicine", nil, nil],
				["Adolescent Medicine", "Family Practice", "Internal Medicine", nil, nil],
				["Arthritis", "Family Practice", "Internal Medicine", nil, nil],
				["Asthma", "Family Practice", "Internal Medicine", nil, nil],
				["Contraceptive Management", "Family Practice", "Internal Medicine", nil, nil],
				["Depression", "Family Practice", "Internal Medicine", nil, nil],
				["Depression", "Family Practice", "Internal Medicine", nil, nil],
				["Diabetes", "Family Practice", "Internal Medicine", nil, nil],
				["Diabetes", "Family Practice", "Internal Medicine", nil, nil],
				["Flu", "Family Practice", "Internal Medicine", nil, nil],
				["Hyperlipidemia", "Family Practice", "Internal Medicine", nil, nil],
				["Hypertension", "Family Practice", "Internal Medicine", nil, nil],
				["Hypertension", "Family Practice", "Internal Medicine", nil, nil],
				["Low Back Pain", "Family Practice", "Internal Medicine", nil, "Chiropractor"],
				["Men's Health", "Family Practice", "Internal Medicine", nil, nil],
				["Pharyngitis", "Family Practice", "Internal Medicine", nil, nil],
				["Preventative Health", "Family Practice", "Internal Medicine", nil, nil],
				["Preventive Care", "Family Practice", "Internal Medicine", nil, nil],
				["Sinusitis", "Family Practice", "Internal Medicine", nil, nil],
				["Sports Medicine", "Family Practice", "Internal Medicine", nil, nil],
				["Well Child Care", "Family Practice", "Internal Medicine", nil, nil],
				["Women's Health", "Family Practice", "Internal Medicine", nil, nil],
				["Difficulty Swallowing ", "Gastroenterology", nil, nil, nil],
				["Achalasia", "Gastroenterology", nil, nil, nil],
				["Abdominal pain", "Gastroenterology", nil, nil, nil],
				["Appendicitis", "Gastroenterology", nil, nil, nil],
				["Diarrhea", "Gastroenterology", nil, nil, nil],
				["Esophagitis", "Gastroenterology", nil, nil, nil],
				["GERD", "Gastroenterology", nil, nil, nil],
				["Heartburn", "Gastroenterology", nil, nil, nil],
				["Gastritis", "Gastroenterology", nil, nil, nil],
				["Hepatitis", "Gastroenterology", nil, nil, nil],
				["Hepatitis A", "Gastroenterology", nil, nil, nil],
				["Hepatitis B", "Gastroenterology", nil, nil, nil],
				["Hepatitis C", "Gastroenterology", nil, nil, nil],
				["Hepatitis E", "Gastroenterology", nil, nil, nil],
				["Hiatal hernia", "Gastroenterology", nil, nil, nil],
				["Irritable Bowel Syndome (IBS)", "Gastroenterology", nil, nil, nil],
				["Rectal Prolapse", "Gastroenterology", nil, nil, nil],
				["Ulcerative Colitis (UC)", "Gastroenterology", nil, nil, nil],
				["Crohn's disease (CD)", "Gastroenterology", nil, nil, nil],
				["Celiac Disease (Gluten allergy)", "Gastroenterology", nil, nil, nil],
				["Diverticulitis", "Gastroenterology", nil, nil, nil],
				["Hemorrhoids", "Gastroenterology", nil, nil, nil],
				["Stomach ulcer (Peptic Ulcer disesae)", "Gastroenterology", nil, nil, nil],
				["Stomach cancer", "Gastroenterology", nil, nil, nil],
				["Esophageal cancer", "Gastroenterology", nil, nil, nil],
				["Colon cancer", "Gastroenterology", nil, nil, nil],
				["Rectal cancer", "Gastroenterology", nil, nil, nil],
				["Anal Fissure", "Gastroenterology", nil, nil, nil],
				["Anorectal Abscess", "Gastroenterology", nil, nil, nil],
				["Constipation", "Gastroenterology", nil, nil, nil],
				["H Pylori", "Gastroenterology", nil, nil, nil],
				["Pancreatitis", "Gastroenterology", nil, nil, nil],
				["Pancreatic cancer", "Gastroenterology", nil, nil, nil],
				["Esophageal stricutre", "Gastroenterology", nil, nil, nil],
				["Pancreatic pseudocyst", "Gastroenterology", nil, nil, nil],
				["Rectal bleeding", "Gastroenterology", nil, nil, nil],
				["Barrett's esophagus", "Gastroenterology", nil, nil, nil],
				["Biliary Tract Surgery", "General surgery", nil, nil, nil],
				["Breast Cancer", "General surgery", nil, nil, nil],
				["Carpal Tunnel Syndrome", "General surgery", nil, nil, nil],
				["Gallstones", "General surgery", nil, nil, nil],
				["Hernias", "General surgery", nil, nil, nil],
				["Pancreas Cancer", "General surgery", nil, nil, nil],
				["Stomach surgery", "General surgery", nil, nil, nil],
				["Colon cancer surgery", "General surgery", nil, nil, nil],
				["Thyroid surgery", "General surgery", nil, nil, nil],
				["Cervical Cancer", "Gynecology oncology", nil, nil, nil],
				["Endometrial Cancer", "Gynecology oncology", nil, nil, nil],
				["Ovarian Cancer", "Gynecology oncology", "Hematology-Oncology", nil, nil],
				["Robotic Surgery", "Gynecology oncology", nil, nil, nil],
				["Vulvar Cancer", "Gynecology oncology", nil, nil, nil],
				["Hereditary Cancer Testing", "Hematology-Oncology", nil, nil, nil],
				["Anemia", "Hematology-Oncology", nil, nil, nil],
				["Bladder Cancer", "Hematology-Oncology", nil, nil, nil],
				["Breast Cancer", "Hematology-Oncology", nil, nil, nil],
				["Breast Cancer", "Hematology-Oncology", nil, nil, nil],
				["Breast Cancer", "Hematology-Oncology", nil, nil, nil],
				["Cancer", "Hematology-Oncology", nil, nil, nil],
				["Cancer Prevention", "Hematology-Oncology", nil, nil, nil],
				["Chronic Leukemias", "Hematology-Oncology", nil, nil, nil],
				["Colon Cancer", "Hematology-Oncology", nil, nil, nil],
				["Colorectal Cancer", "Hematology-Oncology", nil, nil, nil],
				["Hodgkin's Disease", "Hematology-Oncology", "Dermatology", nil, nil],
				["Hospice", "Hematology-Oncology", nil, nil, nil],
				["Kidney Cancer", "Hematology-Oncology", nil, nil, nil],
				["Leukemia", "Hematology-Oncology", nil, nil, nil],
				["Lung Cancer", "Hematology-Oncology", nil, nil, nil],
				["Lymphoma", "Hematology-Oncology", nil, nil, nil],
				["Lymphoma", "Hematology-Oncology", nil, nil, nil],
				["Multiple Myeloma", "Hematology-Oncology", nil, nil, nil],
				["Non-Hodgkin's Lymphomas", "Hematology-Oncology", nil, nil, nil],
				["Palliative Care", "Hematology-Oncology", nil, nil, nil],
				["Prostate Cancer", "Hematology-Oncology", "Urology", nil, nil],
				["Stomach Cancer", "Hematology-Oncology", nil, nil, nil],
				["Uterine Cancer", "Hematology-Oncology", nil, nil, nil],
				["Fever", "Infectious Disease", nil, nil, nil],
				["Cough", "Infectious Disease", nil, nil, nil],
				["Diarrhea", "Infectious Disease", nil, nil, nil],
				["AIDS", "Infectious Disease", nil, nil, nil],
				["Ringworm", "Infectious Disease", nil, nil, nil],
				["HIV", "Infectious Disease", nil, nil, nil],
				["Human Papillomavirus", "Infectious Disease", nil, nil, nil],
				["Food poisoning", "Infectious Disease", nil, nil, nil],
				["Lyme disease", "Infectious Disease", nil, nil, nil],
				["Malaria", "Infectious Disease", nil, nil, nil],
				["Deer tick", "Infectious Disease", nil, nil, nil],
				["West Nile virus", "Infectious Disease", nil, nil, nil],
				["Chikungunya fever", "Infectious Disease", nil, nil, nil],
				["Chronic Kidney Disease", "Nephrology", nil, nil, nil],
				["Dialysis", "Nephrology", nil, nil, nil],
				["End Stage Kidney Disease", "Nephrology", nil, nil, nil],
				["Glomerular Disease", "Nephrology", nil, nil, nil],
				["Hypertension", "Nephrology", nil, nil, nil],
				["Nephritis", "Nephrology", nil, nil, nil],
				["Kidney Stones", "Nephrology", "Urology", nil, nil],
				["Polycystic kidney disease", "Nephrology", "Urology", nil, nil],
				["Kidney Cancer", "Nephrology", "Urology", nil, nil],
				["Low Potassium (hypokalemia)", "Nephrology", nil, nil, nil],
				["Low sodium (hyponatremia)", "Nephrology", nil, nil, nil],
				["Nephrotic syndrome", "Nephrology", nil, nil, nil],
				["Ataxia", "Neurology", nil, nil, nil],
				["Headache", "Neurology", nil, nil, nil],
				["Meningitis", "Neurology", nil, nil, nil],
				["Multiple Sclerosis", "Neurology", nil, nil, nil],
				["Muscle weakness", "Neurology", nil, nil, nil],
				["Migraine", "Neurology", nil, nil, nil],
				["Pain Management", "Neurology", nil, nil, nil],
				["Parkinson's disease", "Neurology", nil, nil, nil],
				["Seizure", "Neurology", nil, nil, nil],
				["Stroke", "Neurology", nil, nil, nil],
				["Tremors", "Neurology", nil, nil, nil],
				["Chiari Malformation", "Neurosurgery", nil, nil, nil],
				["Congenital Malformations of the Brain & Spine", "Neurosurgery", nil, nil, nil],
				["Degenerative Disc Disease", "Neurosurgery", nil, nil, nil],
				["Hydrocephalus", "Neurosurgery", nil, nil, nil],
				["Normal Pressure Hydrocephalus", "Neurosurgery", nil, nil, nil],
				["Spinal Stenosis", "Neurosurgery", nil, nil, nil],
				["Tumor of the Brain & Spine", "Neurosurgery", nil, nil, nil],
				["Abnormal Menstrual Cycles", "Obstetrics Gynecology", "Internal Medicine", nil, nil],
				["Contraception Management", "Obstetrics Gynecology", "Internal Medicine", nil, nil],
				["Da Vinci - Treatment of Ovarian Disease", "Obstetrics Gynecology", nil, nil, nil],
				["Dysmenorrhea", "Obstetrics Gynecology", nil, nil, nil],
				["High Risk Pregnancy Care", "Obstetrics Gynecology", nil, nil, nil],
				["Infertility", "Obstetrics Gynecology", nil, nil, nil],
				["IUD's", "Obstetrics Gynecology", nil, nil, nil],
				["Menopausal Symptoms", "Obstetrics Gynecology", nil, nil, nil],
				["Menopause", "Obstetrics Gynecology", nil, nil, nil],
				["Menorrhagia", "Obstetrics Gynecology", nil, nil, nil],
				["Menstrual Disorders", "Obstetrics Gynecology", nil, nil, nil],
				["Pregnancy", "Obstetrics Gynecology", nil, nil, nil],
				["Endometriosis", "Obstetrics Gynecology", nil, nil, nil],
				["Obstetrics", "Obstetrics Gynecology", nil, nil, nil],
				["Ovarian Cyst", "Obstetrics Gynecology", "Internal Medicine", nil, nil],
				["Pap smear", "Obstetrics Gynecology", nil, nil, nil],
				["Pregnancy", "Obstetrics Gynecology", nil, nil, nil],
				["Routine Gynecological Exams", "Obstetrics Gynecology", nil, nil, nil],
				["Paps", "Obstetrics Gynecology", nil, nil, nil],
				["Urinary Incontinence", "Obstetrics Gynecology", nil, nil, nil],
				["Cataract", "Opthalmology", nil, nil, nil],
				["Glaucoma", "Opthalmology", nil, nil, nil],
				["Chalazion", "Opthalmology", nil, nil, nil],
				["Contact lenses", "Opthalmology", "Internal Medicine", nil, nil],
				["Diabetic eye", "Opthalmology", nil, nil, nil],
				["Diabetic Macular Edema", "Opthalmology", nil, nil, nil],
				["Diabetic Retinopathy", "Opthalmology", nil, nil, nil],
				["Droopy eyelid", "Opthalmology", nil, nil, nil],
				["Orbital trauma", "Opthalmology", nil, nil, nil],
				["Dry Eye", "Opthalmology", nil, nil, nil],
				["Excessive Tearing", "Opthalmology", nil, nil, nil],
				["Eye  Trauma", "Opthalmology", nil, nil, nil],
				["Eye Allergy", "Opthalmology", nil, nil, nil],
				["Orbital tumor", "Opthalmology", nil, nil, nil],
				["Eyelash Enhancement (Latisse)", "Opthalmology", nil, nil, nil],
				["Eyelid Spasm", "Opthalmology", nil, nil, nil],
				["Eyelid Tumor", "Opthalmology", nil, nil, nil],
				["Glaucoma Laser & Surgical Treatment", "Opthalmology", nil, nil, nil],
				["Glaucoma Medical Management", "Opthalmology", nil, nil, nil],
				["Macular Degeneration", "Opthalmology", nil, nil, nil],
				["Misdirected Eyelashes", "Opthalmology", nil, nil, nil],
				["Retinal Detachment", "Opthalmology", nil, nil, nil],
				["Retinal Tears", "Opthalmology", nil, nil, nil],
				["Thyroid (Graves) Eye Disease", "Opthalmology", nil, nil, nil],
				["Rotator cuff tear", "Orthopaedics", nil, nil, nil],
				["Knee Tear Meniscus", "Orthopaedics", nil, nil, nil],
				["Osteoarthritis", "Orthopaedics", nil, nil, nil],
				["Rheumatoid Arthritis", "Orthopaedics", nil, nil, nil],
				["Achilles Tendinitis", "Orthopaedics", nil, nil, nil],
				["Back Pain", "Orthopaedics", "Chiropractor", nil, nil],
				["Carpal tunnel syndrome", "Orthopaedics", nil, nil, nil],
				["Cubital tunnel syndrome", "Orthopaedics", nil, nil, nil],
				["Elbow Pain", "Orthopaedics", "Chiropractor", nil, nil],
				["Fracture", "Orthopaedics", nil, nil, nil],
				["Hip Pain", "Orthopaedics", "Chiropractor", nil, nil],
				["Joint Pain", "Orthopaedics", "Chiropractor", "Rheumatology", nil],
				["Bone cancer", "Orthopaedics", nil, nil, nil],
				["Avascular Necrosis", "Orthopaedics", nil, nil, nil],
				["Knee Pain", "Orthopaedics", nil, nil, nil],
				["Osteoarthritis", "Orthopaedics", nil, nil, nil],
				["Dislocated joint", "Orthopaedics", nil, nil, nil],
				["Shoulder Pain", "Orthopaedics", "Chiropractor", "Rheumatology", nil],
				["Osteomyelitis", "Orthopaedics", nil, nil, nil],
				["Sport Injuries", "Orthopaedics", "Chiropractor", nil, nil],
				["Sports Medicine", "Orthopaedics", nil, nil, nil],
				["Sprains", "Orthopaedics", nil, nil, nil],
				["Anterior cruciate ligament(ACL) tear", "Orthopaedics", nil, nil, nil],
				["Golfer's elbow", "Orthopaedics", nil, nil, nil],
				["Epicondylitis", "Orthopaedics", nil, nil, nil],
				["Hand fracture", "Orthopaedics", nil, nil, nil],
				["Arm fracture", "Orthopaedics", nil, nil, nil],
				["Pediatric Eye Exam", "Pediatric Opthalmology", "Opthalmology", nil, nil],
				["Abdominal Pain", "Pediatrics", nil, nil, nil],
				["Acne", "Pediatrics", nil, nil, nil],
				["Allergic Rhinitis", "Pediatrics", nil, nil, nil],
				["Allergies", "Pediatrics", "Internal Medicine", nil, nil],
				["Asthma", "Pediatrics", "Internal Medicine", nil, nil],
				["Child Physicals", "Pediatrics", nil, nil, nil],
				["Cold and Cough", "Pediatrics", nil, nil, nil],
				["Developmental Delay Evaluation and Management", "Pediatrics", nil, nil, nil],
				["Ear Infections", "Pediatrics", nil, nil, nil],
				["Eczema", "Pediatrics", nil, nil, nil],
				["Flu", "Pediatrics", nil, nil, nil],
				["Growth and Development", "Pediatrics", nil, nil, nil],
				["Newborn Care", "Pediatrics", nil, nil, nil],
				["Obesity", "Internal medicine", nil, nil, nil],
				["Otitis Media", "Pediatrics", nil, nil, nil],
				["Pharyngitis", "Pediatrics", nil, nil, nil],
				["Rashes", "Pediatrics", nil, nil, nil],
				["School Physicals", "Pediatrics", nil, nil, nil],
				["Sinusitis", "Pediatrics", nil, nil, nil],
				["Skin Infections", "Pediatrics", nil, nil, nil],
				["Sports Physicals", "Pediatrics", nil, nil, nil],
				["Well Child & Adolescent Care", "Pediatrics", nil, nil, nil],
				["Blepharoplasty", "Plastic Surgery", nil, nil, nil],
				["Body Contouring", "Plastic Surgery", nil, nil, nil],
				["Breast Augmentation", "Plastic Surgery", nil, nil, nil],
				["Breast Cancer", "Plastic Surgery", nil, nil, nil],
				["Breast Lifts", "Plastic Surgery", nil, nil, nil],
				["Breast Reconstruction", "Plastic Surgery", nil, nil, nil],
				["Breast Reduction", "Plastic Surgery", nil, nil, nil],
				["Breast Surgery", "Plastic Surgery", nil, nil, nil],
				["Cosmetic Surgery", "Plastic Surgery", nil, nil, nil],
				["Eyelid Surgery", "Plastic Surgery", nil, nil, nil],
				["Facelift", "Plastic Surgery", nil, nil, nil],
				["Facial Rejuvenation", "Plastic Surgery", nil, nil, nil],
				["Post Bariatric Body Contouring", "Plastic Surgery", nil, nil, nil],
				["Rhinoplasty", "Plastic Surgery", nil, nil, nil],
				["Tumors of the Hand, Wrist", "Plastic Surgery", nil, nil, nil],
				["Heel Pain", "Podiatry", nil, nil, nil],
				["Bunion", "Podiatry", nil, nil, nil],
				["Plantar wart", "Podiatry", nil, nil, nil],
				["Plantar fascitis", "Podiatry", nil, nil, nil],
				["Heel spur", "Podiatry", nil, nil, nil],
				["Foot pain", "Podiatry", nil, nil, nil],
				["ADHD", "Psychiatry", nil, nil, nil],
				["Anxiety", "Psychiatry", nil, nil, nil],
				["Child Behavior Issues", "Psychiatry", nil, nil, nil],
				["Depression", "Psychiatry", nil, nil, nil],
				["Job Stress", "Psychiatry", nil, nil, nil],
				["Low Self Esteem", "Psychiatry", nil, nil, nil],
				["Parenting Stress", "Psychiatry", nil, nil, nil],
				["Drug addiction", "Psychiatry", nil, nil, nil],
				["Bipolar disorder", "Psychiatry", nil, nil, nil],
				["Schizophrenia", "Psychiatry", nil, nil, nil],
				["Schizoaffective disorder", "Psychiatry", nil, nil, nil],
				["Suicide", "Psychiatry", nil, nil, nil],
				["Panic disorder", "Psychiatry", nil, nil, nil],
				["OCD (Obsessive-compulsive disorder)", "Psychiatry", nil, nil, nil],
				["Borderline personality disorder", "Psychiatry", nil, nil, nil],
				["Anorexia and Bulimia", "Psychiatry", nil, nil, nil],
				["Bulimia", "Psychiatry", nil, nil, nil],
				["Acute Respiratory Distress Syndrome", "Pulmonary", nil, nil, nil],
				["Bronchitis", "Pulmonary Disease", nil, nil, nil],
				["COPD", "Pulmonary Disease", nil, nil, nil],
				["ARDS", "Pulmonary Disease", nil, nil, nil],
				["CPAP", "Pulmonary Disease", nil, nil, nil],
				["Interisitial lung disease", "Pulmonary Disease", nil, nil, nil],
				["Pulmonary Fibrosis", "Pulmonary Disease", nil, nil, nil],
				["Shortness of breath", "Pulmonary Disease", "Cardiology", nil, nil],
				["Sarcoidosis", "Pulmonary Disease", nil, nil, nil],
				["Pleural effusion", "Pulmonary Disease", nil, nil, nil],
				["Mesothelioma", "Pulmonary Disease", nil, nil, nil],
				["Cystic fibrosis", "Pulmonary Disease", nil, nil, nil],
				["Asthma", "Pulmonary Disease", nil, nil, nil],
				["Wheezing", "Pulmonary Disease", nil, nil, nil],
				["Pneumonia", "Pulmonary Disease", nil, nil, nil],
				["Tuberculosis (TB)", "Pulmonary Disease", nil, nil, nil],
				["Asbestosis", "Pulmonary Disease", nil, nil, nil],
				["Lung Cancer", "Pulmonary Disease", nil, nil, nil],
				["Pleural Effusion", "Pulmonary Disease", nil, nil, nil],
				["Pulmonary Embolism", "Pulmonary Disease", nil, nil, nil],
				["Respiratory Failure", "Pulmonary Disease", nil, nil, nil],
				["Sleep Apnea", "Pulmonary Disease", nil, nil, nil],
				["Ventialtor", "Pulmonary Disease", nil, nil, nil],
				["Pulmonary arterial Hypertension", "Pulmonary Hypertension", "Cardiology", "Pulmonary", nil],
				["Gout", "Rheumatology", nil, nil, nil],
				["Lupus (Systemic Lupus)", "Rheumatology", nil, nil, nil],
				["Osteoarthritis", "Rheumatology", nil, nil, nil],
				["Osteoporosis", "Rheumatology", nil, nil, nil],
				["Psoriatic Arthritis", "Rheumatology", nil, nil, nil],
				["Rheumatoid Arthritis", "Rheumatology", nil, nil, nil],
				["Spondylitis", "Rheumatology", nil, nil, nil],
				["Scleroderma", "Rheumatology", nil, nil, nil],
				["Raynaud's phenomenon", "Rheumatology", nil, nil, nil],
				["Infectious arthritis", "Rheumatology", nil, nil, nil],
				["Reactive arthritis", "Rheumatology", nil, nil, nil],
				["Dermatomyositis", "Rheumatology", nil, nil, nil],
				["Polymyositis", "Rheumatology", nil, nil, nil],
				["Fibromyalgia", "Rheumatology", nil, nil, nil],
				["Polymyalgia Rheumatica", "Rheumatology", nil, nil, nil],
				["Reactive arthritis", "Rheumatology", nil, nil, nil],
				["Vasculitis", "Rheumatology", nil, nil, nil],
				["Osteonecrosis", "Rheumatology", nil, nil, nil],
				["Back pain", "Rheumatology", nil, nil, nil],
				["Takayasu's Arteritis", "Rheumatology", nil, nil, nil],
				["SjÃ¶gren's Syndrome", "Rheumatology", nil, nil, nil],
				["Bursitis", "Rheumatology", nil, nil, nil],
				["BPH (Benign Prostate Hyperplasia)", "Urology", nil, nil, nil],
				["Prostatitis", "Urology", nil, nil, nil],
				["Intersitial cystitis", "Urology", nil, nil, nil],
				["Bladder stones", "Urology", nil, nil, nil],
				["Abdominal Aortic Aneurysm", "Vascular Surgery", nil, nil, nil],
				["Aortic Aneurysm", "Vascular Surgery", nil, nil, nil],
				["Blockage of legs", "Vascular Surgery", nil, nil, nil],
				["Carotid Artery Disease", "Vascular Surgery", nil, nil, nil],
				["Carotid stenosis", "Vascular Surgery", nil, nil, nil],
				["Chronic Venous Insufficiency", "Vascular Surgery", nil, nil, nil],
				["Claudication", "Vascular Surgery", nil, nil, nil],
				["Dialysis Access", "Vascular Surgery", nil, nil, nil],
				["Non-Healing Wounds", "Vascular Surgery", nil, nil, nil],
				["Varicose Veins", "Vascular Surgery", nil, nil, nil],
				["Varicose Veins", "Vascular Surgery", nil, nil, nil],
				["Venous Disorders", "Vascular Surgery", nil, nil, nil]]

	def self.seed_conditions
		CONDITIONS.each_with_index do |row, i|
			begin
			  	# If there isn't a condition with this name (titlized becasue of inconsistent capitalization in the provided csv),
			  	# then start the condition creation process.
			  	if Condition.where(name: row[0]).empty?
				  	speciality_records_collector = []
				  	row[1..-1].compact.each do |speciality_name|
				  		# To account for inconsistent capitalization in the csv we are importing from, titlize the name.
				  		# Do not titlize the name if it is in all caps, since is probably an acronym in that case.
				  		formatted_speciality_name = speciality_name == speciality_name.upcase ? speciality_name : speciality_name.titleize
				  		speciality_record_found = PracticeSpeciality.where(name: formatted_speciality_name).first
				  		if speciality_record_found.blank?
				  			# If we couldn't find a record with that name, create it.
				  			speciality_record = PracticeSpeciality.create!(name: formatted_speciality_name)
				  		else
				  			# If we found one or more records with that name (should be only one), take the first.
				  			speciality_record = speciality_record_found
				  		end
				  		# Append to records_collector.
				  		speciality_records_collector = speciality_records_collector + [speciality_record]
				  	end
				  	# Create the condition with a titlized name, and the speciality list we just assembled.
				  	Condition.create!(name: row[0], practice_specialities: speciality_records_collector)
				end
			rescue StandardError => e
				puts "Encountered an error #{e}, when importing conditions row number #{i}: #{row}"
			end
		end
	end


end
