class BlogVideo < ApplicationRecord
  belongs_to :blog
  mount_uploader :video, VideoUploader
  validates :video, presence: true
  validates_integrity_of :video
end
