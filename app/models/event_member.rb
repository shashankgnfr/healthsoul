class EventMember < ApplicationRecord
  belongs_to :event
  belongs_to :user
  validates_associated :user
  belongs_to :resource, polymorphic: true, optional: true

  # Added JS validation
  # validates :name, :email, presence: true

  # validates :speciality , :practice_name, :city, presence: true, if: -> { is_student == 'Practicing Provider' }
  # validates :professional_school, :degree_type, presence: true, if: -> { is_student == 'Student' }
  
  TYPE = [[true, 'Trainee or Student'], [false, 'Practicing Provider']]
  # validates :name, uniqueness: { scope: [:email, :event_id] }

  def convert_lead_to_object
    if self.is_student?
      obj = MedicalStudent.create({
        name: name,
        email: email,
        professional_school: professional_school,
        degree_type: degree_type,
        graduation_year: graduation
      })
    elsif !self.is_student?
      obj = Doctor.create({
        name: name,
        email: email,
        credential: degree_type,
        graduation: graduation,
        med_school_other: professional_school,
        country: 'NA',
        city: city,
        state: 'NA'
      })
    end
    self.update_attributes({resource_id: obj.id, resource_type: obj.class.to_s})
    return obj
  end
end
