class Rating < ApplicationRecord
  paginates_per 20
  belongs_to :location, counter_cache: true
  belongs_to :user, optional: true
  include ReviewCalc
  include Analytics
  include RatingScopes
  has_many :votes, as: :voteable, dependent: :destroy
  has_many :comments, as: :commentable, dependent: :destroy
  after_create :notify_yext

  # validate :user_frequency, on: :create
  # validate :reason_required, on: :create
  validates :use_date, :overall_experience, presence: true
  validate :words_in_comment

  enum status: [:visible, :hidden]

  default_scope { visible }

  scope :admin_all, -> { unscoped.all }

  attr_accessor :short_date

  TravelInsGetIns = {
    "Part of Travel package" => 1,
    "Best rate" => 2,
    "Recommendation from others" => 3,
    "Google search" => 4,
    "Facebook Ad" => 5,
    "Other" => 6,
    "N/A" => 7
  }

  InsGetIns = {
    "Work based" => 2,
    "Best Rate" => 4,
    "Recommendation from others" => 6,
    "Spouse / Family" => 3,
    "National or Government sponsored" => 1,
    "Google Search" => 7,
    "Other" => 5,
  }

  Reasons = {
    "Emergency Room" => "1",
    "Cardiology (Heart Problems)" => "2",
    "Neurology" => "3",
    "Maternity or Gynaecology" => "4",
    "Oncology (Cancer)" => "5",
    "Pediatrics" => "6",
    "Surgery" => "7",
    "Medical admission" => "8",
    "Eye Care" => "10",
    "Orthopaedics (Bones and Joints)" => "11",
    "ENT (Ear , Nose ,Throat)" => "12",
    "Gastroenterology (Stomach)" => "13",
    "Psychiatry" => "24",
    "Dermatology ( Skin )" => "15",
    "Neurosurgery" => "16",
    "Plastic surgery" => "17",
    "Radiology" => "18",
    "Pulmonology ( Lungs )" => "19",
    "Rheumatology" => "20",
    "Urology" => "21",
    "Infertility" => "22",
    "Transplant" => "23",
    # inactive
    "Outpatient procedure" => "14",
    "Other" => "9",
  }

  ReasonsActive = {
    "Emergency Room" => "1",
    "Cardiology (Heart Problems)" => "2",
    "Neurology" => "3",
    "Maternity or Gynaecology" => "4",
    "Oncology (Cancer)" => "5",
    "Pediatrics" => "6",
    "Surgery" => "7",
    "Medical admission" => "8",
    "Eye Care" => "10",
    "Orthopaedics (Bones and Joints)" => "11",
    "ENT (Ear , Nose ,Throat)" => "12",
    "Gastroenterology" => "13",
    "Psychiatry" => "24",
    "Dermatology ( Skin )" => "15",
    "Neurosurgery" => "16",
    "Plastic surgery" => "17",
    "Radiology" => "18",
    "Pulmonology ( Lungs )" => "19",
    "Rheumatology" => "20",
    "Urology" => "21",
    "Infertility" => "22",
    "Transplant" => "23"
  }

  def self.reasons_active_select
    {"All" => ""}.merge(ReasonsActive)
  end

  Scores = [1, 2, 3, 4, 5]
  Recommend = ["Yes", "No", "Maybe"]

  def self.reasons
    pluck(:reason).uniq.map{|r| [r, Reasons.key(r) ]}
  end

  def self.by_type(name)
    joins(:location => [:location_type]).merge(LocationType.where(name: name))
  end

  def reason_label
    Reasons.key(reason)
  end

  def doctor_review?
    false
  end

  def save
    if status_changed?
      if hidden?
        location.decrement(:ratings_count, 1)
      else
        location.increment(:ratings_count, 1)
      end
    end
    s = super
    if s
      RankingJob.perform_async(location)
      DepartmentRankingJob.perform_async(self)
    end
    s
  end

  class New
    def self.call(location, user, params)
      short_date = params[:use_date]
      use_date = parse_use_date(params[:use_date])
      r = Rating.new(params.merge({location: location, use_date: use_date, short_date: short_date}))
      r.user = user
      r
    end

    def self.parse_use_date(date)
      use_date = date.split("-")
      begin
        use_date = Date.new(use_date[1].to_i, use_date[0].to_i)
      rescue
        # ok to rescue with nothing here because the use date will be blank and the modle validation will kick in
        use_date = ""
      end
      use_date
    end
  end

  def over_user_policy?
    Rating
      .where(user: user, location: location)
      .where("created_at >= ?", DateTime.now - 2.months)
      .count >= 2
  end

  def review_body
    body = {
      listingId: location.id,
      total: location.ratings.count,
      averageRating: location.ratings.rating_for(:overall_experience).round(2).to_f,
      maxRating: 5,
      reviews: [review_json]
    }.to_json
  end

  def review_json
    {
      reviewId: id,
      timestamp: created_at,
      authorName: user&.get_full_name || '',
      content: comment,
      url: "https://healthsoul.com#{location.entity_url}",
      rating: overall_experience.to_f
    }
  end

  def notify_yext
    if location.present? && location.live?
      begin
        response = RestClient.post('https://pl.yext.com/notify_review?api_key=Cfp26ndOH8', review_body, {"Content-Type" => 'application/json'})
      rescue => e
        p "failed posting to yext"
      end
    end
  end

  private

  def words_in_comment
    if comment.scan(/\w+/).size < 5
      errors.add(:comment, "Please enter at least 5 words")
    end
  end

  def reason_required
    if location && location.hospital? && self.reason.blank?
      errors.add(:reason, "Please choose a reason for visiting this hospital.")
    end
  end

  def user_frequency
    if over_user_policy?
      errors.add(:user_id, "You have already created 2 reviews in the last two months.")
    end
  end
end
