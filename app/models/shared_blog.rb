class SharedBlog < ApplicationRecord
  include ReviewCalc
  belongs_to :user, class_name: "User", foreign_key: :user_id
  belongs_to :blog, class_name: "Blog", foreign_key: :blog_id

  validates :user_id, :blog_id, presence: true
  validates :user_id, uniqueness: { scope: [:blog_id] }
end
