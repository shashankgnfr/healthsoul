class Search::HealthcareFacility < Search::Base
  
  def location_type_id
    @location_type_id = type_id if type_id.present?
    @location_type_id ||= LocationType.where.not(name: ["Travel Health Insurance", "Hospital", "Practice", "Health Insurance"]).ids
  end

  def countries_and_continents
    Location
      .where("country is not null")
      .select("continent, country_slug, country, country_code")
      .group("continent, country_slug, country, country_code")
      .order("continent, country")
  end

  def show_departments?
    false
  end
  
  def show_speciality?
    true
  end
  
  def possible_types
    LocationType.where.not(name: ["Travel Health Insurance", "Hospital", "Practice", "Health Insurance"]).order(:name) || []
  end

end
