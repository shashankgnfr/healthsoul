class Search::Doctor < Search::Base

  QueryKeys = [ :co, :near, :city, :state, :name, :within, :country_code, :stars, :lat, :lng, :speciality]
  attr_accessor *QueryKeys, :order, :default_country

  def countries_and_continents
    @countries_and_continents ||= DoctorOrganization
      .where("country is not null")
      .where("continent is not null")
      .select("continent, country_slug, country, country_code")
      .group("continent, country_slug, country, country_code")
      .order("continent, country")
  end

  def show_departments?
    false
  end

  def show_type?
    false
  end

  def show_speciality?
    true
  end

  def possible_specialities
    {"Common" => PracticeSpeciality.where(common_speciality: true).order(:name).collect {|opt| [opt.name, opt.id.to_s]}, "Other" => PracticeSpeciality.where(common_speciality: false).order(:name).collect {|opt| [opt.name, opt.id.to_s]}}
  end

  def countries_in_db
    sql_countries = ActiveRecord::Base.connection.execute <<-END
      SELECT distinct "doctors"."country", "doctors"."country_slug" FROM "doctors"
      WHERE ("doctors"."country_code" IS NOT NULL) ORDER BY "doctors"."country" ASC
    END
    sql_countries.values
  end

  def countries_select_code
    countries_in_db
  end

  def cities_select
    sql_cities = ActiveRecord::Base.connection.execute <<-END
      SELECT distinct "doctors"."city", "doctors"."city_slug" FROM "doctors"
      WHERE ("doctors"."city" IS NOT NULL) ORDER BY "doctors"."city" ASC
    END
    sql_cities.values
  end

  def states_select
    sql_states = ActiveRecord::Base.connection.execute <<-END
      SELECT distinct "doctors"."state", "doctors"."state_slug" FROM "doctors"
      WHERE ("doctors"."state" IS NOT NULL) ORDER BY "doctors"."state" ASC
    END
    sql_states.values
  end

  def within
    @within
  end

  def show_city?
    state.present?
  end

  def count
    @count ||= results.total_count
  end

  def possible_states
    @possible_states ||=
    if count > 2000
      ::Doctor.where(country_slug: co).distinct(:state).order(:state).pluck(:state, :state_slug)
    elsif count > 0
      begin
        results
          .select("state, state_slug, count(state) as state_count")
          .group('state').group('state_slug').reorder("state")
          .where("state is not null")
          .all.size.map{|state, count| ["#{state.first} (#{count})", state.last] }
        rescue => e
          p 'could not get state'
          nil
        end
    else
      []
    end
  end

  def practice_search?
    has_query?
  end

  def unfiltered
  end

  def results(page = 1, amount = 25, best_doctors: false)
    return @results if @results.present?
    build_options(page, amount)
    @where_filters = []
    @query_string = name.present? ? name : '*'
    @results = ::Doctor
    if co.present?
      @where_filters.push({ country_slug: co.downcase })
    elsif default_country.present?
      @where_filters.push({ country_slug: default_country.downcase })
    end
    @where_filters.push({ city_slug: city.downcase }) if city.present?
    @where_filters.push({ state_slug: state }) if state.present?
    @where_filters.push({ practice_specialities: [speciality.to_i] }) if speciality.present?
    @where_filters.push({ overall: { gte: stars_min.to_i } }) if stars_min.present?
    unless best_doctors
        begin
          geo_location = Geocoder.search(near).first
          viewport = geo_location.geometry['viewport']

          # T69: Need to search by ZIP code
          distance = Geocoder::Calculations.distance_between(viewport['northeast'].values, viewport['southwest'].values).to_f
          distance += within if within.present?
          lat_lng_array = geo_location.coordinates

          country_code = (geo_location.country_code == 'US' ? 'USA' : geo_location.country_code)&.parameterize
           
          if geo_location.data['postcode_localities']
            @where_filters.push({
              city_slug: geo_location.data['postcode_localities'].map(&:parameterize)
            })
          else
            @where_filters.push({
              city_slug: [geo_location.city&.parameterize]}) if geo_location.city
            @where_filters.push({
              state_slug: [geo_location.state&.parameterize, geo_location.state_code&.parameterize]}) if geo_location.state
            @where_filters.push({
              country_slug: [geo_location.country&.parameterize, country_code]
            }) if geo_location.country
          end
          
        rescue
          # if we cant Geocode the address lets see if the string is one of the slugs
          if near.present?
            @or_filters = [{ city_slug: near&.parameterize }, { state_slug: near&.parameterize }, { country_slug: near&.parameterize }]
          end
        end
    end
    order_array = order_by
    @where_filters.each{ |filter_hash| @options[:where].merge!(filter_hash) }
    @options[:where][:_or] = @or_filters if @or_filters.present?

    if order_array[0] == 'is_claimed' 
      unmapped_type_1 = 'boolean'
      unmapped_type_2 = 'long'
    else
      unmapped_type_1 = 'long'
      unmapped_type_2 = 'boolean'
    end

    order_by = 
      [
        { order_array[0] => {order: order_array[1], unmapped_type: unmapped_type_1 } },
        { order_array[2] => {order: order_array[3], unmapped_type: unmapped_type_2 } }
      ]
   
    order_by.push({ '_geo_distance' => {location: "#{lat}, #{lng}", order: "asc",unit: "km" } }) if lat_lng_present?
    @options[:order] = order_by


    if lat_lng_array.present?
      @results = ::Doctor.search(@query_string, boost_by_distance: {location: {origin: {lat: lat_lng_array[0], lon: lat_lng_array[1]}, function: "linear", scale: "#{distance}mi", decay: 0.5}}, **@options)
    else
      @results = ::Doctor.search(@query_string, **@options)
    end
    @results
  end

  def build_options(page_number, amount)
    options = { fields: ["name^10"], page: page_number || 1, where: {} }
    amount_hash = add_amount_or_all(amount)
    options.merge!(amount_hash) if amount_hash.present?
    @options = options
  end

  def add_amount_or_all(amount)
    { per_page: amount || 10 } unless amount == '*'
  end

  def order_by
    case order
    when "name_a"
      ["name", "ASC", "is_claimed", "DESC"]
    when "name_b"
      ["name", "DESC", "is_claimed", "DESC"]
    when "reviews_a"
      ["reviews", "DESC", "is_claimed", "DESC"]
    when "reviews_b"
      ["reviews", "ASC", "is_claimed", "DESC"]
    when "stars_a"
      ["stars", "DESC", "is_claimed", "DESC"]
    when "stars_b"
      ["stars", "ASC", "is_claimed", "DESC"]
    when "claimed"
      ["is_claimed", "DESC", 'name', 'ASC']
    else
      ["name", "ASC", "is_claimed", "DESC"]
    end
  end

end
