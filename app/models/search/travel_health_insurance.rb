class Search::TravelHealthInsurance < Search::HealthInsurance
  def location_type_id
    @location_type_id ||= LocationType.where(name: "Travel Health Insurance").first.id
  end

  def countries_and_continents
    Location
      .where("country is not null")
      .select("continent, country_slug, country, country_code")
      .group("continent, country_slug, country, country_code")
      .order("continent, country")
  end
end
