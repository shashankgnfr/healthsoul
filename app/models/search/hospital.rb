class Search::Hospital < Search::Base

  def location_type_id
    @location_type_id ||= LocationType.where(name: "Hospital").first.id
  end

  def show_departments?
  	# Feature has been disabled to prevent the specialties dropdown from showing in the hospital search UI
    # has_query?
    false
  end
end
