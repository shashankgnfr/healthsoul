class Search::Practice < Search::Base

  QueryKeys = [ :co, :near, :city, :state, :name, :within, :country_code, :stars, :lat, :lng, :speciality]
  attr_accessor *QueryKeys, :order, :default_country

  def location_type_id
    @location_type_id ||= LocationType.where(name: "Practice").first&.id
  end

  def countries_and_continents
    Location
      .where("country is not null")
      .select("continent, country_slug, country, country_code")
      .group("continent, country_slug, country, country_code")
      .order("continent, country")
  end

  def show_departments?
    false
  end

  def show_speciality?
    true
  end
end
