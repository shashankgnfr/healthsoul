class Search::HealthInsurance < Search::Base

  def location_type_id
    @location_type_id ||= LocationType.where(name: "Health Insurance").first.id
  end

  def show_type?
    false
  end

  def show_departments?
    false
  end

  def show_states?
    false
  end

  def cities_list
    []
  end

  def possible_cities
    []
  end
end
