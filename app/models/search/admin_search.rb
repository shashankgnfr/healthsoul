class Search::AdminSearch < Search::Base
  attr_accessor :location_type_id, :hwaid, :speciality

  def countries
    Location.pluck(:country).uniq.compact
  end

  def countries_in_db
    Location.where("country_code is not null").order(:country).pluck(:country, :country_slug).uniq
  end

  def countries_select_code
    countries_in_db
  end

  def has_query?
    (QueryKeys + [:hwaid] - [:within]).any?{|k| send(k).present? }
  end

  def possible_specialities
    {"Common" => PracticeSpeciality.where(common_speciality: true).order(:name).collect {|opt| [opt.name, opt.id.to_s]}, "Other" => PracticeSpeciality.where(common_speciality: false).order(:name).collect {|opt| [opt.name, opt.id.to_s]}}
  end

  def results
    @results = super
    if hwaid.present?
      @results = Location if @results == []
      @results = @results.where("hwaid like ?", "%#{hwaid}%")
    end
    if !has_query?
      if location_type_id.blank?
        @results = Location.all
      else
        if speciality.present?
          @results = Location.joins(:practice_specialities).where(location_type_id: location_type_id).where(:practice_specialities => {:id => speciality}).order(:hwaid)
        else
          @results = Location.where(location_type_id: location_type_id).order(:hwaid)
        end
      end
    end
    @results
  end


end
