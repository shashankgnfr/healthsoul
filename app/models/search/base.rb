class Search::Base
  include ActiveModel::Model

  QueryKeys = [ :co, :near, :city, :state, :name, :within, :country_code, :stars, :lat, :lng, :sub_type, :departments, :type_id]
  attr_accessor *QueryKeys, :order, :default_country, :without_geolocation

  Stars = {
    "5 Star" => "5",
    "4 Star & above" => "4",
    "3 Star & above" => "3",
    "2 Star & above" => "2",
    "1 Star & above" => "1",
  }

  def countries_and_continents
    @countries_and_continents ||= Location
      .where(location_type_id: location_type_id)
      .where("country is not null")
      .where("continent is not null")
      .select("continent, country_slug, country, country_code")
      .group("continent, country_slug, country, country_code")
      .order("continent, country")
  end

  def practice_search?
    false
  end

  def show_speciality?
    false
  end

  def country
    @country ||= Country.where(slug: selected_country).first
  end

  def continents_to_h
    h = {}
    countries_and_continents.each do |record|
      unless h[record.continent].present?
        c = h[record.continent] = []
      end
      c = h[record.continent] || []
      c << [record.country_code.to_s.downcase, record.country, record.country_slug]
    end
    h
  end

  def selected_country
    if co.present?
      co
    elsif default_country.present?
      default_country
    else
      ""
    end
  end

  def has_query?
    (QueryKeys - [:within]).any?{|k| send(k).present? }
  end

  def has_results?
    count > 0
  end

  def show_city?
    has_results? && possible_cities.any? && !only_country?
  end

  def only_country?
    state.blank? && name.blank? && !lat_lng_present? && stars_min.blank? && cities_list.blank?
  end

  def show_stars?
    has_query?
  end

  def show_type?
    has_query?
    #possible_sub_types.present?
  end

  def show_states?
    possible_states.present?
  end

  def possible_sub_types
    return [] unless has_results?
    begin
      @possible_sub_types ||= unfiltered.to_a
        .group_by(&:sub_type)
        .map{|sub_type, locations| ["#{sub_type} (#{locations.size})", sub_type] }
    rescue
      @possible_sub_types = []
    end
  end

  def possible_states
    return [] unless has_results?
    begin
      @possible_states||= results.map{ |dr| [dr.state, dr.state_slug] }.uniq
    rescue
      @possible_states = []
    end
  end

  def possible_cities
    return [] unless has_results?
    begin
      @possible_cities||= results.map{ |dr| [dr.city, dr.city_slug] }.uniq
    rescue
      @possible_cities = []
    end
  end

  def cities_list
    return [] if cities.nil? || cities.compact.blank?
    cities.reject { |c| c.empty? }
  end

  def stars_min
    return nil if stars.blank?
    stars.reject(&:blank?).sort.first
  end

  def has_advanced?
    name.present? || state.present? || co.present?
  end

  def within
    @within
  end

  def count
    results.size
  end

  def lat_lng
    [lat, lng]
  end

  def lat_lng_present?
    lat.present? && lng.present?
  end

  def position_of(record)
    ids.index(record.id) + 1
  end

  def ids
    @ids ||= results.map(&:id)
  end

  def unfiltered
    return @unfiltered if @unfiltered.present?
    practice = LocationType.where(name: 'Practice').first
    if self.location_type_id == practice.id && speciality.present?
      @unfiltered = Location.joins([:location_type, {location_specialities: :practice_speciality}])
      @unfiltered = @unfiltered.where({ location_specialities: {practice_speciality_id: [speciality.to_i] }})
    elsif location_type_id
      @unfiltered = Location.includes(:location_type).where(location_type_id: location_type_id) 
    else
      @unfiltered = Location.includes(:location_type)
    end
    if without_geolocation.present? && without_geolocation == "1"
      @unfiltered = @unfiltered.where("latitude is null or longitude is null")
    end
    if sub_type.present?
      @unfiltered = @unfiltered.where("sub_type ilike ?", sub_type)
    end
    if state.present?
      @unfiltered = @unfiltered.where("state_slug in (?)", state)
    end
    if name.present?
      @unfiltered = @unfiltered.where("locations.name ilike ?", "%#{name}%")
    end
    if co.present?
      @unfiltered = @unfiltered.where(country_slug: co)
    elsif default_country.present?
      @unfiltered = @unfiltered.where(country_slug: default_country)
    end

    if lat_lng_present? || near.present?
      distance = 0
      begin
        geo_location = Geocoder.search(near).first
        viewport = geo_location.geometry['viewport']
        distance = Geocoder::Calculations.distance_between(viewport['northeast'].values, viewport['southwest'].values).to_f
        country_code = (geo_location.country_code == 'US' ? 'USA' : geo_location.country_code)&.parameterize
        if geo_location.data['postcode_localities'].present?
          @unfiltered =  @unfiltered.where(
            city_slug: geo_location.data['postcode_localities'].map(&:parameterize)
          )
        else
          @unfiltered = @unfiltered.where(city_slug: [geo_location.city&.parameterize]) if geo_location.city
          @unfiltered = @unfiltered.where(state_slug: [geo_location.state&.parameterize, geo_location.state_code&.parameterize]) if geo_location.state
          @unfiltered = @unfiltered.where(country_slug: [geo_location.country&.parameterize, country_code]) if geo_location.country
        end
      rescue
      end
      # TODO: Can we add OR condition?
      if order == "distance_a"
        @unfiltered = @unfiltered.near(near, distance + within.to_f)
      else
        @unfiltered = @unfiltered.near(near, distance + within.to_f, order: false)
      end
    end
    if departments.present?
      @unfiltered = @unfiltered
        .joins(:department_ratings)
        .where("department_ratings.reason IN (?)", departments)
    end
    if @unfiltered == Location
      @unfiltered = []
    else
      if lat_lng_present? && order_by.blank?
        @unfiltered = @unfiltered.where(location_type_id: location_type_id)
      else
        @unfiltered = @unfiltered.where(location_type_id: location_type_id).reorder(order_by)
      end
    end
  end

  def results
    return @results if @results.present?
    @results = unfiltered
    if stars_min.present?
      @results = @results.where("overall >= ?", stars_min )
    end
    if @results == unfiltered
      @results
    else
      if lat_lng_present? && order_by.blank?
        @results = @results.order(order_by)
      else
        @results = @results.reorder(order_by)
      end
    end
  end

  def order_options
    opts = {
      name_a: "Name: A-Z",
      name_b: "Name: Z-A",
      reviews_a: "# Reviews: Most to Least",
      reviews_b: "# Reviews: Least to Most",
      stars_a: "Stars: Most to Least",
      stars_b: "Stars: Least to Most",
      claimed: "Claimed"
    }
    if lat_lng_present?
      opts = opts.merge({"distance_a" => "Distance: Near to Far"})
    end
    opts
  end

  def order_select_options
    order_options.map{|k,v| [v, k] }
  end

  def order_by
    case order
    when "name_a"
      "lower(locations.name) ASC, locations.is_claimed DESC NULLS LAST"
    when "name_b"
      "lower(locations.name) DESC, locations.is_claimed DESC NULLS LAST"
    when "reviews_a"
      "locations.ratings_count DESC, locations.is_claimed DESC NULLS LAST"
    when "reviews_b"
      "locations.ratings_count ASC, locations.is_claimed DESC NULLS LAST"
    when "stars_a"
      "locations.overall DESC NULLS LAST, locations.is_claimed DESC NULLS LAST"
    when "stars_b"
      "locations.overall ASC, locations.is_claimed DESC NULLS LAST"
    when "claimed"
      "locations.is_claimed DESC NULLS LAST, lower(locations.name) ASC"
    else
      "lower(locations.name) ASC, locations.is_claimed DESC NULLS LAST"
    end
  end
end
