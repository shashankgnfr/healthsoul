class Search::MedicalStudent
  include ActiveModel::Model

  QueryKeys = [:name, :professional_school]
  attr_accessor *QueryKeys

  def results
    @results = MedicalStudent.order(name: :asc)
    @results = @results.where(name: name) if name.present?
    @results = @results.where(professional_school: professional_school) if professional_school.present?
  end

end