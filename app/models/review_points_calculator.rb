class ReviewPointsCalculator < Struct.new(:user)
  def save
    user.update(total_points: total_points)
  end

  def total_points
    review_points + questions_points + likes_points + referral_points + shared_points
  end

  def review_points
    ( user.total_reviews) * 5
  end

  def questions_points
    (user.questions.count + user.answers.count) * 2
  end

  def likes_points
    user.votes.count
  end

  def referral_points
    ( user.user_referrals.count ) * 10
  end

  def shared_points
    # as per dr. comment share same or different blog upto 5 times and each time he/she earn 5 point. Thus max should be 25
    [user.shared_blogs.sum(:shared_count), 5].min * 5
  end
end
