class Contact
  include ActiveModel::Model

  attr_accessor :email, :name, :hospital_name, :city, :country, :comments

  #validates :email, presence: true, length: {in:2..255}
  validates :name, :email, :city, :country, presence: true
end
