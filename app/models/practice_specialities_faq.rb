class PracticeSpecialitiesFaq < ApplicationRecord
  belongs_to :practice_speciality, -> { order(:name) }
  belongs_to :user
  has_and_belongs_to_many :countries, -> { order(:name) }

  validates :faq_title, :faq_body, :practice_speciality_id, :user_id,  presence: true
  validates :faq_title, uniqueness: { scope: [:practice_speciality_id] } 

end
