class Blog < ApplicationRecord
  paginates_per 20
  is_impressionable
  include Friendly
  acts_as_taggable

  has_many :advertisements, through: :advertisements_resources
  has_many :advertisements_resources, as: :resource
  has_many :blog_videos, dependent: :destroy
  accepts_nested_attributes_for :blog_videos, allow_destroy: true
  validates :title, :slug, presence: true, uniqueness: true
  validates_associated :blog_videos
  validates :meta_title, :meta_description, :content, presence: true
  
  mount_uploader :featured_image, BlogUploader
  mount_uploader :custom_author_photo, ProfilePhotoUploader
  belongs_to :doctor, optional: true

  normalize_attributes :meta_title, :meta_description, :content, :title
  has_many :shared_blogs, class_name: "SharedBlog", foreign_key: :blog_id

  CUSTOM_TAGS = ['Healthcare', 'Know your Doctor', 'Healthy Lifestyle', 'Know Your Country', 'Health News', 'Patient Stories']
  BLANK_AUTHOR = 'Healthsoul Team'
  BLANK_LOCATION = ''
  BLANK_SPECIALTY = ' '
  BLANK_TAGS = 'Uncategorized'

  scope :title_contains, -> (query) {
    all_words_with_percentage = query.split(' ').map(&:downcase).map {|val| "%#{val}%" }
    where("title ILIKE ALL ( array[?] )", all_words_with_percentage)
  }

  def self.tags_to_filter_by
    available_category_tags_slugs & ActsAsTaggableOn::Tagging.includes(:tag).where(taggable_type: "Blog").map(&:tag).map{|i| ["#{i.name.downcase.gsub(' ', '-')}", i.name]}.uniq
  end

  def self.available_practice_speciality_tags
    PracticeSpeciality.all.map{|i| ["#{i.name.downcase.gsub(' ', '-')}", i.name]}
  end

  def self.available_condition_tags
    Condition.all.map(&:name)
  end

  def self.available_procedure_tags
    Procedure.all.map(&:name)
  end

  def self.available_category_tags
    CUSTOM_TAGS + PracticeSpeciality.all.pluck(:name)
  end

  def self.available_category_tags_slugs
    (custom_tags_slug + available_practice_speciality_tags).uniq
  end

  def self.custom_tags_slug
    ActsAsTaggableOn::Tagging.includes(:tag).where(tags: {name: Blog::CUSTOM_TAGS}).map{|i| ["#{i.tag.name.downcase.gsub(' ', '-')}", i.tag.name]}.uniq
  end

end
