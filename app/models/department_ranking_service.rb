class DepartmentRankingService
  attr_reader :rating

  def initialize(rating)
    @rating = rating
  end

  def department_rating
    @department_rating ||= DepartmentRating.find_or_initialize_by(location: rating.location, reason: reason)
  end

  def location
    rating.location
  end

  def reason
    rating.reason
  end

  def ratings_by_reason
    location.ratings.where(reason: reason)
  end

  def average_rating
    ratings_by_reason.average(:overall_experience).round(1)
  end

  def rank
    if ratings_by_reason.present?
      department_rating.update(rating: average_rating)
    else
      department_rating.destroy
    end
  end
end
