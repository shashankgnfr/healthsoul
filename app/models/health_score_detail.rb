class HealthScoreDetail < ApplicationRecord
  belongs_to :doctor
  belongs_to :health_score
end
