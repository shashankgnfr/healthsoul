class DepartmentRating < ApplicationRecord
  belongs_to :location

  def self.for(reason)
    _rating = where(reason: reason)
    if _rating.present?
      _rating.average(:rating).round(1)
    else
      0
    end
  end

  def department
    Rating::Reasons.key(reason)
  end

end
