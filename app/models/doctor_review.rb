class DoctorReview < ApplicationRecord
  paginates_per 20
  self.table_name = "doctor_reviews"
  belongs_to :doctor
  belongs_to :user
  has_many :votes, as: :voteable, dependent: :destroy
  has_many :comments, as: :commentable, dependent: :destroy
  attr_accessor :short_date
  include RatingScopes
  include Analytics
  include ReviewCalc

  before_validation :check_date
  after_commit :update_overall, :reindex_doctor

  enum where: [:outpatient, :inpatient]
  validates :use_date, :overall_experience, presence: true
  validate :words_in_comment

  after_create :notify_yext

  def over_user_policy?
    DoctorReview
      .where(user: user, doctor: doctor)
      .where("created_at >= ?", DateTime.now - 2.months)
      .count >= 2
  end

  def reason
    where
  end

  def location
    doctor
  end

  def reason_label
    where.titleize
  end

  def self.reasons
    pluck(:where).uniq.map{|r| [r, r.titleize ]}
  end


  def doctor_review?
    true
  end

  def review_body
    body = {
      listingId: doctor.id,
      total: doctor.reviews.count,
      averageRating: doctor.reviews.rating_for(:overall_experience).round(2).to_f,
      maxRating: 5,
      reviews: [review_json]
    }.to_json
  end

  def review_json
    {
      reviewId: id,
      timestamp: created_at,
      authorName: user&.get_full_name || '',
      content: comment,
      url: "https://healthsoul.com#{doctor.entity_url}",
      rating: overall_experience.to_f
    }
  end

  def notify_yext
    if doctor.present? && doctor.live?
      begin
        response = RestClient.post('https://pl.yext.com/notify_review?api_key=Cfp26ndOH8', review_body, {"Content-Type" => 'application/json'})
      rescue => e
        p "failed posting to yext"
      end
    end
  end

  private
  
  def reindex_doctor
    doctor.reindex if doctor.present?
  end

  def update_overall
    RankingJob.perform_async(doctor)
  end

  def check_date
    if (date = use_date_before_type_cast).present? && date.is_a?(String)
      use_date = date.split("-")
      if use_date.size == 2
        begin
          use_date = Date.new(use_date[1].to_i, use_date[0].to_i)
        rescue
          # ok to rescue with nothing here because the use date will be blank and the modle validation will kick in
          use_date = ""
        end
        self.use_date = use_date
      end
    end
  end

  def words_in_comment
    if comment.scan(/\w+/).size < 5
      errors.add(:comment, "Please enter at least 5 words")
    end
  end

  def reason_required
    if location && location.hospital? && self.reason.blank?
      errors.add(:reason, "Please choose a reason for visiting this hospital.")
    end
  end

  def user_frequency
    if over_user_policy?
      errors.add(:user_id, "You have already created 2 reviews in the last two months.")
    end
  end
end
