require 'csv'
class Importer::DoctorUploads
  attr_reader :params, :file

  def initialize(params)
    @params = params[:import]
    @file = params[:import][:file]
  end

  def import
    doctors = []
    csv = CSV.parse(file.tempfile, :headers => true, :encoding => 'ISO-8859-1')
    csv.each do |row|
      doctor = DoctorUpload.new(
      :Name =>sani(row["Name"]),
      :Provider =>sani(row["Provider"]),
      :Gender =>sani(row["Gender"]),
      :Primary_Specialty =>sani(row["Primary Specialty"]),
      :Organization_Legal_Name =>sani(row["Organization Legal Name"]),
      :Credential =>sani(row["Credential"]),
      :Address =>sani(row["Address"]),
      :City =>sani(row["City"]),
      :State =>sani(row["State"]),
      :Country =>sani(row["Country"]),
      :Zip_Code =>sani(row["Zip Code"]),
      :Phone_Number =>sani(row["Phone Number"]),
      :Medical_School_Name =>sani(row["Medical School Name"]),
      :Graduation_Year =>sani(row["Graduation Year"]),
      :Secondary_Specialty_1 =>sani(row["Secondary Specialty 1"]),
      :Secondary_Specialty_2 =>sani(row["Secondary Specialty 2"]),
      :Hospital_Affiliation_Lbn_1 =>sani(row["Hospital Affiliation Lbn 1"]),
      :Hospital_HWA_ID_1 =>sani(row["Hospital HWA ID 1"]),
      :Hospital_Affiliation_Lbn_2 =>sani(row["Hospital Affiliation Lbn 2"]),
      :Hospital_HWA_ID_2 =>sani(row["Hospital HWA ID 2"]),
      :Hospital_Affiliation_Lbn_3 =>sani(row["Hospital Affiliation Lbn 3"]),
      :Hospital_HWA_ID_3 =>sani(row["Hospital HWA ID 3"]),
      :Hospital_Affiliation_Lbn_4 =>sani(row["Hospital Affiliation Lbn 4"]),
      :Hospital_HWA_ID_4 =>sani(row["Hospital HWA ID 4"]),
      :Npi =>sani(row["Npi"]),
      :website =>sani(row["website"]),
      :hsid =>sani(row["hsid"]),
      :Professional_Accepts_Medicare_Assignment =>sani(row["Professional Accepts Medicare Assignment"]),
      :imported => false
      )
      doctors << doctor
    end
    DoctorUpload.import doctors, on_duplicate_key_ignore: true
  end

  private

  def sani(string)
    return nil if string.blank?
    string = string.gsub(/\s+/, ' ').strip
    string.force_encoding('iso8859-1').encode('utf-8')
  end
end
