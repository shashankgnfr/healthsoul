require 'csv'
class Importer::DoctorUpdater
  attr_reader :params, :file

  def initialize(params)
    @params = params[:import]
    @file = params[:import][:file]
  end

  def import
    csv = CSV.parse(file.tempfile, :headers => true)
    doctors = []
    csv.each do |row|
      # rails g migration doctor name gender:integer med_school:references graduation:integer npi medicare:boolean
      if row["Name"].present?
        doctor = Doctor.find_by(name: sani(row["Name"]),
                            provider_type: sani(row["Provider"]),
                            npi: sani(row["Npi"]),
                           )

        if doctor.present?
          # TODO this has changed with removal of DoctorOrganization
          # org = DoctorOrganization.find_or_initialize_by(
          #   name: sani(row["Organization Legal Name"]),
          #   address: sani(row["Address"]),
          #   city: sani(row["City"])
          # )
          # unless org.persisted?
          #   org.state = sani(row["State"])
          #   org.zip = sani(row["Zip Code"])
          #   org.phone = sani(row["Phone Number"])
          #   org.importing = true
          #   org.save
          #   doctor.organization = org
          #   doctor.save
          # end
        else
          p "#{row['Name']} not found "
        end

      end
    end
  end


  private

  def sani(string)
    return nil if string.blank?
    string = string.gsub(/\s+/, ' ').strip
    string.force_encoding('iso8859-1').encode('utf-8')
  end
end
