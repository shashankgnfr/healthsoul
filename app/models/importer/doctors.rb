class Importer::Doctors

  def import
    doctor_uploads = DoctorUpload.where(imported: false).limit(1000)
    doctor_uploads.each do |row|
      npi = sani(row["Npi"])
      if npi.present?
        doctor = Doctor.find_by(npi: npi)
      end

      if doctor.blank?
        doctor = Doctor.find_or_initialize_by(
          name: sani(row["Name"]),
          credential: sani(row["Credential"]),
          graduation: sani(row["Graduation_Year"]),
          city: sani(row["City"]),
          zip: sani(row["Zip_Code"])
        )

        # TODO we could also check on speciality but then we are comparing strings
        # if doctor.persisted? && doctor.primary_speciality != sani(row["Primary_Specialty"])
        #   doctor = Doctor.new(
        #     name: sani(row["Name"]),
        #     credential: sani(row["Credential"]),
        #     graduation: sani(row["Graduation_Year"]),
        #     city: sani(row["City"]),
        #     zip: sani(row["Zip_Code"])
        #   )
        # end
      end

      # lets skip if doctor already in system
      if doctor.persisted?
        row.update(imported: true)
        row.update(dr_id: doctor.id)
      else
        doctor.gender = gender(row["Gender"])
        doctor.npi = sani(row["Npi"])
        doctor.provider_type = sani(row["Provider"])
        doctor.organization_name = sani(row["Organization_Legal_Name"])
        doctor.website = sani(row["Website"])
        doctor.address = sani(row["Address"])
        doctor.hsid = sani(row["hsid"])
        doctor.state = sani(row["State"])
        doctor.country = sani(row["Country"])
        doctor.phone = sani(row["Phone_Number"])
        doctor.medicare = medi(sani(row["Professional_Accepts_Medicare_Assignment"]))
        doctor.med_school = MedSchool.find_or_create_by(
          name: sani(row["Medical_School_Name"]),
        )
        assign_affiliated_hospitals(row, doctor)
        doctor.practice_specialities = practice_specialities(row)

        unless doctor.latitude.present?
          doctor.geocode
        end

        if doctor.save
          p "#{doctor.name} imported"
          row.update(imported: true)
          row.update(dr_id: doctor.id)
        end
      end
    end
    if doctor_uploads.present?
      puts "-------------------"
      puts "-------------------"
      puts "-------------------"
      puts "-------------------"
      puts "-------------------"
      puts "Running Again.....#{doctor_uploads.size} doctors just processed"
      puts "-------------------"
      puts "-------------------"
      puts "-------------------"
      puts "-------------------"
      Importer::Doctors.new.import
    end
  end

  def assign_affiliated_hospitals(row, doctor)
    hos = []
    if row['Hospital_HWA_ID_1'].present?
      hospital = Location.find_by(hwaid: sani(row['Hospital_HWA_ID_1']))
      doctor.hospitals << hospital if hospital.present?
    else
      hos << sani(row['Hospital_Affiliation_Lbn_1'])
    end
    if row['Hospital_HWA_ID_2'].present?
      hospital = Location.find_by(hwaid: sani(row['Hospital_HWA_ID_2']))
      doctor.hospitals << hospital if hospital.present?
    else
      hos << sani(row['Hospital_Affiliation_Lbn 2'])
    end
    if row['Hospital_HWA_ID_3'].present?
      hospital = Location.find_by(hwaid: sani(row['Hospital_HWA_ID_3']))
      doctor.hospitals << hospital if hospital.present?
    else
      hos << sani(row['Hospital_Affiliation_Lbn_3'])
    end
    if row['Hospital_HWA_ID_4'].present?
      hospital = Location.find_by(hwaid: sani(row['Hospital_HWA_ID_4']))
      doctor.hospitals << hospital if hospital.present?
    else
      hos << sani(row['Hospital_Affiliation_Lbn_4'])
    end
    hos.compact.join(", ")
  end

  def gender(gen)
    if gen == "M"
      'male'
    elsif gen.present?
      'female'
    else
      nil
    end
  end

  def medi(med)
    if med == "Y"
      true
    else
      false
    end
  end

  def practice_specialities(row)
    sp = []
    if row["Primary_Specialty"].present?
      sp1 = PracticeSpeciality.find_or_create_by(
        name: sani(row["Primary_Specialty"])
      )
      sp << sp1
    end
    if row["Secondary_Specialty_1"].present?
      sp2 = PracticeSpeciality.find_or_create_by(
        name: sani(row["Secondary_Specialty_1"])
      )
      sp << sp2
    end
    sp
  end

  private

  def sani(string)
    # we've already sanitized from doctor upload
    string
#    return nil if string.blank?
#    string = string.gsub(/\s+/, ' ').strip
#    string.force_encoding('iso8859-1').encode('utf-8')
  end
end
