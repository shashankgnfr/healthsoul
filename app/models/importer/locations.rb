require 'csv'
class Importer::Locations
  attr_reader :params, :file, :location_type_id

  def initialize(params)
    @params = params[:import]
    @file = params[:import][:file]
    @location_type_id = params[:import][:location_type_id]
  end

  def import
    csv = CSV.parse(file.tempfile, :headers => true)
    locations = []
    if validate_file(csv.headers)
      csv.each do |row|
        # TODO: To fix it permenantly we need to make HWAID mandatory.
        # location = LocationImport.find_by_hwaid(sani(row["hwaid"]))
        location = LocationImport.where(["hwaid is not NULL and hwaid = ?", sani(row['hwaid'])]).first
        country_code, continent = set_continent_and_country_code(sani( row["Country"] ), sani( row["Continent"] ))
        if location.present?
          location.update(name: sani(row["Name"]),
                          address: sani(row["Address"]),
                          address2: sani(row["Address2"]),
                          city: sani( row["City"] ),
                          state: sani( row["State"] ),
                          zip: sani( row["Zip"] ),
                          country: sani( row["Country"] ),
                          location_type_id: location_type_id,
                          website: sani(row["Website"]),
                          phone: sani(row["Phone"]),
                          email: sani(row["Email"]),
                          sub_type: sani(row["Type of location"]),
                          continent: continent,
                          country_code: country_code
                         )
          p "Updated #{location.hwaid}"

        else
          location = LocationImport.new(name: sani(row["Name"]),
                                  address: sani(row["Address"]),
                                  address2: sani(row["Address2"]),
                                  city: sani( row["City"] ),
                                  state: sani( row["State"] ),
                                  zip: sani( row["Zip"] ),
                                  country: sani( row["Country"] ),
                                  location_type_id: location_type_id,
                                  website: sani(row["Website"]),
                                  phone: sani(row["Phone"]),
                                  hwaid: sani( row["hwaid"] ),
                                  email: sani(row["Email"]),
                                  sub_type: sani(row["Type of location"]),
                                  continent: continent,
                                  country_code: country_code,
                                  slug: nil
                                 )
          if row["Latitude"].present?
            location.latitude = sani(row["Latitude"])
            location.longitude = sani(row["Longitude"])
          end
          location.importing = true
          locations << location if location.valid?
        end
      end
      LocationImport.import locations
      MakeSlugJob.perform_async
    else
      puts "------------- Invalid columns --------------- "
      false
    end
  end

  def save
    import
  end

  private

  def sani(string)
    return nil if string.blank?
    string = string.gsub(/\s+/, ' ').strip
    string.force_encoding('iso8859-1').encode('utf-8')
  end

  def validate_file(headers)
    headers - correct_headers == []
  end

  def correct_headers
    [ "Name",  "Address", "City", "State", "Zip", "Country", "Website", "Phone", "Email", "Type of location", "hwaid", "Continent", "Latitude", "Longitude" ]
  end

  def set_continent_and_country_code(country, continent)
    c = Country.where(["lower(name) ilike ?", country.downcase]).first
    return c.try(:alpha3), (c.try(:continent) || continent)
  end

end
