class BusinessHour < ApplicationRecord
  enum day: { monday: 0, tuesday: 1, wednesday: 2, thursday: 3, friday: 4, saturday: 5, sunday: 6 }
  belongs_to :hourable, polymorphic: true

  SELECT_VALUES = [ ['Monday', 'monday'], ['Tuesday', 'tuesday'], ['Wednesday', 'wednesday'], ['Thursday', 'thursday'], ['Friday', 'friday'], ['Saturday' ,'saturday'], ['Sunday', 'sunday'] ]

  def self.create_time
    times_for_select = []
    (0..23).each do |hour|
      build_hour = hour < 10 ? "0#{hour}" : hour
      am_pm = hour < 12 ? "AM" : "PM"
      us_time = hour > 12 ? hour - 12 : hour
      ["00", "15", "30", "45"].each do |minutes|
        times_for_select << ["#{us_time}:#{minutes} - #{am_pm}", "#{build_hour}:#{minutes}"]
      end
    end
    times_for_select
  end
end
