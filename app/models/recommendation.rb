class Recommendation < ApplicationRecord
  validates :name, :source_type, :country, :email, presence: true
  normalize_attributes :name, :source_type, :country, :email, :location, :website
  validates :website, url: true, if: Proc.new { |a| a.website? }
end