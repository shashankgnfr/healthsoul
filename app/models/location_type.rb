class LocationType < ApplicationRecord
  has_many :locations

  def hospital?
    name == "Hospital"
  end

  def insurance_company?
    name == "Health Insurance"
  end

  def travel_insurance?
    name == "Travel Health Insurance"
  end

  def practice?
    name == 'Practice'
  end

  def healthcare_facilities?
    !["Hospital", "Health Insurance", "Travel Health Insurance", "Practice"].include?(name)
  end

  def self.practice_obj
    where(name: 'Practice')&.last
  end

end
