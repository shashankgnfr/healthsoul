include WaveCell
class Appointment < ApplicationRecord
  belongs_to :provider, polymorphic: true
  belongs_to :user

  attr_accessor :hour, :minute
  validates_presence_of :time, :date, :provider_id, :user_id
  after_destroy :notify_office_of_cancellation
  after_update :notify_office_of_modification

  def time_slots
    @time_slots ||= AppointmentTimeSlots.new(self).time_slots
  end

  def time_slots_select
    time_slots.map{|ts| [("#{I18n.l(ts, format: :appt)} - #{I18n.l(ts+1.hour, format: :appt)}"), ts] }
  end

  def date_time
    I18n.l(date, format: :appt_date) + " - " + I18n.l(time, format: :appt)
  end

  # Check to see if the office can be contacted by email. This is checked when a user makes an appointment.
  # This just means checking if the doctor has an office email associated with their account.
  def can_office_be_contacted_by_email?
    can_contact_by_email = false
    if provider.should_be_notified_by_email_for_appointments and provider.email.present?
      can_contact_by_email = true
    end
    return can_contact_by_email
  end

  # Check to see if the office can be contacted by sms. This is checked when a doctor with should_be_notified_by_sms
  # makes an appointment. This means check that 
  def can_office_be_contacted_by_sms?
    can_contact_by_sms = false
    if provider.should_be_notified_by_sms_for_appointments and provider.phone.present?
      can_contact_by_sms = true
    end
    return can_contact_by_sms
  end

  # All of the event notification methods follow a similar pattern. They follow a different branch depending on whether the doctor wants
  # to be emailed via email or sms, and send a message through the appropriate medium.
  def notify_office_of_request
    if provider.should_be_notified_by_email_for_appointments and can_office_be_contacted_by_email?
      ApptMailer.notify(self).deliver_now
    elsif provider.should_be_notified_by_sms_for_appointments and can_office_be_contacted_by_sms?
      ApptMailer.notify(self, true).deliver_now
      message = "#{user.get_full_name} has requested an appointment with your office for #{date_time}. Reason: #{reason}. User Contact: #{user.email.present? ? user.email : user.phone_number}."
      send_wavecell_appointment_notification_to_phone_number(provider.phone_number_in_e164_format, message)
    end 
  end

  def notify_office_of_cancellation
    if provider.should_be_notified_by_email_for_appointments and can_office_be_contacted_by_email?
      ApptMailer.notify_of_cancellation(self).deliver_now
    elsif provider.should_be_notified_by_sms_for_appointments and can_office_be_contacted_by_sms?
      ApptMailer.notify_of_cancellation(self, true).deliver_now
      message = "#{user.get_full_name} has cancelled an appointment with your office for #{date_time}. User Contact: #{user.email.present? ? user.email : user.phone_number}."
      send_wavecell_appointment_notification_to_phone_number(provider.phone_number_in_e164_format, message)
    end 
  end

  def notify_office_of_modification
    if provider.should_be_notified_by_email_for_appointments and can_office_be_contacted_by_email?
      ApptMailer.notify_of_modification(self).deliver_now
    elsif provider.should_be_notified_by_sms_for_appointments and can_office_be_contacted_by_sms?
      ApptMailer.notify_of_modification(self, true).deliver_now
      message = "#{user.get_full_name} has modified an appointment with your office. The appointment is now for #{date_time}. Reason: #{reason}. User Contact: #{user.email.present? ? user.email : user.phone_number}."
      send_wavecell_appointment_notification_to_phone_number(provider.phone_number_in_e164_format, message)
    end 
  end

  private

  # Used to make a wavecell api call to send a message regarding appointments. A different message is passed for new appointments, modifications, and cancellations.
  def send_wavecell_appointment_notification_to_phone_number(doctor_phone_number, message)
      begin
          WaveCell.wave_cell_send_sms(doctor_phone_number, message)
      rescue
          return false
      end
  end


end