class EditorialMember < ApplicationRecord
  paginates_per 20

  validates :name, :description, presence: true
  validates :name, uniqueness: { case_sensitive: false }

  mount_uploader :member_photo, ProfilePhotoUploader
  normalize_attributes :designation, :credentials, :city, :state
end
