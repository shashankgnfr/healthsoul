class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :commentable, polymorphic: true

  after_create :notify_yext

  private

  def notify_yext
    commentable.notify_yext
  end

end
