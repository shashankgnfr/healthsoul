class RoleAssigner < Struct.new(:user, :name, :resource)
  attr_reader :claimable

  def assign
    if valid?
      # set_is_claimed(role_resource)
      user.add_role(role_name, role_resource)
      true
    else
      false
    end
  end

  def set_is_claimed(role_resource)
    role_resource.update_column('is_claimed', true) if role_resource.attributes.has_key? 'is_claimed'
  end

  def role_name
    name.to_sym
  end

  def valid?
    role_name.present?
  end

  def role_resource
    resource.present? ? resource : nil
  end
end
