class Promotion < ApplicationRecord
  paginates_per 20
	resourcify
	belongs_to :country
	mount_uploader :promotion_image, PromotionImageUploader
  DISPLAY_ON = ['Providers', 'Users']
  has_many :promotion_winners
end
