class HealthScore < ApplicationRecord
  belongs_to :doctor
  has_many :health_score_details,  dependent: :delete_all
  accepts_nested_attributes_for :health_score_details, reject_if: :all_blank, allow_destroy: true
  # before_save :check_details

  def listing_score
  	return (1..99).to_a.sample
  end

  def rating_score
  	return (1..99).to_a.sample
  end

end
