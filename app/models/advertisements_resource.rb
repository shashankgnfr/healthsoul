class AdvertisementsResource < ApplicationRecord
  belongs_to :advertisement
  belongs_to :resource, polymorphic: true
  validates :resource_type, :advertisement_id, :resource_id, :resource_action, presence: true
end
