class Award < ApplicationRecord
	belongs_to :doctor, class_name: "Doctor", foreign_key: :location_id
	belongs_to :location, class_name: "Location", foreign_key: :location_id
end