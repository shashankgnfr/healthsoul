class Admin::EventMembersController <  Admin::ResourceController

  def index
    if search_params.present?
      @search_term = search_params[:email]
      @resources = EventMember.where("email ilike ? or email ilike ?", "%#{@search_term}%", "#{@search_term}%").order(created_at: :desc).page(params[:page]).per(20)
    else    
      @resources = EventMember.order(created_at: :desc).page(params[:page]).per(20)
    end
  end

  def update
    @resource = klass.find(params[:id])
    if @resource.update(resource_params)
      if params[:commit] == 'Save & Convert'
        obj = @resource.convert_lead_to_object
        if @resource.is_student?
          redirect_to edit_admin_medical_student_path(obj.id), notice: "Event Member Updated Successfully And Converted Leat to Medical Student."  
        else
          redirect_to edit_admin_doctor_path(obj.id), notice: "Event Member Updated Successfully And Converted Lead to Doctor."
        end
      else
        redirect_to update_path, notice: "Record Updated Successfully."
      end
    else
      render :edit
    end
  end

  private

  def search_params
    if params[:event_member].present?
      params.require(:event_member).permit(:email)
    end
  end
end
