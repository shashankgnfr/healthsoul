class Admin::SpecialityMergesController < Admin::BaseController
  def update
    @remove = PracticeSpeciality.find params[:id]
    @new = PracticeSpeciality.find params[:merge][:merge_to]
    @remove.doctors.update_all([ "specialities = replace(specialities, ?, ?)", @remove.name, @new.name ])
    DoctorSpeciality.where(practice_speciality_id: @remove.id).update_all(practice_speciality_id: @new.id)
    @remove.destroy
    redirect_to admin_practice_specialities_path, notice: "Replaced"
  end
end
