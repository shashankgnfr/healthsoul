class Admin::LocationsController <  Admin::ResourceController
  def index
    klass
    if search_params.present?
      @search = Search::AdminSearch.new(search_params)
    else
      @search = Search::AdminSearch.new
     # @search.co = default_locale
    end
    @resources = @search.results.reorder(:hwaid).page(page[:page].to_i).per(100)
  end

  def new
    if params[:location_type_id].present?
      @resource = Location.new(location_type_id: params[:location_type_id])
    end
  end

  def analytics_report
    @resource = Location.where(slug: params[:id]).first
    chart_url = params[:location][:analytic_chart]
    chart_url = chart_url.gsub("data:image/png;base64,", '')
    chart_href = "#{Rails.root}/tmp/chart_#{@resource.class.name.downcase}_#{@resource.id}.png"
    File.open(chart_href, 'wb') do |f|
      f.write(Base64.decode64(chart_url))
    end
    params[:location][:analytic_chart_href] = chart_href
    AnalyticsMailer.analytics_report(@resource, params[:location]).deliver_now
    flash[:notice] = 'Mail sent successfully.'
    redirect_to :back
  end

  private

  def search_params
    if params[:search_admin_search].present?
      params.require(:search_admin_search).permit(:hwaid, :location_type_id, :co, :sub_type, :without_geolocation, :lat, :lng, :near, :city, :state, :name, :within, :co, :stars => [])
    end
  end

  def page
    params.permit(:page)
  end

  def update_path
    [:admin, @resource]
  end
end
