class Admin::UsersController < Admin::ResourceController

  def index
    condition = []
    if search_params.present?
      @resources = User
      @search_term = search_params[:email]
      @resources = @resources.where("email ilike ?", "%#{@search_term}%") if search_params[:email].present?
    else    
      condition << "users.subscription_only is true" if params[:subscription_only].present?
      # User.rb => is_provider - is bugguy method. as roles.include?(:moderator) condition is wrong
      if params[:roles].present?
        condition << " (users.marked_as_provider is true or (roles.name = 'moderator' and roles.resource_type IS NULL AND roles.resource_id IS NULL) )"
      # else
      #    condition << " (users.marked_as_provider is not true and (roles.name != 'moderator' and roles.resource_type IS NULL AND roles.resource_id IS NULL))" 
      end
      @resources = User.joins('
      LEFT  JOIN "users_roles" ON "users_roles"."user_id" = "users"."id"
      LEFT  JOIN "roles" ON "roles"."id" = "users_roles"."role_id"
      LEFT JOIN "user_referrals" ON "user_referrals"."referred_user_id" = "users"."id" 
      LEFT JOIN "users" "referring_users_users" ON "referring_users_users"."id" = "user_referrals"."user_id"')

    end
    @resources = @resources.where(condition.join(' and ')).includes(:referring_user).order(created_at: :desc).page(params[:page]).per(100)
  end

  def update
    if resource_params[:password].blank?
      [:password, :password_confirmation].each { |k| resource_params.delete(k) }
    end
    if @resource.update(resource_params)
      if params[:role_id].present?
        params[:role_id].each do |id|
          @resource.roles.delete(Role.find(id))
        end
        resource = params[:user][:role_resource_type].constantize.find(params[:user][:role_resource_id])
        RoleAssigner.new(@resource, params[:user][:role_name], resource).assign
      end
      redirect_to index_path, notice: "Updated"
    else
      render :edit
    end
  end

  private

  def search_params
    if params[:user].present?
      params.require(:user).permit(:email)
    end
  end
end
