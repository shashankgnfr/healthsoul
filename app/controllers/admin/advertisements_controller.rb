class Admin::AdvertisementsController < Admin::ResourceController

  def index
    @resources = klass.order(:created_at).page params[:page]
  end

  def resource_mapping
    @advertisement = Advertisement.where(id: params[:id]).first
    if request.xhr?
      @resource_data = if params[:resource_type] == 'Blog'
        eval(params[:resource_type]).order(:title).pluck(:id, :title)
      elsif params[:resource_type] == 'ActsAsTaggableOn::Tag'
        ActsAsTaggableOn::Tag.friendly.where(name: Blog.available_category_tags).order(:name).pluck(:id, :name)
      else
        eval(params[:resource_type]).order(:name).pluck(:id, :name)
      end
      @element_id = params[:resource_id]
      @element = params[:resource_type]
      render file: 'admin/advertisements/resource_mapping.js.erb'
    else
      if request.patch?
        params.permit!
        if @advertisement.type.present? && params[@advertisement.type.underscore].present?
          @advertisement.update_attributes(params[@advertisement.type.underscore])
          flash[:notice] = 'Mapped resources successfully.'
          redirect_to action: :index
        elsif !params[@advertisement.type.underscore].present?
          flash[:notice] = 'Map atleast one Resource.'
          render action: :resource_mapping, id: @advertisement.id
        else
          flash.now[:error] = @advertisement.errors.full_messages.to_sentence
          render action: :edit, id: @advertisement.id
        end
      end
      @blogs = Blog.pluck(:title, :id)
      @tags = ActsAsTaggableOn::Tag.friendly.where(name: Blog.available_category_tags).pluck(:name, :id)
    end
  end
end