class Admin::BaseController < ApplicationController
  layout 'admin'
  before_action :authenticate_user!, :authenticate_admin!
  private
  def authenticate_admin!
    unless current_user.has_role?(:admin)
      redirect_to root_path, alert: "You must be an admin to view this page."
    end
  end
end
