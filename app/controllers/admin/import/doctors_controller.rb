class Admin::Import::DoctorsController < Admin::BaseController
  def create
    DoctorImportJob.perform_async(params)
    redirect_to admin_doctors_path, notice: "Doctors Importing"
  end
end
