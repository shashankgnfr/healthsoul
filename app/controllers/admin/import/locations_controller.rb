class Admin::Import::LocationsController < Admin::BaseController
  def create
    ImportJob.perform_async(params)
    redirect_to admin_locations_path, notice: "Locations Importing"
  end
end
