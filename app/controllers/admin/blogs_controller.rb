class Admin::BlogsController < Admin::ResourceController
  def index
    klass
    @resources = blogs_by_search_tags
  end

  def image_upload
    uploader = ApiUploader.new
    uploader.store!(params[:file])
    respond_to do |format|
      format.json { render :json => { status: 'OK', link: uploader.url}}
    end
  end

  private

  def blogs_by_search_tags
    blogs = Blog
    if params['search'].present?
      if params['search']['q'].present?
        @search_query = params['search']['q']
        blogs = Blog.title_contains(@search_query)
      end
      if params['search']['tag'].present?
        @selected_tag = params['search']['tag'].downcase
        tag = ActsAsTaggableOn::Tag.where(["lower(name) in (?)", [@selected_tag, @selected_tag.gsub("-", " ")]]).first
        blogs = blogs.tagged_with(tag)
      end
    end
    blogs.order(created_at: :desc).page(params[:page]).per(20)
  end


end
