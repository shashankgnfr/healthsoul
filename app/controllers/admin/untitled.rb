  class Admin::DoctorsController <  Admin::ResourceController
  def index
    klass
    if params[:search_doctor].present?
      @search = Search::Doctor.new(search_params)
      @resources = @search.results(params[:page], 100)
    else
      @search = Search::Doctor.new
      @resources = Doctor.order(:name).page(params[:page]).per(100)
    end
  end

  def edit
    @resource.health_scores.new
    #health score details
   # @resource.health_score_details_attributes.new

  end

  def update
    if @resource.present?
      total_listing_score = 0
      total_listing_error = 0
      total_ratings = 0
      health_score = 0
      total_reviews = 0
      repution_scoree = 0
      hs = @resource.health_scores.create(:score => health_score )
      if params["doctor"]["health_score_details_attributes"].present?
        params["doctor"]["health_score_details_attributes"].keys.each do |key|
          hs_details = params["doctor"]["health_score_details_attributes"][key]
          params["doctor"]["health_score_details_attributes"][key]["health_score_id"] = hs.id
          # calculation
          #total_rating = listing_score + rating_score
          total_listing_score = (hs_details["name"].to_i + hs_details["address"].to_i  + hs_details["phone"].to_i + hs_details["photo"].to_i + hs_details["website"].to_i + hs_details["speciallity"].to_i + hs_details["hour"].to_i + hs_details["fee"].to_i + hs_details["about_me"].to_i + hs_details["education"].to_i ) + total_listing_score

          if hs_details["name"].to_i == 0 || hs_details["name"].to_i == 1
            total_listing_error = total_listing_error + 1
          end

          if hs_details["address"].to_i == 0 || hs_details["address"].to_i == 1
            total_listing_error = total_listing_error + 1
          end

          if hs_details["phone"].to_i == 0 || hs_details["phone"].to_i == 1
            total_listing_error = total_listing_error + 1
          end

          if hs_details["photo"].to_i == 0 || hs_details["photo"].to_i == 1
            total_listing_error = total_listing_error + 1
          end

          if hs_details["website"].to_i == 0 || hs_details["website"].to_i == 1
            total_listing_error = total_listing_error + 1
          end

          if hs_details["speciallity"].to_i == 0 || hs_details["speciallity"].to_i == 1
            total_listing_error = total_listing_error + 1
          end

          if hs_details["hour"].to_i == 0 || hs_details["hour"].to_i == 1
            total_listing_error = total_listing_error + 1
          end

          if hs_details["fee"].to_i == 0 || hs_details["fee"].to_i == 1
            total_listing_error = total_listing_error + 1
          end

          if hs_details["about_me"].to_i == 0 || hs_details["about_me"].to_i == 1
            total_listing_error = total_listing_error + 1
          end

          if hs_details["education"].to_i == 0 || hs_details["education"].to_i == 1
            total_listing_error = total_listing_error + 1

          end



          # 





          # Total rating score calculation
          # rating_score = hs_details["number_of_reviews"].to_i + rating_score
          # total_reviews = hs_details["rating"].to_i + total_reviews


          total_reviews = hs_details["number_of_reviews"].to_i + total_reviews
          total_ratings = hs_details["rating"].to_i + total_ratings



        end
      end





  


      total_listing_score + ((total_reviews/total_ratings.to_f)*100)
      repution_scoree = (total_ratings/total_reviews.to_f)*100


      health_score = (total_listing_score + repution_scoree)

      puts "*******************************************"
      puts "*******************************************"
      puts "*******************************************"
      puts "*******************************************"

      puts "total_listing_score #{total_listing_score}" 
      puts "total_listing_error #{total_listing_error}" 
      puts "rating_score #{total_ratings}" 
      puts "health_score #{health_score}" 
      puts "total_reviews #{total_reviews}" 
      puts "repution_scoree #{repution_scoree}" 



      hs.update_attributes(:score => health_score ,:listing_score => total_listing_score ,:rating_score => total_reviews )
      if @resource.update(resource_params)
        redirect_to update_path, notice: "Record Updated Successfully."
      else
        render :edit
      end
    else
      flash[:error] = I18n.t('not_found', type: 'Record')
      redirect_to controller: :home, action: :index
    end
  end

# platform: string, website_url: string, h_type: string, name: integer, address: integer, phone: integer, photo: integer, website: integer, speciallity: integer, hour: integer, fee: integer, about_me: integer, education: integer, rating: integer, number_of_reviews: integer, total_rating: integer


  def practices
    @resource_slug = 'practice'
    @practice_id = LocationType.practice_obj&.id
    if practice_params.present?
      location_params = practice_params
      location_params["location_type_id"] = @practice_id
      @search = Search::AdminSearch.new(location_params)
    else
      @search = Search::AdminSearch.new(location_type_id: @practice_id)
    end
    @resources = @search.results.reorder(:hwaid).page(page[:page].to_i).per(100)
    render 'index'
  end


  def search_params
    if params[:search_doctor].present?
      params[:search_doctor][:city] =  params[:search_doctor][:city].parameterize if params[:search_doctor][:city]
       params[:search_doctor][:state] =  params[:search_doctor][:state].parameterize if params[:search_doctor][:state]
      params.require(:search_doctor).permit!
    end
  end

  def practice_params
    if params[:search_admin_search].present?
      params.require(:search_admin_search).permit(:hwaid, :location_type_id, :co, :sub_type, :without_geolocation, :lat, :lng, :near, :city, :state, :name, :within, :co, :speciality, :stars => [])
    end
  end

  def page
    params.permit(:page)
  end

  def analytics_report
    @resource = Doctor.where(slug: params[:id]).first
    chart_url = params[:doctor][:analytic_chart]
    chart_url = chart_url.gsub("data:image/png;base64,", '')
    chart_href = "#{Rails.root}/tmp/chart_#{@resource.class.name.downcase}_#{@resource.id}.png"
    File.open(chart_href, 'wb') do |f|
      f.write(Base64.decode64(chart_url))
    end
    params[:doctor][:analytic_chart_href] = chart_href
    AnalyticsMailer.analytics_report(@resource, params[:doctor]).deliver_now
    flash[:notice] = 'Mail sent successfully.'
    redirect_to :back
  end
end
