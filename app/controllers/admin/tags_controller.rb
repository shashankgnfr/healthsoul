class Admin::TagsController < Admin::BaseController
  def index
    @tags = Blog.available_category_tags.sort
  end

  def edit

    @tag = ActsAsTaggableOn::Tag.friendly.find(params[:id])
  end

  def update
    @tag = ActsAsTaggableOn::Tag.friendly.find(params[:id])
    if @tag.update_attributes(tag_params)
      redirect_to admin_tags_path
    else
      redirect_to :back
    end
  end

  private

  def tag_params
    params.require(:acts_as_taggable_on_tag).permit(:title, :description, :meta_title, :page_content, :featured_image, :featured_image_alt, :custom_meta_tags, :page_faqs)
  end

end
