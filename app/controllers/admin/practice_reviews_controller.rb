class Admin::PracticeReviewsController < Admin::ResourceController

  def index   
    if search_params.present? && params[:button] == 'Search'
      @search_term = search_params[:search]
      @resources = PracticeReview.joins([:location, :user]).where(["users.email ilike ? or locations.name ilike ?", "#{@search_term}%", "#{@search_term}%"]).order(created_at: :desc).page params[:page]    
    else
      @search_term = nil
      @resources = PracticeReview.joins([:location, :user]).order(created_at: :desc).page params[:page]
    end
  end

  private

  def search_params
    if params[:practice_review].present?
      params.require(:practice_review).permit(:search)
    end
  end

end
