class Admin::ResourceController < Admin::BaseController
  require 'csv'
  before_action :find_resource, only: [:show, :edit, :destroy, :update]
  before_action :create_resource, only: [:new]

  def index
    @resources = klass.order(created_at: :desc).page params[:page]
  end

  def new
  end

  def show
  end

  def destroy
    @resource.destroy
    redirect_to index_path, notice: "Record Deleted Successfully."
  end

  def edit
  end

  def create
    @resource = klass.new(resource_params)
    if @resource.save(resource_params)
      redirect_to index_path, notice: "Record Created Successfully."
    else
      render :new
    end
  end

  def update
    if @resource.present?
      if @resource.update(resource_params)
        redirect_to update_path, notice: "Record Updated Successfully."
      else
        render :edit
      end
    else
      flash[:error] = I18n.t('not_found', type: 'Record')
      redirect_to controller: :home, action: :index
    end
  end

  private

  def resource_params
    params.require(controller_name.singularize.to_sym).permit!
  end

  def authorize_admin
    unless current_user.super_admin? || true_user.super_admin?
      redirect_to dashboard_path
    end
  end

  def find_resource
    @resource = klass.find(params[:id])
  end

  def klass
    @resource_name = self.controller_name.camelize
    @resource_slug = self.controller_name
    self.controller_name.singularize.camelize.constantize
  end

  def resource_path(resource)
    [:admin, resource]
  end

  def index_path
    [:admin, controller_name.to_sym]
  end

  def update_path
    index_path
  end

  def create_resource
    @resource = klass.new
  end
end
