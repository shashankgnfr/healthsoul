class Admin::PromotionWinnersController <  Admin::ResourceController
  def index
    @promotion = Promotion.where(id: params[:promotion_id]).first
    @resources = @promotion.promotion_winners.order(created_at: :desc).page params[:page]
  end

  def new
    @promotion = Promotion.where(id: params[:promotion_id]).first
    @resource = @promotion.promotion_winners.new()
  end

  def create
    @promotion = Promotion.where(id: params[:promotion_id]).first
    @resource = @promotion.promotion_winners.new(winner_params)
    if @resource.save!
      redirect_to admin_promotion_promotion_winners_path(@promotion.id), notice: "Record Created Successfully."
    else
      render :new
    end
  end

  def edit
    @promotion = Promotion.where(id: params[:promotion_id]).first
    @resource = PromotionWinner.where(id: params[:id]).first
  end

  def update
    @promotion = Promotion.where(id: params[:promotion_id]).first
    @resource = PromotionWinner.where(id: params[:id]).first
    if @resource.update(winner_params)
      redirect_to admin_promotion_promotion_winners_path(@promotion.id), notice: "Record Updated Successfully."
    else
      render :edit
    end
  end

  def destroy
    @promotion = @resource.promotion
    @resource.destroy
    redirect_to admin_promotion_promotion_winners_path(@promotion.id), notice: "Record Deleted Successfully."
  end


  private
  def winner_params
    params.require(:promotion_winner).permit!
  end
end
