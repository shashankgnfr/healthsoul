class Admin::RatingsController < Admin::ResourceController

  def index   
    if params['commit'] == 'Search' && params[:rating][:search].present?
      @search_term = params[:rating][:search].strip
      @resources = klass.joins([:location, :user]).where(["locations.name ilike ? or users.email ilike ?", "%#{@search_term}%", "%#{@search_term}%"]).order(created_at: :desc).page params[:page]
    elsif params['button'] == 'Download' && params[:rating][:co].present?
      country_name = params[:rating][:co].strip.downcase
      country_code = Location.where("lower(country) = ?",  country_name).first.try(:country_code) || Doctor.where("lower(country) = ?",  country_name).first.try(:country_code)
      download_xls(country_code)
    else
      if params['button'] == 'Download' && !params[:rating][:co].present?
        flash[:notice] = 'Kindly select country.'
      end
      @resources = klass.order(created_at: :desc).page params[:page]
    end
  end

  private

  def find_resource
    @resource = klass.admin_all.find(params[:id])
  end


  def download_xls(country_code)
    drs = DoctorReviewView.connection.execute("
      SET healthsoul.country_code = '#{country_code}';
    ") && DoctorReviewView.all

    hospitals = LocationReviewView.connection.execute("
      SET healthsoul.country_code = '#{country_code}';
      SET healthsoul.location_type = 'Hospital';
    ") && LocationReviewView.all

    book = Spreadsheet::Workbook.new
    header_format = Spreadsheet::Format.new weight: :bold, size: 8

    if drs.present?
      doctor_sheet = book.create_worksheet name: 'Doctor'
      doctor_sheet.row(0).default_format = header_format
      header = ['Name', 'Address', 'City', 'State', 'Country', 'ZIP', 
          'Organization', 'Specialities', 'Phone', 'Inpatient Overall', 'Outpatient Overall', 'Total Overall', 'Explaination of diagnosis', 'Provider trust', 'Staff', 'Ease of scheduling', 'Wait time', 'No of reviews']
      doctor_sheet.row(0).concat header
      drs.each_with_index do |dr, index|
        row_data = [
          dr.name,
          dr.address,
          dr.city,
          dr.state,
          dr.country,
          dr.zip,
          dr.organization_name,
          dr.specialities,
          dr.phone,
          dr.inpatient,
          dr.outpatient,
          dr.overall_experience,
          dr.explaintation,
          dr.trust,
          dr.staff,
          dr.appt,
          dr.wait,
          dr.reviews_count
        ]
        doctor_sheet.row(index+1).concat row_data
      end
    end

    if hospitals.present?
      hospital_sheet = book.create_worksheet name: 'Hospital'
      hospital_sheet.row(0).default_format = header_format
      header = ['Name', 'Address', 'City', 'State', 'Country', 'ZIP', 'Type', 'Phone', 'Overall Experience', "Doctor's care", 'Nursing', 'Cleanliness', 'Food Services', 'Total Reviews']
      hospital_sheet.row(0).concat header
      hospitals.each_with_index do |l, index|
        row_data = [
          l.name,
          l.address,
          l.city,
          l.state,
          l.country,
          l.zip,
          l.location_type,
          l.phone,
          l.overall_experience,
          l.doctor_care,
          l.nursing_care,
          l.cleanliness,
          l.food_services,
          l.reviews_count
        ]
        hospital_sheet.row(index+1).concat row_data
      end
    end
    
    ['Health Insurance', 'Travel Health Insurance'].each do |type|
      LocationReviewView.connection.query_cache.clear

      result = LocationReviewView.connection.execute("
        SET healthsoul.country_code = '#{country_code}';
        SET healthsoul.location_type = '#{type}';
      ") && LocationReviewView.all

      if result.present?
        sheet = book.create_worksheet name: type
        sheet.row(0).default_format = header_format
        header = ['Name', 'Address', 'City', 'State', 'Country', 'ZIP', 'Type', 'Phone', 'Overall Experience', 'Customer Service', 'Cost Satisfaction', 'Claims Process', 'Total Reviews']
        sheet.row(0).concat header
        result.each_with_index do |l, index|
          row_data = [
            l.name,
            l.address,
            l.city,
            l.state,
            l.country,
            l.zip,
            l.location_type,
            l.phone,
            l.overall_experience,
            l.customer_service,
            l.cost_satisfaction,
            l.claims_process,
            l.reviews_count
          ]
          sheet.row(index+1).concat row_data
        end
      end
    end
    
    file_name = "Review_Data_#{country_code}.xls"
    review_file = Tempfile.new(file_name, File.join(Rails.root,"tmp"))
    if !book.worksheets.empty?
      book.write review_file
      send_file review_file, disposition: :attachment, type: 'application/excel', filename: file_name
    else
      # need to optimise by sending AJAX request.
      flash[:notice] = 'No reviews found for requested country.'
      redirect_to action: :index
    end
  end
end
