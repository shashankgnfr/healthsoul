class Admin::DoctorReviewsController < Admin::ResourceController

  def index   
    if search_params.present? && params[:button] == 'Search'
      @search_term = search_params[:search]
      @resources = DoctorReview.joins([:doctor, :user]).where(["users.email ilike ? or doctors.name ilike ?", "#{@search_term}%", "#{@search_term}%"]).order(created_at: :desc).page params[:page]    
    else
      @search_term = nil
      @resources = DoctorReview.joins([:doctor, :user]).order(created_at: :desc).page params[:page]
    end
  end

  private

  def search_params
    if params[:doctor_review].present?
      params.require(:doctor_review).permit(:search)
    end
  end

end
