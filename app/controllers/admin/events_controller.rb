class Admin::EventsController <  Admin::ResourceController
  layout 'design' , only: :show
  
  def show
    @event = Event.find_by_id(params[:id])
    @event_member = @event.event_members.new if @event
    render file: 'events/index'
  end
end
