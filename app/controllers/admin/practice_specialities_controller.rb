class Admin::PracticeSpecialitiesController < Admin::ResourceController

  def index
    if search_params.present?
      @search_term = search_params[:name]
      @resources = PracticeSpeciality.where("name ilike ? or name ilike ?", "%#{@search_term}%", "#{@search_term}%").order(name: :asc).page(params[:page]).per(20)
    else
      @resources = klass.order(name: :asc).page params[:page]
    end
  end

  private

  def search_params
    if params[:practice_speciality].present?
      params.require(:practice_speciality).permit(:name)
    end
  end

  def faqs
    @speciality = klass.where(id: params[:id]).first
    @faqs = @speciality.practice_specialities_faqs.order(created_at: :asc, faq_title: :asc).page params[:page]
  end
end
