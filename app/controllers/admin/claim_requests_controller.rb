class Admin::ClaimRequestsController < Admin::ResourceController
  def new
    @resource_name = self.controller_name.camelize
    @resource = ClaimRequest.new
    @resource.user = current_user
  end

  def create
    @resource_name = self.controller_name.camelize
    @resource = ClaimRequest.new(resource_params.except(:user))
    @resource.created_by = current_user.id
    if resource_params[:user].present?
      @resource.user.update_attributes(resource_params[:user].to_h)
    end
    if @resource.save
      redirect_to action: :index
    else
      render action: :new
    end
  end

  def index
    @resources = ClaimRequest
    if request.xhr?
      claimable_model = params[:claimable]
      claimable_term = params[:claimable_term]
      user_name = params[:user_name]
      if claimable_model && claimable_term
        data = eval(claimable_model).where.not(name: nil).where(["name ilike ? or name ilike ?", "#{claimable_term}%",  "%#{claimable_term}%"]).order(name: :asc).page(params[:page]).per(5)
        render json: data.map{|v| {id: v.id, text: v.name} }
      elsif user_name
        data = User.where.not(email: nil).where(["username ilike ? or username ilike ? or email ilike ? or email ilike ?", "#{user_name}%",  "%#{user_name}%", "#{user_name}%",  "%#{user_name}%"]).order(email: :asc).page(params[:page]).per(5)
        render json: data.map{|v| {id: v.id, text: v.email} }
      end

    elsif params['claim_requests'].present?
      if params['claim_requests']['email'].present?
        @email = params['claim_requests']['email']
        @resources = @resources.email_search(@email.downcase)
      end
    end
    @resources = klass.where(granted: params[:granted]) if params[:granted].present?
    @resources = @resources.order(created_at: :desc).page(params[:page])
  end


  def update
    # To get around some strange behaviour of accepts nested_attributes, just seperate out the user params to update
    # the relevent user fields.
    @user = User.find(resource_params["user_id"])
    @saved = User.transaction do
      begin
        @resource.update_attributes!(resource_params.except(:user))
        @user.update_attributes!(resource_params["user"])
      rescue
        false
      end
    end
    if @saved
      if @resource.granted?
        RoleAssigner.new(@resource.user, "moderator", @resource.claimable).assign
      else
        current_user.remove_role(:moderator, @resource.claimable)
      end
      redirect_to index_path, notice: "Updated"
    else
      render :edit
    end
  end
end
