class Admin::DoctorReportsController < Admin::BaseController
  def index
    if params[:restart].present?
      RestartDoctorImportJob.perform_async(params)
      redirect_to admin_doctor_reports_path
    end
    if params[:search_doctor].present?
      p = params.require(:search_doctor).permit!
      p = p.merge(co: 'USA')
      @report = Search::Doctor.new(p)
    else
      @report = Search::Doctor.new(co: 'USA')
    end
  end

  def doctor_uploads
    @report = DoctorUpload.select(:State, "count(id) as count", "count(nullif(imported, false)) as imported_count").group(:State).order(:State)
    @report_actual = Doctor.select(:state, "count(id) as count").group(:state).order(:state).size
    @total = @report.map(&:count).sum
    @total_queue_uploaded = @report.map(&:imported_count).sum
    @total_actual = @report_actual.map{ |k, v| v }.sum
  end
end
