class PatientStoriesController < ApplicationController
  layout 'design'
  def new
    add_breadcrumb "Patient Story"
    @patient_story = PatientStory.new
  end

  def create
    @patient_story = PatientStory.new(patient_story_params)
    if @patient_story.save
      ContactMailer.request_notification(@patient_story).deliver_now
      ContactMailer.request_acknowledgement(@patient_story).deliver_now
      redirect_to new_patient_story_path, notice: I18n.t('patient_story.submitted')
    else
      render :new
    end
  end

  def index
    # @patient_stories = PatientStory.order(:name, :created_at).page(params[:page]).per(25)
    # set_meta_tags title: t("meta_title.patient_story")
    # set_meta_tags description: t("meta_description.patient_story")
    # @heading = "The Patient Story"
    redirect_to new_patient_story_path
  end

  def show
    @patient_story = PatientStory.where(id: params[:id]).first
    unless @patient_story
      flash[:error] = "Not found requested #{params[:id]}."
      redirect_to root_url
    end
  end

  def download
    patient_story = PatientStory.where(id: params[:id]).first
    if patient_story
      attachment = patient_story.word_document.file.path
      if attachment.blank? or !File.exist? attachment
        flash[:error] = I18n.t('missing_attachment', type: "Patient's word document")
        redirect_to action: :index
      else
        send_file attachment, disposition: :attachment
      end
    else
      flash[:error] = I18n.t('not_found', type: 'Patient Story')
      redirect_to action: :index
    end
  end

  private

  def patient_story_params
    params.require(:patient_story).permit!
  end
end
