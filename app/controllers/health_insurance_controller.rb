class HealthInsuranceController < SearchesController
  def show
    add_breadcrumb 'Health Insurance', search_health_insurances_by_country_path
    set_meta_tags title: t("meta.hi_search", country: formatted_country(current_country))
    set_meta_tags description: t("meta_description.hi_search", country: formatted_country(current_country))
    @heading = "Find Health Insurance"
    @search_class = "health-ins"
    if params[:clear] == "true" || params[:set_locale]
      session[:health_ins_search_url] = nil
    end
    if search_params.present?
      session[:health_ins_search_url] = request.fullpath.gsub("ajax=true", "")
      @search = Search::HealthInsurance.new(search_params)
      @search.order = "reviews_a" if search_params[:order].blank?
      @search_heading = set_heading(search_params[:co] || current_country)
      @search_results = @search.results.page(params[:page]).per(25).decorate
      if params[:ajax] == "true" && params[:page].blank?
        @results = @search.results.page(params[:page]).decorate
        render layout: false and return
      end
    else
      if session[:health_ins_search_url].present?
        redirect_to session[:health_ins_search_url] and return
      end
      @search = Search::HealthInsurance.new
      @search.co = current_country
      @search.order = "reviews_a"
      @search.default_country = current_country
      @search_heading = set_heading(@search.co)
    end
    @search_results = @search.results.page(params[:page]).per(25).decorate
    @blogs = Blog.order(published_at: :desc).limit(4).decorate
  end

  private

  def search_params
    if params[:search].present?
      params[:search_health_insurance] = params[:search]
    end
    if params[:search_health_insurance].present?
      params.require(:search_health_insurance).permit(:order, :departments, :co, :sub_type, :co, :name, :stars => [])
    end
  end

  def set_heading(country)
    country = formatted_country(country)
    "Find the list of Best Health insurance in #{country} enlisted below. Reach the most suitable Health Insurance in #{country} easily and compare health plans !"
  end
end
