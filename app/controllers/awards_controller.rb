class AwardsController < ApplicationController
  layout 'design'

  def index
    add_breadcrumb "Awards", :awards
    set_meta_tags title: "Patient Experience Awards"
    set_meta_tags description: "Patient Experience Awards"
  end

  def gold_doctors
    add_breadcrumb "Gold Doctors", :gold_doctors_awards
    set_meta_tags title: "Excellent Patient Experience Awards Doctors"
    set_meta_tags description: "Excellent Patient Experience Awards Doctors"
    country = params[:location].present? ? params[:location] : "USA"
    @doctors = Doctor.joins(:awards).where("awards.location_type =? AND awards.year =? AND awards.award_type =? AND doctors.country =?", 'Doctor', Date.today.year, 'gold', country).order("doctors.ratings_count desc").limit(50)
  end

  def diamond_doctors
    add_breadcrumb "Diamond Doctors", :diamond_doctors_awards
    set_meta_tags title: "Exceptional Patient Experience Awards Doctors"
    set_meta_tags description: "Exceptional Patient Experience Awards Doctors"
    country = params[:location].present? ? params[:location] : "USA"
    @doctors = Doctor.joins(:awards).where("awards.location_type =? AND awards.year =? AND awards.award_type =? AND doctors.country =?", 'Doctor', Date.today.year, 'diamond', country).order("doctors.ratings_count desc").limit(50)
  end

  def gold_hospitals
    add_breadcrumb "Gold Hospitals", :gold_hospitals_awards
    set_meta_tags title: "Excellent Patient Experience Awards Hospitals"
    set_meta_tags description: "Excellent Patient Experience Awards Hospitals"
    country = params[:location].present? ? params[:location] : "USA"
    @hospitals = Location.joins(:awards).where("awards.location_type =? AND awards.year =? AND awards.award_type =? AND locations.country =?", 'Hospital', Date.today.year, 'gold', country).order("locations.ratings_count desc").limit(50)
  end

  def diamond_hospitals
    add_breadcrumb "Diamond Doctors", :diamond_hospitals_awards
    set_meta_tags title: "Exceptional Patient Experience Awards Hospitals"
    set_meta_tags description: "Exceptional Patient Experience Awards Hospitals"
    country = params[:location].present? ? params[:location] : "USA"
    @hospitals = Location.joins(:awards).where("awards.location_type =? AND awards.year =? AND awards.award_type =? AND locations.country =?", 'Hospital', Date.today.year, 'diamond', country).order("locations.ratings_count desc").limit(50)
  end
end
