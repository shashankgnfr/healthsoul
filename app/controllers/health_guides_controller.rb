class HealthGuidesController < ApplicationController
  layout 'design'
  def index
    set_meta_tags title: t("meta_title.health-guides")
    set_meta_tags description: t("meta_description.health-guides") 
    @co = Country.where("lower(name) ilike ?", current_country.downcase).first.try(:alpha3).downcase
    if @co
      render :show
    else
      redirect_to root_url
    end
  end

  def show
    @co = params[:co]
  end

  def health_guide
    @co = params[:co]
  end
end
