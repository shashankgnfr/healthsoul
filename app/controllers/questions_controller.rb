class QuestionsController < ApplicationController
  layout 'design'
  before_action :authenticate_user!, :find_resource
  add_breadcrumb "Question"

  def new
    set_meta_tags noindex: true
    @question = @resource.questions.new
  end

  def create
    set_meta_tags noindex: true
    @question = @resource.questions.new(question_params)
    @question.user = current_user
    if @question.save
      if @resource.class.to_s == 'Location'
        QuestionMailer.notify_manager(@question).deliver_now
      end
      redirect_to_resource
    else
      render :new
    end
  end

  private

  def question_params
    params.require(:question).permit(:body)
  end

  def find_resource
    if params[:doctor_id].present?
      @resource = Doctor.find(params[:doctor_id])
    else
      @resource = Location.find(params[:location_id])
    end
  end

  def redirect_to_resource
    if @resource.doctor?
      redirect_to helpers.doctor_path(@resource, anchor: "question#{@question.id}"), notice: t("question.created")
    elsif @resource.hospital?
      redirect_to helpers.location_path(@resource, anchor: "question#{@question.id}"), notice: t("question.created")
    else
      redirect_to [@resource, anchor: "question#{@question.id}"], notice: t("question.created") 
    end
  end
end
