include WaveCell
class SessionsController < Devise::SessionsController
  layout 'design'
  after_filter :after_login, only: :create

  def new
	  add_breadcrumb "Login"
    set_meta_tags title: "Read Reviews | Find a Healthcare Provider | Users Sign In"
    super
  end

	def create
		add_breadcrumb "Login"
		super
		# Change a user from subscription only to full user after that user has logged in.
		if current_user.subscription_only
			current_user.update_attribute(:subscription_only, false)
		end
	end

	def login_or_sign_up_phone
		add_breadcrumb "Login on Sign Up Phone"

	    begin
	    	otp = rand.to_s[2...6]
	      phone_number =  "+#{params[:phone_country_code]}#{params[:phone_number]}"
	      #"HealthSoul | Please copy this otp and paste it into your browser #{otp}")
	      text = "Your healthsoul verification code is #{otp}. Stay happy and healthy :)"
	      WaveCell.wave_cell_send_sms(phone_number, text)
	      session[:phone_country_code] = params[:phone_country_code]
	      session[:phone_number] = params[:phone_number]
	      session[:phone_otp] = otp
	      session[:otp_send_time] = Time.now
	      redirect_to verify_phone_path, notice: "We have sent otp on your phone. Please copy and paste it here."
	    rescue  Exception => e
	      # If the message failed to send, return false to let registration#create know that it failed and
	      # behave appropriately.
	      flash[:error] = "Please enter a valid phone number"
	      redirect_to request.referrer
	    end
	end

	def verify_phone
		return redirect_to root_path if session[:phone_number].blank?
		if session[:otp_send_time].present?
                ##if user refreshes page then timer should start from last time
                start_time =  session[:otp_send_time].to_time
                end_time = Time.now
                @timer =  end_time.to_i - start_time.to_i
		end
		if request.post?
			if session[:phone_number] == params[:phone_number] && session[:phone_otp] == params[:phone_otp]
				phone_number_with_code =  "#{session[:phone_country_code]}#{session[:phone_number]}"
				user = User.where(phone_number: phone_number_with_code).first
				msg = "Signed in Successfully!!"
				if user.present? && user.marked_as_provider
                              session[:phone_country_code] = nil
			            session[:phone_number] = nil
			            session[:phone_otp] = nil
			            session[:otp_send_time] = nil
			            flash[:error] =  "This phone number is already registered with us as a provider. This feature is only available for user(s) account."
				     return redirect_to root_path
				end
				if user.blank?
					user = User.create(phone_country_code:  session[:phone_country_code], phone_body: session[:phone_number], phone_number:  phone_number_with_code, password: Devise.friendly_token[0,20])
					user.skip_confirmation!
					msg = "Signed up Successfully!!"
				end
				current_user = user
				sign_in user
				session[:phone_country_code] = nil
		            session[:phone_number] = nil
		            session[:phone_otp] = nil
		            session[:otp_send_time] = nil
				redirect_to root_path, notice: msg
			else
				flash[:error] = "OTP is invaid. Please enter correct otp."
		            render :verify_phone
			end
		end
	end

	def after_login
		session[:ignore_subscription] = nil
	end
end
