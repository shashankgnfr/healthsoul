class FaqAnswersController < ApplicationController
  before_action :authenticate_user!, :find_question

  def create
    @answer = @question.answers.new(answer_params)
    @answer.user = current_user
    if @answer.save
      redirect_to forum_path(anchor: "faq#{@question.id}"), notice: t("answer.created")
    else
      flash[:alert] = t("answer.already_created")
      render :new
    end
  end

  private

  def answer_params
    params.require(:faq_answer).permit(:body)
  end

  def find_question
    @question = Faq.find(params[:faq_id])
  end
end
