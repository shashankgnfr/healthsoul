class DoctorCompareController < CompareController
  def index
    @locations = Doctor.where(id: params[:locations])
    if @locations.present?
      @location_type = "doctor"
    else
      redirect_to search_doctors_path, notice: "Please select doctors to compare first"
    end
  end
end
