class ClaimRequestsController < ApplicationController
  before_action :check_if_logged_in, only: [:create]
  before_action :find_resource
  layout 'design'
  add_breadcrumb 'Claim Request'

  def new
    set_meta_tags noindex: true
    if @location
      @claim_request = @location.claim_requests.new
      @claim_request.user = current_user
    else
      flash[:error] = "Not found requested #{params[:location_id]}."
      redirect_to root_url
    end
  end

  def create
    set_meta_tags noindex: true
    if @location.present?
      @claim_request = @location.claim_requests.new(claim_request_params)
    else
      @claim_request = ClaimRequest.new(claim_request_params)
    end
    @claim_request.user_id = current_user.id
    @claim_request.created_by = current_user.id
    if @claim_request.save
      ClaimRequestMailer.notify(@claim_request).deliver_now
      if @location.present?
        if @location.class.to_s == 'MedicalStudent'
          redirect_to medical_student_path(@location), notice: t("claim_request.saved")
        elsif @location.doctor?
          redirect_to helpers.doctor_path(@location), notice: t("claim_request.saved")
        elsif @location.hospital?
          redirect_to helpers.location_path(@location), notice: t("claim_request.saved")
        else
          redirect_to @location, notice: t("claim_request.saved")
        end
      else
        # Ideally, this should be a render instead of a redirect, but because of layout issues, using render attempts to
        # render the provider_info page in the application layout instead of the design layout. Rather than reproduce all
        # of the styling in application (and potentially breaking something), just redirect to the action.
        redirect_to controller: "static_pages", action: "manage"
      end
    else
      # if the claim request is being made on a location, reload the relevent location claim request new form.
      if @location.present?
        render :new
      # Otherwise it is being made from the generic location claim form on providers_info.
      else
        # Ideally, this should be a render instead of a redirect, but because of layout issues, using render attempts to
        # render the provider_info page in the application layout instead of the design layout. Rather than reproduce all
        # of the styling in application (and potentially breaking something), just redirect to the action.
        redirect_to controller: "static_pages", action: "manage"
      end
    end

  end

  private

  def check_if_logged_in
    if current_user.nil?
      if request.params["claim_request"]["user"].present?
        session[:email] = request.params["claim_request"]["user"]["email"]
      end
      if request.params.slice(:doctor_id).present?
        session[:user_return_to] = create_doctor_claim_request_url(request.params.slice(:claim_request, :doctor_id))
      else
        session[:user_return_to] = create_claim_request_url(request.params.slice(:claim_request, :location_id))
      end
      redirect_to new_user_registration_url(provider: true, hide_claim_request_form: true)
    end
  end

  def claim_request_params
    params.require(:claim_request).permit(:position, :phone, :description, :email, :country_code, :name, :created_by)
  end

  def find_resource
    if params[:location_id].present?
      @location = Location.find(params[:location_id])
    elsif params[:claim_request].present? and params[:claim_request][:location_id].present?
      @location = Location.find(params[:claim_request][:location_id])
    end
  end
end
