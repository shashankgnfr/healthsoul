include WaveCell
class RegistrationsController < Devise::RegistrationsController
  after_filter :add_referred_user, only: :create
  layout 'design'

  def new
    add_breadcrumb "Sign Up"
    set_meta_tags title: "Read Reviews | Find a Healthcare Provider | Users Sign Up"
    @provider = params[:provider].present?
    build_resource
    yield resource if block_given?
    if @provider
      @resource = resource
      @promotion = Promotion.find(params[:id]) if params[:id].present?
      render "shared/provider_info"
    else
      respond_with resource
    end
  end

  def create
    add_breadcrumb "Sign Up"
    # See https://github.com/plataformatec/devise/blob/master/app/controllers/devise/registrations_controller.rb for the default behavior being overriden.
    build_resource(sign_up_params)
    if resource.email.present?
      # Have to skip the confirmation notification and the confirmation separately, and have to skip the confirmation notification before the resource is
      # saved.
      resource.skip_confirmation_notification!
    end

    # If the user was attempting to create a review or a claim request without being logged in, then the route to the resource they were trying to create
    # is stored in a session varriable. Check, and redirect after account creation if this is the case.
    if session[:user_return_to].present?
      redirect_target = session[:user_return_to]
      session.delete(:user_return_to)
      if session[:email].present?
        session.delete(:email)
      end
    end
    resource.save
    yield resource if block_given?
    if resource.persisted?
      # If the user persisted, take the branch that sends a confirmation email.
      if resource.email.present? and resource.phone_number.blank?
        # If the user signed up with an email address but no phone number, autoconfirm the account.
        resource.skip_confirmation!
      end
      if resource.active_for_authentication?
        # Check if the user is already confirmed. This should only be the case if we are skipping confirmation.
        set_flash_message! :notice, :signed_up
        sign_up(resource_name, resource)
        if redirect_target.present?
          redirect_to redirect_target
        elsif resource.is_provider
          @claim_request = ClaimRequest.new
          render "shared/provider_info"
        else
          render "static_pages/_user_benifits_overview"
        end
      else
        if resource.phone_number.present?
          # Otherwise, if the user entered a phone number, send the confirmation code via sms instead, and skip confirmation
          # unless send_twilio_confirmation_message_to_phone_number returned false.
          if send_wavecell_confirmation_message_to_phone_number(resource.phone_number, user_confirmation_url(:confirmation_token => resource.confirmation_token))
            resource.skip_confirmation!
            set_flash_message! :notice, :signed_up_but_unconfirmed_phone
          else
            set_flash_message! :notice, :signed_up_but_messege_not_sent
          end
        else
          # Otherwise just set the flash messege. The confirmation notification itself will be sent via callback unless the skip_confirmation flag was
          # added elsewhere
          set_flash_message! :notice, :"signed_up_but_#{resource.inactive_message}"
        end
        expire_data_after_sign_in!
        respond_with resource, location: after_inactive_sign_up_path_for(resource)
      end
      if sign_up_params[:marked_as_provider]=="true"
           UserMailer.send_welcome_email_to_new_provider(resource.email).deliver
      else
          UserMailer.send_welcome_mailer_to_new_user(resource).deliver
      end
    else
      # If the user was not persisted (i.e. there were errors on the user, don't send confirmation, just reset the form.)
      flash.now[:error] = @user.errors.full_messages.to_sentence
      clean_up_passwords resource
      set_minimum_password_length
      if sign_up_params[:marked_as_provider]=="true"
        render "shared/provider_info"
      else
        respond_with resource
      end
    end

  end

  private
  def sign_up_params
    params.require(:user).permit(:username, :phone_number, :phone_body, :phone_country_code, :email, :password, :password_confirmation, :marked_as_provider, :email_opt_in)
  end

  def account_update_params
    params.require(:user).permit(:username, :phone_number, :phone_body, :phone_country_code, :email, :password, :password_confirmation, :current_password)
  end

  def add_referred_user
    if session[:ref_code].present?
      user = User.find(session[:ref_code])
      if user.referral_activated?
        user.referred_users << resource
        session[:ref_code] = nil

      end
    end
  end

  # Used to make a wavecell api call to send a message to a phone number containing a link to the account confirmation page associated with that phone number.
  def send_wavecell_confirmation_message_to_phone_number(user_phone_number, message_url)
      begin
          phone_number = resource.phone_number_in_e164_format
          WaveCell.wave_cell_send_sms(phone_number, "Confirm your account at #{message_url}")
      rescue
           # If the message failed to send, return false to let registration#create know that it failed and
           # behave appropriately.
           return false
      end
  end
end
