class UsersController < ApplicationController
  before_action :authenticate_user!, only: [ :edit, :update, :create ]

  def edit
    @user = current_user
  end

  def new
    @user = current_user
  end

  def update
    if params[:user][:password].blank?
      params[:user].delete(:password)
      params[:user].delete(:password_confirmation)
    end
    @user = current_user
    if @user.update(user_params)
      sign_in(@user, :bypass => true) if user_params[:password].present?
      redirect_to user_path(current_user), notice: "Profile Updated"
    else
      render :edit
    end
  end

  def show
    if params[:id] == "me"
      authenticate_user!
      @user = current_user
    else
      @user = User.find_by_id(params[:id])
    end
    if @user
      authorize @user
      if request.xhr?
        @shared_blogs = @user.shared_blogs
        render file: 'users/shared_blogs.js.erb'
      else
        @appoiments_doctor = []
        if @user.roles.present? && @user.roles.last.resource_type == "Doctor"
          @doctor_id = @user.roles.last.resource_id
          @appoiments_doctor = Doctor.find(@doctor_id).appointments
        end
        @appointments = Appointment.where(user_id: @user.id)
        @favorites = @user.favorites.decorate
        @managed_locations = Location.with_role(:moderator, @user)
        @managed_profiles = Doctor.with_role(:moderator, @user)
        @shared_blogs_count = @user.shared_blogs.count
        @shared_blogs = @user.shared_blogs.limit(5)
        add_breadcrumb "Profile", user_path(@user)
        render layout: 'design'
      end
    else
      redirect_to :root
    end
  end

  # When a user subscribes to the newsletter, their email is stored in a user object with the flag subscription_only: true. A password is generated automatically, and
  # the user can access a full account at any time by resetting their password.
  def create_subscription_only
    respond_to do |format|
      if params[:ignore_subscription] 
        session[:ignore_subscription] = true
        format.html  { redirect_to(:back, notice: 'Ignore subscription.') }
      elsif user_params["email"].present?
        # Create a default password for a user signing up through the newsletter. The user can reset this password to change the account to a full account.
        new_user = false
        @user = User.where(email: user_params["email"]).first
        unless @user
          new_user = true
          password = Devise.friendly_token.first(10)
          @user = User.new(email: user_params["email"], subscription_only: true, password: password, password_confirmation: password)
        end
        if @user.save
          cookies.permanent[:subscribed_to_newsletter] = true
          if new_user
            UserMailer.notify_info_of_new_subscriber(@user.email).deliver_now
            msg = 'Thank you for subscribing to our Newsletter. You can upgrade to a full account at any time by requesting a password reset. '
          else
            msg = "Thank you for subscribing to our Newsletter."
          end
          format.html  { redirect_to(:back, notice: msg) }         
        else
          format.html  { redirect_to(:back,:notice => "Please verify that the email address you entered is valid, and try again.") }
        end
      else
        format.html  { redirect_to(:back, notice: "Kindly supply email address for subscription.") }
      end
    end
  end


  def mail_calculator_results
    UserMailer.send_calculator_result(calculator_email_params[:email], calculator_email_params[:content], calculator_email_params[:email_subject]).deliver_now
    respond_to do |format|
      format.html  { redirect_to(:back,:notice => "Thank you! Your results have been sent to #{calculator_email_params[:email]}") }
    end

  end

  def provider_email_request
    if interested_provider_contact_params[:email].present? and interested_provider_contact_params[:name].present?
      UserMailer.notify_info_of_interested_provider(interested_provider_contact_params).deliver_now
      UserMailer.confirm_interested_provider(interested_provider_contact_params[:email]).deliver_now
      flash[:notice] = "Thank you for contacting us. Our Team will set up your account within 48-72 business hours."
    else
      flash[:notice] = "Please enter an email address and name."
    end
    redirect_to :root
  end

  def unsubscribe
    user = User.find_by_id(params[:id])
    user.update_columns(email_opt_in: false) if user.present?    
  end


  private

  def user_params
    params.require(:user).permit(:username, :first_name, :last_name, :phone_number, :date_of_birth, :phone_body, :phone_country_code, :promotion_code,
                                :photo, :photo_cache, :remove_photo, :email, :city, :state, :zip, :co, :sex, :age_group, :password, :password_confirmation,
                                :display_badge, :agreed_to_gdpr, :email_opt_in)
  end


  def calculator_email_params
    params.require(:calculator_email).permit(:email, :content, :email_subject)
  end

  def interested_provider_contact_params
    params.require(:interested_provider_contact).permit(:name, :email, :referral_code, :location)
  end
end
