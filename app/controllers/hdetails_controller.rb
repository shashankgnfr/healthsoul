class HdetailsController < ApplicationController
  before_action :set_hdetail, only: [:show, :edit, :update, :destroy]

  # GET /hdetails
  # GET /hdetails.json
  def index
    @hdetails = Hdetail.all
  end

  # GET /hdetails/1
  # GET /hdetails/1.json
  def show
  end

  # GET /hdetails/new
  def new
    @hdetail = Hdetail.new
  end

  # GET /hdetails/1/edit
  def edit
  end

  # POST /hdetails
  # POST /hdetails.json
  def create
    @hdetail = Hdetail.new(hdetail_params)

    respond_to do |format|
      if @hdetail.save
        format.html { redirect_to @hdetail, notice: 'Hdetail was successfully created.' }
        format.json { render :show, status: :created, location: @hdetail }
      else
        format.html { render :new }
        format.json { render json: @hdetail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /hdetails/1
  # PATCH/PUT /hdetails/1.json
  def update
    respond_to do |format|
      if @hdetail.update(hdetail_params)
        format.html { redirect_to @hdetail, notice: 'Hdetail was successfully updated.' }
        format.json { render :show, status: :ok, location: @hdetail }
      else
        format.html { render :edit }
        format.json { render json: @hdetail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /hdetails/1
  # DELETE /hdetails/1.json
  def destroy
    @hdetail.destroy
    respond_to do |format|
      format.html { redirect_to hdetails_url, notice: 'Hdetail was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_hdetail
      @hdetail = Hdetail.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def hdetail_params
      params.require(:hdetail).permit(:platform, :website_url, :name, :address, :phone, :photo, :website, :speciallity, :hour, :fee, :fee, :about_me, :education, :rating, :number_of_reviews, :total_rating, :doctor_id)
    end
end
