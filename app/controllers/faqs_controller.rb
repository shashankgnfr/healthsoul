class FaqsController < ApplicationController
  before_action :authenticate_user!, only: [ :create, :update ]
  layout 'design'

  def index
    add_breadcrumb "FAQs", :faqs
    add_breadcrumb "Forum"
    set_meta_tags title: t("meta_title.forum")
    set_meta_tags description: t("meta_description.forum")
    @header = "Forum"
    @co = current_country.downcase
    @country = selected_country
    @faqs = Faq.where(country: @co)
    @faq = Faq.new(country: @co)
    render :show
  end

  def show
    @co = params[:co]
    @country = Country.find_by(alpha3: @co.upcase)
    @faqs = Faq.where(country: params[:co])
    @faq = Faq.new(country: params[:co])
  end

  def update
    @faq = Faq.find(params[:id])
    authorize @faq, :admin?
    @faq.update(faq_params)
    redirect_to forum_path( anchor: "faq#{@faq.id}"), notice: t("faq.created")
  end

  def create
    @faq = Faq.new(faq_params)
    @faq.user = current_user
    if @faq.save
      redirect_to forum_path( anchor: "faq#{@faq.id}"), notice: t("faq.created")
    else
      render :new
    end
  end

  private

  def faq_params
    if current_user.has_role? :admin
      params.require(:faq).permit(:body, :country, :tag_list)
    else
      params.require(:faq).permit(:body, :country)
    end
  end
end
