class RatingsController < ApplicationController
  before_action :find_resource

  def index
    set_meta_tags noindex: true
    authenticate_user!
    if session[:rating_params].blank?
      if @resource.doctor?
        redirect_to helpers.doctor_path(@resource) and return
      else
        redirect_to helpers.location_path(@resource) and return
      end 
    end
    @rating = Rating::New.(@resource, current_user, session[:rating_params].with_indifferent_access)
    session[:rating_params] = nil
    save_rating
  end

  def new
    set_meta_tags noindex: true   
    @rating = @resource.ratings.new
    @rating.user = current_user
    if @rating.over_user_policy?
      flash[:alert] = t("rating.over_limit")
      redirect_to @resource
    else
      render layout: 'min'
    end
  end

  def create
    set_meta_tags noindex: true
    @rating = init_new
    if @rating.valid?
      if user_signed_in?
        save_rating
      else
        session[:user_return_to] = return_path
        session[:rating_params] = rating_params
        authenticate_user!
      end
    else
      @rating.use_date = @rating.short_date
      flash[:alert] = "Please resolve the errors below"
      render :new
    end
  end

  private

  def return_path
    location_ratings_path(@resource)
  end

  def init_new
    @rating = Rating::New.(@resource, current_user, rating_params)
  end

  def save_rating
    if @rating.save
      RatingMailer.verify_rating(@rating).deliver_now
      RatingMailer.confirmation(@rating).deliver_now
      RatingMailer.notify_manager(@rating).deliver_now
      redirect_to_resource
    else
      if @rating.errors.messages[:user_id].present?
        flash[:alert] = t("rating.over_limit")
        redirect_to_resource
      else
        flash[:alert] = "Please resolve the errors below"
        render :new
      end
    end
  end

  def rating_params
    params.require(:rating).permit(:use_date, :reason, :overall_experience, :doctor_care, :nursing_care, :cleanliness, :food_services, :recommend, :comment, :customer_service, :get_ins, :cost_satisfaction, :claims_process)
  end

  def find_resource
    @resource = Location.find(params[:location_id])
    unless @resource.present?
      flash[:error] = "Not found requested doctor #{params[:location_id]}."
      redirect_to root_url
    else
      @location = @resource.decorate
    end
  end

  def redirect_to_resource
    if @resource.doctor?
      redirect_to helpers.doctor_path(@resource), notice: t("rating.saved")
    elsif @resource.class.to_s == 'Location'
      redirect_to helpers.location_path(@resource), notice: t("rating.saved")
    else
      redirect_to @resource, notice: t("rating.saved")
    end
  end
end
