class AdvertisementsController < ApplicationController

  def show
    begin
      advert = Advertisement.find(params[:id])
      # TODO: In future, we will add 'count' field in impressions table to register per day impression for Advert to reduce infinite records generation.
      if request.xhr?
        register_impressions_per_day(advert)
        @advert = advert
        @advert_div = @advert.subtype == 'Vertical' ? 'v-advert' : 'h-advert'    
        render file: "shared/advert.js.erb"
      else
        impressionist(advert, "Click")
        redirect_to advert.reference_url
      end
    rescue
      redirect_to (request.referrer || root_path)
    end
  end

  private

  def register_impressions_per_day(advert)
    imp = Impression.find_or_initialize_by(
      impressionable_type: 'Advertisement', 
      impressionable_id: advert.id,
      message: 'Impression',
      controller_name: 'advertisements',
      action_name: 'show', 
      created_at: Time.now.beginning_of_day
      )
    imp.counter_per_day += 1
    imp.save
  end

end