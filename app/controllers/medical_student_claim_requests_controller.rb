class MedicalStudentClaimRequestsController < ClaimRequestsController

  def new
    @claim_request = @medical_student.claim_requests.new
    @claim_request.user = current_user
    @location = @medical_student
  end
  
  private

  def find_resource
    if params[:medical_student_id].present?
      @medical_student = MedicalStudent.find(params[:medical_student_id])
      @location = @medical_student
    elsif params[:claim_request].present? and params[:claim_request][:medical_student_id].present?
      @medical_student = MedicalStudent.find(params[:claim_request][:medical_student_id])
      @location = @medical_student
    end
  end
  
end