class EventsController < ApplicationController
  layout 'design'
  add_breadcrumb "Events", :events

  def index
    @event = Event.where(publish: true, active: true).first
    @resource = @event.event_members.new({is_student: false}) if @event
  end
end