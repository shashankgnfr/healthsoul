class SitemapsController < ApplicationController
  layout 'design'
  add_breadcrumb "Sitemap", :sitemap

  def index
    if request.format.html?
      @prefer_countries = Country.where(slug: ['usa', 'india'])
      @common_blogs = ActsAsTaggableOn::Tagging.includes(:tag).where(tags: {name: ['Cardiology', 'Chiropractic', 'Diet and Nutrition', 'Dentistry', 'Know your Doctor', 'Know Your Country']}).order("tags.name").map{|i| ["#{i.tag.name.downcase.gsub(' ', '-')}", i.tag.name]}.uniq
    else
      # it generates XML sitemaps
      locations = Location.includes(:location_type).group_by{|l| l.location_type.name}
      @countries_of_locations_with_reviews = locations.each_with_object({})  do |(lt, locs), h|
        h[lt] = locs.select{|l| l.has_reviews?}.map(&:country).uniq.compact
      end
      @countries_of_locations_without_reviews = locations.each_with_object({})  do |(lt, locs), h|
        h[lt] = locs.select{|l| !l.has_reviews?}.map(&:country).uniq.compact
      end

      @us_docs_with_reviews = Doctor.where(country: ['USA', 'United States']).where('ratings_count > 0').select(:state, :ratings_count).size
      @us_docs_without_reviews = Doctor.where(country: ['USA', 'United States']).where(ratings_count: 0).select(:state, :ratings_count).size
      @indian_docs_with_reviews = Doctor.where(country: ['India', 'INDIA']).select(:state, :ratings_count).where('ratings_count > 0').size
      @indian_docs_without_reviews = Doctor.where(country: ['India', 'INDIA']).select(:state, :ratings_count).where(ratings_count: 0).size
    end
    respond_to do |format|
      format.xml
      format.html
    end
  end


  def main
    @url_props = [
      {path: root_url, priority: 1.0, changefreq: 'daily'},
      {path: about_url, priority: 0.8, changefreq: 'daily'},
      {path: forum_url, priority: 0.8, changefreq: 'daily'},
      {path: faqs_url, priority: 0.7, changefreq: 'monthly'},
      {path: terms_url, priority: 0.8},
      {path: privacy_policy_url, priority: 0.8},
      {path: new_advertising_request_url, priority: 0.2},
      {path: press_url, priority: 0.5, changefreq: 'monthly'}
    ]
    respond_to do |format|
      format.xml
    end
  end

  def categories
    @url_props = [
      {path: search_doctors_url, priority: 1.0, changefreq: 'daily'},
      {path: search_hospitals_url, priority: 1.0, changefreq: 'daily'},
      {path: search_health_insurances_by_country_url, priority: 1.0, changefreq: 'monthly'},
      {path: search_travel_health_insurances_by_country_url, priority: 1.0, changefreq: 'monthly'},
      {path: new_user_session_url, priority: 1.0},
      {path: new_user_registration_url, priority: 1.0},
    ]
    respond_to do |format|
      format.xml
    end
  end

  def doctors_with_data
    countries_for_query = params[:country] == 'India' ? ['India', 'INDIA'] : ['USA', 'United States']
    sitemap_of_doctors_with_reviews_in(params[:sitemap_number], countries_for_query)
    respond_to do |format|
      format.xml {render :doctors}
    end
  end

  def doctors_without_data
    countries_for_query = params[:country] == 'India' ? ['India', 'INDIA'] : ['USA', 'United States']
    sitemap_of_doctors_without_reviews_in(params[:sitemap_number], countries_for_query)
    respond_to do |format|
      format.xml {render :doctors}
    end
  end

  def locations_with_data
    sitemap_of_locations_with_reviews_in(params[:location_type].titleize.singularize, params[:country])
    respond_to do |format|
      format.xml {render :locations}
    end
  end

  def locations_without_data
    sitemap_of_locations_without_reviews_in(params[:location_type].titleize.singularize, params[:country])
    respond_to do |format|
      format.xml {render :locations}
    end
  end

  private

  def locations
    @locations ||= Location.joins([:location_type])
  end

  def sitemap_of_doctors_with_reviews_in(sitemap_number, countries)
    docs = Doctor.where('ratings_count > 0').where(country: countries).where('id > ?', (sitemap_number.to_i-1) * 40_000).limit(40_000)
    @doctors_data = docs.map do |doc|
      {path: helpers.doctor_url(doc), priority: 0.8, changefreq: 'daily'}
    end.compact
  end

  def sitemap_of_doctors_without_reviews_in(sitemap_number, countries)
    docs = Doctor.where(ratings_count: 0, country: countries).where('id > ?', (sitemap_number.to_i-1) * 40_000).limit(40_000)
    @doctors_data = docs.map do |doc|
      {path: helpers.doctor_url(doc), priority: 0.8, changefreq: 'daily'}
    end.compact
  end

  def sitemap_of_locations_with_reviews_in(location_type, country)
    @locations_data = locations.where({location_types: {name: location_type}}).where('country ~* ?', country.titleize).uniq.map do |loc|
      next if !loc.has_reviews?
      {path: helpers.location_url(loc), priority: 0.8, changefreq: 'daily'}
    end.compact
  end

  def sitemap_of_locations_without_reviews_in(location_type, country)
    @locations_data = locations.where({location_types: {name: location_type}}).where('country ~* ?', country.titleize).uniq.map do |loc|
      next if loc.has_reviews?
      {path: helpers.location_url(loc), priority: 0.8, changefreq: 'daily'}
    end.compact
  end
end
