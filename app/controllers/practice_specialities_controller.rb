class PracticeSpecialitiesController < ApplicationController

  def index
    speciality = PracticeSpeciality.where(id: params[:id]).first
    @type = params[:type]
    @sp_faqs = if params[:type] == 'static'
      speciality.practice_specialities_faqs.where("publish = true").left_outer_joins(:countries).where( countries: { id: nil } ).order(id: :asc)
    else
       speciality.practice_specialities_faqs.eager_load(:countries).where("lower(countries.name) = '#{params[:country].downcase}' and publish = true").order(id: :asc)
    end
  end

end