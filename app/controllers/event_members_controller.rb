class EventMembersController < ApplicationController
  layout 'design'

  def index
    @event = Event.where(id: params[:event_id]).first
    @resource = @event_member = @event.event_members.new if @event
    render file: 'events/index'
  end

  def new
    @event = Event.where(id: params[:event_id]).first
    @event_member = @event.event_members.new
  end

  def create
    @event = Event.where(id: params[:event_id]).first
    @event_member = @event.event_members.new(event_member_params.except(:user))
    password = event_member_params[:user][:password]
    if !User.where(email: @event_member.email).first.present?
      # SKIPPING CONFIRMATION MAIL - As per dr's request
      user = User.new(email: @event_member.email, password: password, password_confirmation: password)
      user.skip_confirmation!
      if user.valid?
        user.save!
        @event_member.user = user
      else
        flash[:notice] = "There are some invalid input. Kindly refill form."
        redirect_to event_event_members_path(@event.id)
        return
      end
    end
    if @event_member.save
      EventMailer.request_notification(@event_member).deliver_now
      EventMailer.request_acknowledgement(@event_member).deliver_now
      notice = 'Thank you for your information.'
      notice += ' Now you have access to healthsoul.' if @event_member.reload.user
      flash[:notice] = notice
      redirect_to events_path
    else
      render file: 'events/index'
    end
    
  end

  private

  def event_member_params
    params.require(:event_member).permit!
  end
end