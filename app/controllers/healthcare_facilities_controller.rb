class HealthcareFacilitiesController < SearchesController
  def show
    add_breadcrumb "Healthcare Facilities", :search_healthcare_facilities
    set_meta_tags title: t("meta.facilities_search", country: current_country.titleize)
    set_meta_tags description: t("meta_description.facilities_search")
    @heading = "Find Healthcare Facilities"
    @search_class = "health-fac"
    if params[:clear] == "true" || params[:set_locale]
      session[:healthcare_facilities_search_url] = nil
    end
    if search_params.present?
      session[:healthcare_facilities_search_url] = request.fullpath.gsub("ajax=true", "")
      @search = Search::HealthcareFacility.new(search_params)
      @search.order = "reviews_a" if search_params[:order].blank?
      if params[:ajax] == "true" && params[:page].blank?
        @search_results = @search.results.page(params[:page]).decorate
        render layout: false and return
      end
    else
      if session[:healthcare_facilities_search_url].present?
        redirect_to session[:healthcare_facilities_search_url] and return
      end
      @search = Search::HealthcareFacility.new
      @search.default_country = current_country
      @search.order = "reviews_a"
      @search.co = current_country
    end
    @search_results = @search.results.page(params[:page]).decorate
    @blogs = Blog.order(published_at: :desc).limit(4).decorate
  end

  private

  def search_params
    if params[:search].present?
      params[:search_healthcare_facility] = params[:search]
    end
    params.require(:search_healthcare_facility).permit(:order, :departments, :co, :sub_type, :co, :name, :type_id, :stars => []) if params[:search_healthcare_facility].present?
  end
end
