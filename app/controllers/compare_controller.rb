class CompareController < ApplicationController
  def index
    set_meta_tags noindex: true
    @locations = Location.where(id: params[:locations])
    if @locations.present?
      @location_type = @locations.first.type_slug
    else
      redirect_to search_hospitals_path, notice: "Please select locations to compare first"
    end
  end
end
