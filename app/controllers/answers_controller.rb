class AnswersController < ApplicationController
  before_action :authenticate_user!, :find_question
  layout 'design'
  add_breadcrumb "Answer"

  def new
    @answer = @question.answers.new
  end

  def create
    @answer = @question.answers.new(answer_params)
    @answer.user = current_user
    if @answer.save
      redirect_to_resource
    else
      flash[:alert] = t("answer.already_created")
      render :new
    end
  end

  private

  def answer_params
    params.require(:answer).permit(:body)
  end

  def find_question
    @question = Question.find(params[:question_id])
  end

  def redirect_to_resource
    if @question.location.doctor?
      redirect_to helpers.doctor_path(@question.location, anchor: "question#{@question.id}"), notice: t("answer.created")
    elsif @question.location.hospital?
      redirect_to helpers.location_path(@question.location, anchor: "question#{@question.id}"), notice: t("answer.created")
    else
      redirect_to [@question.location, anchor: "question#{@question.id}"], notice: t("answer.created")
    end
  end
end
