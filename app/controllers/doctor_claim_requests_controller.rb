class DoctorClaimRequestsController < ClaimRequestsController

  def new
    @claim_request = @doctor.claim_requests.new
    @claim_request.user = current_user
    @location = @doctor
  end
  
  private

  def find_resource
    if params[:doctor_id].present?
      @doctor = Doctor.find(params[:doctor_id])
      @location = @doctor
    elsif params[:claim_request].present? and params[:claim_request][:doctor_id].present?
      @doctor = Doctor.find(params[:claim_request][:doctor_id])
      @location = @doctor
    end
  end
  
end
