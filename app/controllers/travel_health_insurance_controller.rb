class TravelHealthInsuranceController < HealthInsuranceController
  def show
    add_breadcrumb "Travel Health Insurance", search_travel_health_insurances_by_country_path
    set_meta_tags title: t("meta.thi_search", country: formatted_country(current_country))
    set_meta_tags description: t("meta_description.thi_search", country: formatted_country(current_country))
    @heading = "Find Travel Health Insurance"
    @search_class = "travel-ins"
    if params[:clear] == "true" || params[:set_locale]
      session[:travel_health_ins_search_url] = nil
    end
    if search_params.present?
      session[:travel_health_ins_search_url] = request.fullpath.gsub("ajax=true", "")
      @search = Search::TravelHealthInsurance.new(search_params)
      @search.order = "reviews_a" if search_params[:order].blank?
      @search_heading = set_heading(search_params[:co] || current_country)
      @search_results = @search.results.page(params[:page]).per(25).decorate
      if params[:ajax] == "true" && params[:page].blank?
        @results = @search.results.page(params[:page]).decorate
        render layout: false and return
      end
    else
      if session[:travel_health_ins_search_url].present?
        redirect_to session[:travel_health_ins_search_url] and return
      end
      @search = Search::TravelHealthInsurance.new
      @search.default_country = current_country
      @search.order = "reviews_a"
      @search.co = current_country
      @search_heading = set_heading(@search.co)
    end
    @search_results = @search.results.page(params[:page]).per(25).decorate
    @blogs = Blog.order(published_at: :desc).limit(4).decorate
  end

  private

  def search_params
    if params[:search].present?
      params[:search_travel_health_insurance] = params[:search]
    end
    params.require(:search_travel_health_insurance).permit(:order, :departments, :co, :sub_type, :co, :name, :stars => []) if params[:search_travel_health_insurance].present?
  end

  def set_heading(country)
    "Find the list of Best Travel Health insurance in #{country.try(:upcase)} enlisted below. Travel Health Insurance is an important companion while travelleing. Reach the most suitable Travel Health Insurance in #{country.try(:upcase)} Easily!"
  end
end
