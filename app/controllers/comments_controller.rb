class CommentsController < ApplicationController
  before_action :find_commentable
  def index
    @comments = policy_scope(Comment)
  end

  def new
    @comment = Comment.new
    @comment.commentable = @commentable
    @comment.user = current_user
    authorize @comment
  end

  def create
    @comment = Comment.new(comment_params)
    @comment.commentable = @commentable
    @comment.user = current_user
    authorize @comment
    if @comment.save
      begin 
        UserMailer.notify_user_of_comment(@comment).deliver_now
      rescue
      end
      redirect_to @commentable.location, notice: t("comment.saved")
    else
      render :new
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:body)
  end

  def find_commentable
    if params[:rating_id].present?
      @commentable = Rating.find(params[:rating_id])
    elsif params[:doctor_review_id].present?
      @commentable = DoctorReview.find(params[:doctor_review_id])
    end
  end
end
