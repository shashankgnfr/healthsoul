class DoctorSearchController < SearchesController
  before_filter :set_breadcrumb

  def show
    @best_doctors ||= false
    @clear = false
    if params[:clear] == "true"
      @clear = true
      session[:doctor_search_url] = nil
    end
    if search_params.present?
      session[:doctor_search_url] = request.fullpath.gsub("ajax=true", "")
      @search = Search::Doctor.new(search_params)
      if practice_params.present?
        @practices = Search::Practice.new(practice_params)
      end
      df_country = if !search_params[:near].present? & params[:set_locale].present?
        params[:set_locale]
      elsif !search_params[:near].present?
        current_country
      elsif search_params[:near].present?
        begin
          geo_location = get_geo_location(search_params[:near])
          @search.city = geo_location.city&.parameterize if geo_location.try(:city)
          @search.state = [geo_location.state&.parameterize, geo_location.state_code&.parameterize] if geo_location.try(:state)
          c = Country.where(alpha2: get_geo_country_code(geo_location))
          c = c.count > 1 ? c.where(["name ilike ? or slug ilike ?", search_params[:near], search_params[:near]]).first : c.first
          c.try(:slug)
        rescue
          params[:set_locale] || current_country
        end
      else
        nil
      end
      @search.default_country = df_country
      @practices.default_country = df_country
      @search_class = "doctors"
      @search.order = order
      @sp = PracticeSpeciality.eager_load({practice_specialities_faqs: :countries}).where(id: @search.speciality).first
      if @sp
        @sp_static_faqs = @sp.practice_specialities_faqs.where("publish = true").left_outer_joins(:countries).where( countries: { id: nil } ).order(id: :asc)
        @sp_dynamic_faqs = @sp.practice_specialities_faqs.eager_load(:countries).where("lower(countries.name) = '#{@search.default_country.try(:downcase)}' and publish = true").order(id: :asc)
        blog_type = @sp&.name
        @blogs = Blog.tagged_with(blog_type, :any => true).order(priority: :asc).limit(4).decorate
      end
      @blogs = Blog.order(published_at: :desc).limit(4).decorate if !@blogs.present?
      @featured_or_sponsored = find_featured_or_sponsored
      if params[:ajax] == "true" && params[:page].blank?
        results = @search.results(params[:page], best_doctors: @best_doctors) || Doctor.none
        practices_results = @practices&.results
        @search_results = DoctorDecorator.decorate_collection(results)
        @practices_results =  practices_results&.page(params[:page])&.decorate
        result_count = results.total_count + practices_results.try(:size).to_i
        non_zero_overall = results.pluck(:overall).reject{|i| i == 0.0}
        @search_heading = set_heading_and_meta(result_count, non_zero_overall, @sp)
        render layout: false and return
      end
    else
      if session[:doctor_search_url].present?
        redirect_to session[:doctor_search_url] and return
      end
      @blogs = Blog.order(published_at: :desc).limit(4).decorate
      @search = Search::Doctor.new
      @search_class = "doctors"
      co = current_country
      unless @clear
        if current_location_selected?
          current_loc = current_location
          @search.near = current_loc['address']
          @search.lat = current_loc['latitude']
          @search.lng = current_loc['longitude']
        end
        @search.co = co
      end
      @search.default_country = co
      @featured_or_sponsored = find_featured_or_sponsored
      @search.order = "claimed"
    end
    unless @clear
      results = @search.results(params[:page], best_doctors: @best_doctors) || Doctor.none
      practices_results  = @practices&.results
      @search_results = DoctorDecorator.decorate_collection(results)
      @practices_results = practices_results&.page(params[:page])&.decorate
      result_count = results.total_count + practices_results.try(:size).to_i
      non_zero_overall = results.pluck(:overall).reject{|i| i == 0.0}
      @search_heading = set_heading_and_meta(result_count, non_zero_overall, @sp)
    end
  end

  def best_doctors
    if request.url.include? '/us/'
      params[:set_locale] = 'usa'
      rewrite_url = request.url.gsub('us', 'usa')
      redirect_to rewrite_url, status: 301
    elsif request.url.include? '/best_doctors/'
      rewrite_url = request.url.gsub('best_doctors', 'best-doctor-ratings').downcase
      rewrite_url = rewrite_url.gsub('new-delhi', 'delhi') if request.url.include? 'new-delhi'
      redirect_to rewrite_url, status: 301
    else
      base_path = request.url.include?('/practices/') ? 'search_best_practices_path' : 'search_best_doctors_path'
      speciality_title = 'Doctor'
      unless params[:speciality] == 'specialist'
        speciality = PracticeSpeciality.where(["title ilike ? or title_slug ilike ?", params[:speciality], params[:speciality]]).first
        add_breadcrumb params[:speciality].titleize, send(base_path, params[:speciality])
        speciality_title = speciality&.title
      end
      add_breadcrumb formatted_country(params[:set_locale]), send(base_path, params[:speciality], params[:set_locale]) if params[:set_locale]
      add_breadcrumb formatted_state(params[:state], params[:set_locale]), send(base_path, params[:speciality], params[:set_locale], params[:state]) if params[:state] 
      add_breadcrumb params[:city].try(:titleize), send(base_path, params[:speciality], params[:set_locale], params[:state], params[:city]) if params[:city]
      address = [params[:city], params[:state], formatted_country(params[:set_locale])].compact.join(', ')
      params[:search_doctor] = {near: address, state: params[:state], city: params[:city], speciality: speciality.try(:id), co: params[:set_locale]}
      @meta_location = formatted_location
      @meta_description = t("meta_description.doctor_search", specialist: speciality_title, location: @meta_location)
      @best_doctors = true
      show
      render :show
    end
  end

  def typeahead_search
    doctors = Search::Doctor.new(search_params).results(1, 10)
    doctors = doctors.map{ |dr| dr.json_for_search }
    render status: 200, json: {status: :success, code: "200", doctors: doctors}
  end

  private

  def set_meta(speciality_title = 'Doctors')
    @meta_location = formatted_location unless @meta_location
    @page_title = if request.url.include? '/practices/'
      t("meta.doctor_search",
        specialist_type: 'Practices',
        location: @meta_location)
    else
      t("meta.doctor_search",
        specialist_type: speciality_title.pluralize,
        location: @meta_location)
    end
    set_meta_tags title: @page_title
    @keywords = "Best #{speciality_title.pluralize} in #{@meta_location}"
    set_meta_tags keywords: "Best #{speciality_title.pluralize} in #{@meta_location}"
    @meta_description ||= t("meta_description.doctor_search", specialist: speciality_title || 'health care industrialist', location: @meta_location)
    set_meta_tags description: @meta_description
  end

  def search_params
    if params[:search].present?
      params[:search_doctor] = params[:search]
    # IF no search parameter supply then we use session -> country
    elsif !params[:search_doctor].present? && session[:current_location]
      params[:search_doctor] = {near: session[:current_location]['country']}
    end
    if params[:search_doctor].present?
      params.require(:search_doctor).permit!
    end
  end

  def practice_params
    if params[:search_doctor].present?
      params[:search_practice] = params[:search_doctor]
    end
    params.require(:search_practice).permit! if params[:search_practice].present?
  end

  def set_heading_and_meta(result_count, overall, sp = nil)
    if sp.present?
      speciality_title = sp&.title
      h1 = sp.doctorised_h1
    else
      speciality_title = 'Doctor'
      h1 = DOCTOR_H1
    end
    set_meta(speciality_title)
    avg_rating = overall.present? ? (overall.sum / overall.count).round(2).to_s : 'N/A'
    h1.
      gsub('%%number%%', result_count.to_s).
      gsub('%%specialist%%', speciality_title).
      gsub('%%location%%', formatted_one_location).
      gsub('%%avg_rating%%', avg_rating)
  end

  def formatted_location
    if @search
      [@search.city.try(:titleize), (@search.state.class == Array ? @search.state.first : @search.state).try(:titleize), formatted_country(@search.default_country)].compact.reject(&:blank?).join(', ')
    elsif params[:set_locale] == 'usa'
      [params[:city].try(:titleize), formatted_state(params[:state], params[:set_locale]), params[:set_locale].try(:upcase)].compact.reject(&:blank?).join(', ')
    else
      [params[:city].try(:titleize), params[:state].try(:titleize), formatted_country(params[:set_locale])].compact.reject(&:blank?).join(', ')
    end
  end

  def formatted_one_location
    uniq_location = [@search.city , (@search.state.class == Array ? @search.state.first : @search.state) , formatted_country(@search.co || @search.default_country)].compact.reject(&:blank?).first
    formatted_country(uniq_location).to_s
  end

  def find_featured_or_sponsored
    # do we need to use provider_type == 'Doctor'?
    conditions = {country_slug: @search.default_country}
    conditions = conditions.merge({city_slug: @search.city.try(:parameterize)}) if  @search.city.present?
    return {
      featured: Doctor.where(conditions).where(featured_or_sponsored: 'Featured').order(name: :asc).pluck(:id),
      sponsored: Doctor.where(conditions).where(featured_or_sponsored: 'Sponsored').order(name: :asc).pluck(:id)
    }
  end

  def set_breadcrumb
    if request.url.include? '/practices/'
      add_breadcrumb "Best Practices", :search_best_practices
    else
      add_breadcrumb "Best doctor ratings", :search_doctors
    end
  end
end
