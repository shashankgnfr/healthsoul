class ApplicationController < ActionController::Base
  include Pundit
  protect_from_forgery with: :exception
  before_action :set_locale, :save_ref_code
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :blog_redirect
  rescue_from Exception, with: :error_handling
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  add_breadcrumb "Home", :root, if: :valid_for_breadcrumb?

  before_action do
    if Rails.env.development?
      Rack::MiniProfiler.authorize_request
    end
  end

  unless Rails.application.config.consider_all_requests_local
    rescue_from AbstractController::ActionNotFound, with: :handle_error
    rescue_from ActiveRecord::RecordNotFound, with: :handle_error
    rescue_from ActionController::RoutingError, with: :handle_error
    rescue_from ActionController::UnknownController, with: :handle_error
    rescue_from ActionController::UnknownFormat, with: :handle_error
  end

  def current_location
    begin
      if session[:country_selected].present?
        country = Country.where(slug: session[:country_selected]).first
        @current_location = session[:current_location] = {
          "country" => country.try(:slug),
          "latitude" => nil,
          "longitude" => nil,
          "address" => formatted_country(country.try(:name)),
          "state" => nil,
          'country_code' => country.try(:alpha3)
        }
      elsif session[:do_not_call_geocoder].blank?
        # Try to get an ip from the ip_lookup api.
        # Current api is capped at 1,000 requests a day
        begin
          # TODO - encache IP addresses -> code to avoid
          # using quota for known IP addresses

          # dup the request so that if it fails, it does not
          # corrupt the request which causes downstream internal server errors
          # NOTE - this was the source of tons of "internal server error" apache
          # requests because there appears to be a defect in the Geocoder gem
          # that corrupts the request object
          l = request.dup.location
        rescue
          # If the geocoder responded with an error, save a session varriable so that we don't call it again.
          session[:do_not_call_geocoder] = true
          l = nil
        end
        if l.present? and l.country_code.present?
          country = Country.where(alpha2: l.country_code.upcase).first
          country_code = if country.present?
            @selected_country = country
          else
            country = Country.where(alpha2: 'US').first
            "usa"
          end
        elsif session[:current_location].present? && session[:current_location]['country'].present?
          country = Country.where(slug: session[:current_location]['country']).first
        else
          l = get_geo_location_by_ip
          country = Country.where(alpha2: get_geo_country_code(l)).first
        end
        address = [l.try(:city), [l.try(:state),  l.try(:postal_code)].join(' ')].flatten.compact.reject(&:blank?).join(',')
        address = formatted_country(l.try(:country)) unless address.present?
        @current_location = session[:current_location] = {
            "country" => country.try(:slug),
            "latitude" => l.try(:latitude),
            "longitude" => l.try(:longitude),
            "address" => address,
            'state' => l.try(:state).try(:parameterize),
            'country_code' => country.try(:alpha3)
          }
      else
        # if no country selected - then it's USA
        @current_location = session[:current_location] = {
          "country" => 'usa',
          "latitude" => nil,
          "longitude" => nil,
          "address" => formatted_country('usa'),
          "state" => nil,
          'country_code' => 'USA'
        }
      end
    rescue
      # if no country selected - then it's USA
      @current_location = session[:current_location] = {
        "country" => 'usa',
        "latitude" => nil,
        "longitude" => nil,
        "address" => formatted_country('usa'),
        "state" => nil,
        'country_code' => 'USA'
      }
    end
  end

  def current_country
    if params[:set_locale].present?
      session[:country_selected] = params[:set_locale]
      current_loc = current_location
      params[:set_locale]
    elsif session[:country_selected].present?
      session[:country_selected]
    else
      current_loc = current_location
      if current_loc.present?
        current_loc["country"]
      else
        current_geo = get_geo_location_by_ip
        Country.where(alpha2: get_geo_country_code(current_geo)).first.try(:slug)
      end
    end
  end

  helper_method :current_country, :selected_country, :current_location, :current_location_selected?

  def current_location_selected?
    session[:current_location].present? && session[:country_selected] == nil
  end

  def selected_country
    # the country db record
    @selected_country ||= Country.where(slug: current_country).first
  end

  def formatted_country(country)
    country.try(:downcase) == 'usa' ? country.try(:upcase) : country.try(:titleize)
  end

  def formatted_state(state, country)
    (state.length == 2 && country.downcase == 'usa') ? state.try(:upcase) : state.try(:titleize) if state
  end

  private

  def valid_for_breadcrumb?
    return self.class.name  != 'HomeController'
  end

  def configure_permitted_parameters
    added_attrs = [:phone_number, :email, :password, :remember_me, :marked_as_provider]
    devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
    devise_parameter_sanitizer.permit :account_update, keys: added_attrs
  end


  def set_locale
    if params[:set_locale].present?
      if params[:set_locale] == "current"
        session[:country_selected] = nil
      else
        session[:country_selected] = params[:set_locale]
      end
    end
  end

  def blog_redirect
    if request.subdomain == 'blog'
      blog_redirect_url = request.url
      new_blog_path_str = 'blog'
      if request.fullpath != '/'
        blog_redirect_url = blog_redirect_url.sub(request.fullpath, '/')
        new_blog_path_str = "blog#{request.fullpath}"
      end
      redirect_to blog_redirect_url.sub('://blog', '://www') + new_blog_path_str
    end
  end

  def user_not_authorized
    flash[:alert] = "You are not authorized to view this page"
    redirect_to(request.referrer || root_path)
  end

  def save_ref_code
    if params[:ref].present?
      session[:ref_code] = params[:ref]
    end
  end

  def get_geo_country_code(geo_location)
    geo_location.try(:country_code) || geo_location
  end

  # get remote_IP based on request in staging/production mode
  def get_geo_location_by_ip
    current_remote_ip = if Rails.env.production? || Rails.env.staging?
      request.remote_ip
    else
      Net::HTTP.get(URI.parse('http://checkip.amazonaws.com/')).squish
    end
    get_geo_location(current_remote_ip)
  end

  def get_geo_location(search_term)
    begin
      Geocoder.search(search_term).first
    rescue Exception => e
      Rails.logger.error 'Exception from GEO coder'
      ExceptionMailer.exception_notification(e, request.url).deliver
      if Rails.env.production? || Rails.env.staging?
        Geocoder.search([HEALTHSOUL_LAT, HEALTHSOUL_LNG]).first
      else
        'US'
      end
    end
  end

  def error_handling(exception)
    if Rails.env.production?
      logger.error("==== Runtime error in application_controller ====\n")
      logger.error(exception)
      logger.error(exception.backtrace.join("\n"))
      render file: "#{Rails.root}/public/500.html", layout: false, status: 500
      ExceptionMailer.runtime_error_mail(exception, request.env, params).deliver
    else
      logger.error(exception)
      logger.error(exception.backtrace.inject(""){|v, e| "#{v}\n#{e}"})
      raise exception
    end
  end

  def search_health_insurances_by_country_path(country = current_country)
    search_health_insurances_path(country)
  end
  helper_method :search_health_insurances_by_country_path

  def search_travel_health_insurances_by_country_path(country = current_country)
    search_travel_health_insurances_path(country)
  end
  helper_method :search_travel_health_insurances_by_country_path

  def search_health_insurances_by_country_url(country = current_country)
    search_health_insurances_url(country)
  end
  helper_method :search_health_insurances_by_country_url

  def search_travel_health_insurances_by_country_url(country = current_country)
    search_travel_health_insurances_url(country)
  end
  helper_method :search_travel_health_insurances_by_country_url

  protected

  def handle_error
    redirect_to root_url
  end
end
