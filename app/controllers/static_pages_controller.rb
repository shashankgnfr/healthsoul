class StaticPagesController < ApplicationController
  layout 'design'

  def press
    add_breadcrumb "Press", :press
  end

  def guide_co
    render "guide_#{params[:co]}"
  end

  def about
    add_breadcrumb 'About Us', :about
  	set_meta_tags title: t("meta_title.about-us")
    set_meta_tags description: t("meta_description.about-us")
    @body_class = 'about-us-page'
  end

	def privacy_policy
    add_breadcrumb "Privacy Policy", :privacy_policy
    set_meta_tags title: t("meta_title.privacy-policy")
    set_meta_tags description: t("meta_description.privacy-policy")
	end

	def terms
    add_breadcrumb "Terms", :terms
    set_meta_tags title: t("meta_title.terms-and-conditions")
  	set_meta_tags description: t("meta_description.terms-and-conditions")
	end

	def faqs
    add_breadcrumb "FAQs", :faqs
    set_meta_tags title: t("meta_title.faqs")
  	set_meta_tags description: t("meta_description.faqs")
	end

	def users_info
    add_breadcrumb "User Info", :users_info
    set_meta_tags title: t("meta_title.users")
    set_meta_tags description: t("meta_description.users")
    @promotion = if params[:id].present?
      Promotion.find(params[:id]) 
    else
      Promotion.where(display_on: 'Users', publish: true).first
    end
    @promotion_winners = Promotion.connection.execute("select json_agg(t.c) as winners, t.promotion_year, t.promotion_month from (
        SELECT json_build_object('name', name, 
        'winner_type', promotion_winners.winner_type, 
        'prize', promotion_winners.prize,
        'month', promotion_winners.month,
        'year', promotion_winners.year) as c, promotion_winners.year as promotion_year, promotion_winners.month as promotion_month
      FROM promotion_winners
      INNER JOIN promotions ON promotions.id = promotion_winners.promotion_id
            where promotion_id = #{@promotion.try(:id)}

      ) as t
      group by  t.promotion_year, t.promotion_month
      order by t.promotion_year desc, t.promotion_month desc"
    ) if @promotion.present?
	end

  def manage
    add_breadcrumb "Provider Info", :manage
    if current_user
      @claim_request = ClaimRequest.new
      set_meta_tags title: t("meta_title.providers")
      set_meta_tags description: t("meta_description.providers")
      # If the user was sent here via a link from a promotion card, find that promotion
      @promotion = if params[:id].present?
        Promotion.find(params[:id]) 
      else
        Promotion.where(display_on: 'Providers', publish: true).first
      end
      render "shared/provider_info"
    else
      redirect_to new_user_registration_path(provider: true, id: params[:id])
    end
  end

  def editorial_board
    add_breadcrumb "Contributors", :editorial_board
    set_meta_tags title: t("meta_title.editorial_board")
    set_meta_tags description: t("meta_description.editorial_board")
    @members = EditorialMember.where(publish: true).order(name: :asc).limit(15)
  end

  def calculators
    add_breadcrumb "Calculators", :calculators
    set_meta_tags title: t("meta_title.calculator", {calculator: 'Calculators'} )
    set_meta_tags description: t("meta_description.calculators")
  end

  def bmi_calculator
    add_breadcrumb "Calculators", :calculators
    add_breadcrumb "BMI Calculators", :bmi_calculator
    set_meta_tags title: t("meta_title.calculator", {calculator: 'BMI Calculator'} )
    set_meta_tags description: t("meta_description.calculators")
  end

  def waist_hip_calculator
    add_breadcrumb "Calculators", :calculators
    add_breadcrumb "Waist Hip Calculators", :waist_hip_calculator
    set_meta_tags title: t("meta_title.calculator", {calculator: 'Waist Hip Ratio Calculator'} )
    set_meta_tags description: t("meta_description.calculators")
  end

  def bmr_calculator
    add_breadcrumb "Calculators", :calculators
    add_breadcrumb "BMR Calculators", :bmr_calculator
    set_meta_tags title: t("meta_title.calculator", {calculator: 'BMR Calculator'} )
    set_meta_tags description: t("meta_description.calculators")
  end

  def calorie_calculator
    add_breadcrumb "Calculators", :calculators
    add_breadcrumb "Calorie Calculators", :calorie_calculator
    set_meta_tags title: t("meta_title.calculator", {calculator: 'Calorie Calculator'} )
    set_meta_tags description: t("meta_description.calculators")
  end
end
