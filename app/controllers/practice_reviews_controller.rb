class PracticeReviewsController < RatingsController

  def index
    set_meta_tags noindex: true
    authenticate_user!
    redirect_to helpers.location_path(@resource) and return if session[:rating_params].blank?
    r_params = session[:rating_params].with_indifferent_access
    @rating = PracticeReview.new(r_params)
    @rating.user = current_user
    @rating.location = @resource
    session[:rating_params] = nil
    save_rating
  end

  def find_resource
    set_meta_tags noindex: true
    @location = @resource = Location.find(params[:location_id]).try(:decorate)
    unless @resource.present?
      flash[:error] = "Not found requested practice #{params[:location_id]}."
      redirect_to root_url
    end
  end

  def rating_params
    params.require(:practice_review).permit(:use_date, :where, :overall_experience, :explaintation, :trust, :staff, :appt, :wait, :skills, :recommend, :comment)
  end

  def init_new
    set_meta_tags noindex: true
    @rating = PracticeReview.new(rating_params)
    @rating.user = current_user
    @rating.location = @resource
    @rating
  end

  def return_path
    location_practice_reviews_path(@resource)
  end


  def save_rating
    set_meta_tags noindex: true
    if @rating.save
      RatingMailer.verify_rating(@rating).deliver_now
      RatingMailer.confirmation(@rating).deliver_now
      RatingMailer.notify_manager(@rating).deliver_now
      redirect_to helpers.location_path(@resource), notice: t("rating.saved")
    else
      if @rating.errors.messages[:user_id].present?
        flash[:alert] = t("rating.over_limit")
        redirect_to @resource
      else
        flash[:alert] = "Please resolve the errors below"
        render :new
      end
    end
  end

end
