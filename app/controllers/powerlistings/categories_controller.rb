class Powerlistings::CategoriesController < Powerlistings::BaseController

  def index
    if params[:id].present?
      primary_speciality = PracticeSpeciality.where(id: params[:id])
    else
      primary_speciality = PracticeSpeciality.all
    end
    render json: primary_speciality.map{ |ps| {id: ps.id, name: ps.name } }
  end

end
