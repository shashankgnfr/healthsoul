class Powerlistings::BaseController < ActionController::Base
  before_action :authorize_request

  def authorize_request
    unless params[:token] == ENV['YEXT_TOKEN']
      render json: {errors: 'Not Authorized', status: 403 } and return
    end
  end

end
