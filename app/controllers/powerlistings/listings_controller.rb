class Powerlistings::ListingsController < Powerlistings::BaseController

  api :GET, '/search', 'For finding HealthcareProfessional and HealthcareFacility'
  param :type, String, desc: 'Must be HealthcareProfessional OR HealthcareFacility', required: true
  param :name, String, desc: 'Name of Listing'
  param :npi, String, desc: 'NPI number for HealthcareProfessional'
  param :phone, String, desc: 'Phone Number'
  param :latitude, String, desc: 'latitude for listing, should include longitude'
  param :longitude, String, desc: 'longitude for listing, should include latitude'
  def index
    entities = find_entities
    array_of_entities = entities.map{ |entity| entity.yext_json }
    render json: array_of_entities
  end

  api :GET, '/details', 'Get details on a specfic listing'
  param :type, String, desc: 'Must be HealthcareProfessional OR HealthcareFacility', required: true
  param :id, String, desc: 'Healthsoul ID of HealthcareProfessional OR HealthcareFacility', required: true
  def show
    search_type = Doctor if params[:type] == 'HealthcareProfessional'
    search_type = Location if params[:type] == 'HealthcareFacility'
    if search_type.present? && params[:id].present?
      entity = search_type.find_by_id(params[:id])
    end
    if entity.present?
      render json: entity.yext_json
    else
      render json: { status: 'Not Found' }, status: 404
    end
  end

  def upsert
    if ['HealthcareProfessional', 'HealthcareFacility'].include?(params[:type])
      ordering_entity = Doctor if params[:type] == 'HealthcareProfessional'
      ordering_entity = Location if params[:type] == 'HealthcareFacility'
      if params[:partnerId].present?
        entity = ordering_entity.find_by_id(params[:partnerId])
        if entity.present?
          begin
            entity.parse_yext_order_body(params)
            entity.live!
            render json: { id: entity.id, status: "LIVE", url: "https://#{request.host}#{entity.entity_url}"  }
          rescue => e
            render json: { status: "BLOCKED Bad Request - Server Error: #{e}" }, status: 400
          end
        else
          render json: { status: 'Not Found' }, status: 404
        end
      elsif params[:yextId].present?
        entity = ordering_entity.find_or_initialize_by(yext_id: params[:yextId])
        begin
          entity.parse_yext_order_body(params)
          entity.live!
          render json: { id: entity.id, status: "LIVE", url: "https://#{request.host}#{entity.entity_url}"  }
        rescue => e
          render json: { status: "REJECTED - Server Error: #{e}" }, status: 400
        end
      else
        render json: { status: 'REJECTED - Partner ID OR Yext ID Not Present' }, status: 400
      end
    else
      render json: { status: 'REJECTED - Type Not Present' }, status: 400
    end
  end

  api :post, '/:id', 'Update a Listing'
  param :type, String, desc: 'Must be HealthcareProfessional OR HealthcareFacility', required: true
  param :id, String, desc: 'Healthsoul ID of HealthcareProfessional OR HealthcareFacility', required: true
  param :name, String, desc: 'Name of Listing'
  param :description, String, desc: 'Description of Listing'
  param :gender, String, desc: 'Gender is male or female'
  param :degrees, Array, desc: 'Array of Degrees' do
    param :abbreviation, String, desc: 'Abbreviation of Degree'
  end
  param :emails, Array, desc: 'Array of emails' do
    param :address, String, desc: 'Address of Email'
  end
  param :locations, Array, desc: 'Array of Locations' do
    param :address, Hash, desc: 'Address Object' do
      param :address, String, desc: 'Street Address of Location'
      param :city, String, desc: 'City of Location'
      param :postalCode, String, desc: 'Postal Code For Location'
      param :state, String, desc: 'State For Location'
      param :countryCode, String, desc: 'Country Code For Location'
    end
  end
  def update
    ordering_entity = Doctor if params[:type] == 'HealthcareProfessional'
    ordering_entity = Location if params[:type] == 'HealthcareFacility'
    if ordering_entity.present? && params[:id].present?
      entity = ordering_entity.find_by_id(params[:id])
      begin
        entity.parse_yext_order_body(params)
        entity.live!
        render json: { id: entity.id, status: "LIVE", url: "https://#{request.host}#{entity.entity_url}"  }
      rescue => e
        render json: { status: "REJECTED - Server Error: #{e}" }, status: 400
      end
    else
      render json: { status: 'REJECTED - Type Or ID Not Present' }, status: 400
    end
  end

  def cancel
    ordering_entity = Doctor if params[:type] == 'HealthcareProfessional'
    ordering_entity = Location if params[:type] == 'HealthcareFacility'
    if ordering_entity.present? && params[:id].present?
      entity = ordering_entity.find_by_id(params[:id])
      if entity.present?
        # remove promo stuff later
        entity.available!
        render json: { status: 200 }
      else
        render json: { status: 'Not Found' }, status: 404
      end
    else
      render json: { status: 'REJECTED - Type Or ID Not Present' }, status: 400
    end
  end

  def suppress
    ordering_entity = Doctor if params[:type] == 'HealthcareProfessional'
    ordering_entity = Location if params[:type] == 'HealthcareFacility'
    if ordering_entity.present? && params[:id].present?
      entity = ordering_entity.find_by_id(params[:id])
      if entity.present?
        entity.canonical_id = params[:canonicalListingId] if params[:canonicalListingId].present?
        if params[:suppress].present?
          params[:suppress] == 'true' ? entity.suppressed! : entity.available!
        else
          entity.suppressed!
        end
        render json: { status: 200 }
      else
        render json: { status: 'Not Found' }, status: 404
      end
    else
      render json: { status: 'REJECTED - Type Or ID Not Present' }, status: 400
    end
  end

  api :GET, '/reviews', 'Get listing reviews'
  param :type, String, desc: 'Must be HealthcareProfessional OR HealthcareFacility', required: true
  param :listingId, String, desc: 'Healthsoul ID of HealthcareProfessional OR HealthcareFacility', required: true
  def reviews
    ordering_entity = Doctor if params[:type] == 'HealthcareProfessional'
    ordering_entity = Location if params[:type] == 'HealthcareFacility'
    if ordering_entity.present? && params[:listingId].present?
      entity = ordering_entity.find_by_id(params[:listingId])
      if entity.present?
        render json: entity.rating_json
      else
        render json: { status: 'Not Found' }, status: 404
      end
    else
      render json: { status: 'REJECTED - Type Or ID Not Present' }, status: 400
    end
  end

  private

  def find_entities
    if ['HealthcareProfessional', 'HealthcareFacility'].include?(params[:type])
      entities = Doctor if params[:type] == 'HealthcareProfessional'
      entities = Location if params[:type] == 'HealthcareFacility'
      if params[:type] == 'HealthcareProfessional' && params[:name].present?
        entities = entities.search(URI.decode(params[:name]))
        entities.results
      else
        entities = entities.search(URI.decode(params[:name])) if params[:name].present?
        if params[:type] == 'HealthcareProfessional' && params[:npi].present?
          entities = entities.where(npi: params[:npi])
        end
        entities = entities.where(phone: params[:phone]) if params[:phone].present?
        entities = entities.where(latitude: params[:latitude]) if params[:latitude].present?
        entities = entities.where(longitude: params[:longitude]) if params[:longitude].present?
      end
    end
    if entities == Doctor || entities == Location
      []
    else
      entities || []
    end
  end


end
