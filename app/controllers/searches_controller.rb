class SearchesController < ApplicationController
  layout 'design'
  def show
    @heading = "Find a Hospital"
    @clear = false
    if params[:clear] == "true"
      @clear = true
      add_breadcrumb "Best hospital ratings", :search_hospitals
      session[:hospital_search] = nil
    end
    if search_params.present?
      session[:hospital_search] = request.fullpath.gsub("ajax=true", "")
      @search = Search::Hospital.new(search_params)
      df_country = if !search_params[:near].present? & params[:set_locale].present?
        params[:set_locale]
      elsif !search_params[:near].present?
        current_country
      elsif search_params[:near].present?
        begin
          @geo_location = get_geo_location(search_params[:near])
          @search.city = @geo_location.city&.parameterize if @geo_location.try(:city)
          @search.state = [@geo_location.state&.parameterize, @geo_location.state_code&.parameterize] if @geo_location.try(:state)
          c = Country.where(alpha2: get_geo_country_code(@geo_location))
          c = c.count > 1 ? c.where(["name ilike ? or slug ilike ?", search_params[:near], search_params[:near]]).first : c.first
          c.try(:slug)
        rescue
          params[:set_locale] || current_country
        end
      else
        nil
      end
      @search.default_country = df_country
      @search_class = "hospitals"
      @search.order = order
      @search_heading = set_heading_and_meta
      if params[:ajax] == "true" && params[:page].blank?
        @blogs = Blog.order(published_at: :desc).limit(4).decorate
        @search_results = @search.results.page(params[:page]).per(25).decorate
        render layout: false and return
      end
    else
      if session[:hospital_search].present?
        redirect_to session[:hospital_search] and return
      end
      @search = Search::Hospital.new
      @search_class = "hospitals"
      unless @clear
        if current_location_selected?
          current_loc = current_location
          @search.near = current_loc['address']
          @search.lat = current_loc['latitude']
          @search.lng = current_loc['longitude']
        end
        @search.co = current_country
      end
      @search.default_country = current_country
      @search.order = "reviews_a"
    end
    @featured_or_sponsored = find_featured_or_sponsored
    @search_heading = set_heading_and_meta
    @blogs = Blog.order(published_at: :desc).limit(4).decorate
    unless @clear
      @search_results = @search.results.eager_load(:practice_specialities).page(params[:page]).per(25).decorate
    end
  end

  def best_hospitals
    if request.url.include? '/best_hospitals/'
      redirect_to request.url.gsub('best_hospitals', 'best-hospital-ratings'), status: 301
    else
      add_breadcrumb "Best hospital ratings", :search_best_hospitals
      add_breadcrumb formatted_country(params[:set_locale]), search_best_hospitals_path(params[:set_locale]) if params[:set_locale]
      add_breadcrumb formatted_state(params[:state], params[:set_locale]), search_best_hospitals_path(params[:set_locale], params[:state]) if params[:state]
      add_breadcrumb params[:city].try(:titleize), search_best_hospitals_path(params[:set_locale], params[:state], params[:city]) if params[:city]
      address = [params[:city], params[:state], params[:set_locale]].compact.join(', ')
      params[:search_hospital] = {near: address, state: params[:state], co: params[:set_locale]}
      @meta_location = formatted_one_location
      show
      render :show
    end
  end

  private

  def set_heading_and_meta
    set_meta
    set_heading
  end

  def set_meta
    @meta_location = formatted_one_location unless @meta_location
    last_updated_at = @search_results.pluck(:updated_at).max if @search_results.present?
    if last_updated_at
      set_meta_tags title: t("meta.hospital_search_updated_at",
        country: @meta_location,
        last_updated_at: last_updated_at.strftime('%b %Y'))
    else
      set_meta_tags title: t("meta.hospital_search",
        country: @meta_location)
    end
    set_meta_tags keywords: "Best Hospitals in #{@meta_location}"
    set_meta_tags description: t("meta_description.hospital_search", location: (@meta_location || 'N/A' )).html_safe
  end

  def order
    if search_params[:order].blank?
      if @search.class == Search::Doctor
        'claimed'
      else
        if search_params[:near].present?
          @search.order = "distance_a"
        else
          @search.order = "reviews_a"
        end
      end
    else
      @search.order = search_params[:order]
    end
  end

  def search_params
    if params[:search].present?
      params[:search_hospital] = params[:search]
    # IF no search parameter supply then we use session -> country
    elsif !params[:search_hospital].present? && session[:current_location]
      params[:search_hospital] = {near: formatted_country(session[:current_location]['country'])}
    end
    if params[:search_hospital].present?
      params.require(:search_hospital).permit(:co, :order, :departments, :lat, :lng, :near, :city, :state, :name,  :co, :stars => [])
    end
  end

  def default_locale
    "USA"
  end

  def set_heading
    "Find the Best Hospitals in #{@meta_location} enlisted below. Reach the most suitable Hospitals in #{@meta_location} easily!"
  end

  def formatted_one_location
    uniq_location = [@search.try(:city) , (@search.try(:state).class == Array ? @search.try(:state).first : @search.try(:state)) , formatted_country(@search.try(:co) || @search.try(:default_country))].compact.reject(&:blank?).first
    formatted_country(uniq_location)
  end

  def find_featured_or_sponsored
    conditions = {country_slug: @search.default_country, location_type_id: @search.location_type_id}
    conditions = conditions.merge({city_slug: @search.city.try(:parameterize)}) if  @search.city.present?
    return {
      featured: Location.where(conditions).where(featured_or_sponsored: 'Featured').order(name: :asc).pluck(:id),
      sponsored: Location.where(conditions).where(featured_or_sponsored: 'Sponsored').order(name: :asc).pluck(:id)
    }
  end
end
