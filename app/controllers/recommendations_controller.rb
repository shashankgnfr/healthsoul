class RecommendationsController < ApplicationController
  layout 'design'
  before_action :set_meta_tag

  # GET /recommendations/new
  def new
    @recommendation = Recommendation.new({source_type: params[:source_type]})
    unless params[:source_type].present?
      flash[:error] = "Not found requested source #{@recommendation.source_type}."
      redirect_to root_url
    end
  end

  # POST /recommendations
  def create
    @recommendation = Recommendation.new(recommendation_params)
    respond_to do |format|
      if @recommendation.save
        RecommendationMailer.notify(@recommendation.reload).deliver_now
        UserMailer.confirm_interested_provider(@recommendation.reload.email).deliver_now
        flash[:notice] = "Thank you for contacting us. Our Team will set up your account within 48-72 business hours."
        format.html { redirect_to eval("search_#{@recommendation.source_type.gsub(' ','_').downcase}s_path"), notice: "Recommendation for #{@recommendation.source_type} was successfully sent."}
      else
        format.html { render :new, params: {source_type: recommendation_params[:source_type]}}
      end
    end
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def recommendation_params
       params.require(:recommendation).permit(:name, :email, :website, :location, :country, :source_type)
    end

    def set_meta_tag
      @type = (params[:source_type] || recommendation_params[:source_type]).gsub('-', ' ').titleize
      add_breadcrumb "Add a #{@type}"
      set_meta_tags title: t("meta_title.recommendation", type: @type)
      set_meta_tags description: t("meta_description.recommendation", type: @type)
    end
end
