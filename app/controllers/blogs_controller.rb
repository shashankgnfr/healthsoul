class BlogsController < ApplicationController
  layout 'design'
  add_breadcrumb "Blog", :blogs
  
  def index
    if params[:tag].present? && params[:tag] != 'NA'
      @tag = nil
      begin
        str_tag = params[:tag].downcase
        @tag = ActsAsTaggableOn::Tag.where(["lower(name) in (?)", [str_tag, str_tag.gsub("-", " ")]]).first
      rescue => e
        p "Could not Parse Param"
      end
    end
    if @tag.present?
      @heading = @tag.title || "The Healthcare Blog"
      set_meta_tags title: @tag.meta_title || @tag.title || @tag.name
      set_meta_tags description: @tag.description || t("meta_description.blog")
    else
      set_meta_tags title: t("meta_title.blog")
      set_meta_tags description: t("meta_description.blog")
      @heading = "The Healthcare Blog"
    end
    @blogs = blogs_by_search_tags
    # If the user is logged in, or has a cookie that indicates they are subscribed to the newsletter,
    # then we will not render a the popup asking the user to subscribe

    # TODO: DO we need to show subscription alert if user is not subscribe yet?
    @render_subscription_modal = ((not (current_user.present?)) and (not (cookies.permanent[:subscribed_to_newsletter]=="true")) and (not (session[:ignore_subscription] == true)))  
    respond_to do |format|
      format.html
      format.rss { render :layout => false }
    end
  end

  def show
    begin
      blog = Blog.find(params[:id])
      @blog = blog.decorate
      @related_blogs = Blog.tagged_with(@blog.tag_list, :any => true).where.not(id: @blog.id).limit(4)
      @vertical_adverts = get_adverts(blog, 'Vertical')
      @horizontal_adverts = get_adverts(blog, 'Horizontal')
      impressionist(blog, "Detail")
      tag = @blog.tag_list.first
      add_breadcrumb tag, blogs_tag_path(tag: tag.parameterize) if tag
      add_breadcrumb @blog.title, blog_path(@blog)
    rescue
      redirect_to blogs_path
    end
  end

  def share_blog
    blog = Blog.where(id: params[:id]).first
    if blog
      shared_blog = blog.shared_blogs.where(user_id: current_user.id, social_platform: params[:social_platform]).first
      if shared_blog
        shared_blog.shared_count += 1
      else
        shared_blog = SharedBlog.new({blog_id: blog.id, user_id: current_user.id, social_platform: params[:social_platform], shared_count: 1})
      end
      # need to use .save to invoke after_commit callback of ReviewCalc
      shared_blog.save
      render status: 200, json: {status: :success, code: "200", message: "Shared blog successfully", shared_count: shared_blog.reload.shared_count}
    end
  end

  private

  def blogs_by_search_tags
    blogs = Blog
    blogs = Blog.title_contains(params[:q]).union(Blog.tagged_with(params[:q])) if params[:q].present?
    blogs = blogs.tagged_with(@tag.name) if @tag.present?
    blogs.order(published_at: :desc).page(params[:page]).all.decorate
  end

  def get_adverts(blog, advert_type)
    advert = []
    # Find Blog Advertisements
    advert << blog.advertisements.where(publish: true, subtype: advert_type)
    # Find Blog Categories Advertisements
    advert << Advertisement.where(id: AdvertisementsResource.where(resource_type: "ActsAsTaggableOn::Tag", resource_id: blog.tags.pluck(:id)).pluck(:advertisement_id).uniq, publish: true, subtype: advert_type)
    if advert.flatten.empty?
      # If no advertisement associated with blog or it's category then use any random advertisement
      advert << Advertisement.where(publish: true, subtype: advert_type) 
    end
    advert = advert.flatten.uniq.compact
  end
end
