class Api::MailchimpController < ApplicationController

	# 1.Blog Any Update Every Month Send mail User by MailChimp Api
	def mailchimp
		uri = URI("https://us16.api.mailchimp.com/3.0/lists/a5ae438733/members")
    http = Net::HTTP.new(uri.host, uri.port)
	  http.use_ssl = true
	  http.verify_mode = OpenSSL::SSL::VERIFY_NONE # You should use VERIFY_PEER in production
	  request = Net::HTTP::Get.new(uri)
	  request["Cache-Control"] = 'no-cache'
    request["Postman-Token"] = 'dfee2f90-fdc3-4c3a-9493-ddd073159095'
    request["Authorization"] = 'Basic ZGF1bGF0cmFtOmFhNTMzYWZhYmJjYjZkZTYwZmUxOWE2ZDYxZWJiYWU2LXVzMTY='
	  response = http.request(request)
	  body = response.read_body
	  parsed_json = JSON.parse(body);
	  @members = parsed_json['members']
	  @members.each do |member|
	  	id = member["id"] #id here
	  	email = member["email_address"] #email
	    @user = User.where(email: [email])
	    @user.each do |user|
        fname = user.first_name
        lname = user.last_name
        points = user.total_points
        reviews = user.ratings.count
		    memberUrl = "https://us16.api.mailchimp.com/3.0/lists/a5ae438733/members/#{id}"
		    #puts memberUrl
		    data = "{\"merge_fields\":{\"FNAME\": \"#{fname}\",\"LNAME\": \"#{lname}\",\"MMERGE4\": \"#{reviews}\",\"MMERGE5\": \"#{points}\"}}";
		    #puts data
			  url = URI(memberUrl)
				http = Net::HTTP.new(url.host, url.port)
		    http.use_ssl = true
		    http.verify_mode = OpenSSL::SSL::VERIFY_NONE # You should use VERIFY_PEER in production
				request = Net::HTTP::Patch.new(url)
				request["Authorization"] = 'Basic ZGF1bGF0cmFtOmFhNTMzYWZhYmJjYjZkZTYwZmUxOWE2ZDYxZWJiYWU2LXVzMTY='
				request["Cache-Control"] = 'no-cache'
				request["Postman-Token"] = '7831a9f0-40b4-4190-a614-ada716f4cf28'
				request.body = data
				#puts request.body
				response = http.request(request)
				#puts response.read_body
				@resultdata = JSON.parse(body);
		  end
		end
  	render json: { status: 200, message: "success", response: @resultdata }	
	end
end
