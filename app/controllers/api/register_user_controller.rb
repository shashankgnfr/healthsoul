class Api::RegisterUserController < ApplicationController
  #before_ before_filter :authenticate_user!
  protect_from_forgery with: :null_session
  skip_before_filter  :verify_authenticity_token

  # 1.create new user
  def create_user
    @user = User.new(email: params[:email],password: params[:password], password_confirmation: params[:password_confirmation])
    if @user.save
     render :status=>200, :json=>{status: :success, code: "200",:message=>"Create User Successfully", user: @user }
    else
    render :status=>401, :json=>{status: :failure, code: "401",:message=>"user email alrady exist"}
    end
  end

  # 2.login by user
  def login
    email = params[:email] if params[:email]
    password = params[:password] if params[:password]
    if email.nil? && password.nil?
      render status: 400, json: { status: :failure, code:"400", message: "request must contain email and password"}
    end
    @user = User.find_by(email: email)
    if @user
      if @user.valid_password? password
        sign_in("user", @user)
        render  status: 200, json: {status: :success, code: "200", message: "Successfully login",user: @user}
      else
        render status: 401, json: {status: :failure, code: "401", message: "Invalid email and password"}
      end
    else
      render status: 401, json: { status: :failure, code: "401", message: "Invalid email and password"}
    end
  end


  # 3. DESTROY User
  def logout
    @user =User.find_by(id: params[:id])
    if @user.nil?
      render status: 401, json: {status: :failure, code: "401", message: "Invalid tocken"}
    else
      sign_out(@user)
      render status: 200, json: { status: :success, code: "200", message: "sign_out Successfully", user: @user }
    end
  end

  # 4. Update User Profile
  def update_profile
    @user = User.find_by(id: params[:id])
    if @user.nil?
      logger.info("User not found.")
      render :status => 404, :json => {:status => "error", :errorcode => "11009", :message => "Invalid userid."}
    else
      @user.update_attributes(params.permit(:email, :username,:first_name,:last_name,:phone_number,:sex,:state,:city,:photo, :co,:date_of_birth,:password,:password_confirmation))
      render :status => 200, :json => {:status => "success",code: "200", :user => @user, :message => "The user has been updated"}
    end
  end

  # 5. User Profile show Total Review and Total Point.
  def user_point_review
    @user = User.find_by(id: params[:id])
    user = @user.ratings.count
    render  status: 200, json: {status: :success, code: "200", message: "User Info", total_point: @user.total_points,total_review: user}
  end

end
