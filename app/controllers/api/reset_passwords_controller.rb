class Api::ResetPasswordsController < ApplicationController

  # 1.Reset User Password.  
  def index
    user = User.find_by_email(user_params)
    if user.present?
     user.send_reset_password_instructions
     render status: 200, json: {status: :success, code: "200", message: "Reset Password Send Email"}     

    else
      render status: 201, json: {status: :failure, code: "201", message: "Invalid  Email"}
    end
  end

 private
  def user_params
    params.require(:email)
  end
end
