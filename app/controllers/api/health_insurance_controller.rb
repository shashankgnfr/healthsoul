class Api::HealthInsuranceController < ApplicationController
	
  # 1.HealthInsurance Search By Name and Country.
  def search_health_insurance
   if search_params.present?
		  @search = Search::HealthInsurance.new(search_params)
		  current_country = search_params[:co]
		  @selected_country ||= Country.where(alpha3: current_countrycountries_and_continents=current_country).first
		  @location_type_id ||= LocationType.where(name: "Health Insurance").first.id
		  @countries_and_continents ||= Location.where(location_type_id: @location_type_id).where("country is not null").where("continent is not null").select("continent, country, country_code").group("continent, country, country_code").order("continent, country").to_a
      if @search.has_results?
		   if @search.count < 1000
         @search= @search.results.limit(15)
        else
         @search = @search.results.limit(25)
        end
		   render status: 200, json: {status: :success,code: "200", message: "Health Insurance information",location: @search}      
      else
       render status: 201, json: {status: :failure, code: "201", message: "Health Insurance not found"}     
      end  
		end
	end


  private

  def order
    if search_params[:order].blank?
      if search_params[:near].present?
        @search.order = "distance_a"
      else
        @search.order = "reviews_a"
      end
    else
      @search.order = search_params[:order]
    end
  end


  def search_params
    if params[:search].present?
      params[:search_health_insurance] = params[:search]
    end
    if params[:search_health_insurance].present?
      params.require(:search_health_insurance).permit(:order, :departments, :co, :sub_type, :co, :name, :stars => [])
    end
  end


  def unfiltered
    return @unfiltered if @unfiltered.present?
    @unfiltered = Location.includes(:location_type)
    if without_geolocation.present? && without_geolocation == "1"
      @unfiltered = @unfiltered.where("latitude is null or longitude is null")
    end
    if sub_type.present?
      @unfiltered = @unfiltered.where("sub_type ilike ?", sub_type)
    end
    if state.present?
      @unfiltered = @unfiltered.where("state ilike ?", state)
    end
    if name.present?
      @unfiltered = @unfiltered.search(name)
    end
    if co.present?
      @unfiltered = @unfiltered.where(country_slug: co)
    end
    if lat_lng_present?
      if order == "distance_a"
        @unfiltered = @unfiltered.near(lat_lng, within)
      else
        @unfiltered = @unfiltered.near(lat_lng, within, order: false)
      end
    end
    if departments.present?
      @unfiltered = @unfiltered
        .joins(:department_ratings)
        .where("department_ratings.reason IN(?)", departments)
    end
    if @unfiltered == Location
      @unfiltered = []
    else
      if lat_lng_present? && order_by.blank?
        @unfiltered = @unfiltered.where(location_type_id: location_type_id)
      else
        @unfiltered = @unfiltered.where(location_type_id: location_type_id).reorder(order_by)
      end
    end
  end


  def results
    return @results if @results.present?
    @results = unfiltered
    if stars_min.present?
      @results = @results.where("overall >= ?", stars_min )
    end
    if cities_list.present?
      @results = @results.where("city IN (?)", cities_list )
    end
    if @results == unfiltered
      @results
    else
      if lat_lng_present? && order_by.blank?
        @results = @results.where(location_type_id: location_type_id).order(order_by)
      else
        @results = @results.where(location_type_id: location_type_id).reorder(order_by)
      end
    end
  end
end
