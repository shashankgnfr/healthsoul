class Api::VotesController < ApplicationController
  respond_to :json
  def create
    @vote = Vote.new(vote_params)
    @vote.user = current_user
    if @vote.save
      render :json => {status: 1, count: @vote.voteable.votes.send(vote_params[:status]).count}
    else
      render :json => {status: 0, count: 0}
    end
  end

  private

  def vote_params
    params.permit(:voteable_type, :voteable_id, :status)
  end
end
