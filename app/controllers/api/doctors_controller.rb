class Api::DoctorsController < ApplicationController
  #skip_before_filter :verify_authenticity_token  
  skip_before_filter :verify_authenticity_token, :if => Proc.new { |c| c.request.format == 'application/json' }


  
  def index
    if params[:q].present?
      @doctors = Doctor.search(params[:q]).limit(20)
    else
      @doctors = Doctor.limit(20)
    end
  end

  # 1.Show Doctor Detials
  def doctor_show
    @doctor = Doctor.find(params[:id]).decorate
    @ratings = @doctor.ratings.order(created_at: :desc)
    @location = @doctor
    @appointment = Appointment.new(provider: @doctor)
    doctor_specialities = DoctorSpeciality.where(doctor_id: @doctor.id).includes(practice_speciality: [:procedures, :conditions])

    practice_specialities = doctor_specialities.collect {|ds| ds.practice_speciality}
    @specialities = practice_specialities.map(&:name)

    condition_list = practice_specialities.collect{ |speciality| speciality.conditions }
    @conditions = condition_list.flatten.uniq.map(&:name)

    procedure_list = practice_specialities.collect{ |speciality| speciality.procedures }
    @procedures = procedure_list.flatten.uniq.map(&:name)
    doctors_school = DoctorsSchool.where(doctor_id: @doctor.id).includes(:med_school)
    med_schools = doctors_school.collect{ |ds| ds.doctors_school }
    @med_schools= med_schools.map(&:name)
    impressionist(@doctor, "Detail")
    
    @defaultDescription= "#{first_specialty = @doctor.practice_specialities.first}"
    @doctor.description = (@doctor.practice_specialities)?@doctor.doctorized_specialty_description(@doctor.practice_specialities.first) : @defaultDescription
    
    render status: 200, json: {status: :success,code:"200",doctor: @doctor, specialities: @specialities ,conditions: @conditions, procedure: @procedure,medicalschool: @med_schools,rating:@ratings}
  end


  # 2.Doctor Review Detials Show
  def doctor_review 
    reviews = DoctorReview.where(doctor_id: params[:doctor_id]).order(created_at: :desc)
    @reviews=reviews.as_json(include: { user: { only: :username }})
    if params[:top_five].present?
      @reviews = @reviews.take(5)
      # We pass the top_five param for the cross site widget. If that param is passed, allow for a cross site response.
      headers['Access-Control-Allow-Origin'] = '*'
      headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    end
    render status: 200, json:{status: :success,code:"200",review: @reviews}
  end

  # 3.Create Review Doctor by User
  def create_review
    @doctor = @resource = Doctor.find(params[:doctor_id])
    @rating = DoctorReview.new(use_date: params[:use_date],user_id: params[:user_id], where: params[:where], overall_experience: params[:overall_experience], explaintation: params[:explaintation], trust: params[:trust], staff: params[:staff], appt: params[:appt], wait: params[:wait], skills: params[:skills], recommend: params[:recommend], comment: params[:comment], status: params[:status])
    @user =User.find(params[:user_id])
    @rating.user =@user
    @rating.doctor = @resource
    if @rating.save
     render :status=>200, :json=>{status: :success, code: "200",:message=>"Create Doctor Review Rating Successfully", review: @rating }
    else
     render :status=>401, :json=>{status: :failure, code: "401",:message=>"Doctor Review Rating not create"}
    end
  end

  # 4.Hospital Detials Show 
  def hospital_show
    @location = Location.find(params[:id])
    @ratings = @location.ratings.order(created_at: :desc)
    impressionist(@location, "Detail")
    @location = @location.decorate 

    @defaultDescription = "#{@location.name} is #{@location.hopital_variation} in #{@location.city_state_country}.  Find patient reviews, specialty ranking and compare with nearby hospitals."
    @location.description = (@location.description)?@location.description: @defaultDescription

    render status: 200, json: {status: :success,code:"200",hospital: @location,rating:@ratings}
  end
  
  # 5.Hospital Review Details Show
  def hospital_review
    if params[:location_id].present? 
      reviews = Rating.where(location_id: params[:location_id]).order(created_at: :desc)
      @reviews=reviews.as_json(include: { user: { only: :username }})
     
      render status: 200, json:{status: :success,code:"200",hospital_review: @reviews}
    else
     render status: 401, json: {status: :failure, code: "401", message: "Invalid Location Id"}
    end
  end
  
  # 6.Create Hospital Review By User
  def create_hospital_review
   @location = @resource = Location.find(params[:location_id])
   @rating = Rating.new(location_id: params[:location_id], use_date: params[:use_date], user_id: params[:user_id], reason: params[:reason], overall_experience: params[:overall_experience], doctor_care: params[:doctor_care], nursing_care: params[:nursing_care], cleanliness: params[:cleanliness], food_services: params[:food_services], recommend: params[:recommend], comment: params[:comment], get_ins: params[:get_ins])
   @user=User.find(params[:user_id])
   @rating.user = @user
   @rating.location = @resource
    if @rating.save
     render :status=>200, :json=>{status: :success, code: "200",:message=>"Create Hospital Review Rating Successfully", review: @rating }
    else
     render :status=>401, :json=>{status: :failure, code: "401",:message=>"Hospital Review Rating not create"}
    end
  end

  # 7.Health Insurance Show Detials
  def healthinsurance_show
    if params[:id].present?
      @location = Location.find(params[:id])
      @ratings = @location.ratings.order(created_at: :desc)
      impressionist(@location, "Detail")
      @location = @location.decorate 

      @defaultDescription = "#{@location.name} is a health insurance company in #{@location.country}. #{@location.name} has several different health plans to suit your insurance needs. You can find contact information for #{@location.name} on this page including their website and phone number.";

      @location.description = (@location.description)?@location.description: @defaultDescription


      render status: 200, json: {status: :success,code:"200",health_insurance: @location}
    else
     render status: 401, json: {status: :failure, code: "401", message: "Invalid Id"} 
    end
  end

  # 8.Health Insurance Review Details
  def healthinsurance_review
   reviews = Rating.where(location_id: params[:location_id]).order(created_at: :desc)
   @reviews=reviews.as_json(include: { user: { only: :username }})
    
   render status: 200, json:{status: :success,code:"200",healthinsurance_review: @reviews }
  end
   
  # 9.Create Insurance Review By User
  def create_insurance_review
   @location= @resource =Location.find(params[:location_id])
   @rating = Rating.new(use_date: params[:use_date], user_id: params[:user_id], overall_experience: params[:overall_experience], recommend: params[:recommend], comment: params[:comment], customer_service: params[:customer_service], get_ins: params[:get_ins], cost_satisfaction: params[:cost_satisfaction], claims_process: params[:claims_process])
   @user=User.find(params[:user_id])
   @rating.user = @user
   @rating.location = @resource
    if @rating.save
     render :status=>200, :json=>{status: :success, code: "200",:message=>"Create Health Insurance Review Rating Successfully", review: @rating }
    else
     render :status=>401, :json=>{status: :failure, code: "401",:message=>"Health Insurance Review Rating not create"}
    end 
  end
end


#10 health details score by doctor

  # def health_score_details_attributes
  #   @healthscore = HealthScoreDetail.find(params[:health_score_details_attributes])
# end

    