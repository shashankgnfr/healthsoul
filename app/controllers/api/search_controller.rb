class Api::SearchController < ApplicationController
  QueryKeys = [ :co, :near, :city, :state, :name, :within, :country_code, :stars, :lat, :lng, :speciality]
  attr_accessor *QueryKeys, :order, :default_country, :without_geolocation

 # 1. Doctor Search By Speciality,Name,Address and Country.
  def search_doctor
    if search_params.present?
      @search = Search::Doctor.new(search_params)
      @search_class = "doctors"
      @search.order = order
      speciality_title = PracticeSpeciality.find_by_id(search_params['speciality'])&.title || 'Doctors'
      set_meta(speciality_title)
      {"Common" => PracticeSpeciality.where(common_speciality: true).order(:name).collect {|opt| [opt.name, opt.id.to_s]}, "Other" => PracticeSpeciality.where(common_speciality: false).order(:name).collect {|opt| [opt.name, opt.id.to_s]}}
      current_country = search_params[:co]
      @selected_country ||= Country.where(alpha3: current_countrycountries_and_continents=current_country).first
      Doctor.where(country_slug: search_params[:co]).distinct(:state).order(:state).pluck(:state)
      results.select("state, count(state) as state_count").group('state').reorder("state").where("state is not null").all.size.map{|state, count| ["#{state} (#{count})", state] }
      @countries_and_continents ||= DoctorOrganization.where("country is not null").where("continent is not null").select("continent, country, country_code").group("continent, country, country_code").order("continent, country").to_a
      if @search.has_results?
       if @search.count < 1000
         @search= @search.results(1, 15)
       else
         @search = @search.results(1, 25)
       end
       render status: 200, json: {status: :success,code: "200", message: "Doctor information", specialities: @search}
      else
       render status: 201, json: {status: :failure, code: "201", message: "Doctor not found"}
      end
    end
  end


  private

  def set_meta(speciality_title)
    location = [@search.city.try(:titleize), @search.state.try(:titleize), @search.default_country.try(:titleize)].compact.reject(&:blank?).join(', ')
    set_meta_tags title: t("meta.doctor_search", 
      specialist_type: speciality_title.pluralize,  
      location: location)
    set_meta_tags keywords: "Best #{speciality_title.pluralize} in #{location}"
  end

  def order
    if search_params[:order].blank?
      if @search.class == Search::Doctor
        'claimed'
      else
        if search_params[:near].present?
          @search.order = "distance_a"
        else
          @search.order = "reviews_a"
        end
      end
    else
      @search.order = search_params[:order]
    end
  end


  def search_params
    if params[:search].present?
      params[:search_doctor] = params[:search]
    end
    if params[:search_doctor].present?
      params.require(:search_doctor).permit(:co, :order, :lat, :lng, :near, :city, :state, :name, :within, :speciality, :co, :stars=>[])
    end
  end

  def lat_lng_present?
    lat.present? && lng.present?
  end


  def unfiltered
    return @unfiltered if @unfiltered.present?
    @unfiltered = ::Doctor
    if without_geolocation.present? && without_geolocation == "1"
      @unfiltered = @unfiltered.where("latitude is null or longitude is null")
    end
    if state.present?
      @unfiltered = @unfiltered.where("state ilike ?", state)
    end
    if name.present?
      @unfiltered = @unfiltered.search(name)
    end
    if co.present?
      @unfiltered = @unfiltered.where(country_slug: co)
    end
    if lat_lng_present?
      if order == "distance_a"
        @unfiltered = @unfiltered.near(lat_lng, within)
      else
        @unfiltered = @unfiltered.near(lat_lng, within, order: false)
      end
    end
    if @unfiltered == ::Doctor
      @unfiltered = ::Doctor
    else
      @unfiltered = @unfiltered.order(order_by).limit(1000)
    end
  end


  def results
    return @results if @results.present?
    @results = unfiltered
    if speciality.present?
      @results = @results.joins(:doctor_specialities).where("doctor_specialities.practice_speciality_id" => speciality)
    end
    
    if stars.present?
      @results = @results.where("overall >= ?", stars_min )
    end
    if @results == ::Doctor
      @results = Doctor.none
    else
      @results = @results.order(order_by)
    end
  end

  def order_by
    case order
    when "name_a"
      "doctors.name ASC"
    when "name_b"
      "doctors.name DESC"
    when "reviews_a"
      "doctors.ratings_count DESC"
    when "reviews_b"
      "doctors.ratings_count ASC"
    when "stars_a"
      "doctors.overall DESC NULLS LAST"
    when "stars_b"
      "doctors.overall ASC"
    else
      "doctors.name ASC"
    end
  end
end
