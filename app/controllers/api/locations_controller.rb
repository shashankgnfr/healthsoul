class Api::LocationsController < ApplicationController

  respond_to :json
  def index
    if params[:q].present?
    	if params[:country_code].present?
    		@locations = Location.search(params[:q]).where(country_code: params[:country_code])
    	else
    		@locations = Location.search(params[:q])
    	end
    else
    	if params[:country_code].present?
    		@locations = Location.where(country_code: params[:country_code]).limit(20)
    	else
    		@locations = Location.all.limit(20)
    	end
    end
  end
  # 1.Hospital Search By Name,Address and Country.
  def search_hospital
    if search_params.present?
      @search = Search::Hospital.new(search_params)
      @search_class = "hospitals"
      @search.order = order
      current_country = search_params[:co]
      @selected_country ||= Country.where(alpha3: current_countrycountries_and_continents=current_country).first
      @location_type_id ||= LocationType.where(name: "Hospital").first.id
      @countries_and_continents ||= Location.where(location_type_id: @location_type_id).where("country is not null").where("continent is not null").select("continent, country, country_code").group("continent, country, country_code").order("continent, country").to_a
      if @search.has_results?
        if @search.count < 1000
         @searchs= @search.results.limit(15)
        else
         @searchs = @search.results.limit(25)
        end
        @searchs.each  do |search|
         search[:place_photo] = search.photo unless search.place_photo.present?
        end
       render status: 200, json: {status: :success,code: "200", message: "hospitals information", location: @searchs}
      else
        render status: 201, json: {status: :failure, code: "201", message: "hospital not found"}
      end
    end
  end

  private

  def order
    if search_params[:order].blank?
      if search_params[:near].present?
        @search.order = "distance_a"
      else
        @search.order = "reviews_a"
      end
    else
      @search.order = search_params[:order]
    end
  end

  def search_params
    if params[:search].present?
      params[:search_hospital] = params[:search]
    end
    if params[:search_hospital].present?
      params.require(:search_hospital).permit(:co, :order, :sub_type, :departments, :lat, :lng, :near, :city, :state, :name, :within, :co, :stars => [])
    end
  end


  def unfiltered
    return @unfiltered if @unfiltered.present?
    @unfiltered = Location.includes(:location_type)
    if without_geolocation.present? && without_geolocation == "1"
      @unfiltered = @unfiltered.where("latitude is null or longitude is null")
    end
    if sub_type.present?
      @unfiltered = @unfiltered.where("sub_type ilike ?", sub_type)
    end
    if state.present?
      @unfiltered = @unfiltered.where("state ilike ?", state)
    end
    if name.present?
      @unfiltered = @unfiltered.search(name)
    end
    if co.present?
      @unfiltered = @unfiltered.where(country_code: co.upcase)
    end
    if lat_lng_present?
      if order == "distance_a"
        @unfiltered = @unfiltered.near(lat_lng, within)
      else
        @unfiltered = @unfiltered.near(lat_lng, within, order: false)
      end
    end
    if departments.present?
      @unfiltered = @unfiltered
        .joins(:department_ratings)
        .where("department_ratings.reason IN(?)", departments)
    end
    if @unfiltered == Location
      @unfiltered = []
    else
      if lat_lng_present? && order_by.blank?
        @unfiltered = @unfiltered.where(location_type_id: location_type_id)
      else
        @unfiltered = @unfiltered.where(location_type_id: location_type_id).reorder(order_by)
      end
    end
  end

  def results
    return @results if @results.present?
    @results = unfiltered
    if stars_min.present?
      @results = @results.where("overall >= ?", stars_min )
    end
    if cities_list.present?
      @results = @results.where("city IN (?)", cities_list )
    end
    if @results == unfiltered
      @results
    else
      if lat_lng_present? && order_by.blank?
        @results = @results.where(location_type_id: location_type_id).order(order_by)
      else
        @results = @results.where(location_type_id: location_type_id).reorder(order_by)
      end
    end
  end
end
