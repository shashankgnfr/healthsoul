class Api::AppointmentController < ApplicationController
 skip_before_action :verify_authenticity_token

    # 1.Create Appointment by user
    def create_appointment
	   @provider = Doctor.find(params[:doctor_id])
	   @appointment = Appointment.new(provider_id: params[:provider_id], date: params[:date], time: params[:time], reason: params[:reason], name: params[:name], email: params[:email], phone: params[:phone], provider_type: params[:provider_type], new_patient: params[:new_patient], user_id: params[:user_id], confirmed: params[:confirmed])
	   @appointment.provider = @provider
	   user = User.find(params[:user_id]).present?
	   @appointment.user.update_attributes(params.permit(:email,:first_name,:last_name,:phone_number))
	   @user= @appointment.user.as_json(only: [:first_name, :last_name])
	   if @appointment.save
	      render status: 200, json: {status: :success,code: "200", message: "Appointments create successfully", appointments: @appointment, name: @user, provider: @provider}
	   else
	      render status: 201, json: {status: :failure, code: "201", message: "We're sorry, but we were unable to contact the office to request your appointment. The contact details provided by the office may be incomplete."}
	   end
    end

	# 2.Show Appointment User
    def appointment_show
 	    if params[:user_id].present?
	     @appointment = User.find(params[:user_id]).appointments
	     @appointment = @appointment.where("date >= ?", Time.zone.now.beginning_of_day)
	     all_provider_ids = @appointment.collect(&:provider_id)
	     provider = Doctor.where(id: all_provider_ids)
	     @provider= @appointment.as_json(include: { provider: { only: [:name, :specialities, :photo]}})
	     render status: 200, json: {status: :success,code: "200", message: "Appointments information", appointments: @provider}
	    else
	      render status: 201, json: {status: :failure, code: "201", message: "Appointment Not Found"}
	    end
    end
end
