class DoctorReviewsController < RatingsController
  def index
    set_meta_tags noindex: true
    authenticate_user!
    redirect_to helpers.doctor_path(@resource) and return if session[:rating_params].blank?
    r_params = session[:rating_params].with_indifferent_access
    @rating = DoctorReview.new(r_params)
    @rating.user = current_user
    @rating.doctor = @resource
    session[:rating_params] = nil
    save_rating
  end

  def find_resource
    set_meta_tags noindex: true
    @doctor = @resource = Doctor.find(params[:doctor_id]).try(:decorate)
    unless @resource.present?
      flash[:error] = "Not found requested doctor #{params[:doctor_id]}."
      redirect_to root_url
    end
  end

  def rating_params
    params.require(:doctor_review).permit(:use_date, :where, :overall_experience, :explaintation, :trust, :staff, :appt, :wait, :skills, :recommend, :comment)
  end

  def init_new
    set_meta_tags noindex: true
    @rating = DoctorReview.new(rating_params)
    @rating.user = current_user
    @rating.doctor = @resource
    @rating
  end

  def return_path
    doctor_doctor_reviews_path(@resource)
  end


  def save_rating
    set_meta_tags noindex: true
    if @rating.save
      RatingMailer.verify_rating(@rating).deliver_now
      RatingMailer.notify_doctor(@rating).deliver_now
      RatingMailer.confirmation(@rating).deliver_now
      if @resource.doctor?
        redirect_to helpers.doctor_path(@resource), notice: t("rating.saved")
      elsif @resource.hospital?
        redirect_to helpers.location_path(@resource), notice: t("rating.saved")
      else
        redirect_to @resource, notice: t("rating.saved")
      end
    else
      if @rating.errors.messages[:user_id].present?
        flash[:alert] = t("rating.over_limit")
        redirect_to @resource
      else
        flash[:alert] = "Please resolve the errors below"
        render :new
      end
    end
  end

end
