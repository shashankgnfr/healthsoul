class PracticeController < SearchesController
  def show
    add_breadcrumb "Practice", :search_practices
    set_meta_tags title: t("meta.practice_search", country: current_country.titleize)
    set_meta_tags description: t("meta_description.practice_search")
    @heading = "Find Practices"
    @search_class = "practice"
    if params[:clear] == "true" || params[:set_locale]
      session[:practice_search_url] = nil
    end
    if search_params.present?
      session[:practice_search_url] = request.fullpath.gsub("ajax=true", "")
      @search = Search::Practice.new(search_params)
      @search.order = "reviews_a" if search_params[:order].blank?
      if params[:ajax] == "true" && params[:page].blank?
        @results = @search.results.page(params[:page]).decorate
        render layout: false and return
      end
    else
      if session[:practice_search_url].present?
        redirect_to session[:practice_search_url] and return
      end
      @search = Search::Practice.new
      @search.default_country = current_country
      @search.order = "reviews_a"
      @search.co = current_country
    end
    @results = @search.results.page(params[:page]).decorate
    @blogs = Blog.order(published_at: :desc).limit(4).decorate
  end

  private

  def search_params
    if params[:search].present?
      params[:search_practice] = params[:search]
    end
    params.require(:search_practice).permit(:order, :departments, :co, :sub_type, :co, :name, :stars => []) if params[:search_practice].present?
  end
end
