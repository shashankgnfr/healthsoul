class PasswordsController < Devise::PasswordsController
  layout 'design'
  add_breadcrumb "Forgot Password"
end
