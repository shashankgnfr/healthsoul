  class DoctorsController < LocationsController
  add_breadcrumb "Best doctor ratings", :search_doctors
  def show
    @doctor = Doctor.find(params[:id]) 
    if @doctor.present?
      if request.xhr?
        register_impressions_per_day
        render file: "doctors/show.js.erb" 
      else
        if @doctor.suppressed?
          if @doctor.canonical_id.present?
            doctor = Doctor.find(@doctor.canonical_id)
            redirect_to helpers.doctor_path(doctor) and return 
          else
            raise ActionController::RoutingError.new('Not Found')
          end
        end
        new_url = helpers.doctor_path(@doctor) 
        redirect_to helpers.doctor_path(@doctor) and return if request.path != new_url
        @doctor = @doctor.decorate
        @ratings = @doctor.ratings.order(created_at: :desc)
        @location = @doctor
        @award = Award.where(location_id: @doctor.id, location_type: "Doctor", year: Date.today.year).first
        @appointment = Appointment.new(provider: @doctor)
        specialities_blogs = @doctor.blogs_by_specialities.order(published_at: :desc).limit(5).decorate
        @blogs = specialities_blogs[0..1]
        @horizontal_blogs = specialities_blogs[2..-1]
        @featured_or_sponsored = find_sponsored_or_nearby
        # Populate speciality, condition, and procedure blog links that should be shown on this doctor's page. Start by eager loading
        # all of that information in the doctor_specialties.
        doctor_specialities = DoctorSpeciality.where(doctor_id: @doctor.id).includes(practice_speciality: [:procedures, :conditions])

        practice_specialities = doctor_specialities.collect {|ds| ds.practice_speciality}
        @specialities = practice_specialities.map(&:name)

        # Used to collect conditions to render on doctor overview page.
        # Condition names double as blog taggables, so we can use this list of conditions
        # to create a list of links to blogs tagged with any given condition.

        condition_list = practice_specialities.collect{ |speciality| speciality.conditions }
        @conditions = condition_list.flatten.uniq.map(&:name)
        
        # practice_specialities = doctor_specialities.map{|hs|hs.top_speciality}
        # @top_speciality = DoctorSpeciality.where(doctor_id: @doctor.id)
        # Used to collect procedures to render on doctor overview page.
        # Procedure names double as blog taggables, so we can use this list of procedures
        # to create a list of links to blogs tagged with any given procedure.
        procedure_list = practice_specialities.collect{ |speciality| speciality.procedures }
        @procedures = procedure_list.flatten.uniq.map(&:name)
        impressionist(@doctor, "Detail")
        if request.url.include? '/doctors/'
          add_breadcrumb @doctor.name
        else
          generate_breadcrumb
        end
      end
    else
      raise ActionController::RoutingError.new('Not Found')
    end
  end

  private

  def generate_breadcrumb
    add_breadcrumb @doctor.top_speciality.titleize, search_best_doctors_path(@doctor.try(:top_speciality), @doctor.try(:country_slug) || 'NA')
    if @doctor.country
      add_breadcrumb formatted_country(@doctor.country), search_best_doctors_path(@doctor.try(:top_speciality), @doctor.try(:country_slug))
      if @doctor.state 
        add_breadcrumb @doctor.state, search_best_doctors_path(@doctor.try(:top_speciality), @doctor.try(:country_slug), @doctor.try(:state_slug))
        if @doctor.city
          add_breadcrumb @doctor.city, search_best_doctors_path(@doctor.try(:top_speciality), @doctor.try(:country_slug), @doctor.try(:state_slug), @doctor.try(:city_slug))
        end
      else
        add_breadcrumb 'NA'
        add_breadcrumb @doctor.city if @doctor.city
      end
    else
      add_breadcrumb 'NA'
      add_breadcrumb @doctor.state if @doctor.state
      add_breadcrumb @doctor.city if @doctor.city
    end
    add_breadcrumb @doctor.name
  end

  def register_impressions_per_day
    imp = Impression.find_or_initialize_by(
      impressionable_type: 'Doctor', 
      impressionable_id: @doctor.id,
      message: 'Impression',
      controller_name: 'doctors',
      action_name: 'show', 
      created_at: Time.now.beginning_of_day
      )
    imp.counter_per_day += 1
    imp.save
  end

  def find_sponsored_or_nearby
    # do we need to use provider_type == 'Doctor'?
    conditions = {country_slug: @doctor.country_slug}
    conditions = conditions.merge({city_slug: @doctor.city_slug}) if @doctor.city_slug.present?
    conditions = conditions.merge({state_slug: @doctor.state_slug}) if @doctor.state_slug.present?
    primary_speciality_id = @doctor.primary_speciality.try(:id)
    conditions = conditions.merge(practice_specialities: {id: primary_speciality_id}) if primary_speciality_id

    return {
      sponsored: Doctor.where(conditions).where(featured_or_sponsored: 'Sponsored').order(name: :asc).limit(2),
      nearby: Doctor.where(conditions).order(name: :asc).limit(3)
    }
  end

end
