class Manage::DoctorsController < Manage::BaseController
    # layout "design"
    # require 'google-places'



  def show
    @doctor = Doctor.find(params[:id])
    authorize @doctor, :manage?
    @doctor = @location = @doctor.decorate
    #tring out google reviews
   # api_key = "AIzaSyBUVqLYYdmb_GcnCegkvaZwESm4revWswg"
    api_key = "AIzaSyBx7GZi0TA0FJR7mG10F6U2v32XoZVTaSM"
    @client = GooglePlaces::Client.new(api_key) 
    #@client = @client.spots(-33.8670522, 151.1957362, :types => 'restaurant')
    #@client.spots(-33.8670522, 151.1957362)
  end

  def reply_review
    @doctor = Doctor.find(params[:doctor_id])
    @rating = Rating.find(params[:rating_id])
 
  end

  def save_review
    @doctor = Doctor.find(params[:doctor_id])
    params[:ratings].each do |rating|
      begin
        @doctor.ratings.create(:comment => "#{rating}",:use_date => Time.now)
      rescue
      end
    end
  end

  def show1
    @doctor = Doctor.find(params[:doctor_id])
    authorize @doctor, :manage?
    @doctor = @location = @doctor.decorate    
  end

  def edit
    @doctor = Doctor.find(params[:id])
    authorize @doctor, :manage?
  end

  def update
    @doctor = Doctor.find(params[:id])
    authorize @doctor, :manage?
    if @doctor.update(permitted_attributes(@doctor))
      redirect_to manage_doctor_path(@doctor), notice: "Profile Updated"
    else
      render :edit
    end
  end
end
