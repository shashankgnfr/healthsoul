class Manage::LocationsController < Manage::BaseController
  layout 'design'
  def show
    @location = Location.find(params[:id])
    authorize @location, :manage?
    @location = @location.decorate
  end

  def edit
    @location = Location.find(params[:id])
    authorize @location, :manage?
  end

  def update
    @location = Location.find(params[:id])
    authorize @location, :manage?
    if @location.update(permitted_attributes(@location))
      redirect_to [:manage, @location], notice: "Updated!"
    else
      render :edit
    end
  end
end
