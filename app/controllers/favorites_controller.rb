class FavoritesController < ApplicationController
  before_action :authenticate_user!

  def create
    current_user.favorites.create(favorite_params)
    redirect_by_type
  end

  def destroy
    user_favorite = current_user.favorites.where(favorite_params)
    user_favorite.first.destroy if user_favorite.present?
    redirect_by_type
  end

  private

  def redirect_by_type
    if params[:type] == 'doctor'
      redirect_to "/doctors/#{params[:slug]}"
    else
      redirect_to "/#{params[:slug]}"
    end
  end

  def favorite_params
    {favoriteable_type: params[:type].capitalize, favoriteable_id: params[:id]}
  end

end
