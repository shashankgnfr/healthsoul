class HsdetailsController < ApplicationController
  before_action :set_hsdetail, only: [:show, :edit, :update, :destroy]

  # GET /hsdetails
  # GET /hsdetails.json
  def index
    @hsdetails = Hsdetail.all
  end

  # GET /hsdetails/1
  # GET /hsdetails/1.json
  def show
    @hsdetail = Hsdetail.find(params[:id])   
  end

  # GET /hsdetails/new
  def new
    @hsdetail = Hsdetail.new
  end

  # GET /hsdetails/1/edit
  def edit
    @hsdetail = Hsdetail.find(params[:id])   
  end

  # POST /hsdetails
  # POST /hsdetails.json
  def create
    @hsdetail = Hsdetail.new(hsdetail_params)

    respond_to do |format|
      if @hsdetail.save
        format.html { redirect_to @hsdetail, notice: 'Hsdetail was successfully created.' }
        format.json { render :show, status: :created, location: @hsdetail }
      else
        format.html { render :new }
        format.json { render json: @hsdetail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /hsdetails/1
  # PATCH/PUT /hsdetails/1.json
  def update
    respond_to do |format|
      if @hsdetail.update(hsdetail_params)
        format.html { redirect_to @hsdetail, notice: 'Hsdetail was successfully updated.' }
        format.json { render :show, status: :ok, location: @hsdetail }
      else
        format.html { render :edit }
        format.json { render json: @hsdetail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /hsdetails/1
  # DELETE /hsdetails/1.json
  def destroy
    @hsdetail.destroy
    respond_to do |format|
      format.html { redirect_to hsdetails_url, notice: 'Hsdetail was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_hsdetail
      @hsdetail = Hsdetail.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def hsdetail_params
      params.require(:hsdetail).permit(:platform, :website_url, :name, :address, :phone, :photo, :website, :speciallity, :hour, :fee, :fee, :about_me, :education, :rating, :number_of_reviews, :total_rating, :doctor_id)
    end
end
