class AppointmentsController < ApplicationController
  layout 'design'
  before_action :find_provider
  def index
    set_meta_tags noindex: true
    @appointments = current_user.present? ? Appointment.where(user_id: current_user.id) : []
    head :ok, content_type: "text/html"
  end

  def create
    set_meta_tags noindex: true
    @appointment = Appointment.new(permitted_attributes(Appointment.new).except("user"))
    @appointment.provider = @provider
    if current_user.present?
      # If a user is logged in, make this appointment for the current user.
      @appointment.user_id = current_user.id
      # First try to update the user associated with the appointment, and make sure that user has all of the required fields.
      if @appointment.user.update_attributes(permitted_attributes(Appointment.new)["user"]) and @appointment.user.can_make_appointment
        if @appointment.save
          begin
            @appointment.notify_office_of_request
          rescue  => exception
            Rails.logger.error "Exception found in notify_office_of_request \nException: #{exception}"
            flash[:error] = "We're sorry, but we were unable to contact the office to request your appointment. The contact details provided by the office may be incomplete."
          end
          redirect_to [@provider, @appointment]
        end
      else
        flash[:error] = "All fields marked with an asterisk are required to make an appointment."
        redirect_to @provider
      end
    else
      # If the current user isn't logged in and has bypassed the client side redirect that should have occured when trying to open the modal,
      # prevent the reference to current user and redirect the user away to prevent a 500.
      flash[:error] = "Please log in or sign up to make an appointment."
      redirect_to @provider
    end
  end

  def destroy
    set_meta_tags noindex: true
    @appointment = Appointment.find(params[:appointment_id])
    @appointment.destroy
    redirect_back fallback_location: appointments_path
  end

  def update
    set_meta_tags noindex: true
    appointment_attributes = permitted_attributes(Appointment.new)
    @user = User.find(current_user.id)
    # Try to update the user first. If this fails abandon the appointmnet update and notify the user.
    if @user.update_attributes(appointment_attributes["user"]) and @user.can_make_appointment
      # Then update the appointment itself. If this fails notify the user.
      if Appointment.find(params[:id]).update_attributes(appointment_attributes.except("user"))
        flash[:notice] = "Thank you! We've updated your appointment."
      else
        flash[:error] = "We're sorry, but we were unable to update your appointment. Please try again or contact the office directly"
      end
    else
      flash[:error]= "All fields marked with an asterisk are required to make an appointment."
    end
    redirect_back fallback_location: appointments_path
  end


  def show
    set_meta_tags noindex: true
    @appointment = Appointment.find(params[:id])
  end

  private

  def find_provider
    @provider = Doctor.find(params[:doctor_id])
  end

end
