class HomeController < ApplicationController
  protect_from_forgery except: ['agree_to_gdpr']

	before_action :populate_index

	layout 'home'

	# By default, the index will contain a doctor search bar, so we populate global variables required to fill out that partial
	def index
		if request.url != root_url && !request.referrer.nil?
			redirect_to request.referrer
		else
			@search = Search::Doctor.new
		  @search_class = "doctors"
		  set_nearby
		  @search.default_country = @country
		  @search.order = "claimed"
			render "index"
		end
	end

	def india
		redirect_to root_path(set_locale: "india")
	end

	# Populate variables required to render searches/main_fields.html.slim (Hospital search fields)
	def prep_hospitals
		@search = Search::Hospital.new
		@search_class = "hospitals"
		set_nearby
		@search.default_country = @country
		@search.order = "reviews_a"
		render "index"
	end

	# Populate variables required to render health_insurance/main_fields.html.slim (Health Insurance search fields)
	def prep_health_insurance
		@search_class = "health-ins"
		@search = Search::HealthInsurance.new
		@search.co = current_country
		@search.order = "reviews_a"
		@search.default_country = @country
		render "index"
	end

	def agree_to_gdpr
		cookies.permanent[:agreed_to_gdpr] = true
		respond_to do |format|
			format.js { render plain: "OK" }
		end
	end

	private
	# populate_index runs before all actions, so any variable that needs a value regardless of what search bar is rendered
	# can be defined here.
	def populate_index
		set_meta_tags title: t("meta_title.home")
		set_meta_tags description: t("meta_description.home")
		geo_location = current_location
		@country = geo_location['country']
		@heading = "Find The Best Healthcare"
		@faq = Faq.new(country: @country.try(:downcase))
		@recently_reviewed_doctors = Doctor.recently_reviewed_for_country(@country, 3).decorate
		@recently_reviewed_hospitals = Location.by_type("Hospital").recently_reviewed_for_country(@country, 3).decorate
		@recently_reviewed_insurance = Location.by_type([ "Health Insurance"]).recently_reviewed_for_country(@country, 3).decorate
		@recently_reviewed_travel = Location.by_type([ "Travel Health Insurance"]).recently_reviewed_for_country(@country, 3).decorate
		@recent_blogs = Blog.order(published_at: :desc).limit(6).decorate
	end

	# Doctor and Hosptial search both use this function to add location data to the search.
	def set_nearby
		unless params[:clear].present?
			if current_location_selected?
				current_loc = session[:current_location]
				@search.near = current_loc['address']
				@search.lat = current_loc['latitude']
				@search.lng = current_loc['longitude']
			end
			@search.co = @country
		end
	end

end
