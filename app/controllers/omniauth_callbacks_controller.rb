class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def self.provides_callback_for(provider)
    class_eval %Q{
      def #{provider}
        auth = env["omniauth.auth"]
        @user = User.find_for_oauth(auth, current_user)

        if @user.persisted?
          sign_in_and_redirect @user, event: :authentication
          set_flash_message(:notice, :success, kind: "#{provider}".capitalize) if is_navigational_format?
        else
          session[:customer_email] = auth.info.email
          session[:identity_id] = @user.current_identity.id
          redirect_to new_signup_path, notice: "Successfully Authenticated, please create your account."
        end
      end
    }
  end

  User.omniauth_providers.each do |provider|
    provides_callback_for provider
  end

  def after_sign_in_path_for(resource)
    if resource.email.present?
      super resource
    else
      finish_signup_path(resource)
    end
  end
end
