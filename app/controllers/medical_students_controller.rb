class MedicalStudentsController < ApplicationController
  layout 'design'

  add_breadcrumb "Trainees", :medical_students

  def index
    set_meta_tags title: t("meta_title.medical_student.index")
    set_meta_tags description: t("meta_title.medical_student.index")
    @search = Search::MedicalStudent.new(search_params)
    @professional_schools = MedicalStudent.pluck(:professional_school).uniq.sort
    if params[:commit] == 'Search'
      if !search_params[:name].present? && !search_params[:professional_school].present?
        flash[:error] = 'Kindly provide filter option.'
        @medical_students = MedicalStudent.order(name: :asc).page(params[:page])
      else
        results = @search.results
        if results.present?
          @medical_students = results.page(params[:page])
        else
          flash[:notice] = 'No requested student found.'
          @medical_students = MedicalStudent.order(name: :asc).page(params[:page])
        end
      end
    else
      @medical_students = MedicalStudent.order(name: :asc).page(params[:page])
    end
  end

  def new
    set_meta_tags title: t("meta_title.medical_student.create")
    set_meta_tags description: t("meta_title.medical_student.create")
    @medical_student = MedicalStudent.new()
  end

  def create
    set_meta_tags title: t("meta_title.medical_student.create")
    set_meta_tags description: t("meta_title.medical_student.create")
    @medical_student = MedicalStudent.new(medical_student_params)
    if @medical_student.save
      flash[:notice] = 'Medical student added successfully.'
      redirect_to action: :index
    else
      render action: :new
    end
  end  

  def show
    @medical_student = MedicalStudent.find(params[:id])
    if @medical_student
      set_meta_tags title: t("meta_title.medical_student.show", name: @medical_student.name)
      set_meta_tags description: "#{@medical_student.name} | #{@medical_student.degree_type}"
      add_breadcrumb @medical_student.name
    else
      flash[:notice] = 'No such Medical student exist.'
      redirect_to action: :index
    end
  end
  
  private

  def search_params
    params.require(:search_medical_student).permit! if params[:search_medical_student].present?
  end

  def medical_student_params
    params.require(:medical_student).permit!
  end
end