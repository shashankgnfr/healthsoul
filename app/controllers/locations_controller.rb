class LocationsController < ApplicationController
  skip_before_action :verify_authenticity_token, only: [:update]
  layout 'design'
  
  def show
    @location = Location.includes(:department_ratings, :location_ratings, :reviews).find(params[:id])
    if @location.present?
      if request.xhr?
        register_impressions_per_day
        render file: "locations/show.js.erb" 
      else
        if @location.hospital?
          if !request.url.include? '/best-hospital-ratings/'
            redirect_to helpers.location_path(@location) and return
          elsif !request.url.include? '/reviews'
            redirect_to helpers.location_path(@location) and return
          else
            generate_hospital_breadcrumb
          end
        elsif @location.practice?
          if !request.url.include? '/practices/'
            redirect_to helpers.location_path(@location) and return
          else
            generate_practice_breadcrumb
          end
        elsif @location.insurance_company?
          if !request.url.include? '/health-insurance/'
            redirect_to helpers.location_path(@location) and return
          else
            generate_location_type_breadcrumb
          end
        elsif @location.travel_insurance?
          if !request.url.include? '/travel-health-insurance/'
            redirect_to helpers.location_path(@location) and return
          else
            generate_location_type_breadcrumb
          end
        else
          generate_location_type_breadcrumb
        end
        add_breadcrumb @location.name
        if @location.suppressed?
          if @location.canonical_id.present?
            location = Location.find(@location.canonical_id)
            redirect_to helpers.location_path(location) and return
          else
            raise ActionController::RoutingError.new('Not Found')
          end
        end
        @blogs = Blog.order(published_at: :desc).limit(4).decorate
        @ratings = @location.ratings.order(created_at: :desc)
        impressionist(@location, "Detail")
        @location = @location.decorate
        loc_type = @location.location_type.try(:name)
        @award = Award.where(location_id: @location.id, location_type: loc_type, year: Date.today.year).first
        render template: 'locations/show.html.erb'
      end
    else
      raise ActionController::RoutingError.new('Not Found')
    end
  end
  alias_method :practice, :show
  alias_method :health_insurance, :show
  alias_method :travel_health_insurance, :show

  def update
    @location = Location.find_by(id: params[:id])
    @location.place_photo = params[:place_photo]
    @location.placeid = params[:placeid]
    @location.importing = true
    @location.save
    render json: {}
  end

  private

  def generate_hospital_breadcrumb
    add_breadcrumb "Best hospital ratings", search_best_hospitals_path(@location.try(:country_slug) || 'NA')
    if @location.country
      add_breadcrumb formatted_country(@location.country), search_best_hospitals_path(@location.try(:country_slug))
      if @location.state
        add_breadcrumb @location.state, search_best_hospitals_path(@location.try(:country_slug) , @location.try(:state_slug))
        if @location.city
          add_breadcrumb @location.city,search_best_hospitals_path(@location.try(:country_slug), @location.try(:state_slug), @location.try(:city_slug))
        end
      else
        add_breadcrumb 'NA'
        add_breadcrumb @location.city if @location.city
      end
    else
      add_breadcrumb 'NA'
      add_breadcrumb @location.state if @location.state
      add_breadcrumb @location.city if @location.city
    end
  end

  def generate_location_type_breadcrumb
    begin
      if @location.insurance_company? or @location.travel_insurance?
        add_breadcrumb @location.location_type.name, eval("search_#{@location.location_type.name.parameterize.underscore.pluralize}_by_country_path('#{@location.country_slug}')")
        add_breadcrumb formatted_country(@location.country), eval("search_#{@location.location_type.name.parameterize.underscore.pluralize}_by_country_path('#{@location.country_slug}')")
      else
        add_breadcrumb @location.location_type.name, eval("search_#{@location.location_type.name.parameterize.underscore.pluralize}_path")
      end
    rescue => e
      add_breadcrumb @location.location_type.name
    end
  end

  def generate_practice_breadcrumb
    add_breadcrumb "Best Practices", search_best_practices_path(@location.try(:top_speciality), @location.try(:country_slug) || 'NA')
    add_breadcrumb @location.try(:top_speciality).try(:titleize), search_best_practices_path(@location.try(:top_speciality), @location.try(:country_slug) || 'NA')
    if @location.country
      add_breadcrumb formatted_country(@location.country), search_best_practices_path(@location.try(:top_speciality), @location.try(:country_slug))
      if @location.state 
        add_breadcrumb @location.state, search_best_practices_path(@location.try(:top_speciality), @location.try(:country_slug), @location.try(:state_slug))
        if @location.city
          add_breadcrumb @location.city, search_best_practices_path(@location.try(:top_speciality), @location.try(:country_slug), @location.try(:state_slug), @location.try(:city_slug))
        end
      else
        add_breadcrumb 'NA'
        add_breadcrumb @location.city if @location.city
      end
    else
      add_breadcrumb 'NA'
      add_breadcrumb @location.state if @location.state
      add_breadcrumb @location.city if @location.city
    end
  end

  def register_impressions_per_day
    imp = Impression.find_or_initialize_by(
      impressionable_type: 'Location', 
      impressionable_id: @location.id,
      message: 'Impression',
      controller_name: 'locations',
      action_name: 'show', 
      created_at: Time.now.beginning_of_day
      )
    imp.counter_per_day += 1
    imp.save
  end
end
