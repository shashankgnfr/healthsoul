class ContactsController < ApplicationController
  layout 'design'
  def new
    add_breadcrumb "Contacts"
    @contact = Contact.new
  end

  def create
    add_breadcrumb "Contacts", :contacts
    @contact = Contact.new(contact_params)
    if @contact.valid?
      ContactMailer.submit(@contact).deliver_now
      redirect_to new_contact_path, notice: "Thank you. Message delivered."
    else
      render :new
    end
  end

  private

  def contact_params
    params.require(:contact).permit!
  end
end
