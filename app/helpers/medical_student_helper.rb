module MedicalStudentHelper

  def back_to_search_medical_student_button
    link_to medical_students_path,  class: 'back-to-search-button' do
      ("<i class='glyphicon glyphicon-arrow-left'></i> " +
       t(:back_to_search)).html_safe
    end
  end

end