module CountryHelper
  def country_collection
    Country.order("CASE WHEN name = 'USA' THEN 0 WHEN name = 'India' THEN 1 END, name asc").pluck(:name)
  end

  def country_alpha3_collection
    Country.order("CASE WHEN name = 'USA' THEN 0 WHEN name = 'India' THEN 1 END, name asc").map{|c| ["#{c.name} - #{c.alpha3}", c.alpha3]}
  end
end