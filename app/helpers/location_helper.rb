module LocationHelper

  def location_path(loc, options={})
    location_url(loc, options.merge(:only_path => true))
  end

  def location_url(loc, options={})
    if loc.hospital?
      url_for(options.merge(controller: '/locations', action: :show,
        country: loc.country_slug.presence || 'NA', 
        state: loc.state_slug.presence || 'NA', 
        city: loc.city_slug.presence || 'NA', 
        id: loc.slug  || loc.id))
    elsif ['Location', 'LocationDecorator'].include?(loc.class.to_s) && loc.location_type.name == 'Practice'
      url_for(options.merge(controller: '/locations', action: :practice,
        top_speciality: loc.top_speciality.presence || 'NA' , 
        country: loc.country_slug.presence || 'NA', 
        state: loc.state_slug.presence || 'NA', 
        city: loc.city_slug.presence || 'NA', 
        id: loc.slug || loc.id))
    elsif loc.insurance_company? 
      url_for(options.merge(controller: '/locations', action: :health_insurance,
        country: loc.country_slug.presence || 'NA', 
        id: loc.slug || loc.id))
    elsif loc.travel_insurance?
      url_for(options.merge(controller: '/locations', action: :travel_health_insurance,
        country: loc.country_slug.presence || 'NA', 
        id: loc.slug || loc.id))
    else
      url_for(options.merge(controller: '/locations', action: :show, id: loc.slug || loc.id))
    end
  end

  def website_link(link)
    return "" if link.blank?
    link_to link, sanitize_link(link), target: "_blank"
  end

  def stars(rating)
    content_tag(:span, nil, data: {stars: rating})
  end

  def number(num)
    "%g" % num
  end

  def sanitize_link(link)
    if link.to_s.include?("http")
      link
    else
      "//#{link}"
    end
  end

  def location_image(location)
    if location.image.present?
      image_location(location)
    else
      no_image_location(location)
    end
  end

  def image_location(location)
    alt = location.photo_alt
    content_tag(:div, nil, class: :circle_image, style: "background-image: url(#{location.image})", title: alt)
  end

  def no_image_location(location)
    alt = location.photo_alt
    content_tag(:div, location.initials, class: :circle_image, title: alt)
  end

  def recommend_tag(bool)
    case bool
    when "Yes"
      content_tag(:span, "Yes", class: "label success")
    when "Maybe"
      content_tag(:span, "Maybe", class: "label secondary")
    else
      content_tag(:span, "No", class: "label warning")
    end
  end

  def back_to_search_button(location)
    if location.doctor?
      path = search_doctors_path
    elsif location.insurance_company?
      path = search_health_insurances_by_country_path
    elsif location.travel_insurance?
      path = search_travel_health_insurances_by_country_path
    elsif location.practice?
      path = search_practices_path
    else
      path = search_hospitals_path
    end
    link_to path,  class: 'back-to-search-button' do
      ("<i class='glyphicon glyphicon-arrow-left'></i> " +
       t(:back_to_search)).html_safe
    end
  end

  def favorites_button(user, location)
    icon_class = 'glyphicon-heart-empty'
    location_type = location.doctor? ? 'doctor' : 'location'
    favorite_text = "Add to Favorites"
    path_for_favorite = "/#{location_type}s/#{location.id}/save_favorite?type=#{location_type}&slug=#{location.slug}"
    if location.favorites.where(user_id: user.id).present?
      icon_class = 'glyphicon-heart'
      path_for_favorite = "/#{location_type}s/#{location.id}/remove_favorite?type=#{location_type}&slug=#{location.slug}"
      favorite_text = "In Favorites"
    end
    link_to path_for_favorite, method: :post do
      ("<h4><i class='glyphicon #{icon_class} favorite-link'></i> #{favorite_text}</h4>").html_safe
    end
  end

  def rating_url(location)
    location.practice? ? new_location_practice_review_url(location) : url_for([:new, location, :rating])
  end

end
