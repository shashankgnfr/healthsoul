module AdvertisementHelper

  def timed_clicks(advert, start_date)
    if start_date.present?
      end_date = Date.today
      advert.impressions.where(message: 'Click').where([" DATE(created_at) between ? and ?", start_date.to_date, end_date.to_date]).count
    else
      advert.impressions.where(message: 'Click').count
    end
  end
end