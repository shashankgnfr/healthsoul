module ApplicationHelper

  def set_default_meta_tags
    display_meta_tags keywords: 'Health care provider, Physician Directory, Healthcare Provider Directory, Medicare Provider Directory, United Healthcare Directory , Health care physicians near me, Doctor Lookup',
      geo: {
        region: 'US-IL',
        placename: 'Springfield',
        position: '39.78;-89.65'
      },
      ICBM: '39.7, -89.65',
      og: {
        title: :title,
        site_name: :site,
        description: :description,
        image: asset_path('search-header.jpg'),
        type: 'website',
        url: request.original_url
      }
      
  end

  def hide_flash_message?
    login_type_controller && login_type_action
  end

  def login_type_controller
    params["controller"]=="sessions" || params["controller"]=="users" || params["controller"]=="registrations"
  end

  def login_type_action
    params["action"]=="new" || params["action"]=="create"
  end

  def referral_link(user)
    link_to nil, referral_url(user)
  end

  def referral_url(user)
    new_user_registration_url + "?ref=#{user.id}"
  end

  def time_for_moment(time)
    content_tag(:span, time, class: 'moment-time') if time.present?
  end

  def bootstrap_class_for(flash_type)
    case flash_type
    when "success"
      "alert-success"   # Green
    when "error"
      "alert-danger"    # Red
    when "alert"
      "alert-danger"   # Yellow
    when "notice"
      "alert-info"      # Blue
    else
      flash_type.to_s
    end
  end

  def timed_impressions(obj, start_date)
    if start_date.present?
      end_date = Date.today
      obj.impressions.where(message: 'Impression').where([" DATE(created_at) between ? and ?", start_date.to_date, end_date.to_date]).sum(:counter_per_day)
    else
      obj.impressions.where(message: 'Impression').sum(:counter_per_day)
    end
  end

  def get_state(states, country)
    if states.class == Array
      if ['USA', 'US', 'United states'].include? country.try(:upcase)
        states.last
      else
        states.first
      end
    else
      states
    end
  end
end
