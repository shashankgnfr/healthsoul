module RoleHelper
  def role_name(role)
    if role.resource.present?
      "#{role.name}, #{role.resource.name}"
    else
      "#{role.name}"
    end
  end
end
