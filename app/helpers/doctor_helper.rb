module DoctorHelper

  def doctor_path(doc, options={})
    doctor_url(doc, options.merge(:only_path => true))
  end

  def doctor_url(doc, options={})
    url_for(options.merge(controller: '/doctors', action: :show,
      top_speciality: doc.top_speciality.presence || 'NA' , 
      country: doc.country_slug.presence || 'NA', 
      state: doc.state_slug.presence || 'NA', 
      city: doc.city_slug.presence || 'NA', 
      id: doc.slug || doc.id))
  end
end