module HomeHelper

  def formatted_statistic(count)
    count.to_f > 100000 ? number_to_human(count, precision: 2) : count
  end
end