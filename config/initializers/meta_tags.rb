MetaTags.configure do |c|
  c.title_limit        = 140
  c.description_limit  = 320
  c.keywords_limit     = 255
  c.keywords_separator = ', '
end
