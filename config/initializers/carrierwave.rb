case Rails.env
when 'production', 'staging'
  CarrierWave.configure do |config|
    config.fog_provider = 'fog/aws'                        # required
    config.fog_credentials = {
      provider:              'AWS',                        # required
      aws_access_key_id:     ENV['AWS_ACCESS'],                        # required
      aws_secret_access_key: ENV['AWS_SECRET'],                        # required
      #region:                'eu-west-1',                  # optional, defaults to 'us-east-1'
      #host:                  's3.example.com',             # optional, defaults to nil
      #endpoint:              'https://s3.example.com:8080' # optional, defaults to nil
    }
    config.fog_directory  = ENV['AWS_BUCKET']                          # required
    config.fog_public     = true                                        # optional, defaults to true
    # config.fog_attributes = { cache_control: "public, max-age=#{365.day.to_i}" } # optional, defaults to {}
    config.storage = :fog
    # config.fog_authenticated_url_expiration = 5.minutes
  end
when 'development', 'test'
  CarrierWave.configure do |config|
    config.storage = :file
    config.enable_processing = true
    config.remove_previously_stored_files_after_update = false
  end
end
