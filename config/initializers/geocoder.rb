case Rails.env
  when 'production'
    Geocoder.configure(
      # Geocoding options
      timeout: 20,                 # geocoding service timeout (secs)

      lookup: :google,
      google: {            # name of geocoding service (symbol)
        api_key: ENV['GOOGLE_API']               # API key for geocoding service
      },
      use_https: true,           # use HTTPS for lookup requests? (if supported)
      #ip_lookup: :freegeoip
      ip_lookup: :ipinfo_io,

      ipinfo_io: {
        api_key: ENV['IPINFO_API']
      },

      #lookup: :location_iq,            # name of geocoding service (symbol)
      #api_key: 'f189dee5960c847f0f1a',               # API key for geocoding service

      # language: :en,              # ISO-639 language code
      # http_proxy: nil,            # HTTP proxy server (user:pass@host:port)
      # https_proxy: nil,           # HTTPS proxy server (user:pass@host:port)
      # cache: nil,                 # cache object (must respond to #[], #[]=, and #keys)
      # cache_prefix: 'geocoder:',  # prefix (string) to use for all cache keys

      # Exceptions that should not be rescued by default
      # (if you want to implement custom error handling);
      # supports SocketError and Timeout::Error
      # always_raise: [],

      # Calculation options
      # units: :mi,                 # :km for kilometers or :mi for miles
      # distances: :linear          # :spherical or :linear
    )
  else 
    Geocoder.configure(
      # Geocoding options
      timeout: 20,
      lookup: :google,
      google: {            # name of geocoding service (symbol)
        api_key: ENV['GOOGLE_API']               # API key for geocoding service
      },
      use_https: true,           # use HTTPS for lookup requests? (if supported)
      ip_lookup: :ipinfo_io     
    )
end
