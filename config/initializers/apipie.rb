Apipie.configure do |config|
  config.app_name = "Healthsoul"
  config.api_base_url = "/powerlistings"
  config.app_info = "All requests must include a valid token."
  config.doc_base_url = "/docs"
  config.translate = false
  config.default_locale = nil
  # where is your API defined?
  config.api_controllers_matcher = "#{Rails.root}/app/controllers/**/*.rb"
end
