ActsAsTaggableOn::Tag.class_eval do
  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]
  mount_uploader :featured_image, BlogUploader
  normalize_attributes :title, :name, :meta_title, :description
end
