# frozen_string_literal: true

if Rails.env.development?
  require "rack-mini-profiler"

  # initialization is skipped so trigger it
  Rack::MiniProfilerRails.initialize!(Rails.application)
  Rack::MiniProfiler.config.authorization_mode = :whitelist

  Rack::MiniProfiler.config.disable_caching = false
  # Rails.application.middleware.delete(Rack::MiniProfiler)
  # Rails.application.middleware.insert_after(Rack::Deflater, Rack::MiniProfiler)
end
