# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path
Rails.application.config.assets.paths += Dir["#{Rails.root}/vendor/assets/*"].sort_by { |dir| -dir.size }
Rails.application.config.assets.paths += Dir["#{Rails.root}/app/assets/fonts"]
# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
Rails.application.config.assets.precompile += %w( jquery.geocomplete.min.js design.css design.js froala_editor.min.js froala_editor_init.js froala_editor.css flexslider.js home.js home_page.css doctor-icon)
Rails.application.config.assets.precompile << /\.(?:svg|eot|woff|ttf)$/
