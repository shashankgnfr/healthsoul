require_relative 'boot'

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "action_cable/engine"
require "sprockets/railtie"
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Healthworld
  class Application < Rails::Application
    config.time_zone = 'Central Time (US & Canada)'

    config.active_job.queue_adapter = :sucker_punch
    ENV['YEXT_TOKEN'] = '2nyWerd35d23deq7W7kLtZ'
    config.active_record.time_zone_aware_types = [:datetime, :time]
    config.eager_load_paths << File.join(Rails.root,'app', 'view_models')
    
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
   # config.generators do |g| g.template_engine :erb 
    # -- all .rb files in that directory are automatically loaded.
  end
end
