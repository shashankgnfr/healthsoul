Rails.application.routes.draw do

  resources :hdetails
  resources :hsdetails
  apipie
  mount ActionCable.server => '/healthsoul_action_cable'

  resources :advertisements, only: [:show]
  resources :events, only: [:show, :index] do
    resources :event_members, only: [:new, :create, :index]
  end
  resources :medical_students, only: [:new, :create, :index, :show] do
    resources :claim_requests, only: [:new, :create], controller: "medical_student_claim_requests"
  end

  get "practice_specialities/:id/:country/:type" => "practice_specialities#index", as: :speciality_faqs
  get 'recommendations/:source_type' => 'recommendations#new', as: :new_recommendation
  resources :recommendations, only: [:create]
  mount LetterOpenerWeb::Engine, at: "/letter_opener" if Rails.env.development?

  # Main App
  devise_for :users, :controllers => {omniauth_callbacks: 'omniauth_callbacks', :registrations => "registrations", sessions: 'sessions', :passwords => "passwords"}
  devise_scope :user do
      post "users/login_or_sign_up_phone" => "sessions#login_or_sign_up_phone", as: :login_or_sign_up_phone
      match "verify_phone" => "sessions#verify_phone", as: :verify_phone, via: [:get, :post]
  end
  resources :users, only: [ :show, :edit, :update] do
    member do
      get 'unsubscribe'
    end
  end

  #Yext
  namespace :powerlistings do
    get 'categories' => 'categories#index', defaults: {format: 'json'}
    get 'search' => 'listings#index', defaults: {format: 'json'}
    get 'details' => 'listings#show', defaults: {format: 'json'}
    post 'order' => 'listings#upsert', defaults: {format: 'json'}
    put '/:id' => 'listings#update', defaults: {format: 'json'}
    delete '/:id' => 'listings#cancel', defaults: {format: 'json'}
    post 'suppress/:id' => 'listings#suppress', defaults: {format: 'json'}
    get 'reviews' => 'listings#reviews', defaults: {format: 'json'}
  end
  resources :patient_stories do
    member do
      get :download
    end
  end

  get 'sitemap', to: 'sitemaps#index', defaults: {format: 'html'}

  post "interested_provider_contact" => "users#provider_email_request", as: :interested_provider_contact
  post "create_subscription_only_user" => "users#create_subscription_only"
  get "create_claim_request", to: "claim_requests#create", as: :create_claim_request
  get "create_doctor_claim_request", to: "doctor_claim_requests#create", as: :create_doctor_claim_request
  get 'contributors' => 'static_pages#editorial_board', as: :editorial_board
  get "privacy-policy" => "static_pages#privacy_policy"
  get "terms" => "static_pages#terms"
  get "copyright" => "static_pages#copyright"
  get "faqs" => "static_pages#faqs"
  get "press" => "static_pages#press"
  get "careers" => "static_pages#careers"
  get "about" => "static_pages#about"
  get "claims/(:id)" => "static_pages#manage", as: :manage
  get "users/(:id)" => "static_pages#users_info", as: :users_info
  get '/home/(:set_locale)' => "home#index"
  get "india" => "home#india"
  get "calculators" => "static_pages#calculators"
  get "bmi_calculator" => "static_pages#bmi_calculator"
  get "waist_hip_calculator" => "static_pages#waist_hip_calculator"
  get "bmr_calculator" => "static_pages#bmr_calculator"
  get "calorie_calculator" => "static_pages#calorie_calculator"
  post "email_my_calculator_results" => "users#mail_calculator_results"

  # Health Guides
  get "health-guide" => "health_guides#index"
  get "health-guides" => "static_pages#health_guide"
  scope 'health-guide', as: :health_guide do
    get ":co" => "health_guides#show", as: :co
  end

  # Faq / Forum
  get "/forum" => "faqs#index"
  resources :faqs, only: [ :create, :update ] do
    resources :faq_answers, only: :create
  end

  get "/blog/tag/:tag" => "blogs#index", as: :blogs_tag
  resources :blogs, path: "blog", only: [ :index, :show] do
    member do
      patch 'share_blog/:social_platform', action: 'share_blog', defaults: {format: 'json'}
    end
  end
  resources :contacts, only: [:new, :create]
  resources :advertising_requests, only: [:new, :create]

  get "locations/compare" => "compare#index", as: :search_hospital_compare
  get "locations/compare" => "compare#index", as: :search_health_insurance_compare
  get "locations/compare" => "compare#index", as: :search_travel_health_insurance_compare
  get "locations/compare" => "compare#index", as: :search_healthcare_facilities_compare
  get "doctors/compare" => "doctor_compare#index", as: :search_doctor_compare

  # Search Prep
  get "home-prep-hospitals" => "home#prep_hospitals", as: :prep_hospital_search
  get "home-prep-health-insurance" => "home#prep_health_insurance", as: :prep_health_insurance_search

  post "agree_to_gdpr" => "home#agree_to_gdpr"
  # Search
  get "hospitals" => "searches#show", as: :search_hospitals
  get "doctors" => "doctor_search#show", as: :search_doctors
  get "typeahead" => "doctor_search#typeahead_search", as: :search_typeahead

  get "practices" => "practice#show", as: :search_practices
  get "health-insurance/(:set_locale)" => "health_insurance#show", as: :search_health_insurances
  get "travel-health-insurance/(:set_locale)" => "travel_health_insurance#show", as: :search_travel_health_insurances
  get "healthcare-facilities/(:set_locale)" => "healthcare_facilities#show", as: :search_healthcare_facilities

  get "best-hospital-ratings/:set_locale/(:state)/(:city)" => "searches#best_hospitals", as: :search_best_hospitals
  get "best_hospitals/:set_locale/(:state)/(:city)" => "searches#best_hospitals"
  get '/best-hospital-ratings/:country/:state/:city/:id/reviews', controller: :locations, action: :show
  get '/best-hospital-ratings/:country/:state/:city/:id', controller: :locations, action: :show
  get '/practices/:top_speciality/:country/:state/:city/:id', controller: :locations, action: :practice

  get '/health-insurance/:country/:id', controller: :locations, action: :health_insurance
  get '/travel-health-insurance/:country/:id', controller: :locations, action: :travel_health_insurance

  get "best_doctors/:speciality/:set_locale/(:state)/(:city)" => "doctor_search#best_doctors"
  get "practices/:speciality/:set_locale/(:state)/(:city)" => "doctor_search#best_doctors", as: :search_best_practices

  get "best-doctor-ratings/:speciality/:set_locale/(:state)/(:city)" => "doctor_search#best_doctors", as: :search_best_doctors
  get '/best-doctor-ratings/:top_speciality/:country/:state/:city/:id', controller: :doctors, action: :show

  post "/blog/image-upload" => 'admin/blogs#image_upload'

  resources :ratings, only: [] do
    resources :comments
  end

  resources :doctor_reviews, only: [] do
    resources :comments
  end
  
  resources :practice_reviews, only: [] do
    resources :comments
  end

  resources :questions, :only => [] do
    resources :answers, only: [:new, :create]
  end

#trailing out health rating
  resources :hs


     get "patient-experience-awards" => "awards#index", as: :awards
     match 'excellent-patient-experience-awards-doctors' => 'awards#gold_doctors', as: :gold_doctors_awards, via: [:get, :post]
     match 'excellent-patient-experience-awards-hospitals' => 'awards#gold_hospitals', as: :gold_hospitals_awards, via: [:get, :post]
     match 'exceptional-patient-experience-awards-doctors' => 'awards#diamond_doctors', as: :diamond_doctors_awards, via: [:get, :post]
     match 'exceptional-patient-experience-awards-hospitals' => 'awards#diamond_hospitals', as: :diamond_hospitals_awards, via: [:get, :post]

  # API
  namespace :api do
    get "votes/:voteable_type/:voteable_id/:status" => "votes#create"
    resources :locations, only: :index
    # resources :doctor_organizations, only: :index
    resources :doctors, only: :index
    resources :reset_passwords, only: [:index, :create]
    resources :register_user, defaults: {format: 'json'} do
      collection do
       post 'create_user'
       post 'login'
       delete 'logout'
       get "/doctor_show" => "/api/doctors#doctor_show"
       get "/hospital_show" => "/api/doctors#hospital_show"
       get "/doctor_review" => "/api/doctors#doctor_review"
       get "/hospital_review" => "/api/doctors#hospital_review"
       post "/update_profile" => "/api/register_user#update_profile"
       get "/healthinsurance_show" => "/api/doctors#healthinsurance_show"
       get "/healthinsurance_review" => "/api/doctors#healthinsurance_review"
       post "/create_review" => "/api/doctors#create_review"
       post "/create_hospital_review" => "/api/doctors#create_hospital_review"
       post "/create_insurance_review" => "/api/doctors#create_insurance_review"
       get "/mailchimp" => "/api/mailchimp#mailchimp"
       post "/create_appointment" => "/api/appointment#create_appointment"
       get "/appointment_show" => "/api/appointment#appointment_show"
       get "/user_point_review" => "/api/register_user#user_point_review"
       get "/search_doctor" => "/api/search#search_doctor"
       get "/search_hospital" => "/api/locations#search_hospital"
       get "/search_health_insurance" => "/api/health_insurance#search_health_insurance"
      end
    end
  end

  # manager
  namespace :manage do
    resources :locations
    resources :doctors do
      get "show1"
      get "reply_review/:rating_id" => "doctors#reply_review"
      post "save_review"
    end
  end

  # Admin
  namespace :admin do
    root to: "dashboard#index"
    resources :advertisements do
      member do
        match 'resource_mapping' => "advertisements#resource_mapping", via: [:get, :patch]
      end
    end
    resources :board_certifications
    resources :doctor_hospitals do
      collection do
        post "import"
      end
    end
    resources :languages
    resources :medical_students
    resources :events
    resources :event_members
    resources :editorial_members
    resources :appointments
    resources :location_types
    resources :locations do
      member do
        post 'analytics_report' => 'locations#analytics_report'
      end
    end
    resources :conditions
    resources :doctors do
      member do
        post 'analytics_report' => 'doctors#analytics_report'
      end
    end
    resources :users
    resources :claim_requests
    resources :questions
    resources :ratings
    resources :doctor_reviews
    resources :practice_reviews
    resources :speciality_merges
    resources :doctor_reports, only: :index
    get "practices" => "doctors#practices"
    get "doctor_reports/doctor_uploads" => "doctor_reports#doctor_uploads"
    resources :practice_specialities do 
      member do
        get 'faqs' => 'practice_specialities#faqs'
      end
    end
    resources :practice_specialities_faqs
    resources :procedures
    resources :blogs
    resources :tags
    namespace :import do
      resources :locations, only: [:new, :create]
      resources :doctors, only: [:new, :create]
    end
    resources :promotions do
      resources :promotion_winners
    end
  end

  resources :doctors, only: :show do
    resources :questions, only: [:new, :create]
    resources :ratings, only: [:new], controller: "doctor_reviews"
    resources :doctor_reviews, only: [:create, :index]
    resources :claim_requests, only: [:new, :create], controller: "doctor_claim_requests"
    resources :appointments, provider_type: 'doctor'
    member do
      post 'save_favorite' => "favorites#create"
      post 'remove_favorite' => "favorites#destroy"
    end
  end

  resources :appointments

  resources :locations, only: [:show], path: "" do
    resources :ratings, only: [:new, :create, :index]
    resources :practice_reviews, only: [:new, :create, :index]
    resources :questions, only: [:new, :create]
    resources :claim_requests, only: [:new, :create]
  end

  resources :claim_requests, only: [:new, :create]

  resources :locations, only: :update do
    member do
      post 'save_favorite' => "favorites#create"
      post 'remove_favorite' => "favorites#destroy"
    end
  end

  root to: "home#index"
end
