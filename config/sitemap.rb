# RAILS_ENV=production rake sitemap:create
SitemapGenerator::Sitemap.compress = :all_but_first
SitemapGenerator::Sitemap.default_host = "https://www.healthsoul.com"
SitemapGenerator::Sitemap.sitemaps_host = "https://www.healthsoul.com"
SitemapGenerator::Interpreter.send :include, LocationHelper
SitemapGenerator::Interpreter.send :include, DoctorHelper

SitemapGenerator::Sitemap.create do
  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #
  # Add '/articles'
  #
  #   add articles_path, :priority => 0.7, :changefreq => 'daily'
  #
  # Add all articles:
  #
  #   Article.find_each do |article|
  #     add article_path(article), :lastmod => article.updated_at
  #   end

  # Static Routes
  add about_path
  add privacy_policy_path
  add terms_path
  add copyright_path
  add faqs_path
  add careers_path
  add press_path
  add manage_path
  add health_guides_path
  add calculators_path
  add bmi_calculator_path
  add waist_hip_calculator_path
  add bmr_calculator_path
  add search_hospitals_path
  add search_doctors_path
  add search_health_insurances_path('usa')
  add search_travel_health_insurances_path('usa')

  # Locations
  Location.includes(:location_type).find_each do |location|
    add location_path(location)
  end

  # Doctors
  Doctor.find_each do |doctor|
    add doctor_path(doctor)
  end

  # Blog
  Blog.find_each do |blog|
    add blog_path(blog)
  end

end
