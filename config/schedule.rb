# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

every :tuesday, :at => "11 am" do
  rake "user_digest_email:send"
end

every 1.month, :at => 'start of the month at 05:00 am' do
  rake "provider_digest_email:send"
end

every 1.year, :at => 'April 1st at 00:01am' do
  rake "awards:distribute_to_doctors"
end

every 1.year, :at => 'April 1st at 02:00am' do
  rake "awards:distribute_to_others"
end

every 3.hours do
  rake "utility:update_best_doctors_and_hospitals[#{ENV['RAILS_ENV']}]"
end
# Learn more: http://github.com/javan/whenever
