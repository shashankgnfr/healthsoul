class AddTitleToPracticeSpecialties < ActiveRecord::Migration[5.0]
  def change
    add_column :practice_specialities, :title, :string
  end
end
