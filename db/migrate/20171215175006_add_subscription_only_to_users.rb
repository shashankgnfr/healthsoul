class AddSubscriptionOnlyToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :subscription_only, :boolean, default: false
  end
end
