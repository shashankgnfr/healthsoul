class AddUserToBlogs < ActiveRecord::Migration[5.0]
  def change
    add_reference :blogs, :doctor, index: true
  end
end
