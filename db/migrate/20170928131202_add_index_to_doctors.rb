class AddIndexToDoctors < ActiveRecord::Migration[5.0]
  def change
    add_index :doctors, :name
    add_index :doctors, :credential
    add_index :doctors, :graduation
  end
end
