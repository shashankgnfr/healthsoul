class CreateUserReferrals < ActiveRecord::Migration[5.0]
  def change
    create_table :user_referrals do |t|
      t.references :user, foreign_key: true
      t.integer :referred_user_id

      t.timestamps
    end
    add_index :user_referrals, :referred_user_id
  end
end
