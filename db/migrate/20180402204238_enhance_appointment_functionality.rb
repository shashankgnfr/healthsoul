class EnhanceAppointmentFunctionality < ActiveRecord::Migration[5.0]
	def change
		add_column :appointments, :new_patient, :boolean, default: true
		add_reference :appointments, :user, index: true
		add_column :appointments, :confirmed, :boolean, default: false
		add_column :users, :date_of_birth, :string
		add_column :doctors, :organization_email, :string
	end
end
