class CreateBlogs < ActiveRecord::Migration[5.0]
  def change
    create_table :blogs do |t|
      t.string :title
      t.text :content
      t.string :featured_image
      t.date :published_at

      t.timestamps
    end
  end
end
