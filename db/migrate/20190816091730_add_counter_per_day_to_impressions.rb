class AddCounterPerDayToImpressions < ActiveRecord::Migration[5.0]
  def up
    add_column :impressions, :counter_per_day, :integer, default: 0
  end

  def down
    remove_column :impressions, :counter_per_day
  end
end
