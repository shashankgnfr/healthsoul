class CreatePromotions < ActiveRecord::Migration[5.0]
  def change
    create_table :promotions do |t|
      t.string :title
      t.string :blurb
      t.text :terms_and_conditions_content
      t.references :country, index: true
      t.boolean :promotion_for_all_countries, default: false
      t.string :promotion_image
      t.integer :priority_index, default: 0
      t.timestamps
    end
  end
end
