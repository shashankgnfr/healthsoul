class CreateAppointments < ActiveRecord::Migration[5.0]
  def change
    create_table :appointments do |t|
      t.time :time
      t.date :date
      t.string :name
      t.string :phone
      t.string :email
      t.string :reason
      t.references :provider, polymorphic: true

      t.timestamps
    end
  end
end
