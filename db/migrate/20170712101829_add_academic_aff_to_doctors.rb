class AddAcademicAffToDoctors < ActiveRecord::Migration[5.0]
  def change
    add_column :doctors, :academic_affiliation, :string
  end
end
