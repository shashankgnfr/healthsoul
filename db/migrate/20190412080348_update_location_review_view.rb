class UpdateLocationReviewView < ActiveRecord::Migration[5.0]
  def up
    execute <<-END
      DROP VIEW location_review_views;
      CREATE VIEW location_review_views AS 
        WITH avg_rating AS 
        (
          select locations.id as location_id,  
            ROUND(avg(overall_experience), 1) as overall_experience,
            ROUND(avg(doctor_care), 1) as doctor_care,
            ROUND(avg(nursing_care), 1) as nursing_care,
            ROUND(avg(cleanliness), 1) as cleanliness,
            ROUND(avg(food_services), 1) as food_services,
            ROUND(avg(cost_satisfaction), 1) as cost_satisfaction,
            ROUND(avg(claims_process), 1) as claims_process,
            ROUND(avg(customer_service), 1) as customer_service,
            count(ratings) as reviews_count
          from locations
          inner join "ratings" ON  "locations"."id" = "ratings"."location_id"
          inner join location_types on locations.location_type_id = location_types.id
          where  country_code = current_setting('healthsoul.country_code')
            and location_types.name = current_setting('healthsoul.location_type') 
          group by locations.id
        )

        select locations.id,  
          locations.name, 
          locations.address,
          locations.city,
          locations.state,
          locations.country,
          locations.zip,
          location_types.name as location_type,
          locations.phone,
          avg_rating.overall_experience,
          avg_rating.doctor_care,
          avg_rating.nursing_care,
          avg_rating.cleanliness,
          avg_rating.food_services,
          avg_rating.cost_satisfaction,
          avg_rating.claims_process,
          avg_rating.customer_service,
          avg_rating.reviews_count
        from locations
        inner join location_types on locations.location_type_id = location_types.id
        inner join avg_rating on locations.id = avg_rating.location_id
        where country_code = current_setting('healthsoul.country_code')
          and location_types.name = current_setting('healthsoul.location_type')
        order by locations.name asc
    END
  end

  def down
    execute <<-END
      DROP VIEW location_review_views;
    END
  end
end
