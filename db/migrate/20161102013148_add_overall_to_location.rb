class AddOverallToLocation < ActiveRecord::Migration[5.0]
  def change
    add_column :locations, :overall, :decimal, default: 0
  end
end
