class AddDefaultOverallToDoctors < ActiveRecord::Migration[5.0]
  def change
    change_column :doctors, :overall, :decimal, :default => 0
  end
end
