class AddIsClaimedToLocations < ActiveRecord::Migration[5.0]
  def change
    add_column :locations, :is_claimed, :boolean, default: false
    ClaimRequest.where(granted: true).each do |cr|
      cr.claimable.update_column('is_claimed', true)
    end
  end
end
