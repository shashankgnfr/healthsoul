class AddFeaturedOrSponsoredToLocation < ActiveRecord::Migration[5.0]
  def change
    add_column :locations, :featured_or_sponsored, :string, default: nil
  end
end
