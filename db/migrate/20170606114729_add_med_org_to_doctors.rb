class AddMedOrgToDoctors < ActiveRecord::Migration[5.0]
  def change
    add_reference :doctors, :doctor_organization, foreign_key: true
  end
end
