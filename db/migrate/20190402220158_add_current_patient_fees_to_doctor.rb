class AddCurrentPatientFeesToDoctor < ActiveRecord::Migration[5.0]
  def change
  	add_column :doctors, :current_patient_fees, :string
  	rename_column :doctors, :fees, :new_patient_fees
  end
end