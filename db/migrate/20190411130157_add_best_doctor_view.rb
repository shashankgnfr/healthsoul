class AddBestDoctorView < ActiveRecord::Migration[5.0]
  def up
    execute <<-END
      CREATE VIEW best_doctor_views AS 
        WITH initial_data AS (
          SELECT COUNT(*) AS dr_count_by_state, state, state_slug, 
          practice_specialities.title AS practice_specialities_title , 
          practice_specialities.name AS practice_specialities_name
          FROM "doctors" 
          INNER JOIN "doctor_specialities" 
          ON "doctor_specialities"."doctor_id" = "doctors"."id" INNER JOIN "practice_specialities" 
          ON "practice_specialities"."id" = "doctor_specialities"."practice_speciality_id" 
          WHERE "doctors"."country_slug" = current_setting('healthsoul.country_slug') 
          and practice_specialities.name in ('Dentistry', 'Chiropractic', 'Cardiology', 'Periodontics', 'Family Medicine')
          GROUP BY practice_specialities.title, practice_specialities_name, state, state_slug
        )

        SELECT practice_specialities_title, practice_specialities_name, json_agg(json_build_object(concat(state, '[',state_slug,']'), dr_count_by_state))
         from initial_data
        GROUP BY practice_specialities_title, practice_specialities_name
        order by practice_specialities_name
    END
  end

  def down
     execute <<-END
      DROP VIEW best_doctor_views;
    END
  end
end
