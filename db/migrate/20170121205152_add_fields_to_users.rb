class AddFieldsToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :city, :string
    add_column :users, :state, :string
    add_column :users, :co, :string
    add_column :users, :sex, :integer
    add_column :users, :age_group, :integer
  end
end
