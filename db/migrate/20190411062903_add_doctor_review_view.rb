class AddDoctorReviewView < ActiveRecord::Migration[5.0]
  def up
    execute <<-END
      CREATE VIEW doctor_review_views AS 
        WITH avg_rating AS 
        (
          select doctors.id as dr_id,  
            ROUND(avg(overall_experience), 1) as overall_experience,
            ROUND(avg(explaintation), 1) as explaintation,
            ROUND(avg(trust), 1) as trust,
            ROUND(avg(staff), 1) as staff,
            ROUND(avg(appt), 1) as appt,
            ROUND(avg(wait), 1) as wait,
            count(doctor_reviews) as reviews_count
          from doctors
          inner join "doctor_reviews" ON  "doctors"."id" = "doctor_reviews"."doctor_id"
          where country_code = current_setting('healthsoul.country_code')
          group by doctors.id
        ),
        avg_inpatient_rating AS 
        (
          select doctors.id as dr_id,  
            ROUND(avg(overall_experience), 1) as overall_experience
          from doctors
          inner join "doctor_reviews" ON  "doctors"."id" = "doctor_reviews"."doctor_id"
          where
            doctor_reviews.where = 1
            and overall_experience > 0
            and country_code = current_setting('healthsoul.country_code')
          group by doctors.id
        ),
        avg_outpatient_rating AS 
        (
          select doctors.id as dr_id,  
            ROUND(avg(overall_experience), 1) as overall_experience
          from doctors
          inner join "doctor_reviews" ON  "doctors"."id" = "doctor_reviews"."doctor_id"
          where
            doctor_reviews.where = 0
            and overall_experience > 0
            and country_code = current_setting('healthsoul.country_code')
          group by doctors.id
        )

        select doctors.id,  
          doctors.name, 
          doctors.address,
          doctors.city,
          doctors.state,
          doctors.country,
          doctors.zip,
          doctors.organization_name,
          doctors.specialities,
          doctors.phone,
          avg_inpatient_rating.overall_experience as inpatient,
          avg_outpatient_rating.overall_experience as outpatient,
          avg_rating.overall_experience ,
          avg_rating.explaintation,
          avg_rating.trust,
          avg_rating.staff,
          avg_rating.appt,
          avg_rating.wait,
          avg_rating.reviews_count
        from doctors
        left join avg_rating on doctors.id = avg_rating.dr_id
        left join avg_inpatient_rating on doctors.id = avg_inpatient_rating.dr_id
        left join avg_outpatient_rating on doctors.id = avg_outpatient_rating.dr_id
        where country_code = current_setting('healthsoul.country_code')
        order by doctors.name asc
    END
  end

  def down
    execute <<-END
      DROP VIEW doctor_review_views;
    END
  end
end
