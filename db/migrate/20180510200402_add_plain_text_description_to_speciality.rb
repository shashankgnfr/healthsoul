class AddPlainTextDescriptionToSpeciality < ActiveRecord::Migration[5.0]
  def change
  	add_column :practice_specialities, :user_facing_description, :string
  end
end
