class CreateDoctors < ActiveRecord::Migration[5.0]
  def change
    create_table :doctors do |t|
      t.string :name
      t.integer :gender
      t.string :credential
      t.references :med_school, foreign_key: true
      t.integer :graduation
      t.string :npi
      t.boolean :medicare

      t.timestamps
    end
  end
end
