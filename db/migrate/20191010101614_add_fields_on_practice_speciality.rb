class AddFieldsOnPracticeSpeciality < ActiveRecord::Migration[5.0]
  def change
    add_column :practice_specialities, :doctorised_h1, :text, default: DOCTOR_H1

    add_column :practice_specialities, :static_faqs, :text

    add_column :practice_specialities, :doctorised_header, :text

    add_column :practice_specialities, :doctorised_footer, :text

  end
end
