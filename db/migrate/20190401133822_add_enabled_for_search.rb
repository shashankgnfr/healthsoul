class AddEnabledForSearch < ActiveRecord::Migration[5.0]
  def change
    add_column :locations, :enabled_for_location_search, :boolean, default: false
    add_column :doctors, :enabled_for_doctor_search, :boolean, default: false

    add_column :locations, :state_slug, :string
    add_column :doctors, :state_slug, :string
    add_column :locations, :city_slug, :string
    add_column :doctors, :city_slug, :string

    # ['state', 'city'].each do |key|
    #   ['Doctor', 'Location'].each do |t|
    #     eval(t).where.not(key => nil).group(key).count.each_pair do |label, count|
    #       slug = label.parameterize
    #       label = label.gsub("'","''")
    #       ActiveRecord::Base.connection.execute("
    #         update #{t.tableize}
    #         set #{key}_slug = '#{slug}'
    #         where #{key} = '#{label}'
    #       ")
    #     end
    #   end
    # end

    location_cities = ['Chandigarh', 'Delhi', 'Mumbai', 'Jaipur', 'Ahmedabad', 'Madras', 'Springfield', 'Chicago']
    location_cities.each do |c| 
      Location.where(city: c).update_all(enabled_for_location_search: true)
    end
    doctor_cities = ['Jaipur', 'Ahmedabad', 'Mumbai', 'Springfield', 'Chicago']
    doctor_cities.each do |c|
      Doctor.where(city: c).update_all(enabled_for_doctor_search: true)
    end
  end
end
