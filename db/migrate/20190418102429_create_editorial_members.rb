class CreateEditorialMembers < ActiveRecord::Migration[5.0]
  def change
    create_table :editorial_members do |t|
      t.string :name, null: false, unique: true
      t.string :member_photo
      t.text :description, null: false
      t.boolean :publish, default: false
      t.string :designation
      t.string :credentials
      t.timestamps
    end
    add_index :editorial_members, :name
  end
end
