class AddPageContentToTag < ActiveRecord::Migration[5.0]
  def change
    add_column :tags, :page_content, :text
  end
end
