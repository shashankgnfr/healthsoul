class AddGrantedToClaimRequests < ActiveRecord::Migration[5.0]
  def change
    add_column :claim_requests, :granted, :boolean, default: false
  end
end
