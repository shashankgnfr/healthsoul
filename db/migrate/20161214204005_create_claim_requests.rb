class CreateClaimRequests < ActiveRecord::Migration[5.0]
  def change
    create_table :claim_requests do |t|
      t.references :user, foreign_key: true
      t.references :location, foreign_key: true
      t.string :position
      t.string :phone
      t.text :description

      t.timestamps
    end
    add_index :claim_requests, [:user_id, :location_id], :unique => true
  end
end
