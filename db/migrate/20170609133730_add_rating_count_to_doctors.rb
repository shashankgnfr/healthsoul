class AddRatingCountToDoctors < ActiveRecord::Migration[5.0]
  def change
    add_column :doctors, :ratings_count, :integer, default: 0
  end
end
