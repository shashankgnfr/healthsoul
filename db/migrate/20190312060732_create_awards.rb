class CreateAwards < ActiveRecord::Migration[5.0]
  def change
    create_table :awards do |t|
      t.integer :location_id
      t.string :location_type
      t.integer :year
      t.string :award_type
      t.timestamps
    end
    add_index :awards, :location_type
    add_index :awards, :year
    add_index :awards, :location_id
  end
end
