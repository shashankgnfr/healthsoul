class AddContent2ToBlog < ActiveRecord::Migration[5.0]
  def change
    # TODO: Add limit of content to 500 characters etc..
    add_column :blogs, :content_2, :text
    add_column :affiliates, :publish, :boolean, default: false
  end
end
