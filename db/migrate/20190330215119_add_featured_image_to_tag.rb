class AddFeaturedImageToTag < ActiveRecord::Migration[5.0]
  def change
    add_column :tags, :featured_image, :string
  end
end
