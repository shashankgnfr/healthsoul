class AddSlugToTag < ActiveRecord::Migration[5.0]
  def change
    add_column :tags, :slug, :string
    add_column :practice_specialities, :slug, :string
    add_index :tags, :slug
    add_index :practice_specialities, :slug
    ActsAsTaggableOn::Tag.all.each do |t|
      t.save!
    end
    PracticeSpeciality.all.each do |ps|
      ps.save!
    end
  end
end
