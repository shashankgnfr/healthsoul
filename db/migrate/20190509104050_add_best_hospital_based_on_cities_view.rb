class AddBestHospitalBasedOnCitiesView < ActiveRecord::Migration[5.0]
  def up
    add_column :locations, :priority_city_level, :integer
    
    execute <<-END
      CREATE VIEW best_hospital_based_on_cities_views AS
        SELECT  COUNT(*) AS count_all, 
          city, city_slug, state, state_slug, country_slug, priority_city_level
        FROM "locations" 
        INNER JOIN "location_types" ON "location_types"."id" = "locations"."location_type_id" 
        WHERE "location_types"."name" = 'Hospital' 
          ANd priority_city_level > 0
        GROUP BY city, city_slug, priority_city_level, state, state_slug , country_slug
        ORDER BY COUNT(locations.id) DESC, city ASC 
    END

    execute <<-END
      DROP VIEW best_doctor_views;
      CREATE VIEW best_doctor_views AS 
        WITH initial_data AS (
          SELECT COUNT(*) AS dr_count_by_city, city, 
          city_slug, state_slug, country_slug,
          practice_specialities.title AS practice_specialities_title , 
          case
            when practice_specialities.name = 'Hematology Oncology' then
              'Oncologist'
            when practice_specialities.name = 'Orthopedics' then
              'Orthopedic'
            else
              practice_specialities.title
          end
          AS practice_specialities_name
          FROM "doctors" 
          INNER JOIN "doctor_specialities" 
          ON "doctor_specialities"."doctor_id" = "doctors"."id" 
          INNER JOIN "practice_specialities" 
          ON "practice_specialities"."id" = "doctor_specialities"."practice_speciality_id" 
          WHERE "doctors"."country_slug" = current_setting('healthsoul.country_slug')
           and doctors.priority_city is true
           and practice_specialities.id in (
            select practice_speciality_id 
            from countries_practice_specialities 
            where countries_practice_specialities.country_id = (
            select id 
            from countries 
            where slug = current_setting('healthsoul.country_slug')))
          GROUP BY practice_specialities.title, practice_specialities_name, city, city_slug, state_slug, country_slug
          order by array_position(
          ARRAY['New York']::varchar[], city), dr_count_by_city desc , city asc
        )

        SELECT practice_specialities_title, practice_specialities_name, 
        json_agg(json_build_object(concat(city, '(',country_slug,')(', state_slug, ')(', city_slug, ')'), dr_count_by_city))
         from initial_data
        GROUP BY practice_specialities_title, practice_specialities_name
        order by practice_specialities_name

    END

    ['Delhi', 'Chandigarh', 'Ludhiana', 'Gurgaon', 'Mohali'].each do |city|
      ActiveRecord::Base.connection.execute("
        update doctors
        set priority_city = true
        where city = '#{city}'
      ")
    end
    
    hospital = LocationType.where(name: 'Hospital').first
    ['Ludhiana', 'Mumbai', 'Jaipur', 'Shimla', 'Patna', 'Raipur'].each do |city|
      ActiveRecord::Base.connection.execute("
        update locations
        set priority_city_level = 1
        where city = '#{city}' and location_type_id = #{hospital.id} and country ilike 'india'
      ")
    end
   
    ActiveRecord::Base.connection.execute("
      update doctors
      set priority_city = false
      where city = 'Springfield' and country_slug ilike 'usa' and state != 'IL'
    ")

    india = Country.where(slug: 'india').first
    usa = Country.where(slug: 'usa').first
    PracticeSpeciality.where(name: ['Orthopedics', 'Urology']).each do |p|
      p.country_ids = [india.id, usa.id]
      p.save
    end   

    ActiveRecord::Base.connection.execute("update locations set state = 'Chhattisgarh' where state = 'Chhattisgarh '")

    ['Panchkula', 'Ahmedabad', 'Mumbai', 'Kharar'].each do |city|
      ActiveRecord::Base.connection.execute("
        update doctors
        set priority_city = false
        where city = '#{city}'
      ")
    end

    ActiveRecord::Base.connection.execute("update doctors set state = 'Delhi', state_slug = 'delhi' where state_slug = 'new-delhi'")

    execute <<-END
      DROP VIEW best_hospital_views;
      CREATE VIEW best_hospital_views AS     
        WITH initial_data AS (
          SELECT  COUNT(*) AS count_all, state, state_slug, country_slug
          FROM "locations" 
          INNER JOIN "location_types" ON "location_types"."id" = "locations"."location_type_id" 
          WHERE "location_types"."name" = 'Hospital' 
            AND country_slug = current_setting('healthsoul.country_slug') 
            AND state is not NULL
          GROUP BY state, state_slug , country_slug
        )
      SELECT *
      FROM initial_data
      ORDER BY count_all DESC  
    END

    ActiveRecord::Base.connection.execute("update doctors set priority_city = false where city = 'Chandigarh' and state = 'Punjab'")

    ActiveRecord::Base.connection.execute("update locations set priority_city_level = 0 where city = 'Chandigarh' and state = 'Punjab' and location_type_id = #{hospital.id}")
  end

  def down
    execute <<-END
      DROP VIEW best_hospital_based_on_cities_views;
    END
    remove_column :locations, :priority_city_level
  end
end
