class AddYextStatusAndIdToDoctorsAndLocations < ActiveRecord::Migration[5.0]
  def change
    add_column :locations, :yext_id, :integer
    add_column :locations, :yext_status, :integer
    add_column :doctors, :yext_id, :integer
    add_column :doctors, :yext_status, :integer
  end
end
