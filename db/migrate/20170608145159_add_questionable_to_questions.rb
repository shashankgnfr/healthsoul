class AddQuestionableToQuestions < ActiveRecord::Migration[5.0]
  def change
    add_reference :questions, :questionable, polymorphic: true
    Question.update_all("questionable_id = location_id, questionable_type = 'Location'")
    remove_column :questions, :location_id
  end
end
