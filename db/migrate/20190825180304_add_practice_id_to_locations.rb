class AddPracticeIdToLocations < ActiveRecord::Migration[5.0]
  def change
    add_column :locations, :practice_id, :string
  end
end
