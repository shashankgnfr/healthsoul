class AddMedicareIdToLocations < ActiveRecord::Migration[5.0]
  def change
  	add_column :locations, :medicare_id, :string
  end
end
