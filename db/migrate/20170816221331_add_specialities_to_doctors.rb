class AddSpecialitiesToDoctors < ActiveRecord::Migration[5.0]
  def change
    add_column :doctors, :specialities, :text
  end
end
