class AddFieldsToRatings < ActiveRecord::Migration[5.0]
  def change
    add_column :ratings, :cost_satisfaction, :decimal, default: 0
    add_column :ratings, :claims_process, :decimal, default: 0
  end
end
