class CreateHospitalAffiliations < ActiveRecord::Migration[5.0]
  def change
    create_table :hospital_affiliations do |t|
      t.references :doctor, foreign_key: true
      t.references :location, foreign_key: true

      t.timestamps
    end
  end
end
