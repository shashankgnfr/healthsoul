class AmendBestHospitalView < ActiveRecord::Migration[5.0]
  def up
    execute <<-END
      DROP VIEW best_hospital_views;
      CREATE VIEW best_hospital_views AS
        SELECT  COUNT(*) AS count_all, state, state_slug, country_slug
        FROM "locations" 
        INNER JOIN "location_types" ON "location_types"."id" = "locations"."location_type_id" 
        WHERE "location_types"."name" = 'Hospital' 
          AND country_slug = current_setting('healthsoul.country_slug') 
          AND state is not NULL
        GROUP BY state, state_slug , country_slug
        ORDER BY COUNT(locations.id) DESC, state ASC 
        LIMIT 18
    END

    create_table :countries_practice_specialities do |t|
      t.integer :country_id, null: false
      t.integer :practice_speciality_id, null: false
      t.timestamps
    end
    add_index(:countries_practice_specialities, [:country_id, :practice_speciality_id], unique: true, name: 'country_speciality')

    india = Country.where(slug: 'india').first
    india.practice_speciality_ids = PracticeSpeciality.where(name: IND_PREFERRED_SPECIALITIES).pluck(:id)
    india.save

    usa = Country.where(slug: 'usa').first
    usa.practice_speciality_ids = PracticeSpeciality.where(name: USA_PREFERRED_SPECIALITIES).pluck(:id)
    usa.save

    execute <<-END
      DROP VIEW best_doctor_views;
      CREATE VIEW best_doctor_views AS 
        WITH initial_data AS (
          SELECT COUNT(*) AS dr_count_by_city, city, 
          city_slug, state_slug, country_slug,
          practice_specialities.title AS practice_specialities_title , 
          case
            when practice_specialities.name = 'Hematology Oncology' then
              'Oncology'
            else
              practice_specialities.title
          end
          AS practice_specialities_name
          FROM "doctors" 
          INNER JOIN "doctor_specialities" 
          ON "doctor_specialities"."doctor_id" = "doctors"."id" 
          INNER JOIN "practice_specialities" 
          ON "practice_specialities"."id" = "doctor_specialities"."practice_speciality_id" 
          WHERE "doctors"."country_slug" = current_setting('healthsoul.country_slug')
           and doctors.priority_city is true
           and practice_specialities.id in (
            select practice_speciality_id 
            from countries_practice_specialities 
            where countries_practice_specialities.country_id = (
            select id 
            from countries 
            where slug = current_setting('healthsoul.country_slug')))
          GROUP BY practice_specialities.title, practice_specialities_name, city, city_slug, state_slug, country_slug
          order by dr_count_by_city desc ,city asc
        )

        SELECT practice_specialities_title, practice_specialities_name, 
        json_agg(json_build_object(concat(city, '(',country_slug,')(', state_slug, ')(', city_slug, ')'), dr_count_by_city))
         from initial_data
        GROUP BY practice_specialities_title, practice_specialities_name
        order by practice_specialities_name

    END
  end

  def down
    execute <<-END
      DROP VIEW best_doctor_views;
      DROP VIEW best_hospital_views;
      DROP table countries_practice_specialities;
    END
  end
end
