class AddCommonSpecialityToPracticeSpeciality < ActiveRecord::Migration[5.0]
  def change
    add_column :practice_specialities, :common_speciality, :boolean, default: false
  end
end
