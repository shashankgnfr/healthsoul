class CreatePatientStories < ActiveRecord::Migration[5.0]
  def change
    create_table :patient_stories do |t|
      t.string :name, null: false
      t.string :email, null: false
      t.string :city
      t.references :country, index: true
      t.text :description, null: false
      t.string :word_document
      t.string :image
      t.string :video
      t.text :comments
      t.timestamps
    end
  end
end
