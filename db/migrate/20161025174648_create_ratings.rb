class CreateRatings < ActiveRecord::Migration[5.0]
  def change
    create_table :ratings do |t|
      t.references :location, foreign_key: true
      t.references :user, foreign_key: true
      t.date :use_date
      t.string :reason
      t.decimal :overall_experience
      t.decimal :doctor_care
      t.decimal :nursing_care
      t.decimal :cleanliness
      t.decimal :food_services
      t.string :recommend
      t.text :comment

      t.timestamps
    end
  end
end
