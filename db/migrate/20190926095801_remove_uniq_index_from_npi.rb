class RemoveUniqIndexFromNpi < ActiveRecord::Migration[5.0]
  def change
    remove_index :doctors, column: :npi
  end
end
