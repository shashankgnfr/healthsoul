class AddCustomMetaTagsToTags < ActiveRecord::Migration[5.0]
  def change
    add_column :tags, :custom_meta_tags, :text
  end
end
