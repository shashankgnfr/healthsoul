class CreateMedicalStudents < ActiveRecord::Migration[5.0]
  def change
    create_table :medical_students do |t|
      t.string :name, null: false
      t.string :photo
      t.string :email, null: false
      t.string :professional_school, null: false
      t.string :degree_type, null: false
      t.integer :graduation_year, null: false
      t.text  :publications
      t.text :care_philosophy
      t.boolean :imported, default: false
      t.string :slug, null: false
      t.timestamps
    end

    add_column :event_members, :user_id, :integer
  end
end
