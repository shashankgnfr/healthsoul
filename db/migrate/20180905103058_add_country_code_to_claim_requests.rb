class AddCountryCodeToClaimRequests < ActiveRecord::Migration[5.0]
  def change
    add_column :claim_requests, :country_code, :string
  end
end
