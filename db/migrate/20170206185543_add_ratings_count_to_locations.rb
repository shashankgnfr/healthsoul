class AddRatingsCountToLocations < ActiveRecord::Migration[5.0]
  def change
    add_column :locations, :ratings_count, :integer
  end
end
