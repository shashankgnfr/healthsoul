class ChangePromotionToWYSIWYG < ActiveRecord::Migration[5.0]
  def change
  	  change_column :promotions, :blurb, :text
  	  change_column :promotions, :title, :text
  end
end
