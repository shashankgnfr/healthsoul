class CreateSharedBlogs < ActiveRecord::Migration[5.0]
  def change
    create_table :shared_blogs do |t|
      t.integer :user_id, null: false
      t.integer :blog_id, null: false
      t.integer :shared_count
      t.string :social_platform
      t.timestamps
    end
  end
end
