class CreateHealthScores < ActiveRecord::Migration[5.0]
  def change
    create_table :health_scores do |t|
      t.references :doctor, foreign_key: true
      t.float :score ,:default => 0.0

      t.timestamps
    end
  end
end
