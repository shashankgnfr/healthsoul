class CreateAdvertisementsResources < ActiveRecord::Migration[5.0]
  def up
    create_table :advertisements_resources do |t|
      t.integer :advertisement_id, null: false
      t.integer :resource_id, default: 0
      t.string :resource_type, null: false
      t.string :resource_action, null: false
      t.timestamps
    end
    add_index :advertisements_resources, [:advertisement_id, :resource_type, :resource_id], unique: true, name: 'advert_resource_index'
  end
  def down
    drop_table :advertisements_resources
  end
end
