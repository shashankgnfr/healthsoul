class AddSpToDoctorUploads < ActiveRecord::Migration[5.0]
  def change
    add_column :doctor_uploads, :spec, :boolean, default: false
  end
end
