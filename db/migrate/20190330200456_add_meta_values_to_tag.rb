class AddMetaValuesToTag < ActiveRecord::Migration[5.0]
  def change
    add_column :tags, :description, :text
    add_column :tags, :title, :string
  end
end
