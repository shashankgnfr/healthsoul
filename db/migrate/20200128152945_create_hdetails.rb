class CreateHdetails < ActiveRecord::Migration[5.0]
  def change
    create_table :hdetails do |t|
      t.string :platform
      t.string :website_url
      t.integer :name
      t.integer :address
      t.integer :phone
      t.integer :photo
      t.integer :website
      t.integer :speciallity
      t.integer :hour
      t.integer :fee
      t.integer :fee
      t.integer :about_me
      t.integer :education
      t.integer :rating
      t.integer :number_of_reviews
      t.integer :total_rating
      t.references :doctor, foreign_key: true

      t.timestamps
    end
  end
end
