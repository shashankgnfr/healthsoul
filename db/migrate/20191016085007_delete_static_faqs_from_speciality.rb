class DeleteStaticFaqsFromSpeciality < ActiveRecord::Migration[5.0]
  def change
    remove_column :practice_specialities, :static_faqs
  end
end
