class AddPhotoAltToLocationDoctor < ActiveRecord::Migration[5.0]
  def change
    add_column :doctors, :photo_alt, :string, default: 'Doctor | HealthSoul'
    add_column :locations, :photo_alt, :string

    # Below command takes 383.1771s on my machine. Thus, i'll run from console on production.
    # execute <<-END
    #   UPDATE doctors 
    #   SET photo_alt = concat(name, ' | HealthSoul') 
    #   WHERE name is not null;

    #   UPDATE doctors 
    #   SET photo_alt = 'Doctor | HealthSoul'
    #   WHERE name is null;
      
    #   UPDATE "locations" 
    #   SET photo_alt = concat(name, ' | HealthSoul') 
    #   WHERE name is not null;

    # END

    # LocationType.all.select('name, id').each do |l|
    #   Location.where(name: nil, location_type_id: l.id).update_all(photo_alt: "#{l.name} | HealthSoul")
    # end
  end
end