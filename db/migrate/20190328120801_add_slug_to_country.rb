class AddSlugToCountry < ActiveRecord::Migration[5.0]
  def change
    add_column :countries, :slug, :string
    add_index :countries, :slug
    Country.all.each do |c|
      c.update_column :slug, c.name.parameterize
    end
  end
end
