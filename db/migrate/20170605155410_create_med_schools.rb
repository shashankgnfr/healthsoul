class CreateMedSchools < ActiveRecord::Migration[5.0]
  def change
    create_table :med_schools do |t|
      t.string :name

      t.timestamps
    end
  end
end
