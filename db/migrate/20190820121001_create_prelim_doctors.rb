class CreatePrelimDoctors < ActiveRecord::Migration[5.0]
  def change
    create_table :prelim_doctors do |t|
      t.string :name
      t.string :credential
      t.string :professional_school
      t.string :speciality 
      t.integer :graduation
      t.boolean :still_in_school 
      t.string :address
      t.string :city
      t.string :state
      t.string :country
      t.string :zip
      t.string :email
      t.string :phone
      t.string :website
      t.string :photo
      t.string :verified, default: false
      t.integer :event_id, foreign_key: true
      t.timestamps
    end
  end
end
