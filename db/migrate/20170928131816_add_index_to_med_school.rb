class AddIndexToMedSchool < ActiveRecord::Migration[5.0]
  def change
    add_index :med_schools, :name
    add_index :doctor_organizations, :name
  end
end
