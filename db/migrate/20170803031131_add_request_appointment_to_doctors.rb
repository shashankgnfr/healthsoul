class AddRequestAppointmentToDoctors < ActiveRecord::Migration[5.0]
  def change
    add_column :doctors, :request_appt, :boolean, default: false
  end
end
