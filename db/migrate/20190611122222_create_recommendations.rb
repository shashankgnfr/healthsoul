class CreateRecommendations < ActiveRecord::Migration[5.0]
  def change
    create_table :recommendations do |t|
      t.string :name , null: false
      t.text :location
      t.string :country, null: false
      t.string :website
      t.string :email, null: false
      t.string :source_type, null: false
      t.timestamps
    end
  end
end
