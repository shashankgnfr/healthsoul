class AddZeroDefaultsToDoctorReviews < ActiveRecord::Migration[5.0]
  def change
    change_column :doctor_reviews, :overall_experience, :decimal, default: 0
    change_column :doctor_reviews, :explaintation, :decimal, default: 0
    change_column :doctor_reviews, :trust, :decimal, default: 0
    change_column :doctor_reviews, :staff, :decimal, default: 0
    change_column :doctor_reviews, :appt, :decimal, default: 0
    change_column :doctor_reviews, :wait, :decimal, default: 0
    change_column :doctor_reviews, :skills, :decimal, default: 0
  end
end
