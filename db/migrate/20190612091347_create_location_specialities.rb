class CreateLocationSpecialities < ActiveRecord::Migration[5.0]
  def change
    create_table :location_specialities do |t|
      t.references :location, foreign_key: true
      t.references :practice_speciality, foreign_key: true

      t.timestamps
    end
  end
end
