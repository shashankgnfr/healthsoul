class AddWebsiteToDoctors < ActiveRecord::Migration[5.0]
  def change
    add_column :doctors, :website, :string
    add_column :doctor_organizations, :website, :string
  end
end
