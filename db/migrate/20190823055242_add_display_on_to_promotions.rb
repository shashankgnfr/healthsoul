class AddDisplayOnToPromotions < ActiveRecord::Migration[5.0]
  def change
    add_column :promotions, :display_on, :string, default: 'Providers'
  end
end
