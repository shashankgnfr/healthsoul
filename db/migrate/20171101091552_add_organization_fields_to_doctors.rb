class AddOrganizationFieldsToDoctors < ActiveRecord::Migration[5.0]
  def change
    add_column :doctors, :address, :string
    add_column :doctors, :organization_name, :string
    add_column :doctors, :city, :string
    add_column :doctors, :state, :string
    add_column :doctors, :zip, :string
    add_column :doctors, :phone, :string
    add_column :doctors, :country, :string
    add_column :doctors, :country_code, :string
    add_column :doctors, :continent, :string
    add_column :doctors, :simple_geocode, :boolean
  end
end
