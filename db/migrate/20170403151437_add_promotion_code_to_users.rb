class AddPromotionCodeToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :promotion_code, :string
  end
end
