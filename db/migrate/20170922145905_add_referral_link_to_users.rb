class AddReferralLinkToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :referral_activated, :boolean, default: false
  end
end
