class AddDoctorIdToDoctorOrganization < ActiveRecord::Migration[5.0]
  def change
    add_column :doctor_organizations, :doctor_id, :integer
  end
end
