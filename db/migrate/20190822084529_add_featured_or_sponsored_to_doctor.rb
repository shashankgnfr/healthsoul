class AddFeaturedOrSponsoredToDoctor < ActiveRecord::Migration[5.0]
  def change
    add_column :doctors, :featured_or_sponsored, :string, default: nil
  end
end
