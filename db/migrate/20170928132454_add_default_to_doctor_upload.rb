class AddDefaultToDoctorUpload < ActiveRecord::Migration[5.0]
  def change
    change_column :doctor_uploads, :imported, :boolean, default: false
  end
end
