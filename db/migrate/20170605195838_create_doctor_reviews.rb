class CreateDoctorReviews < ActiveRecord::Migration[5.0]
  def change
    create_table :doctor_reviews do |t|
      t.references :doctor, foreign_key: true
      t.references :user, foreign_key: true
      t.date :use_date
      t.integer :where
      t.decimal :overall_experience
      t.decimal :explaintation
      t.decimal :trust
      t.decimal :staff
      t.decimal :appt
      t.decimal :wait
      t.decimal :skills
      t.string :recommend
      t.text :comment
      t.integer :status

      t.timestamps
    end
  end
end
