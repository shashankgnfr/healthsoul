class AddOverallToDoctor < ActiveRecord::Migration[5.0]
  def change
    add_column :doctors, :overall, :decimal
  end
end
