class AddIndexToDoctorLocationFields < ActiveRecord::Migration[5.0]
  def change
    add_index :doctors, :state
    add_index :doctors, :country
    add_index :doctors, :country_code
  end
end
