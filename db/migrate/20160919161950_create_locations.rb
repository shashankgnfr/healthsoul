class CreateLocations < ActiveRecord::Migration[5.0]
  def change
    create_table :locations do |t|
      t.string :name
      t.string :address
      t.string :address2
      t.string :city
      t.string :state
      t.string :zip
      t.string :country
      t.string :website
      t.string :phone
      t.string :email
      t.string :sub_type
      t.belongs_to :location_type, foreign_key: true

      t.timestamps
    end
  end
end
