class AddMedicareQualityFields < ActiveRecord::Migration[5.0]
  def change
    add_column :locations, :ownership, :string
    add_column :locations, :emergency_services, :boolean
    add_column :locations, :hospital_overall_rating, :integer
    add_column :locations, :mortality_national, :string
    add_column :locations, :safety_of_care_national, :string
    add_column :locations, :readmission_national, :string
    add_column :locations, :patient_experience_national, :string
    add_column :locations, :effectiveness_of_care_national, :string
    add_column :locations, :timeliness_of_care_national, :string
    add_column :locations, :efficient_use_of_medical_imaging_national, :string
    add_column :locations, :hospital_overall_rating_graph, :integer
    add_column :locations, :mortality_national_graph, :integer
    add_column :locations, :safety_of_care_national_graph, :integer
    add_column :locations, :readmission_national_graph, :integer
    add_column :locations, :patient_experience_national_graph, :integer
    add_column :locations, :effectiveness_of_care_national_graph, :integer
    add_column :locations, :timeliness_of_care_national_graph, :integer
    add_column :locations, :efficient_use_of_medical_imaging_national_graph, :integer

    add_column :locations, :communication_with_nurses_rate, :float
    add_column :locations, :communication_with_drs_rate, :float
    add_column :locations, :responsiveness_of_hospital_staff_rate, :float
    add_column :locations, :care_transition_rate, :float
    add_column :locations, :communication_about_medicine_rate, :float
    add_column :locations, :cleanliness_quietness_rate, :float
    add_column :locations, :discharge_rate, :float
    add_column :locations, :overall_performance_rate, :float
  end
end
