class CreatePromotionWinners < ActiveRecord::Migration[5.0]
  def change
    create_table :promotion_winners do |t|
      t.string :name, not_null: true
      t.string :winner_type, not_null: true
      t.string :prize, not_null: true
      t.string :month, not_null: true
      t.integer :year, not_null: true
      t.references :promotion, foreign_key: true
      t.timestamps
    end
    add_column :promotions, :publish, :boolean, default: false
  end
end
