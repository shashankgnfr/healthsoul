class AddVideoCodeToBlog < ActiveRecord::Migration[5.0]
  def change
    add_column :blogs, :video_html_code, :text
  end
end
