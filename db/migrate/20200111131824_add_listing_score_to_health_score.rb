class AddListingScoreToHealthScore < ActiveRecord::Migration[5.0]
  def change
    add_column :health_scores, :listing_score, :float
    add_column :health_scores, :rating_score, :float
  end
end
