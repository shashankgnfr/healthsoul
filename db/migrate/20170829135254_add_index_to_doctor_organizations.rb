class AddIndexToDoctorOrganizations < ActiveRecord::Migration[5.0]
  def change
    add_index :doctor_organizations, :country_code
  end
end
