class AddIndexToSlug < ActiveRecord::Migration[5.0]
  def change
    add_index :doctors, :state_slug
    add_index :doctors, :city_slug

    add_index :locations, :state_slug
    add_index :locations, :city_slug
  end
end
