class AddDescriptionToDoctors < ActiveRecord::Migration[5.0]
  def change
    add_column :doctors, :description, :text
  end
end
