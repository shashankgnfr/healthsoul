class DeleteMobileFromDoctor < ActiveRecord::Migration[5.0]
  def change
    remove_column :doctors, :mobile_country_code
    remove_column :doctors, :mobile_number
  end
end
