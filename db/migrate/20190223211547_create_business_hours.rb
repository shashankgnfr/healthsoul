class CreateBusinessHours < ActiveRecord::Migration[5.0]
  def change
    create_table :business_hours do |t|
      t.time :start_time
      t.time :end_time
      t.integer :day
      t.boolean :closed
      t.references :hourable, polymorphic: true, index: true

      t.timestamps
    end
  end
end
