class CreateVotes < ActiveRecord::Migration[5.0]
  def change
    create_table :votes do |t|
      t.references :voteable, polymorphic: true
      t.integer :status, default: 0

      t.timestamps
    end
  end
end
