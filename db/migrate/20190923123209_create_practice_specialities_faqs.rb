class CreatePracticeSpecialitiesFaqs < ActiveRecord::Migration[5.0]
  def change
    create_table :practice_specialities_faqs do |t|
      t.references :practice_speciality, foreign_key: true
      t.references :user, foreign_key: true
      t.string :faq_title, not_null: true
      t.text :faq_body, not_null: true
      t.boolean :publish, default: false
      t.timestamps
    end
    create_join_table :countries, :practice_specialities_faqs do |t|
    end    
  end
end
