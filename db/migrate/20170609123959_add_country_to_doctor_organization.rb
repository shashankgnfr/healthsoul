class AddCountryToDoctorOrganization < ActiveRecord::Migration[5.0]
  def change
    add_column :doctor_organizations, :country, :string
    add_column :doctor_organizations, :country_code, :string
    add_column :doctor_organizations, :continent, :string
  end
end
