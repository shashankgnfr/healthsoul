class AddFellowship2ToDoctor < ActiveRecord::Migration[5.0]
  def change
    add_column :doctors, :fellowship_2, :string
    MedSchool.where(name:  'All India Institute of Medical Sciences, New Delhi, Delhi, India' ).update_all(name: 'All India Institute of Medical Sciences, Delhi, India')
  end
end
