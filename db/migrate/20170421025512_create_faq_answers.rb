class CreateFaqAnswers < ActiveRecord::Migration[5.0]
  def change
    create_table :faq_answers do |t|
      t.text :body
      t.references :faq, foreign_key: true
      t.references :user, foreign_key: true
      t.boolean :approved, default: true

      t.timestamps
    end
  end
end
