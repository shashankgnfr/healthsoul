class AddEmailToClaimRequests < ActiveRecord::Migration[5.0]
  def change
    add_column :claim_requests, :email, :string
  end
end
