class AmendmentInDoctor < ActiveRecord::Migration[5.0]
  def change

    # run from console before deloying this code on server.
    # Rake::Task['onetime_task:map_organization_detail_with_doctor'].invoke()

    rename_column :doctors, :organization_phone_country_code, :mobile_country_code
    rename_column :doctors, :organization_phone_body, :mobile_number
    remove_column :doctors, :organization_email
    remove_column :doctors, :organization_website
    remove_column :doctors, :organization_phone

    rename_column :doctor_uploads, :organization_website, :website
  end

end
