class AddCustomAuthorPhotoToBlogs < ActiveRecord::Migration[5.0]
  def change
    add_column :blogs, :custom_author_photo, :string
  end
end
