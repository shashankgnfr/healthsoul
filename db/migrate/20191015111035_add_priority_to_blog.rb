class AddPriorityToBlog < ActiveRecord::Migration[5.0]
  def change
    add_column :blogs, :priority, :integer
  end
end
