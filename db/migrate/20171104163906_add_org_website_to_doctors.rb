class AddOrgWebsiteToDoctors < ActiveRecord::Migration[5.0]
  def change
    add_column :doctors, :organization_website, :string
  end
end
