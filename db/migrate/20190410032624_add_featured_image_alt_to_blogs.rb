class AddFeaturedImageAltToBlogs < ActiveRecord::Migration[5.0]
  def change
    add_column :blogs, :featured_image_alt, :string
  end
end
