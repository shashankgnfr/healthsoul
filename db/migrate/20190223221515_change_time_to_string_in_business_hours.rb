class ChangeTimeToStringInBusinessHours < ActiveRecord::Migration[5.0]
  def change
    change_column :business_hours, :start_time, :text
    change_column :business_hours, :end_time, :text
  end
end
