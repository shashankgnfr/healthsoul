class AddBoardCertificationToDr < ActiveRecord::Migration[5.0]
  def change
    add_column :doctors, :board_certification, :text
  end
end
