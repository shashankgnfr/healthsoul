class CreateFaqs < ActiveRecord::Migration[5.0]
  def change
    create_table :faqs do |t|
      t.text :body
      t.references :user, foreign_key: true
      t.string :country
      t.boolean :approved, default: true

      t.timestamps
    end
  end
end
