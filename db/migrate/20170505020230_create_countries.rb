class CreateCountries < ActiveRecord::Migration[5.0]
  def change
    create_table :countries do |t|
      t.string :alpha2
      t.string :alpha3
      t.string :numeric
      t.string :fips
      t.string :name
      t.string :capital
      t.string :areakm
      t.string :pop
      t.string :continent_code
      t.string :continent

      t.timestamps
    end
  end
end
