class CreateDepartmentRatings < ActiveRecord::Migration[5.0]
  def change
    create_table :department_ratings do |t|
      t.references :location, foreign_key: true
      t.decimal :rating
      t.string :reason

      t.timestamps
    end

    Rating.find_each do |rating|
      DepartmentRankingJob.perform_async(rating)
    end
  end
end
