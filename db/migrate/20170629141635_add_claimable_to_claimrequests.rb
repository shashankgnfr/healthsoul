class AddClaimableToClaimrequests < ActiveRecord::Migration[5.0]
  def change
    add_reference :claim_requests, :claimable, polymorphic: true
    ClaimRequest.update_all("claimable_id = location_id, claimable_type = 'Location'")
    remove_column :claim_requests, :location_id
  end
end
