class AddHoursToDoctor < ActiveRecord::Migration[5.0]
  def change
	add_column :doctors, :monday_hours, :string
	add_column :doctors, :tuesday_hours, :string
	add_column :doctors, :wenesday_hours, :string
	add_column :doctors, :thursday_hours, :string
	add_column :doctors, :friday_hours, :string
	add_column :doctors, :saturday_hours, :string
	add_column :doctors, :sunday_hours, :string
	add_column :doctors, :display_hours_and_fees, :boolean, default: false
  	add_column :doctors, :fees, :string
  end
end
