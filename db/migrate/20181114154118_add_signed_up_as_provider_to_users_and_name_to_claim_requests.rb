class AddSignedUpAsProviderToUsersAndNameToClaimRequests < ActiveRecord::Migration[5.0]
  def change
  	add_column :users, :marked_as_provider, :boolean, default: false
  	add_column :users, :referral_code, :string
  end
end
