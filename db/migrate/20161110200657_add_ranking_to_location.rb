class AddRankingToLocation < ActiveRecord::Migration[5.0]
  def change
    add_column :locations, :in_state_ranking, :integer, default: 0
    add_column :locations, :in_city_ranking, :integer, default: 0
  end
end
