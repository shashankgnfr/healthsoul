class ChangeAcaAffToDoctors < ActiveRecord::Migration[5.0]
  def change
    remove_column :doctors, :academic_affiliation
    add_column :doctors, :academic_affiliation, :integer, default: 0
  end
end
