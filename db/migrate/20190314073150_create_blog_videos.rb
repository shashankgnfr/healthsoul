class CreateBlogVideos < ActiveRecord::Migration[5.0]
  def change
    create_table :blog_videos do |t|
      t.references :blog, foreign_key: true
      t.string :video
      t.timestamps
    end
  end
end
