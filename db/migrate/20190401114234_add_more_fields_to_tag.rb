class AddMoreFieldsToTag < ActiveRecord::Migration[5.0]
  def change
    add_column :tags, :featured_image_alt, :string
    add_column :tags, :meta_title, :string
  end
end
