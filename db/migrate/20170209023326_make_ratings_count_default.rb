class MakeRatingsCountDefault < ActiveRecord::Migration[5.0]
  def change
    change_column :locations, :ratings_count, :integer, :default => 0
    Location.where("ratings_count is null").update_all(ratings_count: 0)
  end
end
