class AddTitleSlugToSpeciality < ActiveRecord::Migration[5.0]
  def up
    add_column :practice_specialities, :title_slug, :string
    set_titles = {
      249 => 'Pediatric Surgeon',
      520 => 'Transplant Nephrologist',
      378 => 'Pulmonary Hypertension Specialist',
      238 => 'Physician',
      537 => 'Prompt Care Doctor',
      402 => 'Pediatric Orthopedician',
      401 => 'Pediatric Neurologist',
      400 => 'Pediatric Nephrologist',
      409 => 'Cosmetic Surgeon',
      427 => 'Physician',
      441 => 'Pediatric Intensivist'
    }

    PracticeSpeciality.where.not(title: nil).each do |sp|
      sp.update_column :title_slug, sp.title.parameterize
    end

    set_titles.each_pair do |k, v|
      title_slug = v.try(:parameterize)
      unless PracticeSpeciality.where(title_slug: title_slug).empty?
        sequence = PracticeSpeciality.where("title_slug like ?", "#{title_slug}-%").count + 2
        title_slug = "#{title_slug}-#{sequence}"
      end
      PracticeSpeciality.where(id: k).first.update_columns(title: v, title_slug: title_slug) 
    end
        
    execute <<-END
      DROP VIEW best_doctor_views;
      CREATE VIEW best_doctor_views AS 
        WITH initial_data AS (
          SELECT COUNT(*) AS dr_count_by_city, city, 
          city_slug, state_slug, country_slug,
          practice_specialities.title_slug AS practice_specialities_title , 
          case
            when practice_specialities.name = 'Hematology Oncology' then
              'Oncologist'
            when practice_specialities.name = 'Orthopedics' then
              'Orthopedic'
            else
              practice_specialities.title
          end
          AS practice_specialities_name
          FROM "doctors" 
          INNER JOIN "doctor_specialities" 
          ON "doctor_specialities"."doctor_id" = "doctors"."id" 
          INNER JOIN "practice_specialities" 
          ON "practice_specialities"."id" = "doctor_specialities"."practice_speciality_id" 
          WHERE "doctors"."country_slug" = current_setting('healthsoul.country_slug')
           and doctors.priority_city is true
           and practice_specialities.id in (
            select practice_speciality_id 
            from countries_practice_specialities 
            where countries_practice_specialities.country_id = (
            select id 
            from countries 
            where slug = current_setting('healthsoul.country_slug')))
          GROUP BY practice_specialities.title_slug, practice_specialities_name, city, city_slug, state_slug, country_slug
          order by array_position(
          ARRAY['New York']::varchar[], city), dr_count_by_city desc , city asc
        )

        SELECT practice_specialities_title, practice_specialities_name, 
        json_agg(json_build_object(concat(city, '(',country_slug,')(', state_slug, ')(', city_slug, ')'), dr_count_by_city))
         from initial_data
        GROUP BY practice_specialities_title, practice_specialities_name
        order by practice_specialities_name

    END
  end

  def down
    execute <<-END
      DROP VIEW best_doctor_views;
    END
    remove_column :practice_specialities, :title_slug
  end
end
