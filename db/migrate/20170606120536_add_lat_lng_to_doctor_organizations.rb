class AddLatLngToDoctorOrganizations < ActiveRecord::Migration[5.0]
  def change
    add_column :doctor_organizations, :latitude, :decimal
    add_column :doctor_organizations, :longitude, :decimal
    add_column :doctor_organizations, :simple_geocode, :boolean
  end
end
