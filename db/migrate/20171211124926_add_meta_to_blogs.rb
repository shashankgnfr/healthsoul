class AddMetaToBlogs < ActiveRecord::Migration[5.0]
  def change
    add_column :blogs, :meta_title, :string
    add_column :blogs, :meta_description, :text
    add_column :blogs, :custom_meta_tags, :text
  end
end
