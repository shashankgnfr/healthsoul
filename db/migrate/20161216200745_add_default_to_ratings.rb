class AddDefaultToRatings < ActiveRecord::Migration[5.0]
  def change
    change_column :ratings, :overall_experience, :decimal, :default => 0
    change_column :ratings, :doctor_care, :decimal, :default => 0
    change_column :ratings, :nursing_care, :decimal, :default => 0
    change_column :ratings, :cleanliness, :decimal, :default => 0
    change_column :ratings, :food_services, :decimal, :default => 0
    change_column :ratings, :customer_service, :decimal, :default => 0
  end
end
