class AddOtherGenderToDoctor < ActiveRecord::Migration[5.0]
  def change
    add_column :doctors, :other_gender, :string
  end
end
