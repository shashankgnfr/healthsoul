class CreateDoctorSpecialities < ActiveRecord::Migration[5.0]
  def change
    create_table :doctor_specialities do |t|
      t.references :doctor, foreign_key: true
      t.references :practice_speciality, foreign_key: true

      t.timestamps
    end
  end
end
