class AddCreatedByToClaimRequest < ActiveRecord::Migration[5.0]
  def change
    add_column :claim_requests, :created_by, :integer
    ClaimRequest.all.each do |c|
      c.update_column :created_by, c.user_id
    end
    remove_column :claim_requests, :email
  end
end
