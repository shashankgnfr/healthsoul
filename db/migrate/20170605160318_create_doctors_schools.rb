class CreateDoctorsSchools < ActiveRecord::Migration[5.0]
  def change
    create_table :doctors_schools do |t|
      t.references :doctor, foreign_key: true
      t.references :med_school, foreign_key: true

      t.timestamps
    end
  end
end
