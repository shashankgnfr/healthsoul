class AddAddressAndZipToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :zip, :string
    add_column :users, :address, :text
  end
end
