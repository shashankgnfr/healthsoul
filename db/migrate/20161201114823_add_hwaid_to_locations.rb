class AddHwaidToLocations < ActiveRecord::Migration[5.0]
  def change
    add_column :locations, :hwaid, :string
    add_index :locations, :hwaid, unique: true
  end
end
