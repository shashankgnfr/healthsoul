class AddDigitialHealtScoreEnabledToDoctor < ActiveRecord::Migration[5.0]
  def change
    add_column :doctors, :digitial_health_score_enabled, :boolean ,:default => false
  end
end
