class CreateAdvertisements < ActiveRecord::Migration[5.0]
  def up
    rename_table :affiliates, :advertisements
    add_column :advertisements, :type, :string
    add_column :advertisements, :display_frequency, :integer
    add_column :advertisements, :country_id, :integer
    add_column :advertisements, :priority, :integer
    add_column :advertisements, :dimension, :string
    add_column :advertisements, :source_image, :string
    add_column :advertisements, :subtype, :string
  end
  def down
    remove_column :advertisements, :type
    remove_column :advertisements, :display_frequency
    remove_column :advertisements, :country_id
    remove_column :advertisements, :priority
    remove_column :advertisements, :dimension
    remove_column :advertisements, :source_image
    remove_column :advertisements, :subtype
    rename_table :advertisements, :affiliates
  end
end
