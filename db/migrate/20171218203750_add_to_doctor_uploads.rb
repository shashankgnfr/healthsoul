class AddToDoctorUploads < ActiveRecord::Migration[5.0]
  def change
    add_column :doctor_uploads, :organization_website, :string
    add_column :doctor_uploads, :hsid, :string, index: true, unique: true
    add_column :doctors, :hsid, :string, index: true, unique: true
  end
end
