class AddCityStateToEditorialMember < ActiveRecord::Migration[5.0]
  def change
    add_column :editorial_members, :city, :string
    add_column :editorial_members, :state, :string
  end
end
