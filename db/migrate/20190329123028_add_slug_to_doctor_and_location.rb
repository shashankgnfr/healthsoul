class AddSlugToDoctorAndLocation < ActiveRecord::Migration[5.0]
  def change
    add_column :doctors, :country_slug, :string
    add_column :locations, :country_slug, :string
    add_index :doctors, :country_slug
    add_index :locations, :country_slug

    # Doctor.where(country_slug: nil).where.not(country: nil).each do |d|
    #   d.update_column :country_slug, d.country.parameterize
    # end

    # Location.where(country_slug: nil).where.not(country: nil).each do |l|
    #   l.update_column :country_slug, l.country.parameterize
    # end

    Doctor.where.not(country: nil).group(:country).count.each_pair do |country, count|
      slug = country.parameterize
      ActiveRecord::Base.connection.execute("
        update doctors
        set country_slug = '#{slug}'
        where country = '#{country}'
      ")
    end

    Location.where.not(country: nil).group(:country).count.each_pair do |country, count|
      slug = country.parameterize
      ActiveRecord::Base.connection.execute("
        update locations
        set country_slug = '#{slug}'
        where country = '#{country}'
      ")
    end

    # Doctor.where.not(country: nil).group(:country).each_pair do |country, drs|
    #   drs.update_all :country_slug, country.parameterize
    # end
    
    # Location.where.not(country: nil).group(:country).each_pair do |country, locs|
    #   locs.update_all :country_slug, country.parameterize
    # end

  end
end
