class AddLatLngToDoctors < ActiveRecord::Migration[5.0]
  def change
    add_column :doctors, :latitude, :decimal
    add_column :doctors, :longitude, :decimal
  end
end
