class AddCountryRankingToLocations < ActiveRecord::Migration[5.0]
  def change
    add_column :locations, :in_country_ranking, :integer, default: 0
  end
end
