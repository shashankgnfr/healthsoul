class AddExtraFieldsToEvents < ActiveRecord::Migration[5.0]
  def change
    remove_column :events, :date
    add_column :events, :promotion, :text
    add_column :events, :start_date, :date
    add_column :events, :end_date, :date
    add_column :events, :banner_image, :string
    add_column :events, :active, :boolean

    add_column :prelim_doctors, :optional_information, :text
    add_column :prelim_doctors, :is_student, :boolean, default: false
    add_column :prelim_doctors, :practice_name, :string
    rename_column :prelim_doctors, :credential, :degree_type

    remove_columns :prelim_doctors, :address, :state, :country, :zip, :photo, :website, :phone

    change_column_null :prelim_doctors, :name, false
    change_column_null :prelim_doctors, :email, false

    rename_table :prelim_doctors, :event_members   
  end
end
