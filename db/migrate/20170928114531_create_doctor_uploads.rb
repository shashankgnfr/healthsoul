class CreateDoctorUploads < ActiveRecord::Migration[5.0]
  def change
    create_table :doctor_uploads do |t|
      t.string :Name
      t.string :Provider
      t.string :Gender
      t.string :Primary_Specialty
      t.string :Organization_Legal_Name
      t.string :Credential
      t.string :Address
      t.string :City
      t.string :State
      t.string :Country
      t.string :Zip_Code
      t.string :Phone_Number
      t.string :Medical_School_Name
      t.string :Graduation_Year
      t.string :Secondary_Specialty_1
      t.string :Secondary_Specialty_2
      t.string :Hospital_Affiliation_Lbn_1
      t.string :Hospital_HWA_ID_1
      t.string :Hospital_Affiliation_Lbn_2
      t.string :Hospital_HWA_ID_2
      t.string :Hospital_Affiliation_Lbn_3
      t.string :Hospital_HWA_ID_3
      t.string :Hospital_Affiliation_Lbn_4
      t.string :Hospital_HWA_ID_4
      t.string :Npi
      t.string :Professional_Accepts_Medicare_Assignment
      t.boolean :imported, default: false

      t.timestamps
    end
  end
end
