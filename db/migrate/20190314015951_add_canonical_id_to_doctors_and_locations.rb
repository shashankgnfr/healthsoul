class AddCanonicalIdToDoctorsAndLocations < ActiveRecord::Migration[5.0]
  def change
    add_column :doctors, :canonical_id, :integer
    add_column :locations, :canonical_id, :integer
  end
end
