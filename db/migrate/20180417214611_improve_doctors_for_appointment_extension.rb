class ImproveDoctorsForAppointmentExtension < ActiveRecord::Migration[5.0]
  def change
  	add_column :doctors, :organization_phone_body, :string
	add_column :doctors, :organization_phone_country_code, :string
  	add_column :doctors, :organization_phone, :string
  	add_column :doctors, :should_be_notified_by_sms_for_appointments, :boolean
  	add_column :doctors, :should_be_notified_by_email_for_appointments, :boolean
  	remove_column :users, :date_of_birth, :string
  	add_column :users, :date_of_birth, :datetime
  end
end
