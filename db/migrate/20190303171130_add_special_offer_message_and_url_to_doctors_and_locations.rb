class AddSpecialOfferMessageAndUrlToDoctorsAndLocations < ActiveRecord::Migration[5.0]
  def change
    add_column :locations, :special_offer_message, :text
    add_column :locations, :special_offer_url, :text
    add_column :doctors, :special_offer_message, :text
    add_column :doctors, :special_offer_url, :text
  end
end
