class AddNpiIndex < ActiveRecord::Migration[5.0]
  def change
    add_index :doctors, :npi
  end
end
