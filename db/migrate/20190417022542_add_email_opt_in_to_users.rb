class AddEmailOptInToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :email_opt_in, :boolean, default: false
  end
end
