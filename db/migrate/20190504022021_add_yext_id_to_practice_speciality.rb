class AddYextIdToPracticeSpeciality < ActiveRecord::Migration[5.0]
  def change
    add_column :practice_specialities, :yext_id, :integer
  end
end
