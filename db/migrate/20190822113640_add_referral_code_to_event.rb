class AddReferralCodeToEvent < ActiveRecord::Migration[5.0]
  def change
    add_column :events, :referral_code, :string
  end
end
