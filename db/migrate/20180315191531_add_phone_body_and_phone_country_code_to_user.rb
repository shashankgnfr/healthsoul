class AddPhoneBodyAndPhoneCountryCodeToUser < ActiveRecord::Migration[5.0]
  def change
  	add_column :users, :phone_country_code, :string
  	add_column :users, :phone_body, :string
  end
end
