class AddPhotoToLocations < ActiveRecord::Migration[5.0]
  def change
    add_column :locations, :photo, :string
    add_column :doctors, :photo, :string
  end
end
