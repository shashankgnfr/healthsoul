class AddIndexToDoctorUploads < ActiveRecord::Migration[5.0]
  def change
    add_index :doctor_uploads, ["Name", "Organization_Legal_Name", "City", "State"], unique: true, name: "unique_name_org"
  end
end
