class RenameEnabledForSearch < ActiveRecord::Migration[5.0]
  def change
    rename_column :doctors, :enabled_for_doctor_search, :priority_city
    rename_column :locations, :enabled_for_location_search, :priority_city

    # Enable cities for India
    cities_for_india = ['Mumbai', 'Ahmedabad', 'Ludhiana', 'Gurgaon', 'Agra', 'Bangalore', 'Pune', 'Chennai', 'Kolkata']
    Doctor.where(city: cities_for_india, country_slug: 'india').update_all(priority_city: true)
    Location.where(city: cities_for_india, country_slug: 'india').update_all(priority_city: true)

    # Enable cities for USA
    cities_for_usa = ['Springfield', 'Chicago', 'New York', 'Los Angeles', 'San Francisco','Houston', 'Orlando', 'Washington']
    Doctor.where(city: cities_for_usa, country_slug: 'usa').update_all(priority_city: true)
    Location.where(city: cities_for_usa, country_slug: 'usa').update_all(priority_city: true)
  end
end
