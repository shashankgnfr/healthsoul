class AddDefaultValueToReferralActivated < ActiveRecord::Migration[5.0]
  def change
    change_column :users, :referral_activated, :boolean, default: true
    User.update_all(referral_activated: true)
  end
end