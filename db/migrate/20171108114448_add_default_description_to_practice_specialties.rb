class AddDefaultDescriptionToPracticeSpecialties < ActiveRecord::Migration[5.0]
  def change
    add_column :practice_specialities, :default_description, :text
  end
end
