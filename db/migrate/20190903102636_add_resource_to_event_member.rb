class AddResourceToEventMember < ActiveRecord::Migration[5.0]
  def change
    add_column :event_members, :resource_id, :integer
    add_column :event_members, :resource_type, :string
    remove_column :event_members, :verified
    add_column :event_members, :verified, :boolean, default: false
  end
end
