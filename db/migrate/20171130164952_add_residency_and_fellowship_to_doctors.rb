class AddResidencyAndFellowshipToDoctors < ActiveRecord::Migration[5.0]
  def change
    add_column :doctors, :residency, :string
    add_column :doctors, :fellowship, :string
  end
end
