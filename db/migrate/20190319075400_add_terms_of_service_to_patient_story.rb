class AddTermsOfServiceToPatientStory < ActiveRecord::Migration[5.0]
  def change
    add_column :patient_stories, :terms_of_service, :boolean, default: :false
    add_column :patient_stories, :title, :string
  end
end
