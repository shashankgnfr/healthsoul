class AddAffiliatedHospitalsToDoctors < ActiveRecord::Migration[5.0]
  def change
    add_column :doctors, :affiliated_hospitals, :string
  end
end
