class AddDrIdToDoctorUploads < ActiveRecord::Migration[5.0]
  def change
    add_column :doctor_uploads, :dr_id, :integer
  end
end
