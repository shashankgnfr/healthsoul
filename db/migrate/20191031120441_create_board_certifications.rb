class CreateBoardCertifications < ActiveRecord::Migration[5.0]
  def change
    create_table :board_certifications do |t|
      t.string :certification, not_null: true
      t.timestamps
    end

    create_table :board_certifications_doctors do |t|
      t.references :board_certification, foreign_key: true
      t.references :doctor, foreign_key: true
    end

    add_column :doctors, :areas_of_expertise, :text

    drs = Doctor.where("board_certification is not null and board_certification != ''")
    drs.each do |d|
      arr_certifications = d.board_certification.split("\n")
      arr_certifications.each do |certification|
        c = BoardCertification.find_or_create_by(certification: certification.strip)
        d.board_certifications << c
      end
    end
    remove_column :doctors, :board_certification
  end
end
