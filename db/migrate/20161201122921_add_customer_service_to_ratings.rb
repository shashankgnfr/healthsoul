class AddCustomerServiceToRatings < ActiveRecord::Migration[5.0]
  def change
    add_column :ratings, :customer_service, :decimal
    add_column :ratings, :get_ins, :string
  end
end
