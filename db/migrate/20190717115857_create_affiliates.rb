class CreateAffiliates < ActiveRecord::Migration[5.0]
  def change
    create_table :affiliates do |t|
      t.string :name, null: false, unique: true
      t.string :reference_url, null: false, unique: true
      t.string :reference_url_text
      t.string :image
      t.string :image_title 
      t.string :image_alt
      t.timestamps
    end
  end
end
