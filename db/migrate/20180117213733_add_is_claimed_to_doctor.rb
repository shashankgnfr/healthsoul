class AddIsClaimedToDoctor < ActiveRecord::Migration[5.0]
  def change
  	add_column :doctors, :is_claimed, :boolean, default: false
  end
end
