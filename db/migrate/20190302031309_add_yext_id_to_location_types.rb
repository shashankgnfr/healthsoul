class AddYextIdToLocationTypes < ActiveRecord::Migration[5.0]
  def change
    add_column :location_types, :yext_id, :integer
  end
end
