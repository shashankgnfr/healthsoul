class CreateLanguages < ActiveRecord::Migration[5.0]
  def change
    create_table :languages do |t|
      t.string :title , not_null: true, unique: true
      t.timestamps
    end
    ['English', 'Hindi', 'Punjabi', 'Spanish', 'Chinese'].each do |i|
      Language.create({title: i})
    end
    create_join_table :doctors, :languages do |t|
    end
  end
end
