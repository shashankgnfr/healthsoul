class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.string :title, null: false, unique: true
      t.datetime :date, null: false
      t.text :description
      t.text :venue
      t.string :logo, null: false
      t.string :organizer
      t.text :exhibit
      t.boolean :publish
      t.timestamps
    end
  end
end
