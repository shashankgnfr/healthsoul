class AddConditionsAndProcedures < ActiveRecord::Migration[5.0]
	def change
		create_table :conditions do |t|
			t.string :name
			t.timestamps
		end

		create_table :procedures do |t|
			t.string :name
			t.timestamps
		end

		create_join_table :conditions, :practice_specialities do |t|
			t.index [:condition_id, :practice_speciality_id], name: "condition_practice_speciality"
			t.index [:practice_speciality_id, :condition_id], name: "practice_speciality, condition"
    	end

    	create_join_table :procedures, :practice_specialities do |t|
			t.index [:procedure_id, :practice_speciality_id], name: "procedure_practice_speciality"
			t.index [:practice_speciality_id, :procedure_id], name: "practice_speciality_procedure"
    	end
	end
end
