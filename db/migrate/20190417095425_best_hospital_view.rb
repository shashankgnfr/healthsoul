class BestHospitalView < ActiveRecord::Migration[5.0]
  def up
    remove_column :locations, :priority_city
    execute <<-END
      CREATE VIEW best_hospital_views AS
        SELECT  COUNT(*) AS count_all, state, state_slug, country_slug
        FROM "locations" 
        INNER JOIN "location_types" ON "location_types"."id" = "locations"."location_type_id" 
        WHERE "location_types"."name" = 'Hospital' 
          AND country_slug = current_setting('healthsoul.country_slug')
        GROUP BY state, state_slug , country_slug
        ORDER BY COUNT(locations.id) DESC, state ASC 
        LIMIT 18
    END
  end

  def down
    execute <<-END
      DROP VIEW best_hospital_views;
    END
  end

end
