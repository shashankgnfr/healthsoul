class AmendFieldsToEventMember < ActiveRecord::Migration[5.0]
  def change
    remove_column :event_members, :still_in_school
    add_column :event_members, :referral_code, :string
  end
end
