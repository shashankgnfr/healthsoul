class AddProviderTypeToDoctors < ActiveRecord::Migration[5.0]
  def change
    add_column :doctors, :provider_type, :string
  end
end
