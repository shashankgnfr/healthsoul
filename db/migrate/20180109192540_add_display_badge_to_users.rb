class AddDisplayBadgeToUsers < ActiveRecord::Migration[5.0]
  def change
  	add_column :users, :display_badge, :boolean, default: true
  end
end
