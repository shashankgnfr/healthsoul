class OverallDefault < ActiveRecord::Migration[5.0]
  def up
    change_column :locations, :overall, :decimal, :default => 0
    Location.where("overall is null").update_all(overall: 0)
  end

  def down

  end
end
