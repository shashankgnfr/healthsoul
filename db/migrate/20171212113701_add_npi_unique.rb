class AddNpiUnique < ActiveRecord::Migration[5.0]
  def change
    remove_index :doctors, :npi
    add_index :doctors, :npi, unique: true
  end
end
