class AddAgreedToGdprToUser < ActiveRecord::Migration[5.0]
  def change
  	add_column :users, :agreed_to_gdpr, :boolean
  end
end
