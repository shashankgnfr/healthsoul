class AddPhotoToLocation < ActiveRecord::Migration[5.0]
  def change
    add_column :locations, :place_photo, :string
    add_column :locations, :placeid, :string
  end
end
