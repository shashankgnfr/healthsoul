class AddFaqsToTag < ActiveRecord::Migration[5.0]
  def change
    add_column :tags, :page_faqs, :text
  end
end
