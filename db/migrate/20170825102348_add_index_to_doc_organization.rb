class AddIndexToDocOrganization < ActiveRecord::Migration[5.0]
  def change
    add_index :doctor_organizations, :state
    add_index :doctor_organizations, :country
  end
end
