class AddSimpleImportToLocations < ActiveRecord::Migration[5.0]
  def change
    add_column :locations, :simple_geocode, :boolean, default: false
  end
end
