# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20200128152945) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "advertisements", force: :cascade do |t|
    t.string   "name",                               null: false
    t.string   "reference_url",                      null: false
    t.string   "reference_url_text"
    t.string   "image"
    t.string   "image_title"
    t.string   "image_alt"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.boolean  "publish",            default: false
    t.string   "type"
    t.integer  "display_frequency"
    t.integer  "country_id"
    t.integer  "priority"
    t.string   "dimension"
    t.string   "source_image"
    t.string   "subtype"
  end

  create_table "advertisements_resources", force: :cascade do |t|
    t.integer  "advertisement_id",             null: false
    t.integer  "resource_id",      default: 0
    t.string   "resource_type",                null: false
    t.string   "resource_action",              null: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.index ["advertisement_id", "resource_type", "resource_id"], name: "advert_resource_index", unique: true, using: :btree
    t.index ["resource_type", "resource_id"], name: "index_advertisements_resources_on_resource_type_and_resource_id", using: :btree
  end

  create_table "answers", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "question_id"
    t.text     "body"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["question_id"], name: "index_answers_on_question_id", using: :btree
    t.index ["user_id"], name: "index_answers_on_user_id", using: :btree
  end

  create_table "appointments", force: :cascade do |t|
    t.time     "time"
    t.date     "date"
    t.string   "name"
    t.string   "phone"
    t.string   "email"
    t.string   "reason"
    t.string   "provider_type"
    t.integer  "provider_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.boolean  "new_patient",   default: true
    t.integer  "user_id"
    t.boolean  "confirmed",     default: false
    t.index ["provider_type", "provider_id"], name: "index_appointments_on_provider_type_and_provider_id", using: :btree
    t.index ["user_id"], name: "index_appointments_on_user_id", using: :btree
  end

  create_table "awards", force: :cascade do |t|
    t.integer  "location_id"
    t.string   "location_type"
    t.integer  "year"
    t.string   "award_type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["location_id"], name: "index_awards_on_location_id", using: :btree
    t.index ["location_type"], name: "index_awards_on_location_type", using: :btree
    t.index ["year"], name: "index_awards_on_year", using: :btree
  end

  create_table "blog_videos", force: :cascade do |t|
    t.integer  "blog_id"
    t.string   "video"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["blog_id"], name: "index_blog_videos_on_blog_id", using: :btree
  end

  create_table "blogs", force: :cascade do |t|
    t.string   "title"
    t.text     "content"
    t.string   "featured_image"
    t.date     "published_at"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "doctor_id"
    t.string   "slug"
    t.string   "meta_title"
    t.text     "meta_description"
    t.text     "custom_meta_tags"
    t.string   "author"
    t.string   "custom_author_photo"
    t.string   "featured_image_alt"
    t.text     "video_html_code"
    t.text     "content_2"
    t.integer  "priority"
    t.index ["doctor_id"], name: "index_blogs_on_doctor_id", using: :btree
  end

  create_table "board_certifications", force: :cascade do |t|
    t.string   "certification"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "board_certifications_doctors", force: :cascade do |t|
    t.integer "board_certification_id"
    t.integer "doctor_id"
    t.index ["board_certification_id"], name: "index_board_certifications_doctors_on_board_certification_id", using: :btree
    t.index ["doctor_id"], name: "index_board_certifications_doctors_on_doctor_id", using: :btree
  end

  create_table "business_hours", force: :cascade do |t|
    t.text     "start_time"
    t.text     "end_time"
    t.integer  "day"
    t.boolean  "closed"
    t.string   "hourable_type"
    t.integer  "hourable_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["hourable_type", "hourable_id"], name: "index_business_hours_on_hourable_type_and_hourable_id", using: :btree
  end

  create_table "claim_requests", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "position"
    t.string   "phone"
    t.text     "description"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.boolean  "granted",        default: false
    t.string   "claimable_type"
    t.integer  "claimable_id"
    t.string   "country_code"
    t.integer  "created_by"
    t.index ["claimable_type", "claimable_id"], name: "index_claim_requests_on_claimable_type_and_claimable_id", using: :btree
    t.index ["user_id"], name: "index_claim_requests_on_user_id", using: :btree
  end

  create_table "comments", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "commentable_type"
    t.integer  "commentable_id"
    t.text     "body"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["commentable_type", "commentable_id"], name: "index_comments_on_commentable_type_and_commentable_id", using: :btree
    t.index ["user_id"], name: "index_comments_on_user_id", using: :btree
  end

  create_table "conditions", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "conditions_practice_specialities", id: false, force: :cascade do |t|
    t.integer "condition_id",           null: false
    t.integer "practice_speciality_id", null: false
    t.index ["condition_id", "practice_speciality_id"], name: "condition_practice_speciality", using: :btree
    t.index ["practice_speciality_id", "condition_id"], name: "practice_speciality, condition", using: :btree
  end

  create_table "countries", force: :cascade do |t|
    t.string   "alpha2"
    t.string   "alpha3"
    t.string   "numeric"
    t.string   "fips"
    t.string   "name"
    t.string   "capital"
    t.string   "areakm"
    t.string   "pop"
    t.string   "continent_code"
    t.string   "continent"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "slug"
    t.index ["slug"], name: "index_countries_on_slug", using: :btree
  end

  create_table "countries_practice_specialities", force: :cascade do |t|
    t.integer  "country_id",             null: false
    t.integer  "practice_speciality_id", null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["country_id", "practice_speciality_id"], name: "country_speciality", unique: true, using: :btree
  end

  create_table "countries_practice_specialities_faqs", id: false, force: :cascade do |t|
    t.integer "country_id",                   null: false
    t.integer "practice_specialities_faq_id", null: false
  end

  create_table "department_ratings", force: :cascade do |t|
    t.integer  "location_id"
    t.decimal  "rating"
    t.string   "reason"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["location_id"], name: "index_department_ratings_on_location_id", using: :btree
  end

  create_table "doctor_organizations", force: :cascade do |t|
    t.string   "name"
    t.string   "address"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.string   "phone"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.decimal  "latitude"
    t.decimal  "longitude"
    t.boolean  "simple_geocode"
    t.string   "country"
    t.string   "country_code"
    t.string   "continent"
    t.string   "website"
    t.integer  "doctor_id"
    t.index ["country"], name: "index_doctor_organizations_on_country", using: :btree
    t.index ["country_code"], name: "index_doctor_organizations_on_country_code", using: :btree
    t.index ["name"], name: "index_doctor_organizations_on_name", using: :btree
    t.index ["state"], name: "index_doctor_organizations_on_state", using: :btree
  end

  create_table "doctor_reviews", force: :cascade do |t|
    t.integer  "doctor_id"
    t.integer  "user_id"
    t.date     "use_date"
    t.integer  "where"
    t.decimal  "overall_experience", default: "0.0"
    t.decimal  "explaintation",      default: "0.0"
    t.decimal  "trust",              default: "0.0"
    t.decimal  "staff",              default: "0.0"
    t.decimal  "appt",               default: "0.0"
    t.decimal  "wait",               default: "0.0"
    t.decimal  "skills",             default: "0.0"
    t.string   "recommend"
    t.text     "comment"
    t.integer  "status"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.index ["doctor_id"], name: "index_doctor_reviews_on_doctor_id", using: :btree
    t.index ["user_id"], name: "index_doctor_reviews_on_user_id", using: :btree
  end

  create_table "doctor_specialities", force: :cascade do |t|
    t.integer  "doctor_id"
    t.integer  "practice_speciality_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["doctor_id"], name: "index_doctor_specialities_on_doctor_id", using: :btree
    t.index ["practice_speciality_id"], name: "index_doctor_specialities_on_practice_speciality_id", using: :btree
  end

  create_table "doctor_uploads", force: :cascade do |t|
    t.string   "Name"
    t.string   "Provider"
    t.string   "Gender"
    t.string   "Primary_Specialty"
    t.string   "Organization_Legal_Name"
    t.string   "Credential"
    t.string   "Address"
    t.string   "City"
    t.string   "State"
    t.string   "Country"
    t.string   "Zip_Code"
    t.string   "Phone_Number"
    t.string   "Medical_School_Name"
    t.string   "Graduation_Year"
    t.string   "Secondary_Specialty_1"
    t.string   "Secondary_Specialty_2"
    t.string   "Hospital_Affiliation_Lbn_1"
    t.string   "Hospital_HWA_ID_1"
    t.string   "Hospital_Affiliation_Lbn_2"
    t.string   "Hospital_HWA_ID_2"
    t.string   "Hospital_Affiliation_Lbn_3"
    t.string   "Hospital_HWA_ID_3"
    t.string   "Hospital_Affiliation_Lbn_4"
    t.string   "Hospital_HWA_ID_4"
    t.string   "Npi"
    t.string   "Professional_Accepts_Medicare_Assignment"
    t.boolean  "imported",                                 default: false
    t.datetime "created_at",                                               null: false
    t.datetime "updated_at",                                               null: false
    t.boolean  "spec",                                     default: false
    t.string   "website"
    t.string   "hsid"
    t.integer  "dr_id"
    t.index ["Name", "Organization_Legal_Name", "City", "State"], name: "unique_name_org", unique: true, using: :btree
  end

  create_table "doctors", force: :cascade do |t|
    t.string   "name"
    t.integer  "gender"
    t.string   "credential"
    t.integer  "med_school_id"
    t.integer  "graduation"
    t.string   "npi"
    t.boolean  "medicare"
    t.datetime "created_at",                                                                   null: false
    t.datetime "updated_at",                                                                   null: false
    t.string   "slug"
    t.integer  "doctor_organization_id"
    t.decimal  "overall",                                      default: "0.0"
    t.decimal  "latitude"
    t.decimal  "longitude"
    t.integer  "ratings_count",                                default: 0
    t.string   "affiliated_hospitals"
    t.string   "photo"
    t.integer  "academic_affiliation",                         default: 0
    t.string   "provider_type"
    t.boolean  "request_appt",                                 default: false
    t.text     "specialities"
    t.text     "description"
    t.string   "website"
    t.string   "address"
    t.string   "organization_name"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.string   "phone"
    t.string   "country"
    t.string   "country_code"
    t.string   "continent"
    t.boolean  "simple_geocode"
    t.string   "residency"
    t.string   "fellowship"
    t.string   "hsid"
    t.string   "monday_hours"
    t.string   "tuesday_hours"
    t.string   "wenesday_hours"
    t.string   "thursday_hours"
    t.string   "friday_hours"
    t.string   "saturday_hours"
    t.string   "sunday_hours"
    t.boolean  "display_hours_and_fees",                       default: false
    t.string   "new_patient_fees"
    t.boolean  "is_claimed",                                   default: false
    t.string   "email"
    t.boolean  "should_be_notified_by_sms_for_appointments"
    t.boolean  "should_be_notified_by_email_for_appointments"
    t.integer  "yext_id"
    t.integer  "yext_status"
    t.text     "special_offer_message"
    t.text     "special_offer_url"
    t.integer  "canonical_id"
    t.string   "current_patient_fees"
    t.string   "country_slug"
    t.boolean  "priority_city",                                default: false
    t.string   "state_slug"
    t.string   "city_slug"
    t.string   "photo_alt",                                    default: "Doctor | HealthSoul"
    t.string   "other_gender"
    t.string   "featured_or_sponsored"
    t.string   "fellowship_2"
    t.text     "areas_of_expertise"
    t.boolean  "digitial_health_score_enabled",                default: false
    t.index ["city_slug"], name: "index_doctors_on_city_slug", using: :btree
    t.index ["country"], name: "index_doctors_on_country", using: :btree
    t.index ["country_code"], name: "index_doctors_on_country_code", using: :btree
    t.index ["country_slug"], name: "index_doctors_on_country_slug", using: :btree
    t.index ["credential"], name: "index_doctors_on_credential", using: :btree
    t.index ["doctor_organization_id"], name: "index_doctors_on_doctor_organization_id", using: :btree
    t.index ["graduation"], name: "index_doctors_on_graduation", using: :btree
    t.index ["med_school_id"], name: "index_doctors_on_med_school_id", using: :btree
    t.index ["name"], name: "index_doctors_on_name", using: :btree
    t.index ["state"], name: "index_doctors_on_state", using: :btree
    t.index ["state_slug"], name: "index_doctors_on_state_slug", using: :btree
  end

  create_table "doctors_languages", id: false, force: :cascade do |t|
    t.integer "doctor_id",   null: false
    t.integer "language_id", null: false
  end

  create_table "doctors_schools", force: :cascade do |t|
    t.integer  "doctor_id"
    t.integer  "med_school_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["doctor_id"], name: "index_doctors_schools_on_doctor_id", using: :btree
    t.index ["med_school_id"], name: "index_doctors_schools_on_med_school_id", using: :btree
  end

  create_table "editorial_members", force: :cascade do |t|
    t.string   "name",                         null: false
    t.string   "member_photo"
    t.text     "description",                  null: false
    t.boolean  "publish",      default: false
    t.string   "designation"
    t.string   "credentials"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "city"
    t.string   "state"
    t.index ["name"], name: "index_editorial_members_on_name", using: :btree
  end

  create_table "event_members", force: :cascade do |t|
    t.string   "name",                                 null: false
    t.string   "degree_type"
    t.string   "professional_school"
    t.string   "speciality"
    t.integer  "graduation"
    t.string   "city"
    t.string   "email",                                null: false
    t.integer  "event_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.text     "optional_information"
    t.boolean  "is_student",           default: false
    t.string   "practice_name"
    t.string   "referral_code"
    t.integer  "user_id"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.boolean  "verified",             default: false
  end

  create_table "events", force: :cascade do |t|
    t.string   "title",         null: false
    t.text     "description"
    t.text     "venue"
    t.string   "logo",          null: false
    t.string   "organizer"
    t.text     "exhibit"
    t.boolean  "publish"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.text     "promotion"
    t.date     "start_date"
    t.date     "end_date"
    t.string   "banner_image"
    t.boolean  "active"
    t.string   "referral_code"
  end

  create_table "faq_answers", force: :cascade do |t|
    t.text     "body"
    t.integer  "faq_id"
    t.integer  "user_id"
    t.boolean  "approved",   default: true
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["faq_id"], name: "index_faq_answers_on_faq_id", using: :btree
    t.index ["user_id"], name: "index_faq_answers_on_user_id", using: :btree
  end

  create_table "faqs", force: :cascade do |t|
    t.text     "body"
    t.integer  "user_id"
    t.string   "country"
    t.boolean  "approved",   default: true
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["user_id"], name: "index_faqs_on_user_id", using: :btree
  end

  create_table "favorites", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "favoriteable_type"
    t.integer  "favoriteable_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["favoriteable_type", "favoriteable_id"], name: "index_favorites_on_favoriteable_type_and_favoriteable_id", using: :btree
    t.index ["user_id"], name: "index_favorites_on_user_id", using: :btree
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
    t.index ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
    t.index ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree
  end

  create_table "hdetails", force: :cascade do |t|
    t.string   "platform"
    t.string   "website_url"
    t.integer  "name"
    t.integer  "address"
    t.integer  "phone"
    t.integer  "photo"
    t.integer  "website"
    t.integer  "speciallity"
    t.integer  "hour"
    t.integer  "fee"
    t.integer  "about_me"
    t.integer  "education"
    t.integer  "rating"
    t.integer  "number_of_reviews"
    t.integer  "total_rating"
    t.integer  "doctor_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["doctor_id"], name: "index_hdetails_on_doctor_id", using: :btree
  end

  create_table "health_score_details", force: :cascade do |t|
    t.string   "platform"
    t.string   "website_url"
    t.string   "h_type"
    t.integer  "name"
    t.integer  "address"
    t.integer  "phone"
    t.integer  "photo"
    t.integer  "website"
    t.integer  "speciallity"
    t.integer  "hour"
    t.integer  "fee"
    t.integer  "about_me"
    t.integer  "education"
    t.integer  "rating"
    t.integer  "number_of_reviews"
    t.integer  "total_rating"
    t.integer  "doctor_id"
    t.integer  "health_score_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["doctor_id"], name: "index_health_score_details_on_doctor_id", using: :btree
    t.index ["health_score_id"], name: "index_health_score_details_on_health_score_id", using: :btree
  end

  create_table "health_scores", force: :cascade do |t|
    t.integer  "doctor_id"
    t.float    "score",         default: 0.0
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.float    "listing_score"
    t.float    "rating_score"
    t.index ["doctor_id"], name: "index_health_scores_on_doctor_id", using: :btree
  end

  create_table "hospital_affiliations", force: :cascade do |t|
    t.integer  "doctor_id"
    t.integer  "location_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["doctor_id"], name: "index_hospital_affiliations_on_doctor_id", using: :btree
    t.index ["location_id"], name: "index_hospital_affiliations_on_location_id", using: :btree
  end

  create_table "hsdetails", force: :cascade do |t|
    t.string   "platform"
    t.string   "website_url"
    t.integer  "name"
    t.integer  "address"
    t.integer  "phone"
    t.integer  "photo"
    t.integer  "website"
    t.integer  "speciallity"
    t.integer  "hour"
    t.integer  "fee"
    t.integer  "about_me"
    t.integer  "education"
    t.integer  "rating"
    t.integer  "number_of_reviews"
    t.integer  "total_rating"
    t.integer  "doctor_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["doctor_id"], name: "index_hsdetails_on_doctor_id", using: :btree
  end

  create_table "identities", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_identities_on_user_id", using: :btree
  end

  create_table "impressions", force: :cascade do |t|
    t.string   "impressionable_type"
    t.integer  "impressionable_id"
    t.integer  "user_id"
    t.string   "controller_name"
    t.string   "action_name"
    t.string   "view_name"
    t.string   "request_hash"
    t.string   "ip_address"
    t.string   "session_hash"
    t.text     "message"
    t.text     "referrer"
    t.text     "params"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "counter_per_day",     default: 0
    t.index ["controller_name", "action_name", "ip_address"], name: "controlleraction_ip_index", using: :btree
    t.index ["controller_name", "action_name", "request_hash"], name: "controlleraction_request_index", using: :btree
    t.index ["controller_name", "action_name", "session_hash"], name: "controlleraction_session_index", using: :btree
    t.index ["impressionable_type", "impressionable_id", "ip_address"], name: "poly_ip_index", using: :btree
    t.index ["impressionable_type", "impressionable_id", "params"], name: "poly_params_request_index", using: :btree
    t.index ["impressionable_type", "impressionable_id", "request_hash"], name: "poly_request_index", using: :btree
    t.index ["impressionable_type", "impressionable_id", "session_hash"], name: "poly_session_index", using: :btree
    t.index ["impressionable_type", "message", "impressionable_id"], name: "impressionable_type_message_index", using: :btree
    t.index ["user_id"], name: "index_impressions_on_user_id", using: :btree
  end

  create_table "languages", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "location_specialities", force: :cascade do |t|
    t.integer  "location_id"
    t.integer  "practice_speciality_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["location_id"], name: "index_location_specialities_on_location_id", using: :btree
    t.index ["practice_speciality_id"], name: "index_location_specialities_on_practice_speciality_id", using: :btree
  end

  create_table "location_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "yext_id"
  end

  create_table "locations", force: :cascade do |t|
    t.string   "name"
    t.string   "address"
    t.string   "address2"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.string   "country"
    t.string   "website"
    t.string   "phone"
    t.string   "email"
    t.string   "sub_type"
    t.integer  "location_type_id"
    t.datetime "created_at",                                                      null: false
    t.datetime "updated_at",                                                      null: false
    t.float    "latitude"
    t.float    "longitude"
    t.decimal  "overall",                                         default: "0.0"
    t.integer  "in_state_ranking",                                default: 0
    t.integer  "in_city_ranking",                                 default: 0
    t.string   "hwaid"
    t.integer  "in_country_ranking",                              default: 0
    t.integer  "ratings_count",                                   default: 0
    t.string   "continent"
    t.string   "slug"
    t.boolean  "simple_geocode",                                  default: false
    t.string   "country_code"
    t.string   "place_photo"
    t.string   "placeid"
    t.string   "photo"
    t.text     "description"
    t.boolean  "is_claimed",                                      default: false
    t.string   "medicare_id"
    t.integer  "yext_id"
    t.integer  "yext_status"
    t.text     "special_offer_message"
    t.text     "special_offer_url"
    t.integer  "canonical_id"
    t.string   "country_slug"
    t.string   "state_slug"
    t.string   "city_slug"
    t.string   "photo_alt"
    t.integer  "priority_city_level"
    t.string   "practice_id"
    t.string   "featured_or_sponsored"
    t.string   "ownership"
    t.boolean  "emergency_services"
    t.integer  "hospital_overall_rating"
    t.string   "mortality_national"
    t.string   "safety_of_care_national"
    t.string   "readmission_national"
    t.string   "patient_experience_national"
    t.string   "effectiveness_of_care_national"
    t.string   "timeliness_of_care_national"
    t.string   "efficient_use_of_medical_imaging_national"
    t.integer  "hospital_overall_rating_graph"
    t.integer  "mortality_national_graph"
    t.integer  "safety_of_care_national_graph"
    t.integer  "readmission_national_graph"
    t.integer  "patient_experience_national_graph"
    t.integer  "effectiveness_of_care_national_graph"
    t.integer  "timeliness_of_care_national_graph"
    t.integer  "efficient_use_of_medical_imaging_national_graph"
    t.index ["city_slug"], name: "index_locations_on_city_slug", using: :btree
    t.index ["country_slug"], name: "index_locations_on_country_slug", using: :btree
    t.index ["hwaid"], name: "index_locations_on_hwaid", unique: true, using: :btree
    t.index ["location_type_id"], name: "index_locations_on_location_type_id", using: :btree
    t.index ["slug"], name: "index_locations_on_slug", unique: true, using: :btree
    t.index ["state_slug"], name: "index_locations_on_state_slug", using: :btree
  end

  create_table "med_schools", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_med_schools_on_name", using: :btree
  end

  create_table "medical_students", force: :cascade do |t|
    t.string   "name",                                null: false
    t.string   "photo"
    t.string   "email",                               null: false
    t.string   "professional_school",                 null: false
    t.string   "degree_type",                         null: false
    t.integer  "graduation_year",                     null: false
    t.text     "publications"
    t.text     "care_philosophy"
    t.boolean  "imported",            default: false
    t.string   "slug",                                null: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "national_averages", force: :cascade do |t|
    t.string   "title"
    t.integer  "average"
    t.boolean  "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "patient_stories", force: :cascade do |t|
    t.string   "name",                             null: false
    t.string   "email",                            null: false
    t.string   "city"
    t.integer  "country_id"
    t.text     "description",                      null: false
    t.string   "word_document"
    t.string   "image"
    t.string   "video"
    t.text     "comments"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.boolean  "terms_of_service", default: false
    t.string   "title"
    t.index ["country_id"], name: "index_patient_stories_on_country_id", using: :btree
  end

  create_table "patient_surveys", force: :cascade do |t|
    t.string   "measure_id"
    t.integer  "rating"
    t.integer  "answer_percent"
    t.integer  "location_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["location_id"], name: "index_patient_surveys_on_location_id", using: :btree
  end

  create_table "practice_reviews", force: :cascade do |t|
    t.integer  "location_id"
    t.integer  "user_id"
    t.date     "use_date"
    t.integer  "where"
    t.decimal  "overall_experience"
    t.decimal  "explaintation"
    t.decimal  "trust"
    t.decimal  "staff"
    t.decimal  "appt"
    t.decimal  "wait"
    t.decimal  "skills"
    t.string   "recommend"
    t.text     "comment"
    t.integer  "status"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["location_id"], name: "index_practice_reviews_on_location_id", using: :btree
    t.index ["user_id"], name: "index_practice_reviews_on_user_id", using: :btree
  end

  create_table "practice_specialities", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",                                                                                                                                                                                                                                                                                null: false
    t.datetime "updated_at",                                                                                                                                                                                                                                                                                null: false
    t.text     "default_description"
    t.boolean  "common_speciality",       default: false
    t.string   "title"
    t.string   "user_facing_description"
    t.string   "slug"
    t.integer  "yext_id"
    t.string   "title_slug"
    t.text     "doctorised_h1",           default: "Find the Best %%specialist%% in %%location%% enlisted below. Read real reviews and reach the most suitable %%specialist%% in %%location%% easily. There are %%number%% %%specialist%% in %%location%% with an average rating of %%avg_rating%% stars."
    t.text     "doctorised_header"
    t.text     "doctorised_footer"
    t.index ["slug"], name: "index_practice_specialities_on_slug", using: :btree
  end

  create_table "practice_specialities_faqs", force: :cascade do |t|
    t.integer  "practice_speciality_id"
    t.integer  "user_id"
    t.string   "faq_title"
    t.text     "faq_body"
    t.boolean  "publish",                default: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.index ["practice_speciality_id"], name: "index_practice_specialities_faqs_on_practice_speciality_id", using: :btree
    t.index ["user_id"], name: "index_practice_specialities_faqs_on_user_id", using: :btree
  end

  create_table "practice_specialities_procedures", id: false, force: :cascade do |t|
    t.integer "procedure_id",           null: false
    t.integer "practice_speciality_id", null: false
    t.index ["practice_speciality_id", "procedure_id"], name: "practice_speciality_procedure", using: :btree
    t.index ["procedure_id", "practice_speciality_id"], name: "procedure_practice_speciality", using: :btree
  end

  create_table "procedures", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "promotion_winners", force: :cascade do |t|
    t.string   "name"
    t.string   "winner_type"
    t.string   "prize"
    t.string   "month"
    t.integer  "year"
    t.integer  "promotion_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["promotion_id"], name: "index_promotion_winners_on_promotion_id", using: :btree
  end

  create_table "promotions", force: :cascade do |t|
    t.text     "title"
    t.text     "blurb"
    t.text     "terms_and_conditions_content"
    t.integer  "country_id"
    t.boolean  "promotion_for_all_countries",  default: false
    t.string   "promotion_image"
    t.integer  "priority_index",               default: 0
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.string   "display_on",                   default: "Providers"
    t.boolean  "publish",                      default: false
    t.index ["country_id"], name: "index_promotions_on_country_id", using: :btree
  end

  create_table "questions", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "body"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "questionable_type"
    t.integer  "questionable_id"
    t.index ["questionable_type", "questionable_id"], name: "index_questions_on_questionable_type_and_questionable_id", using: :btree
    t.index ["user_id"], name: "index_questions_on_user_id", using: :btree
  end

  create_table "ratings", force: :cascade do |t|
    t.integer  "location_id"
    t.integer  "user_id"
    t.date     "use_date"
    t.string   "reason"
    t.decimal  "overall_experience", default: "0.0"
    t.decimal  "doctor_care",        default: "0.0"
    t.decimal  "nursing_care",       default: "0.0"
    t.decimal  "cleanliness",        default: "0.0"
    t.decimal  "food_services",      default: "0.0"
    t.string   "recommend"
    t.text     "comment"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.decimal  "customer_service",   default: "0.0"
    t.string   "get_ins"
    t.decimal  "cost_satisfaction",  default: "0.0"
    t.decimal  "claims_process",     default: "0.0"
    t.integer  "status",             default: 0
    t.index ["location_id"], name: "index_ratings_on_location_id", using: :btree
    t.index ["user_id"], name: "index_ratings_on_user_id", using: :btree
  end

  create_table "recommendations", force: :cascade do |t|
    t.string   "name",        null: false
    t.text     "location"
    t.string   "country",     null: false
    t.string   "website"
    t.string   "email",       null: false
    t.string   "source_type", null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.string   "resource_type"
    t.integer  "resource_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
    t.index ["name"], name: "index_roles_on_name", using: :btree
  end

  create_table "sessions", force: :cascade do |t|
    t.string   "session_id", null: false
    t.text     "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["session_id"], name: "index_sessions_on_session_id", unique: true, using: :btree
    t.index ["updated_at"], name: "index_sessions_on_updated_at", using: :btree
  end

  create_table "shared_blogs", force: :cascade do |t|
    t.integer  "user_id",         null: false
    t.integer  "blog_id",         null: false
    t.integer  "shared_count"
    t.string   "social_platform"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id"
    t.string   "taggable_type"
    t.integer  "taggable_id"
    t.string   "tagger_type"
    t.integer  "tagger_id"
    t.string   "context",       limit: 128
    t.datetime "created_at"
    t.index ["context"], name: "index_taggings_on_context", using: :btree
    t.index ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree
    t.index ["tag_id"], name: "index_taggings_on_tag_id", using: :btree
    t.index ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree
    t.index ["taggable_id", "taggable_type", "tagger_id", "context"], name: "taggings_idy", using: :btree
    t.index ["taggable_id"], name: "index_taggings_on_taggable_id", using: :btree
    t.index ["taggable_type"], name: "index_taggings_on_taggable_type", using: :btree
    t.index ["tagger_id", "tagger_type"], name: "index_taggings_on_tagger_id_and_tagger_type", using: :btree
    t.index ["tagger_id"], name: "index_taggings_on_tagger_id", using: :btree
  end

  create_table "tags", force: :cascade do |t|
    t.string  "name"
    t.integer "taggings_count",     default: 0
    t.string  "slug"
    t.text    "description"
    t.string  "title"
    t.text    "page_content"
    t.string  "featured_image"
    t.string  "featured_image_alt"
    t.string  "meta_title"
    t.text    "custom_meta_tags"
    t.text    "page_faqs"
    t.index ["name"], name: "index_tags_on_name", unique: true, using: :btree
    t.index ["slug"], name: "index_tags_on_slug", using: :btree
  end

  create_table "user_info", id: false, force: :cascade, comment: " for testing" do |t|
    t.integer "user_id"
    t.text    "first_name"
    t.text    "last_name"
    t.text    "mobile_no"
    t.integer "otp"
  end

  create_table "user_referrals", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "referred_user_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["referred_user_id"], name: "index_user_referrals_on_referred_user_id", using: :btree
    t.index ["user_id"], name: "index_user_referrals_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "username"
    t.integer  "total_points",           default: 0
    t.string   "city"
    t.string   "state"
    t.string   "co"
    t.integer  "sex"
    t.integer  "age_group"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "promotion_code"
    t.boolean  "referral_activated",     default: true
    t.boolean  "subscription_only",      default: false
    t.boolean  "display_badge",          default: true
    t.string   "first_name"
    t.string   "last_name"
    t.string   "phone_number"
    t.string   "photo"
    t.string   "phone_country_code"
    t.string   "phone_body"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.datetime "date_of_birth"
    t.boolean  "agreed_to_gdpr"
    t.string   "title"
    t.boolean  "marked_as_provider",     default: false
    t.string   "referral_code"
    t.boolean  "email_opt_in",           default: false
    t.string   "zip"
    t.text     "address"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["username"], name: "index_users_on_username", unique: true, using: :btree
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree
  end

  create_table "votes", force: :cascade do |t|
    t.string   "voteable_type"
    t.integer  "voteable_id"
    t.integer  "status",        default: 0
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "user_id"
    t.index ["user_id"], name: "index_votes_on_user_id", using: :btree
    t.index ["voteable_type", "voteable_id"], name: "index_votes_on_voteable_type_and_voteable_id", using: :btree
  end

  add_foreign_key "answers", "questions"
  add_foreign_key "answers", "users"
  add_foreign_key "blog_videos", "blogs"
  add_foreign_key "board_certifications_doctors", "board_certifications"
  add_foreign_key "board_certifications_doctors", "doctors"
  add_foreign_key "claim_requests", "users"
  add_foreign_key "comments", "users"
  add_foreign_key "department_ratings", "locations"
  add_foreign_key "doctor_reviews", "doctors"
  add_foreign_key "doctor_reviews", "users"
  add_foreign_key "doctor_specialities", "doctors"
  add_foreign_key "doctor_specialities", "practice_specialities"
  add_foreign_key "doctors", "doctor_organizations"
  add_foreign_key "doctors", "med_schools"
  add_foreign_key "doctors_schools", "doctors"
  add_foreign_key "doctors_schools", "med_schools"
  add_foreign_key "faq_answers", "faqs"
  add_foreign_key "faq_answers", "users"
  add_foreign_key "faqs", "users"
  add_foreign_key "favorites", "users"
  add_foreign_key "hdetails", "doctors"
  add_foreign_key "health_score_details", "doctors"
  add_foreign_key "health_score_details", "health_scores"
  add_foreign_key "health_scores", "doctors"
  add_foreign_key "hospital_affiliations", "doctors"
  add_foreign_key "hospital_affiliations", "locations"
  add_foreign_key "hsdetails", "doctors"
  add_foreign_key "identities", "users"
  add_foreign_key "location_specialities", "locations"
  add_foreign_key "location_specialities", "practice_specialities"
  add_foreign_key "locations", "location_types"
  add_foreign_key "patient_surveys", "locations"
  add_foreign_key "practice_reviews", "locations"
  add_foreign_key "practice_reviews", "users"
  add_foreign_key "practice_specialities_faqs", "practice_specialities"
  add_foreign_key "practice_specialities_faqs", "users"
  add_foreign_key "promotion_winners", "promotions"
  add_foreign_key "questions", "users"
  add_foreign_key "ratings", "locations"
  add_foreign_key "ratings", "users"
  add_foreign_key "user_referrals", "users"
  add_foreign_key "votes", "users"
end
