# README

*Widgets
This application includes js/html widgets for embedding information from the healthsoul api into other sites. Since those widgets are minified where they occur in the code, please have a copy of the widget code in the readme for easier review/troubleshooting/modifications:
<script> var destination = \'#{doctor_review_api_register_user_index_url(@doctor, params: {top_five: true})}\';
	var request = new XMLHttpRequest();request.open('GET', destination, true);
	request.onload = function () {
		response = JSON.parse(this.responseText).review;
		var review_list = document.getElementById('review_list');
		Object.entries(response).forEach(
			(entry) => {
				var node = document.createElement('li');
				node.style.cssText = 'border-top: 1px solid black !important; padding-bottom: 10px !important; padding-top: 10px !important;';
				var rating_node = document.createElement('span');
				var date = new Date(entry[1].created_at);
				rating_node.innerHTML = 'Overall Rating: ' + entry[1].overall_experience + '/5.0';
				rating_node.style.cssText = 'color: grey !important; padding-bottom: 10px !important; float: right !important;';
				var comment_node = document.createElement('p');
				comment_node.innerHTML = entry[1].comment;
				comment_node.style.cssText = 'margin-bottom: 15px !important; margin-top: 0px !important;';

				node.appendChild(comment_node);
				var user_node = document.createElement('span');
				user_node.innerHTML = 'Reviewer: '+ entry[1].user.username
				user_node.style.cssText = 'color: grey; padding-bottom: 10px !important;';
				node.appendChild(user_node);
				node.appendChild(rating_node);
				var date_node = document.createElement('span');
				date_node.innerHTML = 'Date: ' + date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear();
				date_node.style.cssText = 'color: grey !important; padding-bottom: 10px !important;';
				node.appendChild(document.createElement('br'));
				node.appendChild(date_node);
				review_list.appendChild(node);
			}
		);
  	};
  	request.send();
</script>
<div id = 'healthsoul_reviews_widget_for_doctor_3' style='font-family: HELVETICA !important; width: 100% !important; max-width: 500px !important; min-height: 300px !important; border: 1px solid black !important;'>
<img src='https://www.healthsoul.com/images/logo.png' style='margin-left: 20px !important; margin-top: 10px !important;'></img>
<ul id='review_list' style='list-style-type: none !important; padding-left: 20px !important; padding-right: 20px !important;'>
</ul>
</div>

=========
**Deploy**

In order to deploy you have to make sure you have the required ruby version installed and set to the default version
*NOTE* ruby 2.3.8 was the required version at the time of writing this Readme, but could have changed, be sure to check the Gemfile to determine which ruby version is the required version
```
rvm use 2.3.8
```
If you don't have ruby 2.3.8 installed, you will be prompted to install it.
Make sure you have all your gems installed
```
bundle install
```
when you are ready to deploy, run the following to:
deploy to production
```
cap production deploy
```
deploy to staging
```
cap staging deploy
```
